var cache       = require('gulp-cached');
var prettier    = require('gulp-prettier');
var gulp        = require('gulp');
var header      = require('gulp-header');
var less        = require('gulp-less');
var notify      = require('gulp-notify');
var pkg         = require('./package.json');
var plumber     = require('gulp-plumber');

// -------------------------------------------------------------------------------------------------
// Shared resource path locations
// -------------------------------------------------------------------------------------------------
// jsWatchMin - Files that will be watched then minified into a single Javascript file upon changes
// jsLint - Files that will be ran through the linter
// cssBootstrap - File that will be used to compile LESS to CSS
// cssWatchMin - Files that will be watched for changes then compiled upon changes
// -------------------------------------------------------------------------------------------------
var srcPaths = {
    aura: ['styles/components/aura/**/*.less'],
    lwc: ['styles/components/lwc/**/*.less'],
    components: ['styles/components/aura/**/*.less', 'styles/components/lwc/**/*.less'],
    cleanCSS: ['./styles/**/*.less'],
    cssWatchMin: ['styles/**/*.less']
};

// -------------------------------------------------------------------------------------------------
// Build resource path locations
// -------------------------------------------------------------------------------------------------
// js - Compiled/minified Javascript output location
// css - Compiled/minified CSS output location
// -------------------------------------------------------------------------------------------------
var buildPaths = {
    aura: 'force-app/main/default/aura/',
    lwc: 'force-app/main/default/lwc/'
};

// -------------------------------------------------------------------------------------------------
// Error handler
// -------------------------------------------------------------------------------------------------
var onError = function (err) {
    notify.onError({
        title: 'Gulp',
        subtitle: 'Failure!',
        message: 'Error: <%= error.message %>'
    })(err);
    this.emit('end');
};

// -------------------------------------------------------------------------------------------------
// Header appended to all compiled files as a notice
// -------------------------------------------------------------------------------------------------
var compiledBanner = [
    '/**',
    ' * <%= pkg.name %> - <%= pkg.description %>',
    ' * @version <%= pkg.version %>',
    ' * @author <%= pkg.author %>',
    ' *',
    ' * COMPILED FILE DO NOT DIRECTLY EDIT',
    ' */',
    ''
].join('\n');

// -------------------------------------------------------------------------------------------------
// CSS Tasks
// -------------------------------------------------------------------------------------------------

// Runs postCSS parser on all custom LESS files.
// https://prettier.io
// -----------------------------
gulp.task('cleanCSS', function () {
    return gulp.src(srcPaths.cleanCSS, {base: './'})
        .pipe(cache('cleanCSS'))
        .pipe(plumber({errorHandler: onError}))
        .pipe(prettier())
        .pipe(gulp.dest('./'));
});

// Compile Aura LESS files into CSS
// -------------------------------
gulp.task('compileAuraCSS', gulp.series('cleanCSS', function () {
        return gulp
            .src(srcPaths.aura, {allowEmpty : true})
            .pipe(cache('auraCSS'))
            .pipe(plumber({errorHandler: onError}))
            .pipe(less())
            .pipe(header(compiledBanner, {pkg: pkg}))
            .pipe(gulp.dest(buildPaths.aura))
            .pipe(
                notify({
                    title: 'Gulp',
                    subtitle: 'Success',
                    message: 'Aura LESS compiled: <%= file.relative %>'
                })
            );
    })
);

// Compile LWC LESS files into CSS
// -------------------------------
gulp.task('compileLWCCSS', gulp.series('cleanCSS', function () {
        return gulp
            .src(srcPaths.lwc, {allowEmpty : true})
            .pipe(cache('lwcCSS'))
            .pipe(plumber({errorHandler: onError}))
            .pipe(less())
            .pipe(header(compiledBanner, {pkg: pkg}))
            .pipe(gulp.dest(buildPaths.lwc))
            .pipe(
                notify({
                    title: 'Gulp',
                    subtitle: 'Success',
                    message: 'LWC LESS compiled: <%= file.relative %>'
                })
            );
    })
);

// Build CSS tasks
// -----------------------------
gulp.task('buildCSS', gulp.parallel('compileAuraCSS', 'compileLWCCSS'));


// -------------------------------------------------------------------------------------------------
// Watch Tasks
// -------------------------------------------------------------------------------------------------
gulp.task('watch:styles', function () {
    gulp.watch(srcPaths.components, gulp.series('compileAuraCSS', 'compileLWCCSS'));
});

gulp.task('watch', gulp.parallel('watch:styles'));

// -------------------------------------------------------------------------------------------------
// The default task (called when you run `gulp` from cli)
// -------------------------------------------------------------------------------------------------
gulp.task('default', gulp.series('buildCSS', 'watch'));