<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CA | Newfoundland and Labrador | All</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">Canada</value>
    </values>
    <values>
        <field>County_Id__c</field>
        <value xsi:type="xsd:string">CANF</value>
    </values>
    <values>
        <field>County__c</field>
        <value xsi:type="xsd:string">All</value>
    </values>
    <values>
        <field>Lead_Owner_Id__c</field>
        <value xsi:type="xsd:string">005f4000002Qt4dAAC</value>
    </values>
    <values>
        <field>Lead_Owner_Name__c</field>
        <value xsi:type="xsd:string">Peter Mann</value>
    </values>
    <values>
        <field>Note__c</field>
        <value xsi:type="xsd:string">In IIR the province code is NF, not NL, so that is why the County Id is set to CANF on this assignment record.</value>
    </values>
    <values>
        <field>State_Province__c</field>
        <value xsi:type="xsd:string">Newfoundland and Labrador</value>
    </values>
</CustomMetadata>
