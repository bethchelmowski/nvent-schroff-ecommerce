<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <description>MX1010</description>
    <label>Mexico_Durango</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">Mexico</value>
    </values>
    <values>
        <field>County_Id__c</field>
        <value xsi:type="xsd:string">MX1010</value>
    </values>
    <values>
        <field>County__c</field>
        <value xsi:type="xsd:string">Durango</value>
    </values>
    <values>
        <field>Lead_Owner_Id__c</field>
        <value xsi:type="xsd:string">005f4000000iwf8AAA</value>
    </values>
    <values>
        <field>Lead_Owner_Name__c</field>
        <value xsi:type="xsd:string">Jerold Sauter</value>
    </values>
    <values>
        <field>Note__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>State_Province__c</field>
        <value xsi:type="xsd:string">Durango</value>
    </values>
</CustomMetadata>
