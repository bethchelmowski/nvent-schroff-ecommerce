<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>North_Carolina_Burke</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">United States</value>
    </values>
    <values>
        <field>County_Id__c</field>
        <value xsi:type="xsd:string">USNCBR</value>
    </values>
    <values>
        <field>County__c</field>
        <value xsi:type="xsd:string">Burke</value>
    </values>
    <values>
        <field>Lead_Owner_Id__c</field>
        <value xsi:type="xsd:string">005f4000000iwuEAAQ</value>
    </values>
    <values>
        <field>Lead_Owner_Name__c</field>
        <value xsi:type="xsd:string">William Yeramian</value>
    </values>
    <values>
        <field>Note__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>State_Province__c</field>
        <value xsi:type="xsd:string">North Carolina</value>
    </values>
</CustomMetadata>
