<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default</label>
    <protected>false</protected>
    <values>
        <field>Action_Set__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Hide_Add_To_Cart__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Hide_Images__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Hide_Unit_Price__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Navigate_to_VF_Page__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Show_Empty_Categories__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Store_Front__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
