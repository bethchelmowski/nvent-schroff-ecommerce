<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Illinois_Henry</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">United States</value>
    </values>
    <values>
        <field>County_Id__c</field>
        <value xsi:type="xsd:string">USILHN</value>
    </values>
    <values>
        <field>County__c</field>
        <value xsi:type="xsd:string">Henry</value>
    </values>
    <values>
        <field>Lead_Owner_Id__c</field>
        <value xsi:type="xsd:string">005f4000000iwtfAAA</value>
    </values>
    <values>
        <field>Lead_Owner_Name__c</field>
        <value xsi:type="xsd:string">Todd Wilkinson</value>
    </values>
    <values>
        <field>Note__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>State_Province__c</field>
        <value xsi:type="xsd:string">Illinois</value>
    </values>
</CustomMetadata>
