import LightningDatatable from "lightning/datatable";
import imageColumn from "./imageColumn.html";
import titleColumn from "./titleColumn.html";
import priceColumn from "./priceColumn.html";
import quantityColumn from "./quantityColumn.html";
import subtotalColumn from "./subtotalColumn.html";
import actionColumn from "./actionColumn.html";

export default class X7sCartTable extends LightningDatatable {
  static customTypes = {
    imageColumn: {
      template: imageColumn,
      typeAttributes: ["Id", "image"],
      standardCellLayout: true
    },
    titleColumn: {
      template: titleColumn,
      typeAttributes: ["Id", "itemNumber"],
      standardCellLayout: true
    },
    priceColumn: {
      template: priceColumn,
      typeAttributes: ["Id", "listPrice", "netPrice"],
      standardCellLayout: true
    },
    quantityColumn: {
      template: quantityColumn,
      typeAttributes: ["Id", "itemNumber", "quantity"],
      standardCellLayout: true
    },
    subtotalColumn: {
      template: subtotalColumn,
      typeAttributes: ["Id", "netPrice", "quantity", "subtotal"],
      standardCellLayout: true
    },
    actionColumn: {
      template: actionColumn,
      typeAttributes: ["itemNumber"]
    }
  };
}