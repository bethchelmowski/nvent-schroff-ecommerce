import { LightningElement, api } from "lwc";

export default class X7sCartTableActionColumn extends LightningElement {
  @api cartRowId;

  labels = {
    addToCart: "Add To Cart"
  };

  handleClick(event) {
    const id = event.target.getAttribute("data-itemnumber");

    console.log("add to cart this item number: ", id);
  }
}