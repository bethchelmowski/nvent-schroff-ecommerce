/**
 * Created by might on 5/19/2020.
 */

import { LightningElement } from "lwc";

import {
  registerListener,
  unregisterAllListeners,
  fireEvent
} from "c/x7sPubSub";

import yourPrice from "@salesforce/label/ccrz.ProductDetails_HRelated_ProductWidget_Your_Price";
import listPrice from "@salesforce/label/ccrz.ProductDetails_ListPrice";
import requestQuote from "@salesforce/label/c.nVent_Request_Quote";
import requestQuoteModalMessage from "@salesforce/label/c.nVent_Request_Quote_Modal_Message";

export default class X7SCartSummary extends LightningElement {
  yourPriceValue;
  yourPriceCurrency;
  listPriceValue;
  listPriceCurrency;

  isModalOpen = false;

  labels = {
    yourPrice,
    listPrice,
    requestQuote,
    requestQuoteModalMessage
  };

  connectedCallback() {
    registerListener(
      "initSummaryTotal",
      this.handleInitUpdateSummaryTotal,
      this
    );
    registerListener("updateSummaryTotal", this.handleUpdateSummaryTotal, this);
  }

  disconnectedCallback() {
    unregisterAllListeners(this);
  }

  handleInitUpdateSummaryTotal(data) {
    console.log("----------handleInitUpdateSummaryTotal data: ", data);
    // calculate yourPriceValue, which is netPrice * quantity held in subtotal
    this.calcSumOfSubTotals(data);
    this.calcListPrice(data);

    // calculate listPriceValue, which is listPrice * quantity held individually
  }

  handleUpdateSummaryTotal(data) {
    console.log("----------handleUpdateSummaryTotal data: ", data);
    this.calcSumOfSubTotals(data);
    this.calcListPrice(data);
    fireEvent("doneUpdating", true);
  }

  calcSumOfSubTotals(data) {
    let sum = 0;
    let currency = "USD";

    data.forEach(item => {
      // sum += parseFloat((item.subtotal.value));
      console.log("item.subtotal: ", item.subTotal);

      // sum += item.subtotal;
      sum += item.subTotal;
      // currency = item.subtotal.currency;
    });

    console.log("sum: ", sum);
    this.yourPriceValue = sum.toString();
    this.yourPriceCurrency = currency;
  }

  calcListPrice(data) {
    let sum = 0;
    let currency = "USD";

    data.forEach(item => {
      const qty = item.quantity;
      const price = parseFloat(item.listPrice.value);
      currency = parseFloat(item.listPrice.currency);

      sum += price * qty;
    });

    this.listPriceValue = sum.toString();
    this.listPriceCurrency = currency;
  }

  handleClick() {
    // TODO: Show modal and call apex method to toggle cart status
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }
}