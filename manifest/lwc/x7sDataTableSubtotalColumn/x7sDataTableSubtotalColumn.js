/**
 * Created by might on 5/18/2020.
 */

import { LightningElement, api } from "lwc";

export default class X7sDataTableSubtotalColumn extends LightningElement {
  @api subtotal;
  @api currencyCode;

  connectedCallback() {
    console.log("subtotal: ", this.subtotal);
  }
}