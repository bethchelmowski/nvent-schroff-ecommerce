import { LightningElement, api } from "lwc";

import {NavigationMixin} from "lightning/navigation";

export default class X7sDataTableTitleColumn extends NavigationMixin(LightningElement) {
  @api cartRowId;
  @api productCode;
  @api productName;

  connectedCallback() {
    console.log("cartRowId: ", this.cartRowId);
    console.log("productCode: ", this.productCode);
    console.log("productName: ", this.productName);
  }

  handleClick() {
    const pageReferenceObject = {
      type: "standard__recordPage",
      attributes: {
        recordId: this.cartRowId,
        objectApiName: 'CC_Product_Detail__c',
        actionName: "view"
      }
    };

    this[NavigationMixin.Navigate](pageReferenceObject);
  }
}