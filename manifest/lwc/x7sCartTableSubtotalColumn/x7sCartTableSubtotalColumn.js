/**
 * Created by might on 5/18/2020.
 */

import {LightningElement, api} from 'lwc';

export default class X7SCartTableSubtotalColumn extends LightningElement {
    @api netPrice;
    @api quantity;
    @api subtotal;

    renderedCallback() {
        console.log('----------- subtotal rendered callback', this.netPrice);

        // this.subtotal = eval(this.netPrice.value + '*' + this.quantity);
    }
}