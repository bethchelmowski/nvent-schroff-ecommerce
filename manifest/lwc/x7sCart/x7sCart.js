import {LightningElement, api, track} from "lwc";
import title from "@salesforce/label/c.nVent_Cart_Headings_Title";

import {registerListener, unregisterAllListeners, fireEvent} from 'c/x7sPubSub';

import {cartState} from './schema.js';

const columns = [
    {
        label: "Image",
        type: "imageColumn",
        fieldName: "image",
        typeAttributes: {
            Id: {fieldName: "Id"},
            image: {fieldName: "image"}
        }
    },
    {
        label: "Item",
        type: "titleColumn",
        fieldName: "itemNumber",
        sortable: true,
        typeAttributes: {
            Id: {fieldName: "Id"},
            itemNumber: {fieldName: "itemNumber"}
        }
    },
    {
        label: "List Price",
        type: "priceColumn",
        fieldName: "listPrice",
        sortable: true,
        typeAttributes: {
            Id: {fieldName: "Id"},
            listPrice: {fieldName: "listPrice"}
        }
    },
    {
        label: "Your Price",
        type: "priceColumn",
        fieldName: "netPrice",
        sortable: true,
        typeAttributes: {
            Id: {fieldName: "Id"},
            netPrice: {fieldName: "netPrice"}
        }
    },
    {
        label: "Quantity",
        type: "quantityColumn",
        fieldName: "quantity",
        sortable: true,
        typeAttributes: {
            Id: {fieldName: "Id"},
            quantity: {fieldName: "quantity"},
            itemNumber: {fieldName: "itemNumber"}
        }
    },
    {
        label: "Action",
        type: "action",
        initialWidth: 160,
        fieldName: "Id",
        typeAttributes: {
            rowActions: [
                {label: "Move to Wishlist", name: "WISHLIST"},
                {label: "Show Components", name: "COMPONENTS"},
                {label: "Delete", name: "DELETE"}
            ],
            Id: {fieldName: "Id"}
        },
        cellAttributes: {alignment: "center"}
    },
    {
        label: "Sub-Total",
        type: "subtotalColumn",
        fieldName: "subtotal",
        sortable: true,
        typeAttributes: {
            Id: {fieldName: "Id"},
            netPrice: {fieldName: "netPrice"},
            quantity: {fieldName: "quantity"},
            subtotal: {fieldName: "subtotal"}
        }
    },
    {
        label: "Actions",
        type: "actionColumn",
        initialWidth: 160,
        fieldName: "action",
        typeAttributes: {
            itemNumber: {fieldName: "itemNumber"}
        }
    }
];

export default class X7sCart extends LightningElement {
    @track data = [];
    @track columns = columns;

    @api sortedBy = "itemNumber";
    @api sortedDirection;

    @api loadData;

    labels = {
        title
    };

    connectedCallback() {
        console.log('cartState: ', cartState.items);
        console.log('loadData: ', this.loadData); // Use this to determine what method to call to get data = Wish List, Cart, My Orders, etc.

        this.updateColumns();
        this.loadCartData();
        this.setDefaultSort();

        registerListener('doneUpdating', this.handleDoneUpdating, this);
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    handleDoneUpdating(data) {
        console.log(data);
    }

    updateColumns() {
        if (this.loadData === "Wish List") {
            let idx = columns.findIndex(x => x.type === "subtotalColumn");
            columns.splice(idx, 1);
        }

        if (this.loadData === "My Cart") {
            let idx = columns.findIndex(x => x.type === "actionColumn");
            columns.splice(idx, 1);
        }
    }

    loadCartData() {
        this.data = cartState.items;
    }

    handleRowAction(event) {
        let actionName = event.detail.action.name;
        let row = event.detail.row;
        console.log("actionName: ", actionName);
        console.log("row: ", row.Id);
    }

    setDefaultSort() {
        this.sortedBy = "itemNumber";
        this.sortDirection = "asc";
    }

    doDelete(event) {
        const rowId = event.detail;
        console.log(rowId);
    }

    doUpdateQuantity(event) {
        const value = event.detail.value;
        const id = event.detail.id;
        console.log('value: ', value);
        console.log('id: ', id);
        // TODO: Get object where itemNumber === value,
        // take new quantity, value, multiply it by netPrice and update subtotal

        const row = this.data.filter(x => x.itemNumber === id)[0];
        const netPrice = row.netPrice.value;
        const quantity = value;

        row.quantity = value;
        row.subtotal.value = eval(value + ' * ' + netPrice);

        console.log('row: ', row);
        console.log('netPrice: ', netPrice);
        console.log('quantity: ', quantity);
        const newPrice = eval(quantity + ' * ' + netPrice);
        console.log(newPrice);

        fireEvent('updateSummaryTotal', newPrice);
    }

    /**
     * on table column click, send column and direction to sortData()
     * @param event
     */
    doSorting(event) {
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.sortData(this.sortedBy, this.sortedDirection);
    }

    /**
     * returns filteredData sorted by fieldName and direction
     * @param fieldName
     * @param direction
     */
    sortData(fieldName, direction) {
        console.log("fieldName: ", fieldName);
        console.log("direction: ", direction);

        let parseData = JSON.parse(JSON.stringify(this.data));
        console.log("parseData: ", parseData);
        // Return the value stored in the field
        const keyValue = a => {
            return a[fieldName];
        };
        const getValue = a => {
            return a.value;
        };

        // checking reverse direction
        let isReverse = direction === "asc" ? 1 : -1;

        // sorting data
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ""; // handling null values
            y = keyValue(y) ? keyValue(y) : "";

            x = typeof x === 'object' ? getValue(x) : x;
            y = typeof y === 'object' ? getValue(y) : y;

            console.log('--------------------');
            console.log('x is typeof: ', typeof x, x);
            console.log('y is typeof: ', typeof y, y);

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        this.data = parseData;
        console.log("this.data: ", this.data);
    }
}