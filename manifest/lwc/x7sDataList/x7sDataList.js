import { LightningElement, api, track } from "lwc";

import title from "@salesforce/label/c.nVent_Cart_Headings_Title";

import { ShowToastEvent } from "lightning/platformShowToastEvent";

import getCurrentCart from "@salesforce/apex/x7S_CartActionController.getCurrentCart";
import getWishlistItems from "@salesforce/apex/x7S_DataTableController.getWishlistItems";

import {
  registerListener,
  unregisterAllListeners,
  fireEvent
} from "c/x7sPubSub";

import { cartState } from "./schema.js";

const columns = [
  {
    label: "Image",
    type: "imageColumn",
    fieldName: "imageUrl",
    typeAttributes: {
      id: { fieldName: "id" },
      imageUrl: { fieldName: "imageUrl" }
    },
    fixedWidth: 75
  },
  {
    label: "Item #",
    type: "titleColumn",
    fieldName: "productSkuCode",
    sortable: true,
    typeAttributes: {
      id: { fieldName: "id" },
      productSkuCode: { fieldName: "productSkuCode" }
    },
    fixedWidth: 100
  },
  {
    label: "Item",
    type: "titleColumn",
    fieldName: "productName",
    sortable: true,
    typeAttributes: {
      id: { fieldName: "id" },
      productName: { fieldName: "productName" }
    },
    fixedWidth: 125,
    wrapText: true
  },
  {
    label: "List Price",
    type: "priceColumn",
    fieldName: "listPrice",
    sortable: true,
    typeAttributes: {
      id: { fieldName: "id" },
      listPrice: { fieldName: "listPrice" },
      currencyISOCode: { fieldName: "currencyISOCode" }
    },
    cellAttributes: { alignment: "right" },
    fixedWidth: 100
  },
  {
    label: "Your Price",
    type: "priceColumn",
    fieldName: "netPrice",
    sortable: true,
    typeAttributes: {
      id: { fieldName: "id" },
      netPrice: { fieldName: "netPrice" },
      currencyISOCode: { fieldName: "currencyISOCode" }
    },
    cellAttributes: { alignment: "right" },
    fixedWidth: 100
  },
  {
    label: "Quantity",
    type: "quantityColumn",
    fieldName: "quantity",
    sortable: true,
    typeAttributes: {
      id: { fieldName: "id" },
      quantity: { fieldName: "quantity" }
    },
    fixedWidth: 75
  },
  {
    label: "Actions",
    type: "action",
    fieldName: "id",
    typeAttributes: {
      rowActions: [
        { label: "Move to Wishlist", name: "WISHLIST" },
        { label: "Show Components", name: "COMPONENTS" },
        { label: "Delete", name: "DELETE" }
      ],
      id: { fieldName: "id" }
    },
    cellAttributes: { alignment: "center" },
    fixedWidth: 75
  },
  {
    label: "Sub-Total",
    type: "subtotalColumn",
    fieldName: "subTotal",
    sortable: true,
    typeAttributes: {
      id: { fieldName: "id" },
      subTotal: { fieldName: "subTotal" },
      currencyISOCode: { fieldName: "currencyISOCode" }
    },
    cellAttributes: { alignment: "right" },
    fixedWidth: 100
  },
  {
    label: "Actions",
    type: "actionColumn",
    initialWidth: 160,
    fieldName: "action",
    typeAttributes: {
      id: { fieldName: "id" }
    }
  }
];

export default class X7sDataList extends LightningElement {
  @track data = [];
  @track columns;

  @api sortedBy = "id";
  @api sortedDirection;

  @api loadData;

  cartId;
  isLoading = true;

  labels = {
    title
  };

  connectedCallback() {
    console.log("cartState: ", cartState.items);
    console.log("loadData: ", this.loadData); // Use this to determine what method to call to get data = Wish List, Cart, My Orders, etc.

    this.columns = JSON.parse(JSON.stringify(columns));

    this.updateColumns();
    this.loadCartData();
    this.setDefaultSort();

    registerListener("doneUpdating", this.handleDoneUpdating, this);
  }

  disconnectedCallback() {
    unregisterAllListeners(this);
  }

  handleDoneUpdating(data) {
    console.log(data);
  }

  updateColumns() {
    let columnsToRemove;

    if (this.loadData === "Wish List") {
      columnsToRemove = ["action", "subTotal", "netPrice", "listPrice"];
      // TODO: Update action dropdown selections
      const actionColumnIndex = this.columns.findIndex(
        x => x.fieldName === "id"
      );

      const showComponentsIndex = this.columns[
        actionColumnIndex
      ].typeAttributes.rowActions.findIndex(x => x.name === "COMPONENTS");

      this.columns[actionColumnIndex].typeAttributes.rowActions.splice(
        showComponentsIndex,
        1
      );

      this.columns[actionColumnIndex].typeAttributes.rowActions.forEach(
        (x, i) => {
          if (x.name === "WISHLIST") {
            this.columns[actionColumnIndex].typeAttributes.rowActions[i].label =
              "Move to Cart";
            this.columns[actionColumnIndex].typeAttributes.rowActions[i].name =
              "CART";
          }
        }
      );
    }

    if (this.loadData === "My Cart") {
      columnsToRemove = ["action"];
    }

    columnsToRemove.forEach(x => {
      const idx = this.columns.findIndex(y => y.fieldName === x);

      console.log("Wish List index: ", idx);

      if (idx !== -1) {
        this.columns.splice(idx, 1);
      }
    });
  }

  loadCartData() {
    if (this.loadData === "My Cart") {
      getCurrentCart()
        .then(res => {
          console.log("getCurrentCart: ", res);
          this.data = res.items;
          this.cartId = res.cartId;
        })
        .then(() => {
          this.initCartSummaryTotal();
        })
        .catch(err => {
          console.error(err);
        })
        .finally(() => {
          this.isLoading = false;
        });
    }

    if (this.loadData === "Wish List") {
      getWishlistItems()
        .then(res => {
          console.log("getWishlistItems: ", res);
          this.data = res.items;
          this.cartId = res.cartId;
        })
        .catch(err => {
          console.error(err);
        })
        .finally(() => {
          this.isLoading = false;
        });
    }
  }

  initCartSummaryTotal() {
    fireEvent("initSummaryTotal", this.data);
  }

  handleRowAction(event) {
    let actionName = event.detail.action.name;
    let row = event.detail.row;
    console.log("actionName: ", actionName);
    console.log("row: ", row);
    switch (actionName) {
      case "DELETE":
        this.handleDeleteRow(row);
        fireEvent("updateSummaryTotal", this.data);
        break;
      case "WISHLIST":
        // Call method to add item to wishlist and then remove it from the table
        this.handleAddToWishlist(row);
        this.handleDeleteRow(row);
        console.log("add to wishlist");
        break;
      case "CART":
        // Call method to add item to cart and then remove it from the table
        this.handleAddToCart(row);
        this.handleDeleteRow(row);
        console.log("add to cart");
        break;
      case "COMPONENTS":
        console.log("show components modal");
        break;
      default:
    }
  }

  handleDeleteRow(row) {
    const { id } = row;
    console.log("id: ", id);

    const idx = this.findRowIndexByItemNumber(id);
    if (idx !== -1) {
      this.data = this.data.slice(0, idx).concat(this.data.slice(idx + 1));
    }
  }

  handleAddToWishlist(row) {
    const { id, productName, quantity } = row;
    console.log("id: ", id);
    console.log("productName: ", productName);
    console.log("quantity: ", quantity);

    const toast = new ShowToastEvent({
      title: "Success!",
      message: `Item ${id} with name ${productName} added to wishlist.`,
      variant: "success"
    });
    this.dispatchEvent(toast);
  }

  handleAddToCart(row) {
    const { id, productName } = row;
    console.log("id: ", id);

    const toast = new ShowToastEvent({
      title: "Success!",
      message: `Item ${id} with name ${productName} added to cart.`,
      variant: "success"
    });
    this.dispatchEvent(toast);
  }

  findRowIndexByItemNumber(id) {
    let ret = -1;

    this.data.some((row, index) => {
      if (row.id === id) {
        ret = index;
        return true;
      }
      return false;
    });

    return ret;
  }

  setDefaultSort() {
    this.sortedBy = "id";
    this.sortDirection = "asc";
  }

  doDelete(event) {
    const rowId = event.detail;
    console.log(rowId);
  }

  doUpdateQuantity(event) {
    const value = event.detail.value;
    const id = event.detail.id;
    console.log("value: ", value);
    console.log("id: ", id);
    // TODO: Get object where itemNumber === value,
    // take new quantity, value, multiply it by netPrice and update subtotal

    const row = this.data.filter(x => x.id === id)[0];
    // const netPrice = row.netPrice.value;
    const quantity = value;

    row.quantity = value;
    // row.subtotal.value = eval(value + " * " + netPrice).toString();

    console.log("row: ", row);
    // console.log("netPrice: ", netPrice);
    console.log("quantity: ", quantity);
    // const newPrice = eval(quantity + " * " + netPrice);
    // console.log(newPrice);

    fireEvent("updateSummaryTotal", this.data);
  }

  /**
   * on table column click, send column and direction to sortData()
   * @param event
   */
  doSorting(event) {
    this.sortedBy = event.detail.fieldName;
    this.sortedDirection = event.detail.sortDirection;
    this.sortData(this.sortedBy, this.sortedDirection);
  }

  /**
   * returns filteredData sorted by fieldName and direction
   * @param fieldName
   * @param direction
   */
  sortData(fieldName, direction) {
    console.log("fieldName: ", fieldName);
    console.log("direction: ", direction);

    let parseData = JSON.parse(JSON.stringify(this.data));
    console.log("parseData: ", parseData);
    // Return the value stored in the field
    const keyValue = a => {
      return a[fieldName];
    };
    const getValue = a => {
      return a.value;
    };

    // checking reverse direction
    let isReverse = direction === "asc" ? 1 : -1;

    // sorting data
    parseData.sort((x, y) => {
      x = keyValue(x) ? keyValue(x) : ""; // handling null values
      y = keyValue(y) ? keyValue(y) : "";

      x = typeof x === "object" ? getValue(x) : x;
      y = typeof y === "object" ? getValue(y) : y;

      console.log("--------------------");
      console.log("x is typeof: ", typeof x, x);
      console.log("y is typeof: ", typeof y, y);

      // sorting values based on direction
      return isReverse * ((x > y) - (y > x));
    });

    this.data = parseData;
    console.log("this.data: ", this.data);
  }
}