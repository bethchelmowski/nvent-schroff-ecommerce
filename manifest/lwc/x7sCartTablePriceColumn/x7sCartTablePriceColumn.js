import {LightningElement, api} from 'lwc';

export default class X7sCartTablePriceColumn extends LightningElement {
    @api cartRowId;
    @api listPrice = false;
    @api netPrice = false;

    renderedCallback() {
        console.log('------------- rendered callback');
        console.log('listPrice: ', this.listPrice);
        console.log('netPrice: ', this.netPrice);
    }

    connectedCallback() {
        console.log('listPrice: ', this.listPrice);
        console.log('netPrice: ', this.netPrice);
    }
}