import { LightningElement, api } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

export default class X7sDataTableActionColumn extends LightningElement {
  @api cartRowId;

  labels = {
    addToCart: "Add To Cart"
  };

  handleClick(event) {
    const id = event.target.getAttribute("data-itemnumber");

    console.log("add to cart this item number: ", id);

    const toast = new ShowToastEvent({
      title: "Success!",
      message: `Item ${id} added to cart.`,
      variant: 'success'
    });
    this.dispatchEvent(toast);
  }
}