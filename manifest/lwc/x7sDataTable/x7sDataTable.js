import LightningDatatable from "lightning/datatable";
import imageColumn from "./imageColumn.html";
import titleColumn from "./titleColumn.html";
import priceColumn from "./priceColumn.html";
import quantityColumn from "./quantityColumn.html";
import subtotalColumn from "./subtotalColumn.html";
import actionColumn from "./actionColumn.html";

export default class X7sDataTable extends LightningDatatable {
  static customTypes = {
    imageColumn: {
      template: imageColumn,
      typeAttributes: ["id", "imageUrl"],
      standardCellLayout: true
    },
    titleColumn: {
      template: titleColumn,
      typeAttributes: ["id", "productSkuCode", "productName"],
      standardCellLayout: true
    },
    priceColumn: {
      template: priceColumn,
      typeAttributes: ["id", "listPrice", "netPrice", "currencyISOCode"],
      standardCellLayout: true
    },
    quantityColumn: {
      template: quantityColumn,
      typeAttributes: ["id", "quantity"],
      standardCellLayout: true
    },
    subtotalColumn: {
      template: subtotalColumn,
      typeAttributes: ["id", "subTotal", "currencyISOCode"],
      standardCellLayout: true
    },
    actionColumn: {
      template: actionColumn,
      typeAttributes: ["id"]
    }
  };
}