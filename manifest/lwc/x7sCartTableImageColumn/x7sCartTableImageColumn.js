/**
 * Created by might on 5/13/2020.
 */

import {LightningElement, api} from 'lwc';

export default class X7SCartTableImageColumn extends LightningElement {
    @api cartRowId;
    @api imageId;

    connectedCallback() {
        console.log('X7SCartTableImageColumn: ', this.cartRowId)
    }
}