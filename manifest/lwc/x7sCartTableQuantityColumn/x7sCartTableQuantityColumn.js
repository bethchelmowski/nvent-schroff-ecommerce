import {LightningElement, api} from "lwc";

export default class X7sCartTableQuantityColumn extends LightningElement {
    @api cartRowId;
    @api cartRowQuantity;

    connectedCallback() {
        const select = this.template.querySelectorAll('select');
        console.log('select: ', select);
        console.log('X7sCartTableQuantityColumn id: ', this.cartRowId);
        console.log('X7sCartTableQuantityColumn quantity: ', this.cartRowQuantity);
        
        // select[0].selectedIndex = '2';
    }

    get options() {
        return [
            {label: "1", value: "1", selected: false},
            {label: "2", value: "2", selected: true},
            {label: "3", value: "3", selected: false}
        ];
    }

    handleChange(event) {
        console.log('inside X7sCartTableQuantityColumn handleChange');
        console.log('this.cartRowId: ', this.cartRowId);
        const id = this.cartRowId;
        const e = CustomEvent('updatequantity', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                value: event.target.value,
                id: id
            }
        });

        this.dispatchEvent(e);
    }
}