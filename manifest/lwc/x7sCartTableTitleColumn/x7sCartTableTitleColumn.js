import { LightningElement, api } from "lwc";

export default class X7sCartTableTitleColumn extends LightningElement {
  @api cartRowId;
  @api itemNumber;

  connectedCallback() {
    console.log("cartRowId: ", this.cartRowId);
    console.log("itemNumber: ", this.itemNumber);
  }
}