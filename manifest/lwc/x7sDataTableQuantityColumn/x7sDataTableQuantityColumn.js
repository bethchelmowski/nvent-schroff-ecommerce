import {LightningElement, api} from "lwc";

export default class X7sDataTableQuantityColumn extends LightningElement {
    @api cartRowId;
    @api quantity;

    options;

    connectedCallback() {
        this.setOptions();
        this.setQuantityValue();
    }

    setOptions() {
        this.options = [
            {label: "1", value: "1", selected: false},
            {label: "2", value: "2", selected: false},
            {label: "2.5", value: "2.5", selected: false},
            {label: "4", value: "4", selected: false},
            {label: "5", value: "5", selected: false},
            {label: "6", value: "6", selected: false},
            {label: "7", value: "7", selected: false},
            {label: "8", value: "8", selected: false},
            {label: "9", value: "9", selected: false},
            {label: "10", value: "10", selected: false}
        ];
    }

    setQuantityValue() {
        this.quantity = this.quantity.toString();
        console.log('x7sDataTableQuantityColumn id: ', this.cartRowId);
        console.log('x7sDataTableQuantityColumn quantity: ', this.quantity);

        for (let i = 0; i < this.options.length; i++) {
            if (this.options[i].value === this.quantity) {
                console.log(this.options[i]);
                this.options[i].selected = true;
                break;
            }
        }
    }

    handleChange(event) {
        console.log('inside x7sDataTableQuantityColumn handleChange');
        console.log('this.cartRowId: ', this.cartRowId);
        const qty = event.target.value;
        this.quantity = qty;
        const id = this.cartRowId;
        const e = CustomEvent('updatequantity', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                value: qty,
                id: id
            }
        });

        this.dispatchEvent(e);
    }
}