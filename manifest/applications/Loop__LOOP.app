<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Loop__DDP__c</defaultLandingTab>
    <description>DocGen powered by Nintex</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Nintex DocGen</label>
    <logo>Loop__LOOP_Files/Loop__LOOP_Logo_gif.png</logo>
    <tabs>Loop__DDP_Admin</tabs>
    <tabs>Loop__DDP__c</tabs>
    <tabs>standard-Document</tabs>
    <tabs>Loop__Document_Request__c</tabs>
    <tabs>Opportunity_Snapshot__c</tabs>
    <tabs>PriceFx</tabs>
</CustomApplication>
