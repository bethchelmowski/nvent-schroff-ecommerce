<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>This app contains tabs related to ConstructConnect Package</description>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>ConstructConnect</label>
    <logo>rdcc__ConstructConnect/rdcc__Logo.png</logo>
    <tabs>rdcc__InsightProjects</tabs>
    <tabs>rdcc__InsightCompanies</tabs>
    <tabs>rdcc__InsightContacts</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-report</tabs>
    <tabs>rdcc__ConstructConnect_Help</tabs>
</CustomApplication>
