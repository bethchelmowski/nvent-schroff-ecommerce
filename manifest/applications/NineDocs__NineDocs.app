<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Create documents, reports and more.</description>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>9Docs</label>
    <tabs>NineDocs__Document__c</tabs>
    <tabs>NineDocs__DocumentActivity__c</tabs>
    <tabs>NineDocs__Template__c</tabs>
    <tabs>NineDocs__EmailTemplate__c</tabs>
    <tabs>Opportunity_Snapshot__c</tabs>
    <tabs>PriceFx</tabs>
</CustomApplication>
