<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_Pending_Approval</fullName>
        <field>Pending_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Check Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Pending_Approval</fullName>
        <field>Pending_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>Publish</fullName>
        <action>PublishAsNew</action>
        <description>Publishes the new article version</description>
        <label>Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
</Workflow>
