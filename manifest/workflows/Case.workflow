<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Assignment_or_Owner_Change_Notification</fullName>
        <description>Case Assignment or Owner Change Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SUPPORTNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Enclosures_Case_incoming_email_notification</fullName>
        <description>Enclosures Case incoming email notification</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_To_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforcedonotreply@nvent.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Enclosures_Case_incoming_email_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Case_Owner_of_Closed_Case_Email</fullName>
        <description>Notify Case Owner of Closed Case Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Thermal_Email_Templates/Thermal_Alert_On_Closed_Case</template>
    </alerts>
    <fieldUpdates>
        <fullName>Re_Open_Closed_Case</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Re-Open Closed Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Assigned_To_Email</fullName>
        <description>This field update populates the Assigned To Email field with the Assigned to users email address.</description>
        <field>Assigned_To_Email__c</field>
        <formula>Assigned_To__r.Email</formula>
        <name>Update Assigned To Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Escalated_Time_Stamp</fullName>
        <field>Escalated_Time_Stamp__c</field>
        <formula>NOW()</formula>
        <name>Update Escalated Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Escalated_Total_Days</fullName>
        <field>Escalated_Total_Days__c</field>
        <formula>IF( ISBLANK ( PRIORVALUE( Escalated_Total_Days__c )),0,PRIORVALUE(Escalated_Total_Days__c))+  (NOW() - Escalated_Time_Stamp__c)</formula>
        <name>Update Escalated Total Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Add Assigned To Email address</fullName>
        <actions>
            <name>Update_Assigned_To_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used in Enclosures case management process</description>
        <formula>AND(RecordType.Name = &quot;Enclosures CC Case&quot;,

AND (ISNEW(), NOT(ISBLANK( Assigned_To__c )))
|| 
AND (NOT(ISNEW()),ISCHANGED( Assigned_To__c))
||
AND (NOT(ISNEW()),AND(NOT(ISBLANK( Assigned_To__c )), ISBLANK( Assigned_To_Email__c ))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case update email notification Enclosures CC</fullName>
        <actions>
            <name>Enclosures_Case_incoming_email_notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Enclosures CC Case</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Web</value>
        </criteriaItems>
        <description>Case update email notification Enclosures CC. This workflow alerts or sends case update notification to the case owner basis &quot;Assigned to&quot; field on enclosures cases.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Case Email Alert</fullName>
        <actions>
            <name>Notify_Case_Owner_of_Closed_Case_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Re_Open_Closed_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Thermal NA Tech Support</value>
        </criteriaItems>
        <description>Alert Case Owner when a customer emails from Closed Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update time stamp when case escalated to Product Management</fullName>
        <actions>
            <name>Update_Escalated_Time_Stamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated to Product Mgt</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update total days when case escalated to Product Management</fullName>
        <actions>
            <name>Update_Escalated_Total_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND ( ISCHANGED (Status), ISPICKVAL( PRIORVALUE (Status), &apos;Escalated to Product Mgt&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
