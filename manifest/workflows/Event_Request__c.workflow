<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Expense_Request_Approved</fullName>
        <description>Expense Request Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Expense_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Expense_Request_Rejected</fullName>
        <description>Expense Request Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Expense_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>No_Inactive_Manager_Alert</fullName>
        <description>No/Inactive Manager Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/No_Inactive_Manager_Notification</template>
    </alerts>
    <alerts>
        <fullName>Visualforce_Expense_Approval_alert</fullName>
        <description>Visualforce Expense Approval alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Visualforce_Expense_Approval_Template</template>
    </alerts>
    <alerts>
        <fullName>Visualforce_Expense_Rejection_alert</fullName>
        <description>Visualforce Expense Rejection alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Visualforce_Expense_Rejection_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Next_Approver</fullName>
        <field>Next_Approver_Name__c</field>
        <name>Clear Next Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Approver</fullName>
        <description>This field contains the last approver in Event Request and is not visible on form. Only CRM Admin can modify/remove this field.</description>
        <field>Last_Approver__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Set Last Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Last_Approver_Corp</fullName>
        <field>Last_Approver__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>Set Last Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Next_Approver_Name</fullName>
        <field>Next_Approver_Name__c</field>
        <formula>Owner:User.Manager.FirstName &amp; &quot; &quot; &amp; Owner:User.Manager.LastName</formula>
        <name>Set Next Approver Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Next_Approver_To_Queue</fullName>
        <field>Next_Approver_Name__c</field>
        <formula>&quot;Second Level Expense Approvals&quot;</formula>
        <name>Set Next Approver To Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Next_Approver_to_Peggy_Baratta</fullName>
        <field>Next_Approver_Name__c</field>
        <formula>&quot;Peggy Baratta&quot;</formula>
        <name>Set Next Approver to Peggy Baratta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_Awaiting_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Awaiting Approval</literalValue>
        <name>Set Status Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_Awaiting_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Awaiting Approval</literalValue>
        <name>Set to Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_to_Expense_Approval</fullName>
        <description>This field update is used to update the Event request record type to Expense Approval Request Approved   to Remove submit for Approval Button on event request</description>
        <field>RecordTypeId</field>
        <lookupValue>Expense_Approval_Request_Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type to Expense Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_to_pending_for_submission</fullName>
        <description>This field update is used to update the status to pending for submission</description>
        <field>Status__c</field>
        <literalValue>Pending Submission</literalValue>
        <name>Update status to pending for submission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_status_to_pending_for_submission</fullName>
        <description>This field update is used to update the status to Pending submission when status is recalled</description>
        <field>Status__c</field>
        <literalValue>Pending Submission</literalValue>
        <name>set  status to pending for submission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Remove Approval Button on event request</fullName>
        <actions>
            <name>Update_record_type_to_Expense_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
