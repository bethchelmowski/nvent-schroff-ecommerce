<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_On_End_To_End_Request_Completion</fullName>
        <description>Email Alert On End To End Request Completion</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>salesforcedonotreply@nvent.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/End_To_End_Request_Completed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Pentair_Update_Schedled_Completion_Date</fullName>
        <description>the Field update updates the Updated  Scheduled Completion date if the value is null</description>
        <field>Updated_Scheduled_Completion__c</field>
        <formula>Scheduled_Completion__c</formula>
        <name>Pentair_Update_Schedled_Completion_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>End To End Request Completed</fullName>
        <actions>
            <name>Email_Alert_On_End_To_End_Request_Completion</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>The Workflow triggers an Email to the creating user of the End to End request when it is Completed.</description>
        <formula>AND(!ISBLANK(Completed_On__c), ISCHANGED(Completed_On__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pentair_Updated_Scheduled_Date_Updation</fullName>
        <actions>
            <name>Pentair_Update_Schedled_Completion_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>End_to_End__c.Updated_Scheduled_Completion__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
