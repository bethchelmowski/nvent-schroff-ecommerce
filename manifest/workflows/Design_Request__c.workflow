<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Design_Request_Missing_Information</fullName>
        <description>Design Request Missing Information</description>
        <protected>false</protected>
        <recipients>
            <field>Inside_Sales_Sales_Engineer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Design_Request_Missing_Information</template>
    </alerts>
    <alerts>
        <fullName>Design_Request_No_Opportunity</fullName>
        <description>Design Request No Opportunity</description>
        <protected>false</protected>
        <recipients>
            <field>Inside_Sales_Sales_Engineer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Design_Request_No_Opportunity</template>
    </alerts>
    <alerts>
        <fullName>Design_Returned_from_Engineering</fullName>
        <description>Design Returned from Engineering</description>
        <protected>false</protected>
        <recipients>
            <field>Inside_Sales_Sales_Engineer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Design_Returned_from_Engineering</template>
    </alerts>
</Workflow>
