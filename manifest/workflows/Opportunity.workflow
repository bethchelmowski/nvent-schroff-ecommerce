<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BIS_Opportunities_Closed_Won_Lost_over_100K</fullName>
        <description>BIS Opportunities Closed Won/Lost over $100K</description>
        <protected>false</protected>
        <recipients>
            <recipient>jan.jonsson@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>krzysztof.czyzewski@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>larry.niblett@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>patrick.clines@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>piotr.zloch@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforcedonotreply@nvent.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/BIS_Deals_Closed_over_100K</template>
    </alerts>
    <alerts>
        <fullName>IHS_Opportunities_Closed_Won_Lost_over_250K</fullName>
        <description>IHS Opportunities Closed Won/Lost over $250K</description>
        <protected>false</protected>
        <recipients>
            <recipient>brendan.drews@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jaap.nieuwenhuizen@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jan.jonsson@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>krzysztof.czyzewski@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>larry.niblett@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>luigi.erbacci@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforcedonotreply@nvent.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/IHS_Deals_Closed_over_250K</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Alert_Deal_Over_1MM</fullName>
        <ccEmails>Maneet.Sidhu@nVent.com</ccEmails>
        <description>Opportunity Alert Deal Over $1MM</description>
        <protected>false</protected>
        <recipients>
            <recipient>brad.faulconer@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>brendan.drews@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jaap.nieuwenhuizen@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jan.jonsson@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jerold.sauter@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>krzysztof.czyzewski@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>larry.niblett@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>luigi.erbacci@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>martin.lee@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>patrick.clines@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>piotr.zloch@nvent.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>salesforcedonotreply@nvent.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Deals_Closed_over_1MM</template>
    </alerts>
    <fieldUpdates>
        <fullName>Increase_Close_Date_Change_Count</fullName>
        <field>Close_Date_Change_Counter__c</field>
        <formula>IF(ISBLANK(Close_Date_Change_Counter__c),1,Close_Date_Change_Counter__c +1)</formula>
        <name>Increase Close Date Change Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_close_date_update</fullName>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Opp close date update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BIS Opportunity Update over %24100K</fullName>
        <actions>
            <name>BIS_Opportunities_Closed_Won_Lost_over_100K</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sales_Team__c</field>
            <operation>equals</operation>
            <value>BIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>&quot;USD 100,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>lessThan</operation>
            <value>&quot;USD 1,000,000&quot;</value>
        </criteriaItems>
        <description>This is an alert for BIS Opportunities Closed Won/Lost deals</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IHS Opportunity Update over %24250K</fullName>
        <actions>
            <name>IHS_Opportunities_Closed_Won_Lost_over_250K</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sales_Team__c</field>
            <operation>equals</operation>
            <value>IHS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>&quot;USD 250,000&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>lessThan</operation>
            <value>&quot;USD 1,000,000&quot;</value>
        </criteriaItems>
        <description>This is an alert for HIS Opportunities Closed Won/Lost deals</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Updates over %241MM</fullName>
        <actions>
            <name>Opportunity_Alert_Deal_Over_1MM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sales_Team__c</field>
            <operation>equals</operation>
            <value>IHS,BIS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterOrEqual</operation>
            <value>&quot;USD 1,000,000&quot;</value>
        </criteriaItems>
        <description>This is an alert for the executive team on Opportunity Closed Won/Lost deals</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity closed date populate</fullName>
        <actions>
            <name>Opp_close_date_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Close Date Change Counter</fullName>
        <actions>
            <name>Increase_Close_Date_Change_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Increases the count every time an opportunity close date is changed.
Used by: Enclosures, EFS and Thermal</description>
        <formula>AND(
ISCHANGED(CloseDate),
OR(
ISPICKVAL(Sales_Team__c, &quot;Hoffman&quot;),
ISPICKVAL(Sales_Team__c, &quot;Schroff&quot;),
ISPICKVAL(Sales_Team__c, &quot;ERICO&quot;),
ISPICKVAL(Sales_Team__c, &quot;ERIFLEX&quot;),
ISPICKVAL(Sales_Team__c, &quot;CADDY&quot;),
ISPICKVAL(Sales_Team__c, &quot;LENTON&quot;),
ISPICKVAL(Sales_Team__c, &quot;IHS&quot;),
ISPICKVAL(Sales_Team__c, &quot;BIS&quot;)
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
