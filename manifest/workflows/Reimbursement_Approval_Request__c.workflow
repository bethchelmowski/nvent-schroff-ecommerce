<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>No_Inactive_Manager_Alert</fullName>
        <description>No/Inactive Manager Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/No_Inactive_Manager_Notification</template>
    </alerts>
    <alerts>
        <fullName>Reimbursement_Request_Approved</fullName>
        <description>Reimbursement Request Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Reimbursement_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Reimbursement_Request_Rejected</fullName>
        <description>Reimbursement Request Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Reimbursement_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Visualforce_Reimbursement_Approval_alert</fullName>
        <description>Visualforce Reimbursement Approval alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Visualforce_Reimbursement_Approval</template>
    </alerts>
    <alerts>
        <fullName>Visualforce_Reimbursement_Rejection_alert</fullName>
        <description>Visualforce Reimbursement Rejection alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Visualforce_Reimbursement_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_To_Paid</fullName>
        <field>Status__c</field>
        <literalValue>Paid</literalValue>
        <name>Set To Paid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_Awaiting_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Awaiting Approval</literalValue>
        <name>Set to Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_Pending_submission</fullName>
        <field>Status__c</field>
        <literalValue>Pending Submission</literalValue>
        <name>Set to Pending submission</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Reimbursement_Request_record_type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Reimbursement_Approval_Request_Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Reimbursement Request record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Remove Approval Button on Reimbursement request</fullName>
        <actions>
            <name>Update_Reimbursement_Request_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Reimbursement_Approval_Request__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
