<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Lead_Assignment_or_Owner_Change_Notification</fullName>
        <description>Lead Assignment or Owner Change Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Lead_MQL_Achieved_Notification</fullName>
        <description>Lead: MQL Achieved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Leads_New_MQL_Alert_WRK</template>
    </alerts>
    <alerts>
        <fullName>Lead_MQL_Email_Notification_Portal_Users</fullName>
        <description>Lead MQL Email Notification Portal Users</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Leads_New_MQL_Alert_for_Portal_Users</template>
    </alerts>
    <rules>
        <fullName>Lead MQL Achieved</fullName>
        <actions>
            <name>Lead_MQL_Achieved_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.nVent_Lifecycle_Stage__c</field>
            <operation>equals</operation>
            <value>MQL</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LastModifiedById</field>
            <operation>equals</operation>
            <value>Marketo Sync</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.OwnerId</field>
            <operation>notEqual</operation>
            <value>Marketo Queue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <description>Notification when MQL is Achieved from Marketo</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
