/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/18/19.
 */

({
	doInit: function(component, event, helper) {
		helper.doCallout(component, 'c.getProductsById',
			{
				'setting'     : component.get('v.setting'),
				'pageSize'    : 1,
				'currentPage' : 1,
				'ids'         : [component.get('v.recordId')]
			}, false, 'Get Products by ID')
			.then($A.getCallback(function (listModel) {
				component.set('v.products', listModel);
				component.set('v.product',  listModel.items[0]);
			}));

	}
});