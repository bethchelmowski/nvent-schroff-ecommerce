/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 8/30/19.
 */

({
	gotoRecord: function (component, event, helper) {
		helper.gotoRecord(component, component.get('v.productId'));
	}
});