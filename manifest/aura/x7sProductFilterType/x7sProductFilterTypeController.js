/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/20/19.
 */

({
	init : function(component, event, helper) {
		helper.doCallout(component, 'c.getProductTypeList', {}, false, 'GetProduct Type List')
		.then( result => {
			let itemList = [];
			itemList.push({key: 'All', value: ''});
			result.forEach(item => itemList.push({key: item, value: item}));
			component.set('v.itemList', itemList);
		});
	},

	onSelectFilter: function(component, event, helper) {
		$A.get("e.c:x7sProductFilterEvent").setParams(
			{
				filterName  : 'search',
				filterValue : component.get('v.productType')
			}
		).fire();
	}
});