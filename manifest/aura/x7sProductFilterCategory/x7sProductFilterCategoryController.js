/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/18/19.
 */

({
	onInit : function(component, event, helper) {
		helper.doCallout(component, 'c.getCategories', {setting: ''}, false, 'Get Categories')
			.then(resultSet => {
				let categoryList = [];

				helper.addCategory(categoryList, resultSet);
				component.set('v.selectedItem', categoryList[0].name);
				component.set('v.itemList', categoryList);
			});
	},

	onCategorySelect: function (component, event, helper) {
		let categoryId = event.getParam('name');
		component.set('v.category', categoryId);

		$A.get("e.c:x7sProductFilterEvent").setParams(
			{
				filterName: helper.filter.CATEGORY,
				filterValue: categoryId
			}
		).fire();
	}
});