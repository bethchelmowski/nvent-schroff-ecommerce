/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/11/19.
 */

({
	addCategory: function(categoryList, items) {
		let self = this;
		items.forEach(item => {
			let newItem = {
				label: item.name,
				name : item.id
			};

			categoryList.push(newItem);

			if (item.children) {
				newItem.items = [];
				self.addCategory(newItem.items, item.children);
			}
		});
	}
});