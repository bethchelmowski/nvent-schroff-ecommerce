/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/21/19.
 */

({
	startFlow: function (component, flowName, product) {
		let flow = component.find('actionFlow');
		let quantity = component.get('v.quantity');

		let input = [
			{
				name:   'productId',
				type:   'String',
				value:  product.id
			},
			{
				name:    'sku',
				type:   'String',
				value:  product.productCode
			},
			{
				name:   'quantity',
				type:   'Number',
				value:  quantity
			},
			{
				name:   'unitPrice',
				type:   'Number',
				value:  product.unitPrice
			}
		];

		console.log('Calling flow: ' + flowName
			+ ' productId:  ' + product.id
			+ ', sku: ' + product.productCode
			+ ', quantity: ' + quantity);
		flow.startFlow(flowName, input);
	}
});