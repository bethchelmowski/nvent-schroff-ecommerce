/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/21/19.
 */

({
	init: function(component, event, helper) {
		let flowItems = component.get('v.actionItems');
		console.log(flowItems);
	},

	updateQuantity : function(component, event, helper) {
		let quantity = event.getParam('quantity');
		component.set('v.quantity', quantity);
	},

	handleMenuSelect: function(component, event, helper) {
		let actionItems     = component.get('v.actionItems');
		let selectedAction  = event.getParam("value");
		let actionItem      = helper.findAction(actionItems.items, selectedAction);
		let product         = component.get('v.product');

		switch(actionItem.actionType) {
			case 'Flow':
				helper.startFlow(component, actionItem.actionValue, product);
				break;
			case 'URL':
				helper.gotoUrl(component, helper.formatString(actionItem.actionValue, product.productCode));
				break;
		}
	},

	flowStatusChange: function (component, event, helper) {
		let status  = event.getParam('status');
		let label   = event.getParam('flowTitle');

		let message = 'Flow completed';
		let toastType = status === 'FINISHED_SCREEN' ? 'info' : 'error';

		helper.showToast(component, 'flowToast', label, message, toastType);
	}
});