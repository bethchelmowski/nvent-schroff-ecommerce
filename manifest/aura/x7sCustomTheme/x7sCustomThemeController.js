/**
 * Created by brandon.franklin on 5/8/2020.
 */

({
    doInit: function(component, event, helper) {
        helper.getBodyClass(component);
    },

    gotoUrl: function() {
        const action = $A.get('e.force:navigateToURL');
        action.setParams({
            url: '/'
        });
        action.fire();
    }
});