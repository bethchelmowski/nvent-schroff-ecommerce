/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/4/19.
 */

({
	init: function (component, event, helper) {
		helper.getProductSpecs(component, event);
	},

	onFilterChange: function (component, event , helper) {
		let filterType  = event.getParam('filterName');
		let filterValue = event.getParam('filterValue');

		if (filterType === helper.filter.CATEGORY) {
			component.set('v.category', filterValue);
			helper.getProductSpecs(component, event);
		}
	},

	onSpecCheckboxChanged: function (component, event, helper) {
		let specName    = event.getSource().get('v.name');
		let checked     = event.getSource().get('v.checked');
		let specId      = event.getSource().get('v.value');
		let filterItem  = helper.buildFilterItem(specId, specName);

		console.log('Filter ' + specName + ' = ' + filterItem);

		$A.get("e.c:x7sProductFilterEvent").setParams(
			{
				filterName   : helper.filter.SPEC,
				filterValue  : filterItem,
				filterAction : checked ? helper.filter.ADD : helper.filter.DELETE
			}
		).fire();
	}
});