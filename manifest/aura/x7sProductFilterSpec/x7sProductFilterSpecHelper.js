/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/7/19.
 */

({
	getProductSpecs: function (component, event) {
		this.doCallout(component, 'c.getProductSpecs',
			{categoryIds: component.get('v.category')},
			false, 'Get Product Specs')
			.then(specs => {
				component.set('v.specCollection', specs);
			});
	}
});