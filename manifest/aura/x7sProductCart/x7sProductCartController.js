/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

({
	init: function (component, event, helper) {
		helper.doCallout(component, "c.getActiveCart", {
			settings: component.get('v.settings'),
			userId  : ''
		}, false, "Get active cart")
			.then(model => {
				component.set('v.model', model);
				component.set('v.cartItems', model.items);
				component.set('v.optionsQuantity', [1,2,3,4,5,6,7,8,9,10]);
			});
	},
	
	onCartUpdate: function (component, event, helper) {
		const setting       = component.get('v.setting');
		const action        = event.getParam('action');
		const itemId        = event.getParam('itemId');
		const itemSku       = event.getParam('itemSku');
		const quantity      = event.getParam('quantity');
		const oldQuantity   = event.getParam('oldQuantity');
		
		helper.doCallout(component, 'c.updateCartItems', {
			setting     : setting,
			action      : action,
			skuList     : [itemSku],
			itemList    : [itemId],
			newValues   : [quantity],
			oldValues   : [oldQuantity]
		}, false, 'Update cart')
			.then(model => {
				component.set('v.model', model);
				component.set('v.cartItems', model.items);
			});
	}
});