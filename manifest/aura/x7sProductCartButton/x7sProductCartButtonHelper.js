/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/16/19.
 */

({
	addToCart: function (component) {
		let self = this;

		let sku         = component.get("v.productId");
		let productName = component.get('v.productName');
		let quantity    = component.get('v.quantity');

		console.log("Add to cart: " + sku);

		$A.get("e.c:x7sProductCartEvent").setParams({
			'action'    : this.action.ADD,
			'itemSku'   : sku,
			'name'      : productName,
			'quantity'  : quantity
		}).fire();

		component.set('v.quantity', 1);

		// Toggle the button label for a while
		component.set('v.buttonAdd', component.get('v.buttonLabelAdded'));
		component.set('v.buttonVariant', this.custom.BUTTON_SUCCESS);

		window.setTimeout(function() {
			component.set('v.buttonAdd', component.get('v.buttonLabelAdd'));
			component.set('v.buttonVariant', self.custom.BUTTON_BRAND);
		}, this.custom.BUTTON_DELAY);
	}
});