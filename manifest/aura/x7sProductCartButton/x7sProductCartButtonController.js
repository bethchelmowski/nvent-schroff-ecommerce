/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/16/19.
 */

({
	addToCart: function (component, event, helper) {
		helper.addToCart(component);
	},

	updateQuantity : function (component, event, helper) {
		let quantity = component.get('v.quantity');

		$A.get("e.c:x7sProductCartQuantityEvent").setParams({
			'quantity'  : quantity
		}).fire();
	}
});