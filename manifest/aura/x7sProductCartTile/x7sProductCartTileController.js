/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

({
	onRemoveItem : function (component, event, helper) {
		helper.fireCartEvent(component, helper.action.REMOVE);
	},

	onValueChanged: function (component, event, helper) {
		let oldValue = event.getParam("oldValue");
		component.set('v.oldQuantity', oldValue);
	},
	
	onQuantityChanged: function(component, event, helper){
		let item = component.get('v.cartItem');
		
		helper.fireCartEvent(component, item.quantity > 0 ?  helper.action.UPDATE : helper.action.REMOVE);
	},

	gotoProduct : function (component, event, helper) {
		helper.gotoRecord(component, component.get('v.cartItem').productId);
	}
});