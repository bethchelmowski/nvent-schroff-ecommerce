/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

({
	fireCartEvent: function (component, action) {
		let item = component.get('v.cartItem');
		let oldQuantity = component.get('v.oldQuantity');
		
		$A.get("e.c:x7sProductCartEvent").setParams({
			action      : action,
			itemId      : item.id,
			itemSku     : item.productCode,
			name        : item.name,
			unitPrice   : item.unitPrice,
			quantity    : item.quantity,
			oldQuantity : oldQuantity
		}).fire();
	}
});