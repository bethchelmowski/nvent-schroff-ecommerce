/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/11/19.
 */

({
	init: function (component, event, helper) {
		helper.setLabel(component);
	}
});