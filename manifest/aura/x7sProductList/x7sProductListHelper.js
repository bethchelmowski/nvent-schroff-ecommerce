/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 8/30/19.
 */

({
	getProducts: function (component) {
		const self     = this;
		const featured = component.get('v.showFeatured');
		const setting  = component.get('v.setting');
		const pageSize = component.get('v.pageLimit');

		const params = featured ? {
			'setting'       : setting
		} : {
			setting       : setting,
			pageSize      : pageSize,
			searchFilter  : component.get('v.searchString'),
			typeFilter    : component.get('v.typeFilter'),
			sortOrder     : component.get('v.sortString'),
			specFilter    : component.get('v.specFilter'),
			minimumPrice  : component.get('v.minimumPrice'),
			maximumPrice  : component.get('v.maximumPrice'),
			categoryId    : component.get('v.category')
		};
		this.dumpParams(component, params, 'GetProducts');
		this.showSpinner(component);

		this.doCallout (component, featured ? 'c.getProductsFeatured' : 'c.getProducts',
			params,
			false,
			'Get Products')
			.then($A.getCallback(function (products) {
				component.set('v.products',         products);
				component.set('v.specList',         products.productSpecs);

				// apply the Custom metadata settings, combined with builder settings
				component.set('v.navigateToVfPage', products.navigateToVfPage);
				component.set('v.optionsQuantity',  products.quantityList);
				component.set('v.showImage',        !products.hideImages    && component.get('v.showImage'));
				component.set('v.showPrice',        !products.hideUnitPrice && component.get('v.showPrice'));
				component.set('v.showAddToCart',    !products.hideAddToCart && component.get('v.showAddToCart'));
				component.set('v.actionItems',      products.actionSet);

				component.set('v.isInit', true);
				self.hideSpinner(component);
			}));
	},

	dumpParams: function(component, params, title) {
		console.log(title);
		console.log('  pageSize      : ' + params.pageSize);
		console.log('  searchFilter  : ' + params.searchFilter);
		console.log('  typeFilter    : ' + params.typeFilter);
		console.log('  sortOrder     : ' + params.sortOrder);
		console.log('  specFilter    : ' + params.specFilter);
	},

	showSpinner: function (component) {
		component.set('v.showSpinner', true);
	},

	hideSpinner: function (component) {
		component.set('v.showSpinner', false);
	},

	getMoreProducts: function (component, event, buttonClicked) {
		let pageSize  = component.get('v.pageSize');
		let pageLimit = component.get('v.pageLimit');

		//pageSize = buttonClicked === 'next' ? ++currentPage : --currentPage;
		if (buttonClicked === 'more') {
			component.set('v.pageLimit', pageLimit + pageSize);
		}

		this.getProducts(component);
	},
	
	addItemsToCart: function (component) {
	
	},
	
	updateCartItems : function(component) {
	
	}
});