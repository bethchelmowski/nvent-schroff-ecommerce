/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 8/30/19.
 */

({
	init: function (component, event, helper) {
		component.set('v.pageLimit', component.get('v.pageSize'));

		// decode the design sort order
		let sortOrder  = component.get('v.sortString');
		let sortString = 'nameAsc';

		switch(sortOrder) {
			case 'Name DESC':
				sortString = 'nameDesc';
				break;
			case 'Price: Low to High':
				sortString = 'priceAsc';
				break;
			case 'Price: High to Low':
				sortString = 'priceDesc';
				break;
		}
		component.set('v.sortString', sortString);

		helper.getProducts(component);
	},

	onFilterChange: function (component, event, helper) {
		let filterType   = event.getParam('filterName');
		let filterValue  = event.getParam('filterValue');
		let filterAction = event.getParam('filterAction');

		switch (filterType) {
			case helper.filter.CATEGORY:
				component.set('v.category', filterValue);
				component.set('v.specFilter', '');
				break;
			case helper.filter.SEARCH:
				component.set('v.searchString', filterValue);
				break;
			case helper.filter.TYPE:
				component.set('v.typeFilter', filterValue);
				break;
			case helper.filter.SORT:
				component.set('v.sortString', filterValue);
				break;
			case helper.filter.SPEC:
				component.set('v.specFilter', helper.updateFilterString(component.get('v.specFilter'), filterValue, filterAction));
				break;
		}

		helper.getProducts(component);
	},

	updateFilter: function(component, event, helper) {

	},

	handlePaginateEvent: function (component, event, helper) {
		helper.getMoreProducts(component, event, event.getParam('buttonClicked'));
	},

	handleFilterEvent: function (component, event, helper) {
		component.set('v.searchString', event.getParam('searchProductCode'));
		component.set('v.filterString', event.getParam('searchProductFamily'));
		component.set('v.sortString',   event.getParam('sortOrder'));
		component.set('v.currentPage',  1);

		helper.getProducts(component);
	},

	handleCartEvent : function (component, event, helper) {
		const setting       = component.get('v.setting');
		const action        = event.getParam('action');
		const sku           = event.getParam('itemSku');
		const productName   = event.getParam('name');
		const quantity      = event.getParam('quantity');
		const title         = component.get('v.toastTitle');

		helper.doCallout(component, 'c.addItemsToCart', {
			setting     : setting,
			skuList     : [sku],
			valueList   : [quantity]
		}, false, title)
			.then(returnValue => {
				if (returnValue.status === true) {
					component.set('v.cartId', returnValue.value);
				}

				let message = component.get('v.toastMessage')
						.replace('{0}', quantity)
						.replace('{1}', productName)
						+ ' - ' + returnValue.message;

				helper.showToast(
					component,
					'notificationLib',
					title,
					message,
					returnValue.status ? helper.action.INFO : helper.action.ERROR
				);
			});
	}
});