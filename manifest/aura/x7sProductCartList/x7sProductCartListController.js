/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

({
	onClearCart: function(component, event, helper) {
		if (confirm("Clear all items from the cart?")) {
			$A.get("e.c:x7sProductCartEvent").setParams({
				action: helper.action.CLEAR
			}).fire();
		}
	}
});