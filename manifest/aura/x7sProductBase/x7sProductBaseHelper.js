/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 8/30/19.
 */

({
	// Constants
	custom: {
		BUTTON_SUCCESS  : 'success',
		BUTTON_INVERSE  : 'inverse',
		BUTTON_BRAND    : 'brand',
		BUTTON_DELAY    : 3000
	},

	action: {
		ADD     : 'add',
		UPDATE  : 'update',
		REMOVE  : 'remove',
		CLEAR   : 'clear',
		SUCCESS : 'success',
		ERROR   : 'error',
		INFO    : 'info'
	},

	navigate: {
		detailPage     : '/ccrz__ProductDetails?viewState=DetailView&cartID=&portalUser=&store=&cclcl=en_US&sku=',
		dynamicKitPage : '/ccrz__ProductDetails?viewState=DetailView&cartID=&portalUser=&store=&cclcl=en_US&sku=',
		checkoutPage   : '/ccrz__CheckoutNew?portalUser=&store=&cclcl=&cartID='
	},

	filter : {
		SPEC    : 'spec',
		SORT    : 'sort',
		CATEGORY: 'category',
		SEARCH  : 'search',
		TYPE    : 'type',
		ADD     : 'add',
		DELETE  : 'del',
		FIELD_SEPARATOR : ';',
		VALUE_SEPARATOR : ':',
		VALUE_DELIMITER : ',',
	},

	doCallout: function(component, method, params, hideToast, toastTitle) {
		const self = this;

		return new Promise($A.getCallback(function(resolve, reject) {
			let action = component.get(method);

			if (params) {
				action.setParams(params);
			}

			action.setCallback(component, function(response) {
				let state = response.getState();

				if (component.isValid() && state === "SUCCESS") {
					resolve(response.getReturnValue());
				} else {
					let errors = response.getError();
					console.log(JSON.parse(JSON.stringify(errors)));

					if (errors && errors[0] && errors[0].message && !hideToast) {
						self.showMessage("error", toastTitle || 'Callback failed', errors[0].message);
					} else if (!hideToast) {
						self.showMessage("error", 'Callback failed', "Unknown Error");
					}

					reject(errors);
				}
			});

			$A.enqueueAction(action);
		}));
	},

	// Note that Boolean entries == false are not added to the filter string
	updateFilterString: function (filterString, newEntry, action) {
		let filterParts =[];

		if (filterString) {
			filterParts = filterString.split(this.filter.FIELD_SEPARATOR);
		}

		if (action === this.filter.ADD) {
			filterParts.push(newEntry);
		} else {
			filterParts = filterParts.filter(entry => {
				entry.substring(0, entry.indexOf(this.filter.VALUE_SEPARATOR)) !== newEntry.substring(0, newEntry.indexOf(this.filter.VALUE_SEPARATOR));
			});
		}

		let filter =  filterParts.join(this.filter.FIELD_SEPARATOR);
		console.log('Updated filter : ' + filter);

		return filter;
	},

	buildFilterItem: function (key, value) {
		return key + this.filter.VALUE_SEPARATOR + value;
	},

	// usage: formatString('My {0} goes {1}', 'name', 'here')
	formatString: function (format) {
		let args = Array.prototype.slice.call(arguments, 1);
		return format.replace(/{(\d+)}/g, (match, number) => {
			return typeof args[number] !== 'undefined'
				? args[number]
				: match;
		});
	},

	findAction: function(data, target) {
		for(let pos=0; pos < data.length; pos++) {
			if (data[pos].name === target) {
				return data[pos];
			}
		}
		return undefined;
	},

	addToCart : function(component, cartItems) {

	},

	gotoUrl : function(component, url) {
		$A.get("e.force:navigateToURL").setParams({'url': url}).fire();
	},

	gotoVFUrl: function(component, url) {
		window.location.href = url;
		//$A.get("e.force:navigateToURL").setParams({'url': url}).fire();
	},

	gotoRecord: function(component, recordId) {
		$A.get("e.force:navigateToSObject")
			.setParams({
				"recordId": recordId,
				"slideDevName": "related"
			}).fire();
	},

	// NOTE: (from Salesforce)
	// LEX uses a page state whitelist to wipe all query parameters
	// they don't want present in the url to avoid any collisions with custom components.
	// Adding a namespace of `c__` to the beginning of you query parameter should allow it to remain.
	// So instead of using lexRecordId as your key you can use c__lexRecordId and the query param should persist.
	navigateToRecordURL : function(component, navService, recordUrl, recordId) {
		let pageReference = {};

		if (this.inLexMode()) {
			pageReference = {
				type: 'standard__webPage',
				attributes: {
					url: component.get('v.sitePrefix')
						+ recordUrl
						+ '?'
						+ this.custom.urlParams.lexRecordId
						+ '='
						+ recordId
				},
				replace: true
			};

		} else {
			pageReference = {
				type: 'standard__recordPage',
				attributes: {
					recordId: recordId,
					objectApiName: 'Idea',
					actionName: 'view'
				}
			};
		}
		navService.navigate(pageReference);
	},

	getSitePath: function (sitePath) {
		let position = sitePath.lastIndexOf('/s');
		return sitePath.substring(0, position);
	},

	showMessage: function(level, title, message) {
		console.log("Message (" + level + "): " + message);
		$A.get("e.force:showToast")
			.setParams({
				title   : title,
				message : message,
				type    : level
			}).fire();
	},

	showToast : function(component, name, title, message, variant) {
		console.log('Toast  ('+ title + ') :' + message);
		component.find(name).showToast({
			title   : title,
			message : message,
			variant : variant | 'info'
		});
	},

	// LEX functions
	inLexMode: function () {
		let lexMode = new RegExp('.*?\/s\/','g').exec(window.location.href) != null;
		return !lexMode;
	}
});