/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/11/19.
 */

({
	onInit: function(component, event, helper) {
		// Figure out what screen size we're looking at
		window.addEventListener("resize", function () {
			helper.setScreenSize(component, event, helper);
		});

		helper.setScreenSize(component, event, helper);
	},

	handlePagination: function(component, event, helper) {
		const nextPage = event.currentTarget.dataset.page;

		component.set("v.currentSlide", parseInt(nextPage));
		helper.setPagination(component, event, helper);
	}

});