/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/17/19.
 */

({
	handleCancel: function (component, event, helper) {
		component.find("productImageOverlay").notifyClose();
	}
});