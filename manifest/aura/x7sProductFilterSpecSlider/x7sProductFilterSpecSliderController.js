/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/24/19.
 */

({
	checkSliderValues : function (component, event, helper) {
		let allValid = component
			.find('sliderInput')
			.reduce((validSoFar, inputControl) => {
				inputControl.reportValidity();
				return validSoFar && inputControl.checkValidity();
			}, true);
		component.set('v.disableSliderButton', !allValid);
	},

	onSliderFiler : function (component, event, helper) {
		let   action        = helper.filter.ADD;
		const spec          = component.get('v.specItem');
		const minValue      = component.get('v.minValue');
		let   maxValue      = component.get('v.maxValue');
		let   minIndex      = spec.displayValues.indexOf(minValue);
		let   filterValues  = [];
		let   selectedValues = [];

		if (minIndex === -1) {
			action = helper.filter.DELETE;
		} else {
			if (maxValue) {
				let maxIndex = spec.displayValues.indexOf(maxValue);

				if (maxIndex === -1) {
					for(let index = 0; index < spec.values.length; ++index)
					{
						if (spec.displayValues[index] >  maxValue) {
							maxIndex = index - 1;
							break;
						}
					}
				}

				for (let pos = minIndex; pos <= maxIndex; pos++) {
					selectedValues.push(spec.displayValues[pos]);
					filterValues.push(spec.values[pos]);
				}
			} else {
				selectedValues.push(spec.displayValues[minIndex]);
				filterValues.push(spec.values[minIndex]);
			}
		}
		component.set('v.selectedValues', selectedValues);

		let filterValue = filterValues.join(',');
		let filterItem  = helper.buildFilterItem(spec.id, filterValue);
		console.log('adding slider filter: ' + filterItem);

		$A.get("e.c:x7sProductFilterEvent").setParams(
			{
				filterName  : helper.filter.SPEC,
				filterValue : filterItem,
				filterAction: action
			}
		).fire();
	}
});