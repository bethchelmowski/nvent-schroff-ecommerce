/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 8/30/19.
 */

({
	gotoRecord: function (component, event, helper) {
		if (component.get('v.navigateToVfPage')) {
			const location   = window.location.href;
			const urlBase    = location.substring(0, location.indexOf('/s/'));
			const product    = component.get('v.product');
			const productUrl = urlBase + helper.navigate.detailPage + product.productCode;

			helper.gotoVFUrl(component, productUrl);
		} else {
			helper.gotoRecord(component, component.get('v.product').id);
		}
	},

	onBuildKit: function (component, event, helper) {
		const location   = window.location.href;
		const urlBase    = location.substring(0, location.indexOf('/s/'));
		const product   = component.get('v.product');
		let buildKitUrl = urlBase + helper.navigate.dynamicKitPage + product.productCode;

		helper.gotoVFUrl(component, buildKitUrl);
	}
});