/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

({
	init: function (component, event, helper) {
		helper.doCallout(component, "c.getActiveCart", {}, false, "Get active cart")
			.then(model => {
				model.subTotal       = model.totalPrice;
				model.estimatedTotal = model.totalPrice;
				model.discountTotal  = 0.0;
				
				component.set('v.model', model);
				component.set('v.cartItems', model.items);
			});
	},
	
	gotoCheckout : function (component, event, helper) {
		const checkoutUrl = component.get('v.checkoutUrl');
		
		if (checkoutUrl && checkoutUrl.length > 1) {
			helper.gotoUrl(component, checkoutUrl);
		}
		else {
			const location = window.location.href;
			const urlBase = location.substring(0, location.indexOf('/s/'));
			const model = component.get('v.model');
			let   vfCheckoutUrl = urlBase + helper.navigate.checkoutPage + model.cartEncId;
			
			helper.gotoVFUrl(component, vfCheckoutUrl);
		}
	}
});