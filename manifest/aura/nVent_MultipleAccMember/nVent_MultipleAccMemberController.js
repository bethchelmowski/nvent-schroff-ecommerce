({
    doInit: function(component, event, helper) {
        console.log('In Doinit');
        console.log('In Doinit'+component.get("v.recordId"));
        // Set the attribute value. 
        // You could also fire an event here instead.
        var action = component.get("c.getAccounMeminfo");
        action.setParams({          
            recordId : component.get("v.recordId")                  
        });
        action.setCallback(this, function(response) {
            console.log('In callback');
            var data = response.getReturnValue();
            console.log('In callback msg '+data);
            // Set the component attributes using values returned by the API call
            var state = response.getState();
            
            if(state == "SUCCESS"){
                console.log('In callback state '+state);
                console.log('response callback '+response.getReturnValue());
                if(response.getReturnValue() != '')
                {
                    component.set("v.msg",response.getReturnValue());
                    component.set("v.isError",true);
                }
                else
                    component.set("v.isError",false);    
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    }
})