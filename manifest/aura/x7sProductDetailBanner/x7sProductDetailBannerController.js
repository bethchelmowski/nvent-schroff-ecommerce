/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/16/19.
 */

({
	doInit: function(component, event, helper) {
		let setting   = component.get('v.setting');

		helper.doCallout(component, 'c.getProductsById',
			{
				'setting'     : setting,
				'pageSize'    : 1,
				'currentPage' : 1,
				'ids'         : [component.get('v.recordId')]
			}, false, 'Get Products by ID')
			.then($A.getCallback(function (listModel) {
				component.set('v.products', listModel);
				component.set('v.product',  listModel.items[0]);
				component.set('v.actionItems', listModel.actionSet);
			}));
	},

	handleButtonClick: function (component, event, helper) {
		const action  = event.getSource().get('v.name');
		const listUrl = component.get('v.listUrl');

		console.log('Clicked : ' + action);
		switch(action) {
			case 'edit':
				break;
			case 'delete':
				break;
			case 'like':
				break;
			case 'follow':
				break;
			case 'share':
				break;
			case 'list':
				helper.gotoUrl(component, listUrl);
				break;
		}
	}
});