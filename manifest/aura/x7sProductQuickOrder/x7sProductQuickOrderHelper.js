/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/17/19.
 */

({
	clearQuickOrder: function (component) {
		const initialCount = component.get('v.initialCount');
		const quickList = [];
		for(let counter = 0; counter < initialCount; counter++) {
			quickList.push({key: '', value: ''});
		}
		component.set('v.quickItems', quickList);	}
});