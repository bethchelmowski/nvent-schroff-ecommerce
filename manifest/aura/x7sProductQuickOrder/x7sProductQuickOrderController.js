/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/17/19.
 */

({
	init : function (component, event, helper) {
		let quickRows = [];
		for(let pos = 0; pos < 10; pos++) {
			quickRows.push({key: pos+1, value:pos+1})
		}
		component.set('v.quickRows', quickRows);

		helper.clearQuickOrder(component);
	},

	onRowCountChange : function(component, event, helper) {
		let currentItems = component.get('v.quickItems');
		helper.clearQuickOrder(component);
		let quickItems   = component.get('v.quickItems');
		for (let pos = 0; pos < quickItems.length; pos++) {
			if (pos < currentItems.length) {
				if (currentItems[pos].key) {
					quickItems[pos].key = currentItems[pos].key;
					quickItems[pos].value = currentItems[pos].value;
				}
			}
		}
		component.set('v.quickItems', quickItems);
	},

	addToCart : function (component, event, helper) {
		const title   = component.get('v.toastTitle');

		let skuList   = [];
		let valueList = [];
		let quickItems = component.get('v.quickItems');

		// get the entries with values
		quickItems.forEach(item => {
			if (item.key) {
				let value = Number(item.value);
				if (isNaN(value) || value === 0) {
					value = 1;
				}
				skuList.push(item.key);
				valueList.push(value);
			}
		});

		if (skuList.length > 0) {
			helper.doCallout(component, 'c.addItemsToCart', {
				setting     : component.get('v.setting'),
				skuList     : skuList,
				valueList   : valueList
			}, false, title)
				.then(returnValue => {
					if (returnValue.status) {
						helper.clearQuickOrder(component);
						helper.showToast(
							component,
							'notificationLib',
							title,
							returnValue.message,
							returnValue.status ? helper.action.INFO : helper.action.ERROR
						);
					}
				});
		}
	}
});