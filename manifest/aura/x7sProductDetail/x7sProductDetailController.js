/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/11/19.
 */

({
	doInit: function(component, event, helper) {
		const imageSize = component.get('v.imageSize');
		let imageWidth  = 6;
		let detailWidth = 6;
		switch(imageSize) {
			case 'Small':
				imageWidth = 2;
				detailWidth = 10;
				break;
			case 'Medium':
				imageWidth = 4;
				detailWidth = 8;
				break;
		}
		component.set('v.imageColumnWidth',  imageWidth);
		component.set('v.detailColumnWidth', detailWidth);

		const zoomSize = component.get('v.zoomWindowSize');
		let zoomClass  = 'slds-modal_medium';
		switch(zoomSize) {
			case 'Small':
				zoomClass = 'slds-modal_small'
				break;
			case 'Medium':
				zoomClass='slds-modal_medium';
				break;
			case 'Large':
				zoomClass= 'slds-modal_large'
				break;
		}
		component.set('v.zoomWindowClass', zoomClass);

		helper.doCallout(component, 'c.getProductsById',
			{
				setting     : component.get('v.setting'),
				pageSize    : 1,
				currentPage : 1,
				ids         : [component.get('v.recordId')]
			}, false, 'Get Products by ID')
			.then(listModel => {
				const product = listModel.items[0];

				if (product) {
					component.set('v.optionsQuantity', listModel.quantityList);
					component.set('v.products', listModel);
					component.set('v.product', product);
					component.set('v.actionItems', listModel.actionSet);
					component.set('v.showDynamicKit', product.productType === 'Dynamic Kit');
					component.set('v.showVFDetail', product.productType !== 'Dynamic Kit');

					component.set('v.showAddToCartButton', (!listModel.hideAddToCart) && component.get('v.showAddToCart'));
					component.set('v.showActionMenuButton', component.get('v.showActionMenu'));
				}
			});

		helper.doCallout(component, 'c.getActiveCart',
			{
				setting: component.get('v.setting')
			} , false, 'Get cart id')
			.then(result => {
				component.set('v.cartId', result);
			});
	},

	handleImageClick: function (component, event, helper) {
		const currentProduct = component.get('v.product');
		const imageSlider    = component.find('imageSlider');

		const currentSlide   = imageSlider ?  imageSlider.get('v.currentSlide') : 0;
		const zoomSize       = component.get('v.zoomWindowClass');

		let modalHeader;
		let modalBody;
		let modalFooter;

		$A.createComponents(
			[
				["c:x7sProductDetailHeader",    {title: currentProduct.name}],
				["c:x7sProductDetailImage",     {product: currentProduct, currentSlide: currentSlide}],
				["c:x7sProductDetailFooter",    {}]
			],
			function (contents, status) {
				if (status === "SUCCESS") {
					modalHeader = contents[0];
					modalBody   = contents[1];
					modalFooter = contents[2];

					component.find('productImageOverlay').showCustomModal(
						{
							header          : modalHeader,
							body            : modalBody,
							footer          : modalFooter,
							showCloseButton : true,
							cssClass        : zoomSize
						})
				}
			});
	},

	onBuildKit: function (component, event, helper) {
		const location  = window.location.href;
		const urlBase   = location.substring(0, location.indexOf('/s/'));
		const product   = component.get('v.product');
		let buildKitUrl = urlBase + helper.navigate.dynamicKitPage + product.productCode;

		helper.gotoVFUrl(component, buildKitUrl);
	},

	onVFDetailPage : function (component, event, helper) {
		const location = window.location.href;
		const urlBase  = location.substring(0, location.indexOf('/s/'));
		const product  = component.get('v.product');
		let productUrl = urlBase + helper.navigate.detailPage + product.productCode;

		helper.gotoVFUrl(component, productUrl);
	},

	handleAddItemToCart : function (component, event, helper) {
		const setting       = component.get('v.setting');
		const itemId        = event.getParam('itemId');
		const productName   = event.getParam('name');
		const quantity      = event.getParam('quantity');
		const title         = component.get('v.toastTitle');

		helper.doCallout(component, 'c.addItemsToCart', {
			setting     : setting,
			skuList     : [itemId],
			valueList   : [quantity]
		}, false, title)
			.then(returnValue => {
				if (returnValue.status === true) {
					component.set('v.cartId', returnValue.value);
				}

				let message = component.get('v.toastMessage')
						.replace('{0}', quantity)
						.replace('{1}', productName)
					+ ' - ' + returnValue.message;

				helper.showToast(
					component,
					'notificationLib',
					title,
					message,
					returnValue.status ? helper.action.INFO : helper.action.ERROR
				);
			});
	}
});