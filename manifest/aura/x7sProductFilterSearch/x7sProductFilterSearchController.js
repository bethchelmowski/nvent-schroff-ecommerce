/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/18/19.
 */

({
	onSearchChange: function (component, event, helper) {
		let searchString = component.get('v.searchString');

		if (searchString.length > component.get('v.minLength') || searchString  === '') {
			$A.get("e.c:x7sProductFilterEvent").setParams(
				{
					filterName: helper.filter.SEARCH,
					filterValue: searchString
				}
			).fire();
		}
	}
});