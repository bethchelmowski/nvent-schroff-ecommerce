/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/16/19.
 */

({
	init: function (component, event, helper) {
		component.set('v.isInit', true);

		// default alignment is start but cannot be specified - rip it out
		let align = component.get('v.align');

		if (align === 'start') {
			$A.util.removeClass(component.find('paginationControl'), 'horizontalAlign');
		}
	},

	paginate: function (component, event, helper) {
		component
			.getEvent('paginateEvent')
			.setParams({'buttonClicked': event.getSource().get('v.name')})
			.fire();
	}
});