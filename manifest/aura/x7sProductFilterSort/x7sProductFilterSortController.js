/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/23/19.
 */

({
	init : function(component, event, helper) {
		let sortItems = [
			{key: component.get('v.sortNameAsc'),   value: 'nameAsc'},
			{key: component.get('v.sortNameDesc'),  value: 'nameDesc'},
			{key: component.get('v.sortPriceAsc'),  value: 'priceAsc'},
			{key: component.get('v.sortPriceDesc'), value: 'priceDesc'}
		];
		component.set('v.itemList', sortItems);
	},

	onSelectSort: function(component, event, helper) {
		$A.get("e.c:x7sProductFilterEvent").setParams(
			{
				filterName  : helper.filter.SORT,
				filterValue : component.get('v.sortOrder')
			}
		).fire();
	}

});