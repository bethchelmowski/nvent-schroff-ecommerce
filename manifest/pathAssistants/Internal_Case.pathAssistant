<?xml version="1.0" encoding="UTF-8"?>
<PathAssistant xmlns="http://soap.sforce.com/2006/04/metadata">
    <active>true</active>
    <entityName>Case</entityName>
    <fieldName>Status</fieldName>
    <masterLabel>Internal Case</masterLabel>
    <pathAssistantSteps>
        <info>&lt;p&gt;Tasks to complete during this stage:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Ensure request has been prioritized against other requests for your SBU&lt;/li&gt;&lt;li&gt;Finalize requirements&lt;/li&gt;&lt;li&gt;Identify impacts to Salesforce (including integrations with Salesforce)&lt;/li&gt;&lt;li&gt;Discuss request with other SBU Admins and identify impacts to other SBUs&lt;/li&gt;&lt;li&gt;Align on solution with SBU Admins &amp;amp; Platform Leader&lt;/li&gt;&lt;li&gt;If applicable, prepare slide for Governance Committee meeting and send to Platform Leader&lt;/li&gt;&lt;/ul&gt;</info>
        <picklistValueName>Analysis In Progress</picklistValueName>
    </pathAssistantSteps>
    <pathAssistantSteps>
        <info>&lt;ul&gt;&lt;li&gt;Finalize deployment plan - please see the &lt;b&gt;Deployment Best Practices&lt;/b&gt; tab on the Case for guidance&lt;/li&gt;&lt;li&gt;Send Communication to SBU Admins (for distribution to their Salesforce users) at least 5 business days prior to deployment&lt;ul&gt;&lt;li&gt;For larger projects, recommend communication is sent 10 business days prior to deployment&lt;/li&gt;&lt;/ul&gt;&lt;/li&gt;&lt;/ul&gt;</info>
        <picklistValueName>Awaiting Deployment</picklistValueName>
    </pathAssistantSteps>
    <pathAssistantSteps>
        <info>&lt;p&gt;&lt;span style=&quot;font-size: 13px;&quot;&gt;Send Communication to SBU Admins informing them that the deployment is complete&lt;/span&gt;&lt;/p&gt;</info>
        <picklistValueName>Deployed</picklistValueName>
    </pathAssistantSteps>
    <pathAssistantSteps>
        <fieldNames>Impacted_SBUs__c</fieldNames>
        <fieldNames>Date_Request_Approved__c</fieldNames>
        <fieldNames>Acceptance_Criteria__c</fieldNames>
        <info>&lt;p&gt;&lt;span style=&quot;font-size: 13px;&quot;&gt;Update to &lt;/span&gt;&lt;b&gt;In Progress&lt;/b&gt; once you have received approval from the Governance Committee &amp;amp; you are actively working on the request. As a reminder, your request should be built in the &lt;b&gt;SBUAdminDE&lt;/b&gt; sandbox.&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;The following information should be documented prior to moving the a Case to &lt;b&gt;In Progress&lt;/b&gt;:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Impacted SBUs&lt;/li&gt;&lt;li&gt;Date Request Approved (if Governance Committee Approval if Required)&lt;/li&gt;&lt;li&gt;Acceptance Criteria&lt;/li&gt;&lt;/ul&gt;</info>
        <picklistValueName>In Progress</picklistValueName>
    </pathAssistantSteps>
    <pathAssistantSteps>
        <fieldNames>OwnerId</fieldNames>
        <info>&lt;p&gt;Cases in the &lt;b&gt;New&lt;/b&gt; status are not assigned.&lt;/p&gt;&lt;ul&gt;&lt;li&gt;If you take ownership of this case, please change the status to &lt;b&gt;Analysis In Progress&lt;/b&gt;&lt;/li&gt;&lt;li&gt;Ensure the Description contains the Business Objective or Challenge that is driving this request. It is recommended to use a &lt;b&gt;User Story&lt;/b&gt; format (Example: As a support team member [&lt;b&gt;Role&lt;/b&gt;], I want to be notified when a request is sent to the support team [&lt;b&gt;Feature&lt;/b&gt;], so that I can take action on the request [&lt;b&gt;Outcome&lt;/b&gt;].):&lt;ul&gt;&lt;li&gt;As a…&lt;ul&gt;&lt;li&gt;&lt;b&gt;Role&lt;/b&gt;: Identify the Users that the story will be written for&lt;/li&gt;&lt;/ul&gt;&lt;/li&gt;&lt;li&gt;… I want to …&lt;ul&gt;&lt;li&gt;&lt;b&gt;Feature&lt;/b&gt;: This is the task that you would like the user to be able to perform&lt;/li&gt;&lt;/ul&gt;&lt;/li&gt;&lt;li&gt;so I can …&lt;ul&gt;&lt;li&gt;&lt;b&gt;Outcome&lt;/b&gt;: this is the goal or result that is achieved from the action&lt;/li&gt;&lt;/ul&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/li&gt;&lt;/ul&gt;</info>
        <picklistValueName>New</picklistValueName>
    </pathAssistantSteps>
    <pathAssistantSteps>
        <info>&lt;p&gt;Move a request to &lt;b&gt;On Hold&lt;/b&gt; if it is awaiting prioritization against other requests. &lt;b&gt;On Hold&lt;/b&gt; means that it is not actively being worked on.&lt;/p&gt;</info>
        <picklistValueName>On Hold</picklistValueName>
    </pathAssistantSteps>
    <pathAssistantSteps>
        <info>&lt;p&gt;If Governance Committee rejects the request, please select this Status.&lt;/p&gt;</info>
        <picklistValueName>Request Rejected</picklistValueName>
    </pathAssistantSteps>
    <pathAssistantSteps>
        <info>&lt;ul&gt;&lt;li&gt;Once SBU Admins have all signed off on the solution, document your manual deployment steps and move the changes to the &lt;b&gt;Staging&lt;/b&gt; sandbox.&lt;ul&gt;&lt;li&gt;Recommend using this deployment as a &amp;quot;trial&amp;quot; deployment to prepare you for the actual deployment to production&lt;/li&gt;&lt;/ul&gt;&lt;/li&gt;&lt;li&gt;SBU Admins to smoke test solution in Staging, once approved, move to Awaiting Deployment&lt;/li&gt;&lt;li&gt;Cannot move to this stage until all SBU &amp;quot;Sign Off&amp;quot; checkboxes are checked&lt;/li&gt;&lt;/ul&gt;</info>
        <picklistValueName>Sign Off Received</picklistValueName>
    </pathAssistantSteps>
    <pathAssistantSteps>
        <info>&lt;ul&gt;&lt;li&gt;UAT testing should be conducted by the requesting SBU in the &lt;b&gt;SBUAdminDE&lt;/b&gt; sandbox prior to sending to the other SBU Admins for their review&lt;/li&gt;&lt;li&gt;Once solution is finalized, send to SBU Admins for their review and approval&lt;/li&gt;&lt;li&gt;Begin preparing Communication Plan, including your planned deployment date&lt;ul&gt;&lt;li&gt;To lessen the impact on Salesforce users, deployments should be done over the weekend&lt;/li&gt;&lt;/ul&gt;&lt;/li&gt;&lt;/ul&gt;</info>
        <picklistValueName>UAT</picklistValueName>
    </pathAssistantSteps>
    <recordTypeName>Internal_Case</recordTypeName>
</PathAssistant>
