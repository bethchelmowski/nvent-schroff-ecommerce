trigger Pentair_RFQ_XML_DataLoad on RFQ_XML__c (after insert, after update) {
    ID rfqRecordId;
    String rfqType;
    Decimal lineNumber;
    
    if (Pentair_RFQController.runOnce())
    {    
        for (RFQ_XML__c rfqObject: Trigger.new)
        { 
            rfqRecordId = rfqObject.Id;
            rfqType = rfqObject.Type__c;
            lineNumber = rfqObject.Line__c;
            
            if (rfqType == 'Quote')
            {            
                Pentair_RFQXMLParse.ProcessXMLFile(rfqObject, lineNumber);
            }     
            
            if (rfqType == 'Order' && lineNumber == 1)
            {
                Pentair_RFQXMLParse.ProcessOrderXMLFile(rfqObject);
            }
        }
    }
}