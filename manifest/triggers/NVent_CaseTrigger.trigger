/**********************************************************************
Name:  NVent_CaseTrigger
Developed by: Anvi Kanodra
=======================================================================
Purpose:                                                            
To invoke trigger operation on Case Object. Trigger framework is used
to implement the trigger. Write all the business logic in 
CC_CaseTriggerHandler class. 
=======================================================================
History                                                            
-------                                                            
VERSION  AUTHOR            DATE              DETAIL
1.0 -                                   Initial Development
***********************************************************************/
trigger NVent_CaseTrigger on Case (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    NVent_TriggerDispatcher.run(new NVent_CaseTriggerHandler());
}