trigger Pentair_ShareEndToEndRecord on End_to_End__c (after update) {
    for(End_to_End__c e2eObject : trigger.new){ 
        End_to_End__c oldRFQ = Trigger.oldMap.get(e2eObject.ID);
        
        if (oldRFQ.OwnerId != e2eObject.OwnerId && e2eObject.CreatedById != e2eObject.OwnerId)
        {
			Boolean isCreated = Pentair_ManualSharingRecord.manualShareE2ERead(e2eObject.Id, e2eObject.CreatedById);
                
                if (!isCreated)
                    System.debug('Failed on sharing record for ' + e2eObject.CreatedById + ' on record: ' + e2eObject.Id); 
        }
    }
}