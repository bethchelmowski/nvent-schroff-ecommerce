trigger Pentair_EndToEnd on End_to_End__c (before insert, after update) {
  /* if (Trigger.isBefore) {
        for (End_to_End__c endToEndObject: Trigger.new)
        {
            String ownerId = endToEndObject.OwnerId;
            
            List<User> userList  = [SELECT NAME FROM USER WHERE ID =: ownerId];
                
            if (userList.size() > 0)
                endToEndObject.Owner_Name__c = userList[0].Name;   
            
            endToEndObject.Scheduled_Completion__c = Pentair_RFQController.SetDateByBusinessDays(Date.today(), 3);
        }
    }
    
    if(Trigger.isUpdate) {         
        for (End_to_End__c endToEndObject: Trigger.new)
        {
            End_to_End__c oldObject = Trigger.oldMap.get(endToEndObject.ID);
            
            String ownerId = endToEndObject.OwnerId;
            
            if (oldObject.OwnerId != endToEndObject.OwnerId)
            {
                System.debug('Owner Updated');
                
                List<User> userList  = [SELECT NAME FROM USER WHERE ID =: ownerId];
                List<Group> groupList  = [SELECT NAME FROM Group WHERE ID =: ownerId];
                
                End_to_End__c aux = new End_to_End__c(ID=endToEndObject.Id);
                
                if (userList.size() > 0)
                    aux.Owner_Name__c = userList[0].Name;
                if (groupList.size() > 0)
                    aux.Owner_Name__c = groupList[0].Name;
                
                update aux;
            }
            
            //Update the Notes on the RFQ Record
            if (oldObject.Notes__c != endToEndObject.Notes__c)
            {
                RFQ__c rfqObject = [SELECT Notes__c FROM RFQ__c WHERE ID =:endToEndObject.RFQ__c];
                
                RFQ__c aux = new RFQ__c(ID=endToEndObject.RFQ__c);
                if (rfqObject.Notes__c != null)
                    aux.Notes__c = rfqObject.Notes__c + ' ' + endToEndObject.Notes__c;
                else
                    aux.Notes__c = endToEndObject.Notes__c;
                update aux;
            }
            
            //Update the Related_Item_Numbers__c on the RFQ Record
            if (oldObject.Related_Item_Numbers__c != endToEndObject.Related_Item_Numbers__c)
            {
                RFQ__c rfqObject3 = [SELECT Related_Item_Numbers__c FROM RFQ__c WHERE ID =:endToEndObject.RFQ__c];
                
                RFQ__c aux3 = new RFQ__c(ID=endToEndObject.RFQ__c);
                if (rfqObject3.Related_Item_Numbers__c != null)
                    aux3.Related_Item_Numbers__c = rfqObject3.Related_Item_Numbers__c + ' ' + endToEndObject.Related_Item_Numbers__c;
                else
                    aux3.Related_Item_Numbers__c = endToEndObject.Related_Item_Numbers__c;
                update aux3;
            }
        }
    }*/
}