trigger Pentair_UpdateParentOppTotals on Parent_Opportunity__c (after insert, after update) 
{
	try
    {
    	Pentair_UpdateParentOppTotalsHelper HelperClass = new Pentair_UpdateParentOppTotalsHelper();
        if (trigger.IsUpdate)
    	{
    		HelperClass.CheckforCurrencyChange(Trigger.new,Trigger.oldMap);
            HelperClass.CalculateTotalAmount(Trigger.new,Trigger.oldMap);
        }
    
    } 
    catch (Exception e) 
    {
		System.debug(e);
	}
}