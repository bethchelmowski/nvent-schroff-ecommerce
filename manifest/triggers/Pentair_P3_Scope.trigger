trigger Pentair_P3_Scope on RFQ__c (after insert, after update) {
    if (Trigger.IsInsert)
    {
        for (RFQ__c rfqObject : trigger.new)
        {
                String multiSelect = rfqObject.Resources_Includes__c;
                if (multiSelect != null)
                {
                    String scopeField = '';
                    RFQ__c aux  = new RFQ__c(Id = rfqObject.Id); 
                    
                    if (multiSelect.contains('Engineering'))
                         scopeField = 'E';
                    if (!multiSelect.contains('Engineering'))
                        scopeField = '';
                    
                    if (rfqObject.Product_Type__c == 'Standard')
                        scopeField = scopeField + 'P';
                    else if (rfqObject.Product_Type__c == 'Modified')
                        scopeField = scopeField + 'Pm';
                    else if (rfqObject.Product_Type__c == 'Customized')
                        scopeField = scopeField + 'Pc';
                    else if (rfqObject.Product_Type__c == 'Engineered')
                        scopeField = scopeField + 'Pe';
                    
                    if (multiSelect.contains('Installation') || multiSelect.contains('Supervision') || 
                        multiSelect.contains('Commissioning') || multiSelect.contains('Maintenance'))
                         scopeField = scopeField + 'C';
                    
                    aux.Scope__c = scopeField;
                    update aux;
                }
        }
    }
    
    if(Trigger.isUpdate) { 
        for (RFQ__c rfqObject : trigger.new)
        {
            RFQ__c oldObject = Trigger.oldMap.get(rfqObject.ID);
            if (rfqObject.Resources_Includes__c != oldObject.Resources_Includes__c || 
                rfqObject.Product_Type__c != oldObject.Product_Type__c)
            {
                String multiSelect = rfqObject.Resources_Includes__c;
                if (multiSelect != null)
                {
                    String scopeField = '';
                    RFQ__c aux  = new RFQ__c(Id = rfqObject.Id); 
                    
                    if (multiSelect.contains('Engineering'))
                         scopeField = 'E';
                    if (!multiSelect.contains('Engineering'))
                        scopeField = '';
                    
                    if (rfqObject.Product_Type__c == 'Standard')
                        scopeField = scopeField + 'P';
                    else if (rfqObject.Product_Type__c == 'Modified')
                        scopeField = scopeField + 'Pm';
                    else if (rfqObject.Product_Type__c == 'Customized')
                        scopeField = scopeField + 'Pc';
                    else if (rfqObject.Product_Type__c == 'Engineered')
                        scopeField = scopeField + 'Pe';
                    
                    if (multiSelect.contains('Installation') || multiSelect.contains('Supervision') || 
                        multiSelect.contains('Commissioning') || multiSelect.contains('Maintenance'))
                         scopeField = scopeField + 'C';
                    
                    aux.Scope__c = scopeField;
                    update aux;
                }
            }
            
            if (rfqObject.Linked_External_System__c != null && 
                rfqObject.Linked_External_System__c != oldObject.Linked_External_System__c )
            {
                List<RecordType> recordTypeId = [select Id from RecordType where DeveloperName = 'P3_ReadOnly' AND sobjecttype = 'RFQ__C' limit 1];
                
                if (recordTypeId.size() > 0)
                {
                    RFQ__c aux  = new RFQ__c(Id = rfqObject.Id); 
                    aux.RecordTypeId = recordTypeId[0].Id;
                    update aux;
                }
            }
        }
    }
}