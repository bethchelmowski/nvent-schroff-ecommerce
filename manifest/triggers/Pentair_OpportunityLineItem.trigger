trigger Pentair_OpportunityLineItem on OpportunityLineItem (after insert) {
    for (OpportunityLineItem lineObject: Trigger.new)
    { 
        List<Opportunity> getOpportunityRecord = [SELECT ID, First_Revenue_Date__c, CloseDate FROM Opportunity
                                                 WHERE ID =: lineObject.OpportunityId];
        
        if (getOpportunityRecord.size() > 0)
        {
            //If Service Date is null pull the First Revenue Date
            if (lineObject.ServiceDate == null)
            {
                OpportunityLineItem aux = new OpportunityLineItem(ID=lineObject.Id);
                
                if (getOpportunityRecord[0].First_Revenue_Date__c == null)
                    aux.ServiceDate = getOpportunityRecord[0].CloseDate;
                else
                    aux.ServiceDate = getOpportunityRecord[0].First_Revenue_Date__c;
                
                update aux;
            }
        }
    }
}