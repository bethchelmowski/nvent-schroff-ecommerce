trigger Pentair_ShareRFQRecord on RFQ__c (after update) {
        
    if (Pentair_RFQController.runOnce())
        {
    for(RFQ__c rfqObject : trigger.new){
        
        RFQ__c oldRFQ = Trigger.oldMap.get(rfqObject.ID);
        if (oldRFQ.OwnerId != rfqObject.OwnerId && rfqObject.CreatedById != rfqObject.OwnerId)
        {
            Boolean isCreated = Pentair_ManualSharingRecord.manualShareRFQRead(rfqObject.Id, rfqObject.CreatedById);
                
                if (!isCreated)
                    System.debug('Failed on sharing record for ' + rfqObject.CreatedById + ' on record: ' + rfqObject.Id);
        }
    }
        }
}