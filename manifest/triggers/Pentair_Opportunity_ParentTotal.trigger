trigger Pentair_Opportunity_ParentTotal on Opportunity (after insert, after delete, after update) 
{

    try {
            Pentair_UpdateParentFlagHelper UpdateParentOpportunity = new Pentair_UpdateParentFlagHelper();
            if(trigger.isInsert)
            {
                UpdateParentOpportunity.InsertredOrDeletedOpportunity(trigger.new);
            }
            if(trigger.isUpdate)
            {
                UpdateParentOpportunity.UpdatedOpportunity(trigger.new, Trigger.oldMap);
            }
            if(trigger.isDelete)
            {
                UpdateParentOpportunity.InsertredOrDeletedOpportunity(trigger.old);
            }
            
        }
    catch (Exception e) 
        {
            System.debug(e);
        }
}