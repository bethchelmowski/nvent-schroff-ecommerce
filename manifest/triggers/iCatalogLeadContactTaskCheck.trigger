trigger iCatalogLeadContactTaskCheck on Lead (after insert) {
    if(Trigger.isAfter) {
        if (Trigger.isInsert) {    
            Set<String> leadCatalogEmailUser = new Set<String>();
            for (Lead leadObject: Trigger.new)
            {
                //Checks to make sure the record contains data
                if (leadObject.Catalog_User__c != NULL)
                { 
                	//Get the Email record
                	leadCatalogEmailUser.add(leadObject.Email);
                    
                	//Check if the Contact Exisits
                    try
                    {
                        //Searchs for the user record to make sure it finds a match
                        Contact[] contactObjects = [
                            select Id, OwnerId from Contact 
                            where Email IN : leadCatalogEmailUser
                            and IsDeleted = false
                        ];
                        
                        //If it finds a match to the contact record we need to qualify the lead
                        if (contactObjects.size() > 0)
                        {                           
                            //Create Task Records for the Contact Record
                            Pentair_iCatalogController.CreateTaskRecord(contactObjects[0].OwnerId, 
                                                                        leadObject.Id, 
                                                                        contactObjects[0].Id, 
                                                                        false);
                            
                            if (leadObject.Create_Follow_Up_Tasks__c)
                            {
                                Pentair_iCatalogController.CreateTaskRecord(contactObjects[0].OwnerId, 
                                                                        leadObject.Id, 
                                                                        contactObjects[0].Id, 
                                                                        true);
                            }
                        }
                        else
                        {
							 //Create Task Records for the Contact Record
                            Pentair_iCatalogController.CreateTaskRecord(leadObject.OwnerId, 
                                                                        leadObject.Id, 
                                                                        null, 
                                                                        false);
                            
                            if (leadObject.Create_Follow_Up_Tasks__c)
                            {
                                Pentair_iCatalogController.CreateTaskRecord(leadObject.OwnerId, 
                                                                        leadObject.Id, 
                                                                        null, 
                                                                        true);
                            }                            
                        }
                    }
                    catch (DmlException dm)
                    {
                        
                    }

                }
            }
        }
    }
}