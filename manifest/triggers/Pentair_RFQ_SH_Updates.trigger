//**************************************************************************************************************************//
//                                  nVent Electrical/Electronic Manufacturing                                               //
//  Class Name: Pentair_RFQ_SH_Updates                                                                                      //                              
//  Apex Trigger: This is trigger on RFQ__c with after insert , before insert, before Update                                                                                //
//  Created By: Slalom                                                                                                      //      
//  Created Date: 4/4/2018                                                                                                  //
//--------------------------------------------------------------------------------------------------------------------------//
//  ||  Version ||  Date of Change  ||  Editor      ||  Enh Req No  ||  Change Purpose                                      //
//  ||  1.0     ||  4/4/2018        ||  Slalom      ||              ||  Initial Creation                                    //
//  ||  2.0     ||  11/2/2018       ||  Tredence    ||  Ad-001      ||  Fixed RFQ Assigning Issue                           //
//  ||  3.0     ||  11/20/2018      ||  Tredence    ||  A-0215      ||  Update RFQ Total and the Lineitems change of ERP Num//
//**************************************************************************************************************************//  
trigger Pentair_RFQ_SH_Updates on RFQ__c (after insert , before insert, before Update)
{ 
    if(Trigger.isInsert && Trigger.isAfter){
    for (RFQ__c rfqObject: Trigger.new)
        {
            
            
        RFQ__c aux  = new RFQ__c(Id = rfqObject.Id); 
        
        //Only features done for Schroff and Hoffman
        if (rfqObject.Sales_Team__c == 'Schroff' || rfqObject.Sales_Team__c == 'Hoffman')
        {         
            //Set the Scheduled Completion Date
            if (rfqObject.Type__c == 'Customized')
            {
                //add 5 business days
                if (rfqObject.RFQ_Received__c != null)
                    aux.Scheduled_Completion__c = Pentair_RFQController.SetDateByBusinessDays(rfqObject.RFQ_Received__c, 5);
            }
            else
            {
                //add 2 business days
                if (rfqObject.RFQ_Received__c != null)
                    aux.Scheduled_Completion__c = Pentair_RFQController.SetDateByBusinessDays(rfqObject.RFQ_Received__c, 2);
            }
            //Set the Product Type from the RFQ Type
            if (rfqObject.Sales_Team__c == 'Schroff' && aux.Product_Type__c != null)
                aux.Product_Type__c = rfqObject.Type__c;
            
            //Get the ERP Number from the Account Record
            //aux.ERP_Customer_Number__c = Pentair_RFQController.GetERPNumber(rfqObject.Account__c);

        }
        
        //Get the Assign To Person
        if (rfqObject.Assigned_To__c == null)
         aux.Assigned_To__c = Pentair_RFQController.GetAssignedto(rfqObject.Account__c, rfqObject.OwnerId,rfqObject.RecordTypeId);//Ad-001-- Added RecordTypeId to assign right customer care user
        //aux.Proposal_Owner__c = Pentair_RFQController.GetAssignedto(rfqObject.Account__c, rfqObject.OwnerId);
            
        update aux;
    }
}
    //****
    if(Trigger.isInsert && Trigger.isBefore){
        set<id> accid = new set<id>();
        for(RFQ__c rfq : Trigger.new){
            
            accid.add(rfq.account__c);
        }         
         map<string,Map<String,List<AccountTeamMember>>> accTeamMap = new map<String,map<String,List<AccountTeamMember>>>();
        for(AccountTeamMember accMem :  [SELECT TeamMemberRole,UserId ,AccountId  FROM AccountTeamMember WHERE AccountId in:accid and TeamMemberRole IN ('Schroff Rep','Hoffman Rep') ] ){
            Map<String,List<AccountTeamMember>> accTeam = new Map<String,List<AccountTeamMember>>();
            List<AccountTeamMember> actLst= new List<AccountTeamMember>();
            if(accTeamMap.containsKey(accMem.AccountId)){
                if((accTeamMap.get(accMem.AccountId)).containskey(accMem.TeamMemberRole)){
                   actLst = (accTeamMap.get(accMem.AccountId)).get(accMem.TeamMemberRole);
                } 
                               
            }
            actLst.add(accMem);
            accTeam.put(accMem.TeamMemberRole, actLst);
            accTeamMap.put(accMem.AccountId, accTeam);  
            
        }
         
        for(RFQ__c rfq : Trigger.new){
            if(accTeamMap.containsKey(rfq.Account__c)){
                String salesTeam = rfq.Sales_Team__c == 'Schroff'? 'Schroff Rep':'Hoffman Rep';
                if(accTeamMap.get(rfq.Account__c).containsKey(salesTeam)){
                    if( ((accTeamMap.get(rfq.Account__c)).get(salesTeam)).size() > 1){
                        rfq.Proposal_Owner_Status__c = 'Assign Maually';
                        rfq.Proposal_Owner__c=NULL;
                    }
                    else{
                        if(((accTeamMap.get(rfq.Account__c)).get(salesTeam)).size() == 1)
                        rfq.Proposal_Owner__c = ((accTeamMap.get(rfq.Account__c)).get(salesTeam)[0]).UserId;
                    }
                }
            }
        }     
         
        
    }
// A-0215 Tredence -- Added these changes to Update the RFQ Total if The ERP number is Changed -- Code Changes start here.   
    if(Trigger.isUpdate && Trigger.isBefore)
    {
        set<id> RFQWithModifiedERPNo = new set<id>();
        Map<id,RFQ__c> oldRFQRecord = Trigger.oldmap;
        for(RFQ__c newRfqrec: Trigger.new)
        {
            RFQ__c oldRfqrec = oldRFQRecord.get(newRfqrec.id);
            if(oldRfqrec.ERP_Number__c != newRfqrec.ERP_Number__c)
            {
                //RFQWithModifiedERPNo.add(newRfqrec.Id);
                newRfqrec.Value__c = 0.00;
                //newRfqrec.CurrencyIsoCode = '';
            }
        }
        system.debug('----The Final Total Amount is---- '+Trigger.new[0].value__c );
    }
// A-0215 Tredence -- Added these changes to Update the RFQ Total if The ERP number is Changed -- Code Changes Ends here.
}