// This Trigger will run on lead records and if the field Catalog User contains a validate email address it will find a match and update the owner record
trigger iCatalogUserCheck on Lead (before insert) {
    
        // disable triggers centrally via Custom Settings - All Triggers should have this
    EnvironmentConfig__c envConfig = EnvironmentConfig__c.getInstance();
    if(envConfig.Disable_Automated_Processes__c == true) {
        System.debug('Config Setting has been set to disable all triggers & flows');
        return;
    }
	// else continue with trigger logic below - reach out to Uday with questions
    
    if(Trigger.isBefore) {
        if (Trigger.isInsert) {    
            for (Lead leadObject: Trigger.new)
            {
                Set<String> leadCatalogEmailUser = new Set<String>();
                //Get the Catalog User record
                leadCatalogEmailUser.add(leadObject.Catalog_User__c);		

                //Checks to make sure the record contains data
                if (leadObject.Catalog_User__c != NULL)
                {         
                    //Update Owner
                    try
                    {
                        //Searchs for the user record to make sure it finds a match
                        User[] userObjects = [
                            select Id from User 
                            where Email IN :leadCatalogEmailUser 
                            and isActive = true 
                            and usertype= 'standard'
                        ];
                        
                        //If it finds a match update the OwnerId
                        if (userObjects.size() > 0)
                            leadObject.OwnerId = userObjects[0].Id;   
                    }
                    catch (DmlException dm)
                    {
                        
                    }
                   
                }
            }
        }
    }
}