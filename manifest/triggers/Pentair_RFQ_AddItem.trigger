//**************************************************************************************************************************//
//                                  nVent Electrical/Electronic Manufacturing                                               //
//  Class Name: Pentair_RFQ_AddItem                                                                                         //                              
//  Purpose of the Trigger: This is a trigger on RFQ with after update and after insert                                     //
//  Created By: Slalom                                                                                                      //      
//  Created Date: 4/4/2018                                                                                                  //
//--------------------------------------------------------------------------------------------------------------------------//
//  ||  Version ||  Date of Change  ||  Editor      ||  Enh Req No  ||  Change Purpose                                      //
//  ||  1.0     ||  4/4/2018        ||  Slalom      ||              ||  Initial Creation                                    //
//  ||  2.0     ||  11/20/2018      ||  Tredence    ||  A-0215      ||  Update RFQ Total and the Lineitems change of ERP Num//
//**************************************************************************************************************************//
trigger Pentair_RFQ_AddItem on RFQ__c (after insert, after update) {
if (Pentair_RFQController.runOnceRFQTrigger())
        {     
    System.debug('RFQ Line Item Trigger');
    if (Trigger.isInsert) {
        String erpNumber;
        String rfqId;
        String salesTeam = '';
        for (RFQ__c quoteObject: Trigger.new)
        {
            erpNumber = quoteObject.ERP_Number__c;
            rfqId = quoteObject.Id;
            salesTeam = quoteObject.Sales_Team__c;
        }     
        
        System.debug('ERP Number: ' + erpNumber);
            if (erpNumber != null)
            {
                Boolean isSchroffHoffman = false;
                if (salesTeam != null)
                {
                    if ((salesTeam.contains('Schroff') || salesTeam.contains('Hoffman')))
                        isSchroffHoffman = true;
                }
                
                if (erpNumber.Length() > 6 && isSchroffHoffman)
                {
                    System.debug('Found ERP  Number After Insert');
                    Pentair_RFQXMLParse.RFQXMLLines(erpNumber, 'Quote', rfqId);
                }
            }
    }
    
// A-0215 - Tredence -- Added these changes to delete the line items if The ERP number is Changed -- Code Changes start here.
    if(Trigger.isUpdate && Trigger.isafter) 
    { 
        system.debug('------- After Update Trigger to Delete -------');
        if(Pentair_UtilityClass.AllowToDeleteLineitemsonChangeofERPNum)
        {
            set<id> RFQWithModifiedERPNo = new set<id>();
            Map<id,RFQ__c> oldRFQRecord = Trigger.oldmap;
            for(RFQ__c newRfqrec: Trigger.new)
            {
                RFQ__c oldRfqrec = oldRFQRecord.get(newRfqrec.id);
                if(oldRfqrec.ERP_Number__c != newRfqrec.ERP_Number__c)
                {
                    RFQWithModifiedERPNo.add(newRfqrec.Id);
                }
            }
            if(RFQWithModifiedERPNo.size()>0)
            {
                List<RFQ_Line_Item__c> lineItemsToDelete = [SELECT ID FROM RFQ_Line_Item__c WHERE RFQ__c IN :RFQWithModifiedERPNo];
                Pentair_UtilityClass.AllowToDeleteLineitemsonChangeofERPNum = False;
                delete lineItemsToDelete;
            }
        }
// A-0215 - Tredence -- Added these changes to delete the line items if The ERP number is Changed -- Code Changes Ends here.
 

        for (RFQ__c quoteObject: Trigger.new)
        {
            RFQ__c oldObject = Trigger.oldMap.get(quoteObject.ID);
            Id recordTypeId = [select Id from RecordType where DeveloperName = 'RFQ' AND sobjecttype = 'RFQ__C' limit 1].Id;
            System.debug('ERP Number: ' + quoteObject.ERP_Number__c);
            if (quoteObject.ERP_Number__c != null)
            {
                System.debug('Found ERP Number After Update');
                if (quoteObject.ERP_Number__c != oldObject.ERP_Number__c)
                {
                    if (quoteObject.ERP_Number__c.Length() > 6 && quoteObject.recordTypeId == recordTypeId)
                    {
                        System.debug('Found XML File');
                        String xErpNumber = '';
                        try
                        {
                            xErpNumber = String.valueOf(Integer.valueOf(quoteObject.ERP_Number__c.replaceFirst('^0+','')));
                        }
                        catch (Exception ex)
                        {
                            xErpNumber = String.valueOf(quoteObject.ERP_Number__c.replaceFirst('^0+',''));
                        }
                        Pentair_RFQXMLParse.RFQXMLLines(xErpNumber, 'Quote', quoteObject.Id);
                    }
                }
                
                if (quoteObject.Value__C != null)
                {
                    if (quoteObject.recordTypeId == recordTypeId)
                    {
                        Boolean isNotValidate = false;
                        System.debug('Final Total Value: ' + quoteObject.Value__C );
                        System.debug('Final Rollup Value: ' + quoteObject.Line_Item_Rollup__c.setScale(2, RoundingMode.HALF_UP));
                        if (quoteObject.Value__C.setScale(2, RoundingMode.HALF_UP) != quoteObject.Line_Item_Rollup__c.setScale(2, RoundingMode.HALF_UP))
                        {
                            Decimal checkRange = quoteObject.Line_Item_Rollup__c.setScale(2, RoundingMode.HALF_UP) -
                                quoteObject.Value__C.setScale(2, RoundingMode.HALF_UP); 
                        }
                        
                        if (quoteObject.Line_Item_Rollup__c > 0)
                        {                        
                            RFQ__C updateRFQTotal = new RFQ__C(
                                ID=quoteObject.Id,
                                Value__C = quoteObject.Line_Item_Rollup__c,
                                Is_Not_Validate__C = isNotValidate
                            );                     
                            update updateRFQTotal;
                        }
                    }
                }
            }
            
            if (oldObject.Status__c != quoteObject.Status__c)
            {
                RFQ__c aux  = new RFQ__c(Id = quoteObject.Id); 
                aux.Phase__c = 'New';
                update aux;
            }
        }
        }
    }
}