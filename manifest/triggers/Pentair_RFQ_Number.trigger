trigger Pentair_RFQ_Number on RFQ__c (after insert) {
    
    System.debug('RFQ Proposal Number Trigger');
    
    String quoteId;
    String quoteNumber;
    String proposalNumber;
    String opportunityId = '';
    Decimal revisionNumber = 0;
    
    for (RFQ__c quoteObject: Trigger.new)
    {
        quoteId = quoteObject.Id;
        quoteNumber = quoteObject.RFQ_Auto_Number__c;
        proposalNumber = quoteObject.Proposal_Number__c;
        revisionNumber = quoteObject.Revision_Number__c;
    }
    
    if (proposalNumber == null)
    {
        RFQ__c aux  = new RFQ__c(Id = quoteId); 
        aux.Proposal_Number__c = Pentair_RFQController.SetProposalNumer(quoteNumber, quoteId);
        update aux;
    }
    else
    {
        //Clone Record
        System.debug('Is Cloning');
        RFQ__c aux2  = new RFQ__c(Id = quoteId); 
        aux2.Revision_Number__c = revisionNumber + 1;
        aux2.RFQ_Received__c = Date.valueOf(date.today());
        aux2.Requested_By__c = Date.valueOf(date.today());
        update aux2;
    }
}