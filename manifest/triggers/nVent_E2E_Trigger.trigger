trigger nVent_E2E_Trigger on End_to_End__c (before delete, before insert, before update, 
                                    after delete, after insert, after update) {
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
            nVent_E2E_TriggerHandler.afterinsertUtil(Trigger.new);
        if(Trigger.isUpdate)
            nVent_E2E_TriggerHandler.afterUpdateUtil(Trigger.new,Trigger.Oldmap);
        if(Trigger.isDelete)
            nVent_E2E_TriggerHandler.afterDeleteUtil(Trigger.Old);
    }
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
            nVent_E2E_TriggerHandler.beforeinsertUtil(Trigger.new);
        if(Trigger.isUpdate)
            nVent_E2E_TriggerHandler.beforeUpdateUtil(Trigger.new,Trigger.Oldmap);
    }

}