/* ********************************************
Developer - Apurva Prasade
Trigger - nVent_Opportunity_Trigger / Opportunity
Description - Opportunity combined trigger

Developer   Date        Version
Apurva P    15-07-2019  V1.0

********************************************** */

trigger nVent_Opportunity_Trigger on Opportunity (before delete, before insert, before update, 
                                    after delete, after insert, after update) {

    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
            nVent_OpportunityTriggerHandler.afterinsertUtil(Trigger.new);
        if(Trigger.isUpdate)
            nVent_OpportunityTriggerHandler.afterUpdateUtil(Trigger.new,Trigger.Oldmap);
        if(Trigger.isDelete)
            nVent_OpportunityTriggerHandler.afterDeleteUtil(Trigger.Old);
    }
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
            nVent_OpportunityTriggerHandler.beforeinsertUtil(Trigger.new);
        if(Trigger.isUpdate)
            nVent_OpportunityTriggerHandler.beforeUpdateUtil(Trigger.new,Trigger.Oldmap);
    }

}