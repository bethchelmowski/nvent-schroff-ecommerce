public class x7S_TestUtility {
    
    public static Account createAccount(Boolean isInsert){
        
        Account account = new Account();
        account.name = 'Test Portal Account';
        if(isInsert)
            insert account;
        return account;
    }

    /*
    * Method Name: createContact
    * Description: Creating contact record.
    * @param: Boolean isInsert, Integer num , Id accountId
    * @return: Contact
    */
    public static Contact createContact(Boolean isInsert, Integer num , Id accountId){
        Contact contact = new Contact();
        contact.lastname = 'TestLast'+num;
        contact.AccountId = accountId;
        
        if(isInsert)
            insert contact;
        return contact;
    }

    /** 
    * Method Name: createUser
    * Description: Creating user record.
    * @param: Boolean isInsert,Integer num , Id profileId, Id contactId
    * @return: User
    */
    public static User createUser(Boolean isInsert, Integer num , Id profileId, Id contactId){
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        User user = new User();
        user.Email='james.dsouza'+randomInt+'@gmail.com';
        user.ProfileId = profileId; 
        user.Username='james.dsouza'+randomInt+'@gmail.com.test'; 
        user.Alias = 'JDS';
        user.TimeZoneSidKey ='America/New_York';
        user.EmailEncodingKey='ISO-8859-1';
        user.LocaleSidKey='en_US';
        user.LanguageLocaleKey='en_US';
        user.ContactId = contactId;
        user.PortalRole = 'Manager';
        user.FirstName = 'James';
        user.LastName = 'Dsouza'+num;
        user.IsActive = true;
        
        if(isInsert)
            insert user;
        return user;
    }

    /** 
    * Method Name: createCommunityUser
    * Description: Creating Community user record 
    * @return: User
    */
    public static User createCommunityUser(){
        Profile communityUserProfile = [SELECT Id FROM Profile WHERE Name='nVent Customer Community Plus Login User Profile' LIMIT 1];
        Map<String,Object> retData =
            ccrz.ccApiTestData.setupData(new Map<String,Map<String,Object>>{
                ccrz.ccApiTestData.ACCOUNT_DATA => new Map<String,Object>{
                    ccrz.ccApiTestData.ACCOUNT_LIST => new List<Map<String,Object>>{
                        new Map<String,Object>{
                            'name' => 'testAccount1',
                                'ccrz__dataId__c' => 'testAccount1'
                                }
                    }
                },
                    ccrz.ccApiTestData.CONTACT_DATA => new Map<String,Object>{
                        ccrz.ccApiTestData.CONTACT_LIST => new List<Map<String,Object>>{
                            new Map<String,Object>{
                                'ccrz__dataId__c' => 'testContact1',
                                    'account' => new Account(ccrz__dataId__c = 'testAccount1'),
                                    'email' => 'testcontact1.ccrz@cloudcraze.com',
                                    'lastName' => 'Contact1',
                                    'firstName' => 'test'
                                    }
                        }
                    },
                        ccrz.ccApiTestData.USER_DATA => new Map<String,Object>{
                            ccrz.ccApiTestData.USER_LIST => new List<Map<String,Object>>{
                                new Map<String,Object>{
                                    'ccrz__dataId__c' => 'testUser1',
                                        'alias' => 'defusr1',
                                        'email' => 'test.ccrz1@cloudcraze.com',
                                        'lastName' => 'User1',
                                        'firstName' => 'Test1',
                                        'languageLocaleKey' => 'fr',
                                        'localeSIDKey' => 'fr_FR',
                                        'emailEncodingKey' => 'UTF-8',
                                        'profileId' => communityUserProfile.Id,
                                        'username' => System.currentTimeMillis() + 'test1@cloudcraze.com',
                                        'ccrz__CC_CurrencyCode__c' => 'EUR',
                                        'contact' => new Contact(ccrz__dataId__c = 'testContact1'),
                                        'timezoneSIDKey' => 'GMT'
                                        }
                            }
                        }
            });
        Map<String,Object> theData = (Map<String,Object>)retData.get(ccrz.ccApiTestData.USER_DATA);
        List<sObject> theList = (List<sObject>)theData.get(ccrz.ccApiTestData.USER_LIST); 
        User theUser = (User)theList.get(0);
        
        return theUser;
    }
    /**
    * Method Name: createCart
    * Description: Creating cc cart record.
    * @param: Boolean isInsert 
    * @return: ccrz__E_Cart__c 
    */    
    public static ccrz__E_Cart__c createCart(Boolean isInsert ){
        ccrz__E_Cart__c cart = new ccrz__E_Cart__c();
        cart.ccrz__CartStatus__c = 'Open';
        cart.ccrz__CartType__c = 'Cart';
        cart.ccrz__User__c = UserInfo.getUserId();
        
        if(isInsert){
            insert cart;
        }
        return cart;
    }

    public static List<ccrz__E_CartItem__c> createCartItem(Integer noOfRecords , Id cartId, Boolean isInsert ){
        List<ccrz__E_CartItem__c> cartItemList = new List<ccrz__E_CartItem__c>();
        
        for(Integer i = 1; i <= noOfRecords; i++){
            ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c();
            cartItem.ccrz__Cart__c = cartId;
            cartItem.ccrz__Quantity__c = 1.000;
            cartItem.ccrz__Price__c = 100.000;
            cartItem.ccrz__ItemStatus__c = 'Available';
            cartItemList.add(cartItem);    
        }
        if(isInsert){
            insert cartItemList;
        }
        return cartItemList;
    } 
    
    /**
    * Method Name: createCart
    * Description: Creating cc cart record.
    * @param: Boolean isInsert 
    * @return: ccrz__E_Cart__c 
    */    
    public static ccrz__E_Cart__c createWishlist(Boolean isInsert ){
        ccrz__E_Cart__c wishlist = new ccrz__E_Cart__c();
        wishlist.ccrz__CartStatus__c = 'Open';
        wishlist.ccrz__CartType__c = 'WishList';
        wishlist.ccrz__User__c = UserInfo.getUserId();
        
        if(isInsert){
            insert wishlist;
        }
        return wishlist;
    }

    /*
    * Method Name: createProduct
    * Description: Creating cc product record.
    * @param: Boolean isInsert,String prodName,String prodId, String prodSku 
    * @return: ccrz__E_Product__c
    */
    public static ccrz__E_Product__c createProduct(Boolean isInsert,String prodName,String prodId, String prodSku){
        ccrz__E_Product__c prod = new ccrz__E_Product__c();
        prod.Name=prodName;
        prod.ccrz__ProductId__c =prodId;
        prod.ccrz__SKU__c=prodSku;
        prod.ccrz__Storefront__c='DefaultStore';
        prod.ccrz__ProductStatus__c ='Released';
        prod.ccrz__LongDescRT__c='dummy product with dummy description';
        prod.ccrz__ShortDescRT__c='dummy product with dummy description';
        
        if(isInsert){
            insert prod;
        }
        return prod;
    }

    /*
    * Method Name: createProductMedia
    * Description: Creating cc productmedia record.
    * @param: Boolean isInsert , ccrz__E_Product__c prod 
    * @return: ccrz__E_ProductMedia__c
    */
    public static ccrz__E_ProductMedia__c createProductMedia(Boolean isInsert , Id prodId){
        ccrz__E_ProductMedia__c prodMedia=new ccrz__E_ProductMedia__c();
        prodMedia.ccrz__FilePath__c='BC-COFMAC.jpeg';
        prodMedia.ccrz__StaticResourceName__c='CC_Capricorn_Assets_2';
        prodMedia.ccrz__MediaType__c = 'Product Search Image';
        prodMedia.ccrz__ProductMediaSource__c = 'Static Resource';
        prodMedia.ccrz__Enabled__c = true;
        prodMedia.ccrz__Product__c=prodId;
        if(isInsert){
            insert prodMedia;
        }
        return prodMedia;
    }
}