@isTest
private class Pentair_Test_LeadRFQConvertPage {

    static testMethod void myUnitTest() {
        Account newAccount = new Account(
            name = 'Test Account',
            ShippingCountryCode = 'US',
            BillingCity = 'Example',
            BillingStreet = '1111 Example',
            BillingCountryCode = 'US'
        );
        
        Insert newAccount;
        
        Lead newLead = new Lead(
                        Company = 'Test Account', LastName= 'Test Lead',
                        LeadSource = 'Web', Sales_Team__c = 'Hoffman',
                        Status = 'Converted');
        
        Insert newLead;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(newLead);
        Pentair_RFQConvertController leadController = new Pentair_RFQConvertController(stdController);
        
        leadcontroller.leadToConvert = newLead;
        
        leadController.getMyComponentController();
        leadController.getmyDescriptionComponentController();
        leadController.getmyTaskComponentController();
        leadController.getThis();
        
        PageControllerBase pgBase = new PageControllerBase();
        pgBase.getMyComponentController();
        pgBase.getmyDescriptionComponentController();
        pgBase.getmyTaskComponentController();
        pgBase.getThis();
        pgBase.getmyReminderComponentController();
        
        ComponentControllerBase compBase = new ComponentControllerBase();
        compBase.pageController = pgBase;
        compBase.pageControllerDescription = pgBase;
        compBase.pageControllerReminder = pgBase;
        compBase.pageControllerTask = pgBase;
        
        
        leadController.setComponentController(new Pentair_RFQConvertCoreController());
        
        leadController.myComponentController.selectedAccount = newAccount.Id;
        leadController.myComponentController.leadConvert = newLead;
        
        
        Contact contactID = leadController.myComponentController.contactID;
        leadController.myComponentController.doNotCreateOppty = true;
        List<SelectOption> leadStatuses = leadController.myComponentController.LeadStatusOption;
        
        RFQ__C rfqID = leadController.myComponentController.rfqID;
        leadController.myComponentController.sendOwnerEmail = true;
                
        leadController.convertLead();   
        leadController.PrintErrors(new List<Database.Error>());
        leadController.PrintError('Test');
        
        //see if the new account was created
        Account [] checkAccount = [SELECT Id FROM Account WHERE Name ='Test Account' ];
        system.debug(checkAccount);
        system.assertEquals(1, checkAccount.size(), 'There was a problem converting lead to an account');
        
        //see if the new account was created
        Contact [] checkContact = [SELECT Id FROM Contact WHERE Name ='Test Lead' ];
        system.debug(checkContact);
        system.assertEquals(1, checkContact.size(), 'There was a problem converting lead to a contact');
        
        //
        leadController.myComponentController.accountChanged();
        
        leadController.myComponentController.selectedAccount = 'NEW';
        
        leadController.myComponentController.accountChanged();
        
        // test the reminder time as a French user to test the 24 hour clock
        Profile p = [select id from profile where name='Standard User'];

         User u = new User(alias = 'standt', email='standarduser@testorg.com',

            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='fr',

            localesidkey='fr', profileid = p.Id,

            timezonesidkey='America/Los_Angeles', 
            username='testUser@testleadconvert.com');
                 
         leadController.myComponentController.selectedAccount = 'NONE';
         
         //test the situation where there is a due date but no subject
         leadController.myComponentController.leadConvert.Status = 'NONE';
         
         //convert the lead
         leadController.convertLead();
         
         leadController.myComponentController.accountLookedUp();       
        
    } 
    
    static testMethod void UnitTestRFQController() {
        Account newAccount = new Account(
            name = 'Test Account',
            ShippingCountryCode = 'US',
            BillingCity = 'Example',
            BillingStreet = '1111 Example',
            BillingCountryCode = 'US'
        );
        
        Insert newAccount;
        
        User singleUser = [SELECT ID FROM USER LIMIT 1];
        
        //Sample Test Account Record
        Contact newContact = new Contact(
            firstname = 'Test First',
            lastname = 'Test Last Name',
            AccountId = newAccount.Id,
            OwnerId = singleUser.Id
        );
        Insert newContact;               
        
        Pentair_RFQConvertCoreController controller = new Pentair_RFQConvertCoreController();
        
        controller.selectedAccount = newAccount.Id;
        controller.contactID = newContact;
        
        controller.ValidateOwner(singleUser.Id);        
        controller.accountChanged();
        //controller.accountLookedUp();

    }
}