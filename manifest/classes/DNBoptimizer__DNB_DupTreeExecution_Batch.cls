/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DNB_DupTreeExecution_Batch implements Database.Batchable<SObject> {
    global DNB_DupTreeExecution_Batch(Set<String> DDunsIds, Map<String,String> dunsRecordID, Map<String,String> dunsParentDuns) {

    }
    global void execute(Database.BatchableContext BC, List<Account> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
