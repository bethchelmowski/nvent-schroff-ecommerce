/**
 * Created By ankitSoni on 06/01/2020
 */
@IsTest
public class x7S_DataTableControllerTest {
    
    @isTest
    static void getWishlistItems(){
        User testUser = x7S_TestUtility.createCommunityUser();
        Account account = [SELECT Id , Name FROM Account WHERE Name ='testAccount1'];
        Contact contact = [SELECT Id , Name FROM Contact WHERE Name ='test Contact1'];
        
        System.runAs(testUser){

            ccrz__E_Product__c product = x7S_TestUtility.createProduct(true, 'dummy', 'dummy1', 'dummy1');
            ccrz__E_ProductMedia__c productMedia = x7S_TestUtility.createProductMedia(true, product.Id);

            ccrz__E_Cart__c wishlist = x7S_TestUtility.createWishlist(false);
            wishlist.ccrz__Storefront__c = 'DefaultStore';
            wishlist.ccrz__Account__c = account.Id;
            wishlist.ccrz__Contact__c = contact.Id;
            insert wishlist;
            
            List<ccrz__E_CartItem__c> wishlistItem = x7S_TestUtility.createCartItem(1, wishlist.Id, false);
            wishlistItem[0].ccrz__Product__c = product.Id;
            insert wishlistItem;

            x7S_WishListModel returnValue = x7S_DataTableController.getWishlistItems();
            System.assertNotEquals(null, returnValue);
            System.assertNotEquals(null, returnValue.items);
        }
    }
}