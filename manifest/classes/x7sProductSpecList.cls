/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/2/19.
 */

public with sharing class x7sProductSpecList implements Comparable
{
	@AuraEnabled
	public String groupName;

	@AuraEnabled
	public List<x7sProductSpec> items;

	public x7sProductSpecList()
	{
		this.groupName  = '';
		this.items      = new List<x7sProductSpec>();
	}

	public x7sProductSpecList(String groupName)
	{
		this();
		this.groupName = groupName;
	}

	public Integer compareTo(Object param1)
	{
		return this.groupName.compareTo((String)((x7sProductSpecList)param1).groupName);
	}

	public void sortSpecs()
	{
		items.sort();
	}

	public x7sProductSpec addSpec(x7sProductSpec newSpec)
	{
		System.debug('addSpec');

		x7sProductSpec spec = containsSpec(newSpec.name);

		if (spec == null)
		{
			System.debug(' spec not found - inserting');
			items.add(newSpec);
			spec = newSpec;
		}
		else
		{
			System.debug('  spec found - checking value');
			if (!spec.values.contains(newSpec.values[0]))
			{
				System.debug('    value not found, inserting');
				spec.values.add(newSpec.values[0]);
			}
		}

		return spec;
	}

	// check for duplicates
	@TestVisible
	private x7sProductSpec containsSpec(String name)
	{
		System.debug('   containsSpec');
		x7sProductSpec found = null;

		for (x7sProductSpec spec : items)
		{
			if (spec.name == name)
			{
				found = spec;
				break;
			}
		}
		return found;
	}

	public void Dump(String title)
	{
		Integer counter = 0;

		System.debug('x7sProductSpecList: '+ title);
		System.debug('Group Name: ' + groupName);
		for(x7sProductSpec spec : items)
		{
			spec.Dump(String.valueOf(counter++));
		}
	}
}