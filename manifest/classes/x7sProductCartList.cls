/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

public with sharing class x7sProductCartList
{
	@AuraEnabled
	public Boolean status;

	@AuraEnabled
	public String cartEncId, message;

	@AuraEnabled
	public Decimal totalQuantity, totalPrice, itemCount;

	@AuraEnabled
	public List<x7sProductCartModel> items;

	public x7sProductCartList()
	{
		cartEncId = '';
		status = false;
		items  = new List<x7sProductCartModel>();
	}

	public x7sProductCartModel findCartItem(String productId)
	{
		for (x7sProductCartModel item : items)
		{
			if (item.productId == productId)
			{
				return item;
			}
		}

		return null;
	}

	public void dump(String title, Boolean depth)
	{
		System.debug('Cart: ' + title);
		System.debug('Cart id       : ' + cartEncId);
		System.debug('Item count    : ' + itemCount);
		System.debug('total qty     : ' + totalQuantity);
		System.debug('total price   : ' + totalPrice);

		if (depth) {
			Integer pos = 0;
			for (x7sProductCartModel item : items)
			{
				item.dump(String.valueOf(++pos));
			}
		}
	}
}