public class Pentair_ERICO_OrderStatus {   
    public String cid {get; set;}
    public String sourcesystem {get; set;}
    public String id {get; set;}
    public String oid {get; set;}
    public String clid {get; set;}
    
   	public String orderKey = '';
    
    public class OrderStatusHeader{
        public String custNum {get;set;}
        public String orderNumber {get;set;}
        public String orderDate {get;set;}
        public String purchaseOrder {get;set;}
        public String status {get;set;}
        public String holdCode {get;set;}
    }
    
    public class OrderDetail{
        public String orderNumber {get;set;}
        public String orderDate {get;set;}
        public String customerNumber {get;set;}
        public String purchaseOrder {get;set;}
        public String status {get;set;}
        public String companyName {get;set;}
        public String addr1 {get;set;}
        public String addr2 {get;set;}
        public String addr3 {get;set;}
        public String city {get;set;}
        public String state {get;set;}
        public String postalCode {get;set;}
        public String hasShipments {get;set;}
        public String holdCode {get;set;}
    }
    
    public class OrderDetailLines{
        public String lineNumber {get;set;}
        public String partNumber {get;set;}
        public String quantityOrdered {get;set;}
        public String quantityShipped {get;set;}
        public String quantityReserved {get;set;}
        public String status {get;set;}
        public String promisedShipDate {get;set;}
    }
    
    Pentair_ERICOWebService webServiceController = new Pentair_ERICOWebService();
    
    public list<OrderStatusHeader> lstOrderStatusHeader {set; get;}
    public list<OrderDetail> lstOrderDetail {set; get;}
    public list<OrderDetailLines> lstOrderDetailLines {set; get;}
    
    public void getOrderDetail()
    {
        this.oid = ApexPages.CurrentPage().getParameters().get('oid').replace(' ', '');
        this.cid = ApexPages.CurrentPage().getParameters().get('cid');
        this.sourcesystem = ApexPages.CurrentPage().getParameters().get('ssid');
        this.id = ApexPages.CurrentPage().getParameters().get('id');
        
        /*System.debug('oid: ' + oid);
        System.debug('cid: ' + cid);
        System.debug('sourcesystem: ' + sourcesystem);
        System.debug('id: ' + id);*/
        
        lstOrderDetail = new list<OrderDetail>();
        lstOrderDetailLines = new list<OrderDetailLines>();
        
        List<String> lstMACPAC;
        if (cid.contains(';'))
        {
        	lstMACPAC = cid.split(';');
            System.debug('lstMACPAC Fields: ' + lstMACPAC);
        }
        
        List<String> lstRecords;
        lstRecords = lstMACPAC[0].split('\\.');
        
        /*System.debug('lstMACPAC Fields [0]: ' + lstRecords[0]);
        System.debug('lstMACPAC Fields [1]: ' + lstRecords[1]);
        */
        XmlDom.Element xmlClaimHeader = webServiceController.getOrder(oid, lstRecords[1]);
        
        if (xmlClaimHeader != null)
        {
            OrderDetail cHeader = new OrderDetail();
            
            //System.debug('xx OrderDetail: ' + xmlClaimHeader.getValue('OrderNumber'));
            
        	cHeader.orderNumber = xmlClaimHeader.getValue('OrderNumber');
            cHeader.orderDate = xmlClaimHeader.getValue('OrderDate');
            cHeader.customerNumber = xmlClaimHeader.getValue('CustomerNumber');
            cHeader.purchaseOrder = xmlClaimHeader.getValue('PurchaseOrder');
            cHeader.status = xmlClaimHeader.getValue('HeaderStatus');
            cHeader.companyName = xmlClaimHeader.getValue('CompanyName');
            if (xmlClaimHeader.getValue('Address1') != null)
            	cHeader.addr1 = xmlClaimHeader.getValue('Address1');
            else
                cHeader.addr1 = '';
            if (xmlClaimHeader.getValue('Address2') != null)
            	cHeader.addr2 = xmlClaimHeader.getValue('Address2');
            else
                cHeader.addr2 = '';
            if (xmlClaimHeader.getValue('Address3') != null)
            	cHeader.addr3 = xmlClaimHeader.getValue('Address3');
            else
                cHeader.addr3 = '';
            cHeader.city = xmlClaimHeader.getValue('City');
            cHeader.state = xmlClaimHeader.getValue('StateProvince');
            cHeader.postalCode = xmlClaimHeader.getValue('PostalCode');
            cHeader.hasShipments = xmlClaimHeader.getValue('HasShipments');
            cHeader.holdCode = xmlClaimHeader.getValue('HoldCode');
            
            lstOrderDetail.add(cHeader);
            
            XmlDom.Element[] lines = xmlClaimHeader.getElementsByTagName('Line');
            if (lines != null) {
                for (XmlDom.Element lineElement : lines) {
                    OrderDetailLines xLine = new OrderDetailLines();
                    
                    xLine.lineNumber = lineElement.getValue('LineNumber');
                    xLine.partNumber = lineElement.getValue('PartNumber');
                    xLine.quantityOrdered = lineElement.getValue('QuantityOrdered');
                    xLine.quantityReserved = lineElement.getValue('QuantityReserved');
                    xLine.quantityShipped = lineElement.getValue('QuantityShipped');
                    xLine.status = lineElement.getValue('LineStatus');
                    xLine.promisedShipDate = lineElement.getValue('PromisedShipDate');
                    
                    lstOrderDetailLines.add(xLine);
                }
            }
        }
    }
    
    public void getOrderStatusHeader()
    {        
        lstOrderStatusHeader = new list<OrderStatusHeader>();
        
        this.cid = ApexPages.CurrentPage().getParameters().get('cid');
        this.sourcesystem = ApexPages.CurrentPage().getParameters().get('ssid');
        this.id = ApexPages.CurrentPage().getParameters().get('id');
        System.debug('cid Fields: ' + cid);
        
        String [] lstMACPAC = new List<String>();
        if (cid.contains(';'))
        {
        	lstMACPAC = cid.split(';');
            System.debug('lstMACPAC Fields: ' + lstMACPAC);
        }
        else
        {
            lstMACPAC.add(cid);
        }
        
        for (String singleId : lstMACPAC) 
        {        
            List<String> lstRecords;
            lstRecords = singleId.split('\\.');
            
            //System.debug('Record 0: ' + singleId);
            
            sourcesystem = lstRecords[1];            
            XmlDom.Element xmlClaimHeader = webServiceController.getOrderHeaders(lstRecords[0], sourcesystem, null, null, null);

            if (xmlClaimHeader != null)
            {
                XmlDom.Element[] elements = xmlClaimHeader.getElementsByTagName('Header');
                if (elements != null) {
                    for (XmlDom.Element element : elements)
                    {
                        OrderStatusHeader cHeader = new OrderStatusHeader();
                        
                        //System.debug('xx OrderNumber: ' + element.getValue('OrderNumber'));
                        cHeader.custNum = lstRecords[0];
                        cHeader.orderNumber = element.getValue('OrderNumber');
                        cHeader.orderDate = element.getValue('OrderDate');
                        cHeader.purchaseOrder = element.getValue('PurchaseOrder');
                        cHeader.holdCode = element.getValue('HoldCode');
                        cHeader.status = element.getValue('Status');
                        
                        lstOrderStatusHeader.add(cHeader);
                    }
                }
            }
        }
        System.debug('lstOrderStatusHeader '+lstOrderStatusHeader);
    }
    
    public String getSfIdQueryParam() {
        String sfId = ApexPages.currentPage().getParameters().get('id');
        return sfId == null ? '' : '&id=' + sfId;
    }
    
    public PageReference cancel()
 	{       
        /*System.debug('CID: ' + ApexPages.CurrentPage().getParameters().get('cid'));
        System.debug('id: ' + ApexPages.CurrentPage().getParameters().get('id'));
        System.debug('ssid: ' + ApexPages.CurrentPage().getParameters().get('ssid'));
        */
        PageReference redirectPage = Page.Pentair_ERICOMain;
        redirectPage.getParameters().put('cid',ApexPages.CurrentPage().getParameters().get('cid'));
        redirectPage.getParameters().put('id',ApexPages.CurrentPage().getParameters().get('id'));
        redirectPage.getParameters().put('ssid',ApexPages.CurrentPage().getParameters().get('ssid'));
    	redirectPage.setRedirect(true);
    	return redirectPage;
    }
}