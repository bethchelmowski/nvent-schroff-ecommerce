/* ********************************************
Developer -     Apurva Prasade
Class -         nVent_E2ETriggerTest 
Description -   Test class to cover E2E trigger

Developer   Date        Version
Apurva P    23-09-2019  V1.0

********************************************** */
@isTest
public class nVent_E2ETriggerTest 
{
    @TestSetup
    static void createData()
    {
        Profile prflst = [Select Id from Profile where Name =: 'System Administrator' limit 1];
        
        User objUser = new User();
        objUser.FirstName = 'fNameTest';
        objUser.LastName = 'lNameTest';
        objUser.Email = 'aptest@test.com';
        objUser.isActive = true;
        objUser.ProfileId = prflst.Id;
        objUser.Username = 'aptest@test.com' + System.currentTimeMillis();
        objUser.CompanyName = 'testComp';
        objUser.Title = 'Developer';
        objUser.Alias = 'testal';
        objUser.TimeZoneSidKey = 'America/Los_Angeles';
        objUser.EmailEncodingKey = 'UTF-8';
        objUser.LanguageLocaleKey = 'en_US';
        objUser.LocaleSidKey = 'en_US';
        insert objUser;

        Account objAccount = new Account();
        objAccount.Name = 'testCompany';
        objAccount.Subvertical__c = 'Oil and Gas';
        objAccount.BillingCountry = 'United States';
        objAccount.BillingCity = 'Test';
        objAccount.BillingState = 'Utah';
        objAccount.BillingStreet = 'test';
        insert objAccount;

        Id rfqvRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        RFQ__c objRFQ = new RFQ__c();
        objRFQ.Account__c = objAccount.Id;
        objRFQ.RecordTypeId = rfqvRecordTypeId;
        objRFQ.Product_Type__c = 'Standard';
        objRFQ.ERP_Number__c = 'ABDC1234';
        objRFQ.RFQ_Received__c = Date.today();
        objRFQ.Sales_Team__c = 'Schroff';
        objRFQ.Type__c = 'Customized';
    insert objRFQ;

        End_to_End__c obj = new End_to_End__c();
        obj.Name = 'ObjE2E';
        obj.Request_Type__c = 'Calculations';
        obj.Scheduled_Completion__c = Date.today();
        obj.Product_Type__c = 'Standard';
        obj.RFQ__c = objRFQ.Id;
        insert obj;
        
        End_to_End__c obj1 = new End_to_End__c();
        obj1.Name = 'ObjE2E';
        obj1.Request_Type__c = 'Calculations';
        obj1.Scheduled_Completion__c = Date.today();
        obj1.Product_Type__c = 'DYS - Design your Standard';
        obj1.RFQ__c = objRFQ.Id;
        insert obj1;
        //obj.OwnerId = 
    }

    public static testMethod void testafterTrigger()
    {
        End_to_End__c obj = [Select Id, Name,Owner_Name__c,Product_Type__c from End_to_End__c where Product_Type__c = 'Standard' Limit 1];
        obj.Product_Type__c = 'DYS - Design your Standard';
        update obj;
        obj.Product_Type__c = 'Modified';
        update obj;
        
        User uObj = [Select Id from User where Email = 'aptest@test.com' Limit 1];
        obj.OwnerId = uObj.Id;
        update obj;
        System.debug('Owner_Name__c '+obj);
        //System.assertEquals(expected, obj.Owner_Name__c);
    }
}