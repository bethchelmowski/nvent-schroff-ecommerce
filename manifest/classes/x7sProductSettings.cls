/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/15/19.
 */

public with sharing class x7sProductSettings
{
	private final String DEFAULT_NAME       = 'Default';
	private final String SETTINGS_NAME      = 'x7sProductSettings__mdt';
	private final String ACTION_SET_NAME    = 'x7sProductActionSet__mdt';
	private final String ACTION_NAME        = 'x7sProductAction__mdt';
	private final String QUANTITY_DEFAULTS  = '1,2,3,4,5,6,7,8,9,10,12,15,20,30,50,100';

	private final Integer   MAX_ACTIONS     = 5;

	private final List<String> settingFieldList = new List<String>
	{
		'Id',
		'DeveloperName',
		'Label',
		'Show_Empty_Categories__c',
		'Hide_Add_To_Cart__c',
		'Hide_Images__c',
		'Hide_Unit_Price__c',
		'Navigate_to_VF_Page__c',
		'Store_Front__c',
		'Action_Set__c'
	};

	private final List<String> actionListFields = new List<String>
	{
		'Id',
		'DeveloperName',
		'Label',
		'Action_1__c',
		'Action_2__c',
		'Action_3__c',
		'Action_4__c',
		'Action_5__c',
		'Title__c',
		'iconName__c'
	};

	private final List<String> actionFields = new List<String>
	{
		'Id',
		'DeveloperName',
		'Label',
		'Action_Title__c',
		'Action_Type__c',
		'Action_Value__c',
		'New_Window__c'
	};

	@AuraEnabled
	public String storeFront;

	@AuraEnabled
	public Boolean showEmptyCategories, hideAddToCart, hideImages, hideUnitPrice, navigateToVfPage;

	@AuraEnabled
	public List<String> quantityList;

	@AuraEnabled
	public x7sProductActionSet actionSet;

	public x7sProductSettings()
	{
		showEmptyCategories = false;
		storeFront          = '';
		hideAddToCart       = hideImages = hideUnitPrice = navigateToVfPage = false;
		quantityList        = new List<String>();
		actionSet           = new x7sProductActionSet();
	}

	public x7sProductSettings(String name)
	{
		this();
		getProductSettings(name);
	}

	@TestVisible
	private void getProductSettings(String name)
	{
		if (String.isBlank(name))
		{
			name = DEFAULT_NAME;
		}

		String query = ' SELECT ' +  String.join(settingFieldList, ',') +  ' FROM ' + SETTINGS_NAME;
		query += ' WHERE DeveloperName = \'' + name + '\'';

		try
		{
			x7sProductSettings__mdt settings = (x7sProductSettings__mdt)Database.query(query);
			getSettings(settings);
		}
		catch (QueryException ex) { System.debug('x7sProduct Settings not found for ' + name);}
	}

	@TestVisible
	private void getSettings(x7sProductSettings__mdt setting)
	{
		this.hideAddToCart          = setting.Hide_Add_To_Cart__c;
		this.hideImages             = setting.Hide_Images__c;
		this.hideUnitPrice          = setting.Hide_Unit_Price__c;
		this.storeFront             = setting.Store_Front__c;
		this.navigateToVfPage       = setting.Navigate_to_VF_Page__c;
		this.showEmptyCategories    = setting.Show_Empty_Categories__c;
		this.quantityList           = new List<String>(QUANTITY_DEFAULTS.split(','));

		if (setting.Action_Set__c != null)
		{
			getActionSet(setting.Action_Set__c);
		}
	}

	@TestVisible
	private void getActionSet(String actionSetId)
	{
		System.debug('getActionSet');

		String query = ' SELECT ' + String.join(actionListFields, ',') + ' FROM ' + ACTION_SET_NAME;
		query += ' WHERE Id = \'' + actionSetId + '\' LIMIT 1';

		x7sProductActionSet__mdt actionSetRecord = (x7sProductActionSet__mdt) Database.query(query);
		setActionSet(actionSetRecord);

		for (Integer actionCount = 1; actionCount <= MAX_ACTIONS; ++actionCount)
		{
			String actionName = 'Action_' + actionCount + '__c';
			Id     actionId   = (Id) actionSetRecord.get(actionName);

			if (actionId != null)
			{
				String actionQuery = ' SELECT ' + String.join(actionFields, ',') + ' FROM ' + ACTION_NAME;
				actionQuery += ' WHERE Id =\'' + actionId + '\' LIMIT 1';

				x7sProductAction__mdt actionItem = (x7sProductAction__mdt)Database.query(actionQuery);

				if (actionItem != null)
				{
					this.actionSet.items.add(setActionModel(actionItem));
				}
			}
		}
	}

	@TestVisible
	private void setActionSet(x7sProductActionSet__mdt actionSetRecord)
	{
		actionSet.label     = actionSetRecord.Label;
		actionSet.title     = actionSetRecord.Title__c;
		actionSet.iconName  = actionSetRecord.IconName__c;
	}

	@TestVisible
	private static x7sProductActionModel setActionModel(x7sProductAction__mdt actionItem)
	{
		x7sProductActionModel model = new x7sProductActionModel();

		model.name          = actionItem.DeveloperName;
		model.label         = actionItem.Label;
		model.actionType    = actionItem.Action_Type__c;
		model.actionValue   = actionItem.Action_Value__c;
		model.title         = actionItem.Action_Title__c;
		model.newWindow     = actionItem.New_Window__c;

		return model;
	}

	public void Dump(String title)
	{
		System.debug('Settings for: ' + title);
		System.debug(' hide add to cart : ' + hideAddToCart);
		System.debug(' hide images      : ' + hideImages);
		System.debug(' hide unit price  : ' + hideUnitPrice);
		System.debug(' navigate to VF   : ' + navigateToVfPage);
		System.debug(' show empty cat   : ' + showEmptyCategories);
		System.debug(' store front      : ' + storeFront);
		System.debug(' quantity list    : ' + quantityList);

		if (actionSet.items.size() > 0)
		{
			actionSet.Dump('actionSet');
		}
	}
}