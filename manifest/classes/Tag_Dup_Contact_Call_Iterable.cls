//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: Tag_Dup_Contact_Call_Iterable	                                                                            //
// 	Purpose: Calling instance of Iterable for custom setting Duplicate_Contact__c											//								
//  Created By: Tredence																									//			
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//


global class Tag_Dup_Contact_Call_Iterable implements iterable<Tag_Dup_Contact_Iterable.DuplicatesList>
{
   global Iterator<Tag_Dup_Contact_Iterable.DuplicatesList> Iterator()
   {
      return new Tag_Dup_Contact_Iterable();
   }
}