//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: Tag_Dup_Account_Helper	                                                                                    //
// 	Purpose: Test Class for Batch Class Tag_Dup_Account_Batch																//						                        //
//  Created By: Tredence																									//			
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//


@istest
public class Tag_Dup_Account_Test {
    
    @istest static void Dup_Account_TestMethodPos_Scenario()
    {
        //Creating test data
        Account acc1 =new Account();
        acc1.Name='Test1';
        insert acc1;
        
        Account acc2=new Account();
        acc2.Name='Test2';
        insert acc2;

        Duplicate_Accounts__c dup= new Duplicate_Accounts__c();
        dup.Name=acc1.Id;
        dup.Master_Account_ID__c=acc2.Id;
        dup.Tagged__c=False;
        insert dup;
        
        
        Test.startTest();
        Tag_Dup_Account_Batch T1=new Tag_Dup_Account_Batch();
        database.executeBatch(T1);
        Test.stopTest();
        
        //Retrieving the records after runnning batch class and verifying our test
        Account a=[Select Id,Name,Master_Account__c,IsDuplicate__c,DeDuped_on__c,IsPartner from Account Where Id=:acc1.Id];
        system.assertEquals('Dup[Test1]',a.Name);
        system.assertEquals(acc2.Id,a.Master_Account__c);
        System.assertEquals(Date.today(),a.DeDuped_on__c);
        System.assertEquals(True,a.IsDuplicate__c);
        

        
        
    }      
}