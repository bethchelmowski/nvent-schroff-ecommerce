/* ********************************************
Developer -     Apurva Prasade
Class -         nVent_E2E_TriggerHandler
Description -   Trigger handler for E2E trigger

Developer   Date        Version
Apurva P    20-09-2019  V1.0

********************************************** */

public class nVent_E2E_TriggerHandler 
{
    public static void beforeinsertUtil(list<End_to_End__c> listNewE2E)
    {
        beforeInsertSH(listNewE2E);
        beforeInsertOwnerChanged(listNewE2E);
    }

    public static void beforeUpdateUtil(list<End_to_End__c> listNewE2E,map<Id, End_to_End__c> mapOldE2E)
    {
        beforeUpdateSH(listNewE2E,mapOldE2E);
        beforeUpdateOwnerChanged(listNewE2E,mapOldE2E);
    }

    public static void afterinsertUtil(list<End_to_End__c> listNewE2E)
    {
        afterInsertRFQRIN(listNewE2E);
        afterInsertRFQNotes(listNewE2E);
    } 
    public static void afterUpdateUtil(list<End_to_End__c> listNewE2E,map<Id, End_to_End__c> mapOldE2E)
    {
        afterUpdateRFQsNotes(listNewE2E,mapOldE2E);
        afterUpdateRFQsRIN(listNewE2E,mapOldE2E);
        afterUpdateShareE2E(listNewE2E,mapOldE2E);
    }

    public static void afterDeleteUtil(list<End_to_End__c> lisOldE2E)
    {
    }
    /**
    Update owner name on change of E2E owner - Apurva
     */
    public static void beforeUpdateOwnerChanged(list<End_to_End__c> listNewE2E,map<Id, End_to_End__c> mapOldE2E)
    {
        list<End_to_End__c> listE2EChanged = new list<End_to_End__c>();
        set<Id> setOwnerIds = new set<Id>();
        map<Id,User> mapUsers = new map<Id,User>();
        map<Id,Group> mapGroup = new map<Id,Group>();
        for(End_to_End__c obj : listNewE2E)
        {
            if(obj.OwnerId != mapOldE2E.get(obj.Id).OwnerId)
            {
                listE2EChanged.add(obj);
                setOwnerIds.add(obj.OwnerId);
            }
        }
        if(listE2EChanged.size() > 0 && setOwnerIds.size() > 0) 
        {
            for(User obj : [SELECT Name FROM User WHERE Id in: setOwnerIds])
            mapUsers.put(obj.Id, obj);
            for(Group obj : [SELECT Name FROM Group WHERE Id in: setOwnerIds])
                mapGroup.put(obj.Id, obj);
            for(End_to_End__c obj : listE2EChanged)
            {
                if(mapUsers.size() > 0)
                    obj.Owner_Name__c = mapUsers.get(obj.OwnerId).Name;
                if(mapGroup.size() > 0)
                    obj.Owner_Name__c = mapGroup.get(obj.OwnerId).Name;
            }
        }
    }
    /**
     Update RFQ Notes and related Items with E2E notes and related Items respectively  - On change of 
     */
    public static void afterUpdateRFQsNotes(list<End_to_End__c> listNewE2E, map<Id, End_to_End__c> mapOldE2E)
    {
        map<Id,End_to_End__c> mapE2ENotes = new map<Id,End_to_End__c>();
        set<Id> setRFQNotes = new set<Id>();
        list<RFQ__c> listUpdate = new list<RFQ__c>();
        for(End_to_End__c obj : listNewE2E)
        {
            if(obj.Notes__c != mapOldE2E.get(obj.Id).Notes__c ||(mapOldE2E.get(obj.Id).Notes__c != null && obj.Notes__c == null))
            {
                mapE2ENotes.put(obj.RFQ__c,obj);
                setRFQNotes.add(obj.RFQ__c);
            }
        }
        if(setRFQNotes.Size() > 0 && mapE2ENotes.size() > 0)
        {
            for(RFQ__c obj : [SELECT Notes__c FROM RFQ__c WHERE ID in: setRFQNotes])
            {
                if (obj.Notes__c != null)
                  obj.Notes__c = obj.Notes__c + ' ' + mapE2ENotes.get(obj.Id).Notes__c;
                else
                    obj.Notes__c = mapE2ENotes.get(obj.Id).Notes__c;
                listUpdate.add(obj);
            }
        }
        
        if(listUpdate.size() > 0)
            update listUpdate;
        
    }
    
    public static void afterUpdateRFQsRIN(list<End_to_End__c> listNewE2E, map<Id, End_to_End__c> mapOldE2E)
    {
        map<Id,End_to_End__c> mapE2ERN = new map<Id,End_to_End__c>();
        set<Id> setRFQRN = new set<Id>();
        list<RFQ__c> listUpdate = new list<RFQ__c>();
        for(End_to_End__c obj : listNewE2E)
        {
            if(obj.Related_Item_Numbers__c != mapOldE2E.get(obj.Id).Related_Item_Numbers__c)
            {
                mapE2ERN.put(obj.RFQ__c,obj);
                setRFQRN.add(obj.RFQ__c);
            }
        }
        
        if(setRFQRN.size() > 0 && mapE2ERN.size() > 0)
        {
            for(RFQ__c obj : [SELECT Related_Item_Numbers__c FROM RFQ__c WHERE ID in: setRFQRN])
            {
                if (obj.Related_Item_Numbers__c != null)
                    obj.Related_Item_Numbers__c = obj.Related_Item_Numbers__c + ' ' + mapE2ERN.get(obj.Id).Related_Item_Numbers__c;
                else
                    obj.Related_Item_Numbers__c = mapE2ERN.get(obj.Id).Related_Item_Numbers__c;
                listUpdate.add(obj);
            }
        }
        if(listUpdate.size() > 0)
            update listUpdate;
        
    }
    /**
     Update RFQ Notes and related Items with E2E notes and related Items respectively 
     */
    public static void afterInsertRFQRIN(list<End_to_End__c> listNewE2E)
    {
        map<Id,End_to_End__c> mapE2ERN = new map<Id,End_to_End__c>();
        set<Id> setRFQRN = new set<Id>();
        list<RFQ__c> listUpdate = new list<RFQ__c>();
        for(End_to_End__c obj : listNewE2E)
        {
            mapE2ERN.put(obj.RFQ__c,obj);
            setRFQRN.add(obj.RFQ__c);
        }
        if(setRFQRN.size() > 0)
        {
            for(RFQ__c obj : [SELECT Related_Item_Numbers__c FROM RFQ__c WHERE ID in: setRFQRN])
            {
                if (obj.Related_Item_Numbers__c != null)
                    obj.Related_Item_Numbers__c = obj.Related_Item_Numbers__c + ' ' + mapE2ERN.get(obj.Id).Related_Item_Numbers__c;
                else
                    obj.Related_Item_Numbers__c = mapE2ERN.get(obj.Id).Related_Item_Numbers__c;
                listUpdate.add(obj);
            }
        }
        if(listUpdate.size() > 0)
            update listUpdate;
        
    }
    
    public static void afterInsertRFQNotes(list<End_to_End__c> listNewE2E)
    {
        map<Id,End_to_End__c> mapE2ENotes = new map<Id,End_to_End__c>();
        set<Id> setRFQNotes = new set<Id>();
        list<RFQ__c> listUpdate = new list<RFQ__c>();
        for(End_to_End__c obj : listNewE2E)
        {
            mapE2ENotes.put(obj.RFQ__c,obj);
            setRFQNotes.add(obj.RFQ__c);
        }
        if(setRFQNotes.Size() > 0) 
        {
            for(RFQ__c obj : [SELECT Notes__c FROM RFQ__c WHERE ID in: setRFQNotes])
            {
                if (obj.Notes__c != null)
                  obj.Notes__c = obj.Notes__c + ' ' + mapE2ENotes.get(obj.Id).Notes__c;
                else
                    obj.Notes__c = mapE2ENotes.get(obj.Id).Notes__c;
                listUpdate.add(obj);
            }
        }
        
        if(listUpdate.size() > 0)
            update listUpdate;
        
    }

    /**
        Need to optimise this code
    */
    public static void afterUpdateShareE2E(list<End_to_End__c> listNewE2E,map<Id, End_to_End__c> mapOldE2E)
    {
        for(End_to_End__c obj : listNewE2E)
        {
            if(obj.OwnerId != mapOldE2E.get(obj.Id).OwnerId && obj.CreatedById != mapOldE2E.get(obj.Id).OwnerId)
            {
                Boolean isCreated = Pentair_ManualSharingRecord.manualShareE2ERead(obj.Id, obj.CreatedById);
                if(!isCreated)
                    System.debug('Failed on sharing record for ' + obj.CreatedById + ' on record: ' + obj.Id); 
            }
        }
    }
    /**
    Update owner name on insert of E2E - Apurva
     */
    public static void beforeInsertOwnerChanged(list<End_to_End__c> listNewE2E)
    {
        list<End_to_End__c> listE2EChanged = new list<End_to_End__c>();
        set<Id> setOwnerIds = new set<Id>();
        map<Id,User> mapUsers = new map<Id,User>();
        map<Id,Group> mapGroup = new map<Id,Group>();
        for(End_to_End__c obj : listNewE2E)
        {
            listE2EChanged.add(obj);
            setOwnerIds.add(obj.OwnerId);
        }
        if(listE2EChanged.size() > 0 && setOwnerIds.size() > 0)
        {
            for(User obj : [SELECT Name FROM User WHERE Id in: setOwnerIds])
            mapUsers.put(obj.Id, obj);
            for(Group obj : [SELECT Name FROM Group WHERE Id in: setOwnerIds])
                mapGroup.put(obj.Id, obj);
            for(End_to_End__c obj : listE2EChanged)
            {
                if(mapUsers.size() > 0)
                    obj.Owner_Name__c = mapUsers.get(obj.OwnerId).Name;
                if(mapGroup.size() > 0)
                    obj.Owner_Name__c = mapGroup.get(obj.OwnerId).Name;
            }
        }
    }
    
    /**
        Populate Scheduled completion date based on product type 
     */
    public static void beforeUpdateSH(list<End_to_End__c> listNewE2E,map<Id, End_to_End__c> mapOldE2E)
    {
        for(End_to_End__c obj : listNewE2E)
        {
            if(obj.Product_Type__c != mapOldE2E.get(obj.Id).Product_Type__c)
            {
                if(obj.Product_Type__c == 'DYS - Design your Standard')
                    obj.Scheduled_Completion__c = Pentair_RFQController.addDate(obj.CreatedDate, 1).date();
                else
                    obj.Scheduled_Completion__c = Pentair_RFQController.addDate(obj.CreatedDate, 3).date();
            }
        }
    }
    /**
        Populate Scheduled completion date based on product type 
     */
    public static void beforeInsertSH(list<End_to_End__c> listNewE2E)
    {
        for(End_to_End__c obj : listNewE2E)
        {            
            if(obj.Product_Type__c == 'DYS - Design your Standard')
                obj.Scheduled_Completion__c = Pentair_RFQController.addDate(Datetime.now(), 1).date();
           else
               obj.Scheduled_Completion__c = Pentair_RFQController.addDate(Datetime.now(), 3).date();
        }
    }
}