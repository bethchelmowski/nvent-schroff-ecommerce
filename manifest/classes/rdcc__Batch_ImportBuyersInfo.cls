/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Batch_ImportBuyersInfo implements Database.AllowsCallouts, Database.Batchable<SObject>, System.Schedulable {
    global void execute(System.SchedulableContext SC) {

    }
    global void execute(Database.BatchableContext info, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext info) {

    }
    global List<SObject> start(Database.BatchableContext info) {
        return null;
    }
}
