/* ********************************************
Developer -     Apurva Prasade
Class -         nVent_ExceptionUtilityTest
Description -   Test class to cover Exception utility

Developer   Date        Version
Apurva P    07-08-2019  V1.0

********************************************** */
@isTest
public class nVent_ExceptionUtilityTest {
    
    public static testMethod void testafterTrigger()
    {	
        DmlException ex = new DmlException();
        nVent_ExceptionUtility.createLog(ex);
    }

}