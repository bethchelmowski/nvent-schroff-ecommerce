/**
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

public with sharing class x7sProductActionSet
{
	@AuraEnabled
	public String name, label, title, iconName;

	@AuraEnabled
	public List<x7sProductActionModel> items;


	public x7sProductActionSet()
	{
		name        = '';
		label       = '';
		title       = '';
		iconName    = '';
		items       = new List<x7sProductActionModel>();
	}

	public void Dump(String message)
	{
		System.debug('Dump action set  : ' + message);
		System.debug('  name            : ' + name);
		System.debug('  label           : ' + label);
		System.debug('  title           : ' + title);
		System.debug('  icon            : ' + iconName);

		if (items != null)
		{
			Integer count = 0;
			for(x7sProductActionModel item : items)
			{
				item.Dump('item ' + ++count);
			}
		}
	}
}