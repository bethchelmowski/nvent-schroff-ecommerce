/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 8/30/19.
 */

public with sharing class x7sProductListController
{
	@AuraEnabled
	public static x7sProductReturn getActiveCart(String setting)
	{
		x7sProductReturn returnValue = new x7sProductReturn();

		String  userId = UserInfo.getUserId();
		x7sProductCartList  activeCart  = x7sProductCartController.getActiveCart(setting, userId);

		returnValue.status  = String.isNotBlank(activeCart.cartEncId);
		returnValue.value   = activeCart.cartEncId;
		returnValue.data    = JSON.serialize(activeCart);
		returnValue.message = String.format(System.Label.x7sProductToastCartItemsAdded,
			new List<String>
			{
				String.valueOf(activeCart.itemCount.intValue()),
				String.valueOf(activeCart.totalPrice)
			});

		System.debug('getActiveCart return: ' + returnValue);
		return returnValue;
	}

	@AuraEnabled
	public static x7sProductReturn addItemsToCart(String setting, List<String> skuList, List<String> valueList)
	{
		x7sProductReturn returnValue = new x7sProductReturn();
		Savepoint        savepoint   = Database.setSavepoint();
		Boolean          success     = false;

		System.debug('addItemsToCart: ' + skuList + ', ' + valueList);

		x7sProductCartList model = x7sProductCartController.updateCartItems(
			setting,
			skuList,
			null,
			null,
			valueList,
			x7sProductCartController.CART_ADD);

		if (String.isNotBlank(model.cartEncId))
		{
			x7sProductReturn cartReturn = getActiveCart(setting);

			returnValue.status  = success;
			returnValue.message = cartReturn.message;
			returnValue.value   = model.cartEncId;
			returnValue.data    = JSON.serialize(model);

		} else
		{
			returnValue.message = 'Failed to add to cart';
			Database.rollback(savepoint);
		}

		System.debug('addItemsToCart: ' + returnValue);
		return returnValue;
	}

	@AuraEnabled
	public static x7sProductListModel getProductsById(
		String setting,
		List<String> ids)
	{
		x7sProductListModel listModel = new x7sProductListModel();
		x7sProductSettings  settings  = new x7sProductSettings(setting);
		listModel.updateSettings(settings);

		Set<String> productIdList = new Set<String>(ids);

		Map<String, Object> inputData = new Map<String, Object>{
			ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccAPIProduct.PRODUCTIDLIST => productIdList,
			ccrz.ccApi.SIZING => new Map<String, Object> {ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL}},
			ccrz.ccAPIProduct.PARAM_INCLUDE_PRICING => true,
			ccrz.ccApi.SZ_REL => new List<String>{'Categories__r', 'CategoryI18Ns__r'}
		};

		if (String.isNotBlank(settings.storeFront))
		{
			inputData.put(ccrz.ccAPIProduct.PRODUCTSTOREFRONT, settings.storeFront);
		}

		try {
			Map<String, Object> outputData = ccrz.ccAPIProduct.fetch(inputData);

			if (outputData.get(ccrz.ccAPIProduct.PRODUCTLIST) != null) {
				List<Map<String, Object>> outputProductList = (List<Map<String, Object>>) outputData.get(ccrz.ccAPIProduct.PRODUCTLIST);

				if (outputProductList.size() > 0)
				{
					System.debug('Data record 0: ' +  outputProductList[0]);
					for(String keyString : outputProductList[0].keySet())
					{
						System.debug('    [' + keyString + '] = ' + outputProductList[0].get(keyString));
					}
				}

				listModel = processOutput(listModel, (List<Map<String, Object>>) outputData.get(ccrz.ccAPIProduct.PRODUCTLIST));
			}
		} catch (Exception e) {
			System.debug('Product fetch exception: ' + e.getMessage());
		}

		return listModel;
	}

	@AuraEnabled
	public static x7sProductListModel getProducts(
		String setting,
		Integer pageSize,
		String searchFilter,
		String typeFilter,
		String sortOrder,
		String specFilter,
		Decimal minimumPrice,
		Decimal maximumPrice,
		String categoryId)
	{
		System.debug('getProducts');
		System.debug('    setting name  : ' + setting);
		System.debug('    page size     : ' + pageSize);
		System.debug('    searchFilter  : ' + searchFilter);
		System.debug('    sort          : ' + sortOrder);
		System.debug('    specFilter    : ' + specFilter);
		System.debug('    category      : ' + categoryId);

		// get the root category
		if (String.isBlank(searchFilter) && String.isBlank(categoryId)) {
			List<x7sProductCategory> categories = x7sProductCategoryController.getCategories('');
			categoryId = categories[0].id;
		}

		x7sProductListModel listModel = new x7sProductListModel();
		x7sProductSettings  settings  = new x7sProductSettings(setting);
		settings.Dump('getProducts settings');
		listModel.updateSettings(settings);

		ccrz.ccLog.log(System.LoggingLevel.INFO, 'M:E', 'getProducts' );

		Map<String, Object> inputData = setupInitialInput(settings.storeFront, sortOrder, pageSize);
		Map<String, Object> outputData;

		if (String.isNotBlank(searchFilter))
		{
			inputData.put(ccrz.ccAPICategory.PRODUCTSEARCH, true);
			inputData.put(ccrz.ccService.SEARCHSTRING, searchFilter);

			try
			{
				Map<String, Object> searchOutputData = ccrz.ccAPIProduct.search(inputData);

				if (searchOutputData.get(ccrz.ccService.SEARCHRESULTS) != null)
				{
					Set<String> searchResultIdSet = (Set<String>) searchOutputData.get(ccrz.ccService.SEARCHRESULTS);

					Map<String, Object> fetchInputData = new Map<String, Object>
					{
						ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
						ccrz.ccAPIProduct.PRODUCTIDLIST => searchResultIdSet,
						ccrz.ccAPIProduct.PARAM_INCLUDE_PRICING => true
					};

					fetchInputData = getSortOrder(fetchInputData, sortOrder);

					try
					{
						outputData = ccrz.ccAPIProduct.fetch(fetchInputData);
						if (outputData.get(ccrz.ccAPIProduct.PRODUCTLIST) != null)
						{
							listModel = processOutput(listModel, (List<Map<String, Object>>) outputData.get(ccrz.ccAPIProduct.PRODUCTLIST));
						}
					} catch (Exception ex)
					{
						System.debug('Exception: ' + ex.getMessage());
					}
				} else
				{
					System.debug('Search did not return any results for ' + searchFilter);
				}
			} catch (Exception ex) { System.debug('Search exception: ' + ex.getMessage());}
		}
		else if (String.isNotBlank(categoryId))
		{
			List<String> categoryIdList = categoryId.split(x7sProductUtility.VALUE_DELIMITER);
			inputData.put(ccrz.ccAPIProduct.CATEGORY_IDS, new Set<String> (categoryIdList));

			if (String.isNotBlank(specFilter))
			{
				inputData = setupSpecFilter(inputData, specFilter);
			}

			try
			{
				outputData = ccrz.ccAPIProduct.find(inputData);

				if (outputData.get(ccrz.ccAPIProduct.PRODUCTLIST) != null)
				{
					listModel = processOutput(listModel, (List<Map<String, Object>>) outputData.get(ccrz.ccAPIProduct.PRODUCTLIST));
				}
			} catch (Exception e) { System.debug('Exception: ' + e.getMessage());}
		}

		listModel.totalCount  = (Integer) outputData.get(ccrz.ccAPIProduct.COUNT);
		listModel.pageHasMore = (Boolean)outputData.get(ccrz.ccAPIProduct.HAS_MORE);

		System.debug('Products found: ' + listModel.totalCount);
		System.debug('Product count : ' + listModel.items.size());
		System.debug('Products      : ' + listModel);

		return listModel;
	}

	@TestVisible
	private static Map<String, Object> setupInitialInput(String storeFont, String sortOrder, Integer pageSize)
	{
		Map<String, Object> initialData = new Map<String, Object>
		{
			ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccAPIProduct.PARAM_INCLUDE_PRICING => true,
			ccrz.ccAPIProduct.PRODUCT_LIMIT => pageSize,
			ccrz.ccAPIProduct.INCLUDE_COUNT => true,
			ccrz.ccApi.SIZING => new Map<String, Object> {ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL}}
		};

		if (String.isNotBlank(storeFont))
		{
			initialData.put(ccrz.ccAPIProduct.PRODUCTSTOREFRONT, storeFont);
		}

		return getSortOrder(initialData, sortOrder);
	}

	// specId1:value1;specId2:value1,value2
	@TestVisible
	private static Map<String, Object> setupSpecFilter(Map<String, Object> initialData, String specFilter)
	{
		if (String.isNotBlank(specFilter))
		{
			List<Map<String, Object>> filterSet   = new List<Map<String, Object>>();
			List<String> filterItems = specFilter.split(x7sProductUtility.FIELD_SEPARATOR);

			for (String filterKey : filterItems)
			{
				Map<String, Object> filterItem = new Map<String, Object>();
				filterItem.put('sfid', filterKey.substringBefore(x7sProductUtility.VALUE_SEPARATOR));
				List<Map<String, Object>> specList = new List<Map<String, Object>>();

				String valueList = filterKey.substringAfter(x7sProductUtility.VALUE_SEPARATOR);
				for(String value : valueList.split(x7sProductUtility.VALUE_DELIMITER))
				{
					specList.add(new Map<String, Object>{'value' => value});
				}

				filterItem.put('specValues', specList);
				System.debug('adding spec filter: ' + filterItem);

				filterSet.add(filterItem);
			}

			if (filterSet.size() > 0)
			{
				System.debug('Filter set: ' + filterSet);
				initialData.put(ccrz.ccAPIProduct.PRODUCT_FILTERS, filterSet);
				System.debug('Spec filter added: ' + initialData);
			}
		}

		return initialData;
	}

	private static x7sProductListModel processOutput(x7sProductListModel listModel, List<Map<String, Object>> outputProductList)
	{
		for (Map<String, Object> productEntry : outputProductList )
		{
			listModel.addProduct(getModel(productEntry));
		}

		return listModel;
	}

	@TestVisible
	private static x7sProductModel getModel(Map<String, Object> productMap)
	{
		System.debug('Processing product : ' + productMap);

		x7sProductModel model = new x7sProductModel();

		model.id                = (String)  productMap.get('sfid');
		model.name              = (String)  productMap.get('sfdcName');
		model.description       = (String)  productMap.get('shortDesc');
		model.productCode       = (String)  productMap.get('SKU');
		model.productCategory   = (String)  productMap.get('category');
		model.unitPrice         = (Decimal) productMap.get('price');
		model.productType       = (String)  productMap.get('productType');
		model.longDescription   = (String)  productMap.get('longDesc');

		// images
		List<Map<String,Object>> mediaList = (List<Map<String,Object>>)productMap.get('EProductMediasS');
		model.imageUrl.addAll(x7sProductUtility.getImageSrc(mediaList, 'Product Search Image'));

		return model;
	}

	@TestVisible
	private static Map<String, Object> getSortOrder(Map<String, Object> dataMap, String sortOrder)
	{
		String sortString = ccrz.ccAPIProductIndex.BY_NAME;
		Boolean descending = false;

		switch on sortOrder
		{
			when 'nameDesc'
			{
				descending = true;
			}
			when 'priceAsc'
			{
				sortString = ccrz.ccAPIProductIndex.BY_PRICE;
			}
			when 'priceDesc'
			{
				sortString = ccrz.ccAPIProductIndex.BY_PRICE;
				descending = true;
			}
		}

		dataMap.put(ccrz.ccService.ORDERBY, sortString);
		dataMap.put(ccrz.ccService.SORTDESC, descending);

		return dataMap;
	}

	@AuraEnabled
	public static List<x7sProductCategory> getCategories(String setting)
	{
		return x7sProductCategoryController.getCategories(setting);
	}

	@AuraEnabled
	public static x7sProductListModel getProductsFeatured(String setting)
	{
		x7sProductListModel listModel = new x7sProductListModel();

		return listModel;
	}

	@AuraEnabled (Cacheable=true)
	public static List<String> getProductTypeList()
	{
		return x7sProductUtility.getPicklistValues('ccrz__E_Product__c', 'ccrz__ProductType__c');
	}
}