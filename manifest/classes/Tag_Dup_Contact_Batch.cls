//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: Tag_Dup_Contact_Batch	                                                                                    //
// 	Purpose: Batch Class for Tagging Duplicates Contact																	    //				                       
//  Created By: Tredence																									//			
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//



global class Tag_Dup_Contact_Batch implements Database.Batchable<Tag_Dup_Contact_Iterable.DuplicatesList>,Database.Stateful {
    global integer passed=0;
    global integer failed=0;
    global integer total=0;
    global string finalstr= 'Record Id, Error Status, Error Message, Error Fields \n';
    
    global Iterable<Tag_Dup_Contact_Iterable.DuplicatesList> start(Database.BatchableContext bc) 
    {
        return new Tag_Dup_Contact_Call_Iterable();
    }
    
    global void execute(Database.BatchableContext bc,List<Tag_Dup_Contact_Iterable.DuplicatesList> records )
    {
        
        //Creating set of Ids of Contacts and Master Contacts
        set<string> DupIds=new set<string>();
        for(Tag_Dup_Contact_Iterable.DuplicatesList d: records)
        {
            DupIds.add(d.dupContactId);
            DupIds.add(d.MasterContactId);
        }
        
        List<Contact> ContactIds=new List<Contact>();
        
        ContactIds=[Select Id,AccountId,FirstName,LastName,Name,Master_Contact__c,IsDuplicate__c,DeDuped_on__c from Contact where Id In:DupIds];
        
        List<Contact> contacts=new List<Contact>();
        
        //Creating map of Contacts and their Master Contacts
        Map<Id,Contact> Contactsmap=new map<Id,Contact>(); 
        
        For(Contact con: ContactIds)
        {
            Contactsmap.put(con.Id,con);
        }
        
        
        //Returning a list of contacts after tagging using method UpdateContacts of helper class Tag_Dup_Contact_Helper
        contacts=Tag_Dup_Contact_Helper.UpdateContacts(records,Contactsmap);
        
        total=total+contacts.size();
        
        List<Duplicate_Contact__c> dups =[Select Name,Master_Contact_ID__c,Tagged__c from Duplicate_Contact__c where Name In:DupIds];
   
        List<Duplicate_Contact__c> duptoupdates=new list<Duplicate_Contact__c>();
        
        //Creating a map of custom setting
        map<string,Duplicate_Contact__c> duplicatesmap=new map<string,Duplicate_Contact__c>();
        for(Duplicate_Contact__c d: dups)
        {
            duplicatesmap.put(d.Name,d);
        }
        
        
        try
        {
        //Updating the list of contacts which are tagged 
        database.saveResult[] results=database.update(contacts,false);

        //Checking the status of each update
        for(integer i=0;i<contacts.size();i++)
        {
            database.SaveResult s=results[i];
            //Tag_Dup_Contact_Iterable.DuplicatesList Ids=records[i];
            Contact c1=contacts[i];
            if(!s.isSuccess())
            {
                
                failed++;
                for(Database.Error err : s.getErrors())
                {
                    
                    string recordString = '"'+c1.Id+'","'+err.getStatusCode()+'","'+err.getMessage().escapecsv()+'","'+err.getFields() +'"\n';
                    finalstr = finalstr +recordString;
                    
                }
            }
            else
            {
                //Updating Tagged flag in custom setting as True
                Duplicate_Contact__c d1=duplicatesmap.get(c1.Id);
                d1.Tagged__c=True;
                duptoupdates.add(d1);
                passed++;
            }
            
            
        }
        }
        catch(Exception ex)
        {
            string recordString = '"'+ +'","'+ex.getTypeName()+'","'+ex.getMessage().escapecsv()+'","'+ex.getStackTraceString() +'"\n';
            finalstr = finalstr +recordString;
            failed=failed+contacts.size();
            //failed++;
        }

        try
        {
        	if(duptoupdates.size()>0)
        	update duptoupdates;
         }
        catch(Exception ex)
        {
            system.debug('****** ERROR: '+ex.getMessage()+' Line Number '+ex.getLineNumber());
        }
        
    }
    
    global void finish(Database.BatchableContext bc)
    {   
        //Sending notification email using helper class Tag_Dup_Contact_Helper
        Tag_Dup_Contact_Helper.Email_StatusOfbatch(finalstr,passed,failed,total);    
    }
}