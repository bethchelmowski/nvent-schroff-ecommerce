/**********************************************************************
Name:  NVent_CaseTriggerHandler
Developed by: Anvi Kanodra
=======================================================================
Req: 
Use Trigger Framework for all Triggers.
=======================================================================
Purpose:   
This is a single handler class for all Case Trigger.
All the business logic is written in helper class.
=======================================================================
History
-------                                                            
VERSION  AUTHOR            DATE              DETAIL
Initial Draft

***********************************************************************/
public class NVent_CaseTriggerHandler implements NVent_ITriggerHandler
{
   
    
    public void beforeInsert(List<Case> listOfNewCases) {
      system.debug('hello');
        NVent_CaseTriggerHelper.createContact(listOfNewCases);
      	NVent_CaseTriggerHelper.assignOwnerAccTeam(listOfNewCases);
        NVent_CaseTriggerHelper.assignBusinessHours(listOfNewCases);
    } 
    
    public void beforeUpdate(Map<Id, SObject> mapOfIdSObjectNew , Map<Id, SObject> mapOfIdSObjectOld ) {
       
    }
    
    public void beforeDelete(Map<Id, SObject> mapOfIdCasesOld) {
        // write business logic to be implemented in before delete operation
    }
    
    public void afterInsert(Map<Id, SObject> mapOfIdCasesNew) {         
        system.debug('hello');
        
        //NVent_CaseTriggerHelper.createContact(mapOfIdCasesNew.values());
        
    }
    
    public void afterUpdate(Map<Id, SObject> mapOfIdCasesNew, Map<Id, SObject> mapOfIdCasesOld) {
       
    }
    
    public void afterDelete(Map<Id, SObject> mapOfIdCasesOld) {
        
    }
    public void afterUndelete(Map<Id, SObject> mapOfIdCasesOld) {
        
    }  
    
}