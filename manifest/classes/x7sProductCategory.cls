/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/10/19.
 */

public with sharing class x7sProductCategory
{
	@AuraEnabled
	public String id, name;

	@AuraEnabled
	public List<x7sProductCategory> children;

	public x7sProductCategory()
	{
		this.id     = '';
		this.name   = '';
	}

	public x7sProductCategory(String id, String name)
	{
		this();

		this.id     = id;
		this.name   = name;
	}

	public void Dump(String title)
	{
		System.debug('x7sProductCategory: '  + title);
		System.debug('id    : ' + this.id);
		System.debug(('name : ' + this.name));

		if (children != null)
		{
			System.debug(' -- children');
			Integer counter = 0;
			for(x7sProductCategory child : children)
			{
				child.Dump('child ' + counter++);
			}
		}
	}
}