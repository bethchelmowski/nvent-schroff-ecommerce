/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/16/19.
 */

@IsTest (SeeAllData=true)
private class x7sProductSettings_t
{
	@IsTest
	static void getSettings()
	{
		x7sProductSettings settings = new x7sProductSettings('');
		System.assertNotEquals(null, settings);

		x7sProductSettings settings2 = new x7sProductSettings('noname');
		System.assertNotEquals(null, settings2);
	}

	static testMethod void testSetActionModel()
	{
		x7sProductAction__mdt action = new x7sProductAction__mdt();
		action.DeveloperName    = 'Default';
		action.Label            = 'Label1';
		action.Action_Type__c   = 'Flow';
		action.Action_Title__c  = 'Title1';
		action.Action_Value__c  = 'value1';
		action.New_Window__c    = true;

		x7sProductActionModel model = x7sProductSettings.setActionModel(action);
		System.assertNotEquals(null, model);
	}

	static testMethod void testSetActionSet()
	{
		x7sProductSettings settings = new x7sProductSettings();
		x7sProductActionSet__mdt actionSetRecord = new x7sProductActionSet__mdt();
		actionSetRecord.DeveloperName = 'Default';
		actionSetRecord.Label = 'Label1';
		actionSetRecord.IconName__c = 'icon1';

		settings.setActionSet(actionSetRecord);
		System.assertEquals('icon1', settings.actionSet.iconName);
	}
}