public class Pentair_RFQ_XML_COND {   
    public class RFQ_Line_Item_Cond
    {
        public string erpQuoteNumber { set; get; }
        public string documentCondition { set; get; }
        public string condutionItemNumber { set; get; }
        public string stepNumber { set; get; }
        public string conditionType { set; get; }
        public string calcTypeForCondution { set; get; }
        public string conditionBaseValue { set; get; }
        public string rate { set; get; }
        public string currencyKey { set; get; }
        public string condutionValue { set; get; }
        public string conditionPricingUnit { set; get; }
        public string conditionUsedForStat { set; get; }
        public string conditionIsInactive { set; get; }
    }
    
    public static void AdditionalLines(List<RFQ_XML_COND__C> rfqLineItemConds, String currencyCode, String outputLanguage, String rfqId)
    {
        List<RFQ_Line_Item__c> CreateUpdateRFQLines = new List<RFQ_Line_Item__c>();
		
		
		String xPackagingFee = 'Packaging Fee';
        String xInsuranceFee = 'Insurace Fee';
        
        if (outputLanguage == 'DE' || outputLanguage == 'Z1')
        {
            xPackagingFee = 'Verpackungskosten';
            xInsuranceFee = 'Versicherungskosten';
        }
        
		//Check for ZPAC
        Decimal zpacPrice = 0.00;
        for (RFQ_XML_COND__C query1 : rfqLineItemConds)
        {
            if (query1.CONDITION_TYPE__C == 'ZPAC' && Decimal.valueof(query1.CONDITION_VALUE__c) > 0 &&
               query1.CONDITION_IS_INACTIVE__c != 'W')
            {
                zpacPrice = zpacPrice + Decimal.valueof(query1.CONDITION_VALUE__c);
            }
        }
        if (zpacPrice>0)
        	CreateUpdateRFQLines.addall(CreateLineItems(999998, 1, 0, '', xPackagingFee, currencyCode, zpacPrice, zpacPrice, 0, '', rfqId, false));
        
        //Check for AMIZ
        Decimal AMIZPrice = 0.00;
        for (RFQ_XML_COND__C query2 : rfqLineItemConds)
        {
            if (query2.CONDITION_TYPE__C == 'AMIZ' && Decimal.valueof(query2.CONDITION_VALUE__c) > 0 &&
               query2.CONDITION_IS_INACTIVE__c != 'W')
            {
                AMIZPrice = AMIZPrice + Decimal.valueof(query2.CONDITION_VALUE__c);
            }
        }
        if (AMIZPrice>0)
        	CreateUpdateRFQLines.addall(CreateLineItems(999999, 1, 0, '', 'Small Order Surcharge', currencyCode, AMIZPrice, AMIZPrice, 0, '', rfqId, false));
        
		 //Check for ZINS
        Decimal ZINSPrice = 0.00;
        for (RFQ_XML_COND__C query3 : rfqLineItemConds)
        {
            if (query3.CONDITION_TYPE__C == 'ZINS' && Decimal.valueof(query3.CONDITION_VALUE__c) > 0 &&
               query3.CONDITION_IS_INACTIVE__c != 'W')
            {
                ZINSPrice = ZINSPrice + Decimal.valueof(query3.CONDITION_VALUE__c);
            }
        }
        if (ZINSPrice>0)
        	CreateUpdateRFQLines.addall(CreateLineItems(999997, 1, 0, '', xInsuranceFee, currencyCode, ZINSPrice, ZINSPrice, 0, '', rfqId, false));
        
		//Check for ZFRG
        Decimal ZFRGPrice = 0.00;
        for (RFQ_XML_COND__C query4 : rfqLineItemConds)
        {
            if ((query4.CONDITION_TYPE__C == 'ZFRG' || query4.CONDITION_TYPE__C == 'HD00')
                && Decimal.valueof(query4.CONDITION_VALUE__c) > 0 && query4.CONDITION_IS_INACTIVE__c != 'W')
            {
                ZFRGPrice = ZFRGPrice + Decimal.valueof(query4.CONDITION_VALUE__c);
            }
        }
        if (ZFRGPrice>0)
        	CreateUpdateRFQLines.addall(CreateLineItems(999997, 1, 0, 'Freight', 'Freight', currencyCode, ZFRGPrice, ZFRGPrice, 0, '', rfqId, false));
		
		
		if(!CreateUpdateRFQLines.isEmpty())
			CreateUpdateLineItems(CreateUpdateRFQLines, rfqId);
    }
	
    public static void ProcessLineItems(String erpNumber, List<RFQ_XML__c> rfqLineItems, String rfqId, List<RFQ_XML_COND__C> rfqLineItemConds)
    {
		List<RFQ_Line_Item__c> CreateUpdateRFQLines = new List<RFQ_Line_Item__c>();
		
        System.debug('rfqLineItemConds size: ' + rfqLineItemConds.size());
		
        for(RFQ_XML__c rfqLineItem : rfqLineItems) {
		
			Integer outputLineNumber = 0;
			String outputMaterial = '';
			String outputItemDescription = '';
			//Integer outputItemQuantity = 1; Tredence - Commenting the line to consider Decimal values as well
			Decimal outputItemQuantity = 1.00;// Tredence - Converted the Type to Decimal to consider decimal values as well
			Decimal outputForeignUnitPrice = 0.00;
			Decimal outputForeighListPrice = 0.00;
			Decimal outputLocalListPrice = 0.00;
			String outputCurrencyCode = '';
			Decimal outputNetValue = 0.00;
			Decimal outputDiscount = 0.00;
			String outputMaterialDet = '';
			Boolean outputConfCompntFlag;
			String xErpNumber= '%'+erpNumber+'%';
			
		
			//Add Values to a List
			List<RFQ_Line_Item_Cond> allItemCond = new List<RFQ_Line_Item_Cond>();

			for (RFQ_XML_COND__C cond : rfqLineItemConds)
			{
				if (cond.CONDITION_ITEM_NUMBER__C == rfqLineItem.SALES_DOC_ITEM__C) 
				{
					RFQ_Line_Item_Cond singleCond = new RFQ_Line_Item_Cond();
					singleCond.erpQuoteNumber = erpNumber;
					singleCond.documentCondition = cond.DOCUMENT_CONDITION__c;
					singleCond.condutionItemNumber = cond.CONDITION_ITEM_NUMBER__c;
					singleCond.stepNumber = cond.STEP_NUMBER__c;
					singleCond.conditionType = cond.CONDITION_TYPE__c;
					singleCond.calcTypeForCondution = cond.CAL_TYPE_FOR_CONDITION__c;
					singleCond.conditionBaseValue = cond.CONDITION_BASE_VALUE__c;
					if(cond.RATE__c != null) {
						if (cond.RATE__c.contains('-'))
							singleCond.rate = '-' + cond.RATE__c.replace('-', '');
						else
							singleCond.rate = cond.RATE__c;
					}	
					singleCond.currencyKey = cond.CURRENCY_KEY__c;
					if(cond.CONDITION_VALUE__c != null) {
						if (cond.CONDITION_VALUE__c.contains('-'))
							singleCond.condutionValue = '-' + cond.CONDITION_VALUE__c.replace('-', '');
						else
							singleCond.condutionValue = cond.CONDITION_VALUE__c;
					}	
					singleCond.conditionPricingUnit = cond.CONDITION_PRICING_UNIT__c;
					singleCond.conditionUsedForStat = cond.CONDITION_USED_FOR_STAT__c;
					singleCond.conditionIsInactive = cond.CONDITION_IS_INACTIVE__c;

					allItemCond.Add(singleCond);
				}
			}       
			
			//Begin
			// Grab the default values from SAP with no calculations
			if (rfqLineItem.SALES_DOC_ITEM__C != null)
				outputLineNumber =  Integer.valueof(rfqLineItem.SALES_DOC_ITEM__C); //Remove the leading zeros
			if (rfqLineItem.QUANTITY_LINE__c != null)
				//outputItemQuantity = Integer.valueof(rfqLineItem.QUANTITY_LINE__c);// Tredence-Commenting the to consider Decimal values as well
				outputItemQuantity = Decimal.valueof(rfqLineItem.QUANTITY_LINE__c);// Tredence - Converted the Type to Decimal to consider decimal values as well
			if (rfqLineItem.SUBTOTAL_1__c != null)
				outputNetValue = Decimal.valueof(rfqLineItem.SUBTOTAL_1__c);
			if(rfqLineItem.MATERIAL_NUMBER__c != null) {
				try
				{
					outputMaterial = String.valueof(Integer.valueof(rfqLineItem.MATERIAL_NUMBER__c.replaceFirst('^0+','')));
				}
				catch (exception ex)
				{
					outputMaterial = rfqLineItem.MATERIAL_NUMBER__c.replaceFirst('^0+','');
				}
			}
			outputItemDescription = rfqLineItem.DESCRIPTION_LINE__c;
			outputCurrencyCode = rfqLineItem.DOCUMENT_CURRENCY__C;
			outputMaterialDet = rfqLineItem.MATERIAL_ENTERED__c;
			
			//Loop throught All Item Conditions
			//Get Condution Pricing Unit & Condution Value
			String lv_price_unit;
			Decimal lv_base_price = 0.00;
			Decimal lv_price = 0.00;
			for (RFQ_Line_Item_Cond query1 : allItemCond)
			{
				if (query1.calcTypeForCondution == 'C' && query1.conditionUsedForStat == null &&
				   query1.conditionIsInactive == null)
				{
					lv_price_unit = query1.conditionPricingUnit;
					if (query1.condutionValue != null)
					{
						lv_base_price += Decimal.valueof(query1.condutionValue);
					}
				}
			}
			
			System.debug('Line lv_base_price : ' + lv_base_price);
			
			if (lv_price_unit == '1')
			{
				if (lv_base_price != null && outputItemQuantity > 0)
				{
					lv_price = lv_base_price / outputItemQuantity;
					outputForeignUnitPrice = lv_price;
				}
			}
			else
			{
				if (lv_base_price != null && lv_price_unit != null && outputItemQuantity > 0 && Integer.valueof(lv_price_unit) > 0)
				{
					lv_price = lv_base_price / (outputItemQuantity * Integer.valueof(lv_price_unit));
					outputForeignUnitPrice = lv_price;
				}
			}
			
			System.debug('Line lv_price : ' + lv_price);
			System.debug('Line outputForeignUnitPrice : ' + outputForeignUnitPrice);
			
			//Check if the High Level Item BOM Contains data
			if (rfqLineItem.HIGH_LVL_ITEM_BOM__C == '000000')
				outputConfCompntFlag = false;
			else
				outputConfCompntFlag = true;
			
			//Sets the Local List Price
			Boolean foundPB00 = false;
			Boolean foundZCPL = false;
			Decimal lv_local_rate_price = 0.00;
			Decimal lv_conditionBaseValue = 0.00;
			Decimal lv_conditionValue = 0.00;
			String lv_localCurrencyCode;
			for (RFQ_Line_Item_Cond query2 : allItemCond)
			{
				if (query2.conditionType == 'ZCPL')
				{
					foundZCPL = true;
					lv_local_rate_price = Decimal.valueof(query2.rate) ;
					lv_localCurrencyCode = query2.currencyKey;
					lv_conditionBaseValue = Decimal.valueof(query2.conditionBaseValue);
					lv_conditionValue = Decimal.valueof(query2.condutionValue);
				}
				if (query2.conditionType == 'PB00' && Decimal.valueof(query2.rate) > 0)
					foundPB00 = true;
			}
			if (foundZCPL && !foundPB00)
				outputLocalListPrice = lv_local_rate_price;
			
			System.debug('Line lv_local_rate_price : ' + lv_local_rate_price);
			System.debug('Line lv_conditionBaseValue : ' + lv_conditionBaseValue);
			System.debug('Line lv_conditionValue : ' + lv_conditionValue);
			
			//Check the Currency Values
			if (lv_localCurrencyCode != null && lv_localCurrencyCode != rfqLineItem.DOCUMENT_CURRENCY__c)
			{
				if (lv_conditionBaseValue > 0)
					lv_conditionBaseValue = lv_conditionBaseValue / 10;
				
				if (lv_conditionValue > 0 && lv_conditionBaseValue > 0)
					outputForeighListPrice = lv_conditionValue / lv_conditionBaseValue;
			}
			else
				outputForeighListPrice = outputLocalListPrice;
			
			//Discount
			Decimal lv_rate = 0.00;
			for (RFQ_Line_Item_Cond query3 : allItemCond)
			{
				if (query3.conditionIsInactive == null && CheckStepNumber(Integer.valueof(query3.stepNumber)))
				{
					if (query3.calcTypeForCondution == 'A')
						lv_rate = Decimal.valueof(query3.rate) / 10;  
					
					if (lv_rate < 0)
						lv_rate = lv_rate * -1;

					outputDiscount = outputDiscount + lv_rate;
				}
			}
			outputDiscount = outputDiscount * -1;     
			
			//Check if the Update Foreign Unit Price has more than one
			/*
			Integer xCounter = 0;
			for (RFQ_Line_Item_Cond query4 : allItemCond)
			{        
				if (Decimal.valueof(query4.rate) < 0 && query4.conditionIsInactive == null &&
				   query4.conditionUsedForStat == null)
				{
					xCounter = xCounter + 1;
				}
			}
			
			System.debug('query4 Counter: ' + xCounter);
			*/
			
			//Update Foreign Unit Price
			Decimal lv_fuprice = 0.00;
			Decimal lv_furate = 0.00;
			Decimal lv_fudis = 0.00;
			Integer xFinalCounter = 0;
			
			//if (xCounter == 1)
			//	lv_fuprice = outputForeignUnitPrice.setScale(2, RoundingMode.HALF_UP);
		   // else
				lv_fuprice = outputForeignUnitPrice;
			
				for (RFQ_Line_Item_Cond query4 : allItemCond)
				{        
					if (Decimal.valueof(query4.rate) < 0 && query4.conditionIsInactive == null &&
					   query4.conditionUsedForStat == null)
					{
						xFinalCounter = xFinalCounter + 1;
						lv_furate = Decimal.valueof(query4.rate) * -1;
						
						if (query4.calcTypeForCondution == 'A')
						{
							lv_furate = lv_furate / 10;                        
							System.debug('Line Q4 lv_furate : ' + lv_furate);    
							if (query4.conditionType == 'ZID1')
								lv_fudis = (lv_furate / 100) * lv_fuprice;
							else
								lv_fudis = ((lv_furate / 100) * lv_fuprice); //.setScale(2, RoundingMode.HALF_UP);
							
							//if (xCounter == xFinalCounter)
							//	lv_fudis = lv_fudis.setScale(2, RoundingMode.HALF_UP);
							
							System.debug('Line Q4 lv_fudis : ' + lv_fudis);
							System.debug('Line Q4 lv_fuprice : ' + lv_fuprice);
							System.debug('Line Q4 outputForeignUnitPrice : ' + outputForeignUnitPrice);             
			 
							lv_fuprice = lv_fuprice - lv_fudis;
						}
						else
							outputForeignUnitPrice = outputForeighListPrice - outputForeignUnitPrice;
					}      
				}   
			
			outputForeignUnitPrice = lv_fuprice; //.setScale(2, RoundingMode.HALF_UP);
			
			System.debug('Line Update FUP outputForeignUnitPrice : ' + outputForeignUnitPrice);
			System.debug('Line Update FUP outputForeighListPrice : ' + outputForeighListPrice);
			
			//Check for ZCFG
			for (RFQ_Line_Item_Cond query5 : allItemCond)
			{
				if (query5.conditionType == 'ZCFG')
				{
					outputForeignUnitPrice = Decimal.valueof(query5.rate);
					outputNetValue = Decimal.valueof(query5.condutionValue);
					break;
				}
			}
			
			//Updates to the Foreign Unit and List Price
			Decimal lv_unit = 0.00;
			if (outputForeignUnitPrice < outputForeighListPrice && outputForeighListPrice > 0)
			{
				//lv_unit = (outputForeignUnitPrice.setScale(2, RoundingMode.HALF_UP) / outputForeighListPrice.setScale(2, RoundingMode.HALF_UP)) * 100;
				lv_unit = (outputForeignUnitPrice / outputForeighListPrice) * 100;
				//lv_unit = 100 - lv_unit.setScale(2, RoundingMode.HALF_UP);
				lv_unit = 100 - lv_unit;
				outputDiscount = (lv_unit * -1); //.setScale(2, RoundingMode.HALF_UP);
				
				//outputDiscount = outputDiscount.setScale(1, RoundingMode.HALF_UP);
			}
			if (outputForeignUnitPrice > outputForeighListPrice)
			{
				outputForeighListPrice = outputForeignUnitPrice;
			}
			
			CreateUpdateRFQLines.addall(CreateLineItems(outputLineNumber, outputItemQuantity, outputNetValue, outputMaterial, outputItemDescription,
                       outputCurrencyCode, outputForeignUnitPrice, outputForeighListPrice, outputDiscount,
                       outputMaterialDet, rfqId, outputConfCompntFlag));
		
		}
		
        //Create the Line Items
		if(!CreateUpdateRFQLines.isEmpty())
			CreateUpdateLineItems(CreateUpdateRFQLines, rfqId);
    }
    
    public static Boolean CheckStepNumber(Integer stepNumber)
    {
        Boolean returnCheck = false;
        
        Integer range1Low = 21;
        Integer range1High = 64;
        Integer range2Low = 110;
        Integer range2High = 121;
        
        if (stepNumber >= range1Low && stepNumber <= range1High)
            returnCheck = true;
        
        if (stepNumber >= range2Low && stepNumber <= range2High)
            returnCheck = true;
        
        return returnCheck;
    }
    
    /*public static List<RFQ_Line_Item__c> CreateLineItems(Integer lineNumber, Integer itemQuantity, Decimal netValue, String materialNumber,
                                      String itemDescription, String currencyCodeValue, Decimal foreignUnitPrice,
                                      Decimal foreighListPrice, Decimal discount, String materialDet, String rfqId,
                                      Boolean confCompntFlag)*/
    public static List<RFQ_Line_Item__c> CreateLineItems(Integer lineNumber, Decimal itemQuantity, Decimal netValue, String materialNumber,
                                      String itemDescription, String currencyCodeValue, Decimal foreignUnitPrice,
                                      Decimal foreighListPrice, Decimal discount, String materialDet, String rfqId,
                                      Boolean confCompntFlag)// Tredence - Converted the type of itemQuantity to Decimal
    {
        
		List<RFQ_Line_Item__c> CreateUpdateRFQLines = new List<RFQ_Line_Item__c>();
		
		System.debug('RFQ Line Item : lineNumber : ' + lineNumber);
        System.debug('RFQ Line Item : itemQuantity : ' + itemQuantity);
        System.debug('RFQ Line Item : netValue : ' + netValue);
        System.debug('RFQ Line Item : materialNumber : ' + materialNumber);
        System.debug('RFQ Line Item : itemDescription : ' + itemDescription);
        System.debug('RFQ Line Item : currencyCodeValue : ' + currencyCodeValue);
        System.debug('RFQ Line Item : foreignUnitPrice : ' + foreignUnitPrice);
        System.debug('RFQ Line Item : foreighListPrice : ' + foreighListPrice);
        System.debug('RFQ Line Item : discount : ' + discount);
        System.debug('RFQ Line Item : materialDet : ' + materialDet);
						   

            //New Record
            RFQ_Line_Item__c newLineItems = new RFQ_Line_Item__c();
            newLineItems.RFQ__c = rfqId;
			if(materialNumber != null)
				newLineItems.Material_Number__c = materialNumber; //Item Number
            newLineItems.Position__c = lineNumber; //Title
            newLineItems.Name = itemDescription;
			if(discount != null)
				newLineItems.Discount_Percentage__c = Math.ABS(discount.setScale(1, RoundingMode.HALF_UP));
            if(itemQuantity != null)
				newLineItems.Quantity__c =  Math.roundToLong(itemQuantity);
            if (foreignUnitPrice != null)
            	newLineItems.Price_List__c = foreignUnitPrice;
            if (foreighListPrice != null)
            	newLineItems.Price_Gross__c = foreighListPrice;
            newLineItems.CurrencyIsoCode = currencyCodeValue;
            if (confCompntFlag)
                newLineItems.Value__c = 0;
            else
            {
          		if(itemQuantity != null && foreignUnitPrice != null)
					newLineItems.Value__c = (itemQuantity * foreignUnitPrice).setScale(2, RoundingMode.HALF_UP);
            }
            
            CreateUpdateRFQLines.add(newLineItems);
			
			
        return CreateUpdateRFQLines;
    }
	
	public static void CreateUpdateLineItems(List<RFQ_Line_Item__c> lineItems, String rfqId)
	{
		set<RFQ_Line_Item__c> DMLupsertlineItems = new set<RFQ_Line_Item__c>();
		List<RFQ_Line_Item__c> DMLlineItems = new List<RFQ_Line_Item__c>();
		
		System.debug('**RFQ Drl Count : ' + lineItems.size());
		System.debug('**RFQ Drl : ' + lineItems);
		
		if(rfqId != null) {
			List<RFQ_Line_Item__c> rlines = new List<RFQ_Line_Item__c>([SELECT ID, Name, Material_Number__c, Position__c, RFQ__C FROM RFQ_Line_Item__c where RFQ__C =:rfqId]);
		
			if(!lineItems.isEmpty()) {

				for(RFQ_Line_Item__c Drl : lineItems){
					
					if(!rlines.isEmpty()) {
						for(RFQ_Line_Item__c rl : rlines) {
							
							if(rl.Position__c == Drl.Position__c && rl.RFQ__C == Drl.RFQ__C) {
								
								Drl.ID = rl.ID;
								System.debug('**Matched RFQ rl ID : ' + rl);
								System.debug('**Matched RFQ Drl ID : ' + Drl);
								Break;
							}
						}
					}
						DMLupsertlineItems.add(Drl);
						System.debug('**Context Add RFQ Drl ID : ' + Drl);
						
				}
				
				DMLlineItems.addall(DMLupsertlineItems);
			}
		}
		
		if(!DMLlineItems.isEmpty())
			upsert DMLlineItems;

	}
	
	
 }