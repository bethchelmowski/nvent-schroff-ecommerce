global class MergeDuplicateAccountIterable implements Iterator<accountTableList>{ 

   public List<accountTableList> Accounts {get; set;} 
   public Integer i {get; set;} 

    public MergeDuplicateAccountIterable() { 
       
        List<String> AccountData = new List<String>();
        Accounts = new List<accountTableList>();
        i=0;
        
        //List<Document> pro = new List<Document>([select id, body from document where lastmodifieddate=TODAY and name like 'Integration File%' order by createddate desc  limit 1]);
        //List<Document> pro = new List<Document>([select id, body from document where name like 'Integration File%' order by createddate desc  limit 1]);
        List<Duplicate_Accounts__c> DuplicateAccnt = new List<Duplicate_Accounts__c>();
		DuplicateAccnt =Duplicate_Accounts__c.getall().values();
        
        /*if(!pro.isEmpty())
            AccountData = EncodingUtil.base64Encode(pro[0].body).split('/');
        system.debug('AccountData '+AccountData);
        if(!AccountData.isEmpty()) {
        
            for(String str : AccountData) {
            
                List<String> ProjectIDs = new List<String>();
            
                if(String.isNotBlank(str)) 
                    ProjectIDs = str.split('\\+');
                
                //system.debug('** Dup Id '+ ProjectIDs[0].trim());
                //system.debug('** Parent Id '+ ProjectIDs[1].trim()); 
                
                if(!ProjectIDs.isEmpty())
                    Accounts.add(new accountTableList(ProjectIDs[0].trim(), ProjectIDs[1].trim()));
                    
            }*/
        if(!DuplicateAccnt.isEmpty())
        {
        	for(Duplicate_Accounts__c AccntIds : DuplicateAccnt)
            {	
                if(AccntIds.name != null && AccntIds.Master_Account_ID__c != null)
                	Accounts.add(new accountTableList(AccntIds.name,AccntIds.Master_Account_ID__c));
            }
        } 
        else 
        {
            MergeDuplicateAccounts mer = new MergeDuplicateAccounts();
            mer.sendNoRecordsNotification();
        }
 
   }   

   global boolean hasNext(){ 
       
       if(i >= Accounts.size()) {
           return false; 
       } else {
           return true; 
       }
   }    

   global accountTableList next(){ 

       i++; 
       return Accounts[i-1]; 
   }
   
   global class accountTableList {
   
        public String dupProjectID{get;set;}
        public String parentProjectID{get;set;}
        
        public accountTableList(String dupProjectID, String parentProjectID){
        
            this.dupProjectID = dupProjectID;
            this.parentProjectID = parentProjectID;        
        }
   } 
}