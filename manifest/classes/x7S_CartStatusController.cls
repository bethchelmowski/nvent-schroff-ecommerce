/**
 * Created By ankitSoni on 04/30/2020
 */
public class x7S_CartStatusController {
    
    @AuraEnabled
    public static String fetchCartStatus(){

        String cartStatus = '';
        String userId = ''; // Might get from FrontEnd Component
        List<String> cartIdList = new List<String>();

        if (string.isBlank(userId))
		{
			userId = UserInfo.getUserId();
        }
        
        System.debug('Current User Id fetchCartStatus() - '+userId);
        
        Map<String, Object> inputData = new Map<String, Object> {
			ccrz.ccApi.API_VERSION    => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccApiCart.ACTIVECART => true,
            ccrz.ccApiCart.BYOWNER    => userId,
			ccrz.ccApiCart.BYUSER     => userId,
			ccrz.ccApi.SIZING         => new Map<String, Object>
				{ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL}}
		};
		try {
			Map<String, Object> outputData = ccrz.ccApiCart.fetch(inputData);
			
			if (outputData.get(ccrz.ccApiCart.CART_OBJLIST) != null) {
				List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) outputData.get(ccrz.ccApiCart.CART_OBJLIST);
				
				if (outputCartList.size() > 0)
				{
                    for(Map<String,Object> item : outputCartList){
                        cartIdList.add((String)item.get('sfid'));
                        cartStatus = (String) item.get('cartStatus');
                    }
                }
            }
		} catch (Exception ex) { 
            System.debug('getActiveCart Exception: ' + ex.getMessage());
        }

        System.debug('Cart Ids '+cartIdList);

        return cartStatus;
    }

}