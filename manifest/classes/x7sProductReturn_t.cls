/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/16/19.
 */

@IsTest
private class x7sProductReturn_t
{
	@IsTest
	static void testReturnValue()
	{
		x7sProductReturn returnValue = new x7sProductReturn();
		System.assertNotEquals(null, returnValue);
		System.assertEquals(false, returnValue.status);
		System.assert(String.isBlank(returnValue.message));
		System.assert(String.isBlank(returnValue.data));

		returnValue = new x7sProductReturn(true, 'message');
		System.assertNotEquals(null, returnValue);
		System.assertEquals(true, returnValue.status);
		System.assert(String.isNotBlank(returnValue.message));

		returnValue = new x7sProductReturn(true, 'message', 'value');
		System.assertNotEquals(null, returnValue);
		System.assertEquals(true, returnValue.status);
		System.assertEquals('message', returnValue.message);
		System.assertEquals('value', returnValue.value);
	}
}