/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Batch_ConstructConnectLogsCleanup implements Database.Batchable<SObject>, System.Schedulable {
    global Batch_ConstructConnectLogsCleanup() {

    }
    global void execute(System.SchedulableContext SC) {

    }
    global void execute(Database.BatchableContext BC, List<rdcc__Construct_Connect_Log__c> logs) {

    }
    global void finish(Database.BatchableContext info) {

    }
    global Database.QueryLocator start(Database.BatchableContext info) {
        return null;
    }
}
