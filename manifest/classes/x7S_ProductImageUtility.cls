/**
 * Created By ankitSoni on 05/27/2020
 */
public with sharing class x7S_ProductImageUtility {
    
    final static String NAMESPACE    = 'CCRZ';

    /**
     * @return : Product Image url
     */
    public static List<String> getImageSrc(List<Map<String,Object>> mediaList, String imageTarget)
	{

		List<String> imageSrc = new List<String>();
		System.debug('Media List size-' + mediaList.size());

		for (Integer mediaPos = 0; mediaPos < mediaList.size(); mediaPos++)
		{
			String mediaType = (String) mediaList[mediaPos].get('mediaType');
			if (mediaType == imageTarget)
			{
				String mediaSource = (String) mediaList[mediaPos].get('productMediaSource');

                if(mediaSource == 'Static Resource'){
                    imageSrc.add('/resource/' + NAMESPACE + '__'
                    + (String) mediaList[mediaPos].get('staticResourceName')+ '/'
                    + (String) mediaList[mediaPos].get('filePath'));
                }
                else {
                    imageSrc.add((String) mediaList[mediaPos].get('URI'));  
                }
				
			}
		}

		return imageSrc;
	}

}