public class Pentair_ERICOWebService {
    
    public static String WEBSERVICE_URL = 'https://api.erico.com/pops/';
    public static String WEBSERVICE_USERNAME = 'sapops';
    public static String WEBSERVICE_PASSWORD = 'gjf^!J91WKJ2x';
    public static String ORDERSTATUS_URL = 'orderstatus/';       
    public static String CLAIMS_URL = 'claims/';
    public static String GENERAL_ACCT_URL = 'general/';
    public static String DEFAULT_SEARCH_STATUS = 'a';
    public static String URLPATH = '';
    
    public XmlDom.Element getClaimDetail(String claimNumber) {
        
        System.debug('xx WEBSERVICE_URL : ' + WEBSERVICE_URL);
            
        URLPATH = WEBSERVICE_URL + CLAIMS_URL + 'claim/' +  EncodingUtil.urlEncode(claimNumber, 'UTF-8');
        
        System.debug('xx URLPATH ' + URLPATH);
        
        String xml = '';                
        if(Test.isRunningTest())
        {
            xml = '<ResultSet>' +
                  '<ClaimNumber>176999</ClaimNumber>' +
                  '<CompanyName>AL KHAYARIN SWITCHGEAR</CompanyName>' +
                  '<CustomerNumber>129746</CustomerNumber>' +
                  '<Contact>Amir Shahzad</Contact>' +
                  '<ClaimDate>2011-12-08</ClaimDate>' +
                  '<SourceDocument>IN 11070576</SourceDocument>' +
                  '<OpenedBy>BZARANS</OpenedBy>' +
                  '<OpenedLocation>ETN</OpenedLocation>' +
                  '<AssignedTo>BZARANS</AssignedTo>' +
                  '<Line>' +
                    '<ReasonCode>WL-Wrongly Labeled/Loaded</ReasonCode>' +
                    '<Release>0</Release>' +
                    '<Shipment>1</Shipment>' +
                    '<LineNumber>1</LineNumber>' +
                    '<PartNumber>563900</PartNumber>' +
                    '<PartDescription>BD 80-100A 6 TER</PartDescription>' +
                  '</Line>' +
                '</ResultSet>';
        }
        else
        {
            xml = callRESTService(URLPATH);
        }
    
        XmlDom doc = new XmlDom(xml); 
        XmlDom.Element resultSet = doc.getElementByTagName('ResultSet');
        
        if (resultSet != null)
        {
            return resultSet;
        }
        else 
            return null;
    }
    
    public XmlDom.Element getClaimHeader(String customerNumber, String sourceSystem, String search,
        String searchStatus, String rma, String searchDate) {
        
        System.debug('xx WEBSERVICE_URL : ' + WEBSERVICE_URL);
            
        if (sourceSystem == null)
            sourceSystem = '01';    
        if (searchStatus == null)
            searchStatus = '0';
        if (searchDate == null)
            searchDate = datetime.now().format('yyyy-MM-dd');
        if (rma == null)
            rma = 'A';
            
        URLPATH = WEBSERVICE_URL + CLAIMS_URL + 'headers/' + 
            EncodingUtil.urlEncode(sourceSystem, 'UTF-8') + '/' +        
            EncodingUtil.urlEncode(customerNumber, 'UTF-8') + '?status=' + 
            EncodingUtil.urlEncode(searchStatus, 'UTF-8') + '&rma=' + 
            EncodingUtil.urlEncode(rma, 'UTF-8') + '&date=' + 
            EncodingUtil.urlEncode(searchDate, 'UTF-8');
        
        System.debug('xx URLPATH ' + URLPATH);
            
        /** - Future
        if (search != null && search != '') {
            URLPATH += '&search=' + EncodingUtil.urlEncode(search.trim(), 'UTF-8');
        }
        **/
        
        String xml = '';                
        if(Test.isRunningTest())
        {
            xml = '<ResultSet>' +
                  '<SourceSystem>02</SourceSystem>' +
                  '<CustomerNumber>129746</CustomerNumber>' +
                  '<SearchFilter />' +
                  '<StatusFilter>A</StatusFilter>' +
                  '<DateFilter>2018-03-19</DateFilter>' +
                  '<NumHeaders>3</NumHeaders>' +
                  '<Header>' +
                    '<OrderNumber>D32675</OrderNumber>' +
                    '<OrderDate>2011-12-08</OrderDate>' +
                    '<PurchaseOrder>KGS-11S-152(R)</PurchaseOrder>' +
                    '<Status>Complete</Status>' +
                    '<HoldCode />' +
                  '</Header>' +
                '</ResultSet>';
        }
        else
        {
            xml = callRESTService(URLPATH);
        }
            
        XmlDom doc = new XmlDom(xml); 
        XmlDom.Element resultSet = doc.getElementByTagName('ResultSet');
        
        if (resultSet != null)
        {
            return resultSet;
        }
        else 
            return null;
    }
    
    public XmlDom.Element getOrder(String orderNumber, String sourceSystem) {
        System.debug('xx WEBSERVICE_URL : ' + WEBSERVICE_URL);
            
        if (sourceSystem == null)
            sourceSystem = '01';    
            
        URLPATH = WEBSERVICE_URL + ORDERSTATUS_URL + 'order/' + 
            EncodingUtil.urlEncode(sourceSystem, 'UTF-8') + '/' +        
            EncodingUtil.urlEncode(orderNumber, 'UTF-8');
        
        System.debug('xx URLPATH ' + URLPATH);
        
        String xml = '';                
        if(Test.isRunningTest())
        {
            xml = '<ResultSet>' +
                  '<OrderNumber>1</OrderNumber>' +
                '<OrderDate>2018-03-19</OrderDate>' +
                '<CustomerNumber>1</CustomerNumber>' +
                '<PurchaseOrder>1</PurchaseOrder>' +
                '<HeaderStatus>1</HeaderStatus>' +
                '<CompanyName>1</CompanyName>' +
                '<Address1>1</Address1>' +
                '<Address2>1</Address2>' +
                '<Address3>1</Address3>' +
                '<City>1</City>' +
                '<StateProvince>1</StateProvince>' +
                '<PostalCode>1</PostalCode>' +
                '<HasShipments>1</HasShipments>' +
                '<HoldCode>1</HoldCode>' +
                  '<Line>' +
                    '<LineNumber>1</LineNumber>' +
                    '<PartNumber>1</PartNumber>' +
                    '<QuantityOrdered>1</QuantityOrdered>' +
                    '<QuantityReserved>1</QuantityReserved>' +
                    '<QuantityShipped>1</QuantityShipped>' +
                    '<LineStatus>1</LineStatus>' +
                    '<PromisedShipDate>2018-03-19</PromisedShipDate>' +
                  '</Line>' +
                '</ResultSet>';
        }
        else
        {
            xml = callRESTService(URLPATH);
        }
    
        XmlDom doc = new XmlDom(xml); 
        XmlDom.Element resultSet = doc.getElementByTagName('ResultSet');
        
        if (resultSet != null)
        {
            return resultSet;
        }
        else 
            return null;
    }
    
    public XmlDom.Element getOrderHeaders(String customerNumber, String sourceSystem,
        String search, String searchStatus, String searchDate) {
        
        System.debug('xx WEBSERVICE_URL : ' + WEBSERVICE_URL);
            
        if (sourceSystem == null)
            sourceSystem = '01';    

        if (searchStatus == null)
            searchStatus = DEFAULT_SEARCH_STATUS;
        if (searchDate == null)
            searchDate = datetime.now().format('yyyy-MM-dd');
            
        URLPATH = WEBSERVICE_URL + ORDERSTATUS_URL + 'headers/' + 
            EncodingUtil.urlEncode(sourceSystem, 'UTF-8') + '/' +        
            EncodingUtil.urlEncode(customerNumber, 'UTF-8') + '?status=' + 
            EncodingUtil.urlEncode(searchStatus, 'UTF-8') + '&date=' + 
            EncodingUtil.urlEncode(searchDate, 'UTF-8');
            
        /** - Future
        if (search != null && search != '') {
            URLPATH += '&search=' + EncodingUtil.urlEncode(search.trim(), 'UTF-8');
        }
        **/
        
        String xml = '';                
        if(Test.isRunningTest())
        {
            xml = '<ResultSet>' +
                  '<SourceSystem>02</SourceSystem>' +
                  '<CustomerNumber>129746</CustomerNumber>' +
                  '<SearchFilter />' +
                  '<StatusFilter>A</StatusFilter>' +
                  '<DateFilter>2018-03-19</DateFilter>' +
                  '<NumHeaders>3</NumHeaders>' +
                  '<Header>' +
                    '<OrderNumber>D32675</OrderNumber>' +
                    '<OrderDate>2011-12-08</OrderDate>' +
                    '<PurchaseOrder>KGS-11S-152(R)</PurchaseOrder>' +
                    '<Status>Complete</Status>' +
                    '<HoldCode />' +
                  '</Header>' +
                '</ResultSet>';
        }
        else
        {
            xml = callRESTService(URLPATH);
        }
    
        XmlDom doc = new XmlDom(xml); 
        XmlDom.Element resultSet = doc.getElementByTagName('ResultSet');
        
        if (resultSet != null)
        {
            return resultSet;
        }
        else 
            return null;
    }
    
    public XmlDom.Element getShipment(String orderNumber, String sourceSystem) {
        
        if (orderNumber != null) {

            if(sourceSystem == null)
                sourceSystem = '01';              
            
            URLPATH = WEBSERVICE_URL + ORDERSTATUS_URL + 'shipments/' + 
                EncodingUtil.urlEncode(sourceSystem, 'UTF-8') + '/' +        
                EncodingUtil.urlEncode(orderNumber, 'UTF-8');
            
            String xml = '';                
            if(Test.isRunningTest())
            {
                xml = '<ResultSet>' +
                      '<SourceSystem>02</SourceSystem>' +
                      '<CustomerNumber>129746</CustomerNumber>' +
                      '<SearchFilter />' +
                      '<StatusFilter>A</StatusFilter>' +
                      '<DateFilter>2018-03-19</DateFilter>' +
                      '<NumHeaders>3</NumHeaders>' +
                      '<Header>' +
                        '<OrderNumber>D32675</OrderNumber>' +
                        '<OrderDate>2011-12-08</OrderDate>' +
                        '<PurchaseOrder>KGS-11S-152(R)</PurchaseOrder>' +
                        '<Status>Complete</Status>' +
                        '<HoldCode />' +
                      '</Header>' +
                    '</ResultSet>';
            }
            else
            {
                xml = callRESTService(URLPATH);
            }    
            
            XmlDom doc = new XmlDom(xml); 
            XmlDom.Element resultSet = doc.getElementByTagName('ResultSet');
            if (resultSet != null) {
                return resultSet;                
            } else      
                return null;    
        }
        return null;
    }

    public String callRESTService(String url) 
    {    
        String returnBody = '';
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        Blob headerValue = Blob.valueOf( WEBSERVICE_USERNAME + ':' + WEBSERVICE_PASSWORD );
        String authorizationHeader = 'BASIC ' +
        EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setMethod('GET');
        req.setEndpoint(url); 
        try {
            if(Test.isRunningTest())
            {
                returnBody = '<ResultSet></ResultSet>';
            }
            else
            {
                HTTPResponse resp = http.send(req);       
                        
                if (resp.getStatusCode() == 200) {
                    returnBody = resp.getBody();
                } else if (resp.getStatusCode() == 400) {
                    //throw new BadRequestException();        
                } else if (resp.getStatusCode() == 404) {
                    //throw new NotFoundException();
                } else if (resp.getStatusCode() == 503) {
                    //throw new ServiceUnavailableException();
                } else
                    returnBody = '';
            }
        } catch (Exception e) {
            returnBody = '';
        }
        
        return returnBody;
    }
}