/* ********************************************
Developer -     Apurva Prasade
Class -         nVent_ExceptionUtility
Description -   Exception handeling class 

Developer   Date        Version
Apurva P    06-08-2019  V1.0

********************************************** */

public class nVent_ExceptionUtility {
    
	public static void createLog(Exception e)
    {
        Exception_Log__c ex = new Exception_Log__c();
        ex.Error_Message__c = e.getMessage();
        ex.Stack_Trace_String__c = e.getStackTraceString();
        ex.Line_Number__c = e.getLineNumber();
        ex.Cause__c = e.getTypeName();
        insert ex;
    }
}