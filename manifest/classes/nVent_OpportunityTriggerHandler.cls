/* ********************************************
Developer -     Apurva Prasade
Class -         nVent_OpportunityTriggerHandler
Description -   Trigger handler for Opportunity trigger

Developer   Date        Version
Apurva P    15-07-2019  V1.0

********************************************** */

public class nVent_OpportunityTriggerHandler {
    
    public static void beforeinsertUtil(list<Opportunity> listNewOpp)
    {
    }

    public static void beforeUpdateUtil(list<Opportunity> listNewOpp,map<Id, Opportunity> mapOldOpp)
    {
        beforeUpdateAddError(listNewOpp,mapOldOpp);
    }

    public static void afterinsertUtil(list<Opportunity> listNewOpp)
    {
        Pentair_UpdateParentFlagHelper UpdateParentOpportunity = new Pentair_UpdateParentFlagHelper();
        UpdateParentOpportunity.InsertredOrDeletedOpportunity(listNewOpp);
    }

    public static void afterUpdateUtil(list<Opportunity> listNewOpp,map<Id, Opportunity> mapOldOpp)
    {
        Pentair_UpdateParentFlagHelper UpdateParentOpportunity = new Pentair_UpdateParentFlagHelper();
        UpdateParentOpportunity.UpdatedOpportunity(listNewOpp, mapOldOpp);
        afterUpdatePopulateSubvertical(listNewOpp,mapOldOpp);
    }

    public static void afterDeleteUtil(list<Opportunity> listOldOpp)
    {
        Pentair_UpdateParentFlagHelper UpdateParentOpportunity = new Pentair_UpdateParentFlagHelper();
        UpdateParentOpportunity.InsertredOrDeletedOpportunity(listOldOpp);
    }

    public static void beforeUpdateAddError(list<Opportunity> listNewOpp, map<Id, Opportunity> mapOldOpp)
    {
        //map to keep track of the contact_required = 1
        Map<String, Opportunity> oppy_contact = new Map<String, Opportunity>();

        //Trigger.new is an array of opportunities 
        //and adds any that have Contact_Required = 1 to the oppy_contact Map
        for (Integer i = 0; i < listNewOpp.size(); i++) {
            if (listNewOpp[i].contact_required__c == 1) 
                oppy_contact.put(listNewOpp[i].id,listNewOpp[i]);
        }

        //map to keep track of the opportunity contact roles
        map<Id, OpportunityContactRole> oppycontactroles = new map<Id, OpportunityContactRole>();

        //select OpportunityContactRoles for the opportunities with contact role required 
        List<OpportunityContactRole> roles = [select OpportunityId, IsPrimary from OpportunityContactRole
            where (OpportunityContactRole.IsPrimary = True and OpportunityContactRole.OpportunityId
            in :oppy_contact.keySet())];        

        for (OpportunityContactRole ocr : roles)
            oppycontactroles.put(ocr.OpportunityId,ocr);
            
        // Loop through the opportunities and check if they exists in the contact roles map or contact role isn't required    
        for (Opportunity oppy : listNewOpp) 
        {
            Opportunity oldRFQ = mapOldOpp.get(oppy.ID);
            if(oppy.StageName !=  oldRFQ.StageName)
            {
                if  (oppy.contact_required__c == 1 && !oppycontactroles.containsKey(oppy.id) && oppy.Primary_Contact__c == null)
                    oppy.addError('To move past Establish, a Primary Contact is required.');       
            }
        } 
    }

    /*public static void afterUpdateParentOpportunity(list<Opportunity> listNewOpp, map<Id, Opportunity> mapOldOpp)
    {
        try {
            Pentair_UpdateParentFlagHelper UpdateParentOpportunity = new Pentair_UpdateParentFlagHelper();
            UpdateParentOpportunity.InsertredOrDeletedOpportunity(trigger.new);
            UpdateParentOpportunity.UpdatedOpportunity(trigger.new, Trigger.oldMap);
            if(trigger.isDelete)
            {
                UpdateParentOpportunity.InsertredOrDeletedOpportunity(trigger.old);
            }
        }
        catch (Exception e) 
        {
            System.debug(e);
        }
    }*/

    public static void afterUpdatePopulateSubvertical(list<Opportunity> listNewOpp, map<Id, Opportunity> mapOldOpp)
    {
        map<Id, list<Opportunity>> mapAccOpp = new map<Id, list<Opportunity>>();
        list<Opportunity> listOpportunity = new list<Opportunity>(); 
        set<Id> setAccId = new set<Id>();
        list<Account> listAccToUpdate = new list<Account>();
        for(Opportunity obj : listNewOpp)
        {
            if(obj.StageName != mapOldOpp.get(obj.Id).StageName && obj.StageName == 'Closed Won')
            {
                listOpportunity.add(obj);
                setAccId.add(obj.AccountId);
            }
        }

        for(Opportunity obj : [Select Id, Opportunity_Subvertical_1__c, AccountId, StageName from Opportunity where AccountId in: setAccId])
        {
            if(mapAccOpp.containsKey(obj.AccountId))
            {
                list<Opportunity> listTemp = mapAccOpp.get(obj.AccountId);
                listTemp.add(obj);
                mapAccOpp.put(obj.AccountId,listTemp);
            }
            else 
            {
                list<Opportunity> listTemp = new list<Opportunity>();
                listTemp.add(obj);
                mapAccOpp.put(obj.AccountId,listTemp);
            }
        }

        for(Account obj : [Select Id,Closed_Won_Subverticals__c from Account where Id in: mapAccOpp.keySet()])
        {
            if(mapAccOpp.containsKey(obj.Id))
            {
                obj.Closed_Won_Subverticals__c = '';
                System.debug('obj '+obj.Closed_Won_Subverticals__c);
                if(mapAccOpp.get(obj.Id).size() > 0)
                {
                    for(Opportunity opp : mapAccOpp.get(obj.Id))
                    {
                        if(opp.Opportunity_Subvertical_1__c != null && opp.StageName == 'Closed Won')
                        {
                            if(!obj.Closed_Won_Subverticals__c.contains(opp.Opportunity_Subvertical_1__c))
                            {
                                obj.Closed_Won_Subverticals__c = obj.Closed_Won_Subverticals__c+opp.Opportunity_Subvertical_1__c+',';
                                System.debug('obj '+obj.Closed_Won_Subverticals__c);
                            }
                        }
                    }  
                }
                obj.Closed_Won_Subverticals__c = obj.Closed_Won_Subverticals__c.removeEnd(',');
                System.debug('obj '+obj.Closed_Won_Subverticals__c);
            }
            listAccToUpdate.add(obj);
        }
        if(listAccToUpdate.size() > 0)
            update listAccToUpdate;
    }
}