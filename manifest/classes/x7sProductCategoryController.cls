/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/24/19.
 */

public with sharing class x7sProductCategoryController
{
	@AuraEnabled
	public static List<x7sProductCategory> getCategories(String setting)
	{
		x7sProductSettings  settings  = new x7sProductSettings(setting);
		Boolean             showEmpty = settings.showEmptyCategories;

		List<x7sProductCategory> categories = new List<x7sProductCategory>();

		Map<String, Object> catTreeRet = ccrz.ccAPICategory.getTree(new Map<String, Object>{
			ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION
		});

		// Get Category Tree from the return data
		List<Object> treeData = (List<Object>)catTreeRet.get(ccrz.ccAPICategory.CATEGORYTREE);
		System.debug('Tree Data: ' + treeData);

		if (treeData.size() > 0)
		{
			addCategories(categories, treeData, showEmpty);
		}

		System.debug('Categories:');
		System.debug(categories);

		return categories;
	}

	@TestVisible
	private static void addCategories(List<x7sProductCategory> categories, List<Object> items, Boolean showEmpty)
	{
		for (Object catEntry : items)
		{
			Map<String, Object> catItem  = (Map<String, Object>) catEntry;

			if (!showEmpty)
			{
				Decimal productCount = (Decimal)catItem.get('productCount');
				if (productCount == null || productCount == 0) { continue;}
			}

			x7sProductCategory  category = new x7sProductCategory(
				(String) catItem.get('sfid'),
				(String) catItem.get('name')
			);

			categories.add(category);

			if (catItem.get('children') != null)
			{
				category.children = new List<x7sProductCategory>();
				addCategories(category.children, (List<Object>) catItem.get('children'), showEmpty);
			}
		}
	}
}