global class updateRFQXML implements Database.Batchable<sObject>{

   global Database.QueryLocator start(Database.BatchableContext BC){
      String Query = '';
	  if(Test.isRunningTest()){
		Query = 'select id from RFQ_XML__c LIMIT 1';
	  } else
		Query = 'select id from RFQ_XML__c';
	  return Database.getQueryLocator(Query);
   }

   global void execute(Database.BatchableContext BC, List<RFQ_XML__c> scope){
     
     List<RFQ_XML__c> rfqLines = new List<RFQ_XML__c>();
     for(RFQ_XML__c r : scope){
         RFQ_XML__c rf = new RFQ_XML__c(id=r.ID);
         rfqLines.add(rf);
     }
     update rfqLines;
    }

   global void finish(Database.BatchableContext BC){
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'abhilash.tallur@tredence.com'};
        mail.settoAddresses(toAddresses);
        mail.setSenderDisplayName('nVent RFQ XML');
        mail.setSubject('RFQ XML Mock Update'); 
        
        mail.setPlainTextBody('Hi,\n\n Completed the RFQ XML mock Update. \n\nRegards\n nVent Admin');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        
   }
}