//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: Tag_Dup_Contact_Helper	                                                                                    //
// 	Purpose: Helper Class for Batch Class Tag_Dup_Contact_Helper															//						                        //
//  Created By: Tredence																									//			
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//



public class Tag_Dup_Contact_Helper 
{
    //Method for Updating Fields in Contact which are tagged as duplicate
    public static List<Contact> UpdateContacts(List<Tag_Dup_Contact_Iterable.DuplicatesList> records,map<Id,Contact> Contactsmap)
    {
        //creating a map for accounts for checking if account related to contact is not a partner account
        set<Id> AccountIds=new set<Id>();
        map<Id,Account> accountsmap=new map<Id,Account>();
        
        List<Contact> contacts =[Select ID,AccountID from Contact where Id In:Contactsmap.KeySet()];
        List<Contact> contactstoupdate=new list<Contact>();
        
        for(Contact c:contacts)
        {
            AccountIds.add(c.AccountId);
        }
        List<Account> Accounts=[Select Id,IsPartner from Account where Id In: AccountIds];
        for(Account a:Accounts)
        {
            accountsmap.put(a.Id,a);
        }
        
        //updating fields if account related to contact is not partner
        for(Tag_Dup_Contact_Iterable.DuplicatesList dup: records)
        {
            Contact con=Contactsmap.get(dup.dupContactId);
            Contact mas=Contactsmap.get(dup.MasterContactId);
            Account acc=accountsmap.get(con.AccountId);
            Account masacc=accountsmap.get(mas.AccountId);
            if(acc.IsPartner==False && masacc.IsPartner==False)
            {
                if(con.FirstName!=null)
                {
                    string FirstNametoupdate=con.FirstName;
                    con.FirstName='Dup['+FirstNametoupdate+'';
                    string LastNametoupdate=con.LastName;
                    con.LastName=''+LastNametoupdate+']';
                }
                else
                {
                    string LastNametoupdate=con.LastName;
                    con.LastName='Dup['+LastNametoupdate+']';
                }
                con.DeDuped_on__c=Date.today();
                con.IsDuplicate__c=True;
                con.Master_Contact__c=dup.MasterContactId;
                contactstoupdate.add(con);
            }
        }
        return contactstoupdate;
    }
    
    //Method for sending notification email about records processed
    public static void Email_StatusOfbatch(string finalstr,integer passed,integer failed, integer total)
    {
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= 'DebugLogs.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        
        
        Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
        string[] toAddress=new string[]{'pranshu.agarwal@tredence.com','arun.kumar@tredence.com','nvent_sfdc@tredence.com'};
        mail.setToAddresses(toAddress);
        mail.setSubject('Status of records processed');
        mail.setPlainTextBody('Hello All, \n\nScheduled Contact De-Duplication is completed. \n Total records processed ' + total + ' with '+ failed +' records failed and ' + passed +' records passed. \n\nRegards\nnVent SFDC');
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        
    }
}