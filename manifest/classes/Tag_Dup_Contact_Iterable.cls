//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: Tag_Dup_Contact_Iterable	                                                                                //
// 	Purpose: Iterable for custom setting Duplicate_Contact__c											                    //
//  Created By: Tredence																									//			
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//


global class Tag_Dup_Contact_Iterable implements Iterator<DuplicatesList> 
{
    global List<DuplicatesList> duplicates {get;set;}
    global Integer i {get; set;}
        global boolean hasNext()
    {
        if(i>=duplicates.size())
            return false;
        else
            return True;
    }
    global DuplicatesList next()
    { 
       i++; 
       return duplicates[i-1]; 
   }
    public Tag_Dup_Contact_Iterable() 
    {   	 
        duplicates = new list<DuplicatesList>();
        Date date_today=Date.today();
        date_today=date_today.addDays(-2);
        list<Duplicate_Contact__c> dupcon=[Select Name, Master_Contact_ID__c,Tagged__c from Duplicate_Contact__c where Tagged__c=False AND CreatedDate>=:date_today];
        i=0;
        
        if(!dupcon.isEmpty())
        {
        	for(Duplicate_Contact__c dup : dupcon)
            {	 	
                    //adding data to our iterator using constructor
                	duplicates.add(new DuplicatesList(dup.Name,dup.Master_Contact_ID__c,dup.Tagged__c));
            }
        } 
    }
       
       //wrapper class
       global class DuplicatesList 
       {
   
        global String dupContactId{get;set;}
        global String MasterContactId{get;set;}
        global boolean Tagged{get;set;}
        
        //creating constructor   
        global DuplicatesList(String dupContactId, String MasterContactId,boolean Tagged)
        {
        
            this.dupContactId = dupContactId;
            this.MasterContactId = MasterContactId; 
            this.Tagged =Tagged;
        }
}
}