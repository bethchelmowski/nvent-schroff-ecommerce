/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/2/19.
 */

public with sharing class x7sProductListModel
{
	@AuraEnabled
	public List<x7sProductModel> items { get; set; }

	@AuraEnabled
	public Integer totalCount, pageSize;

//	@AuraEnabled
//	public Map<String, Object> currentIndex { get; set; }

	@AuraEnabled
	public Boolean pageHasMore, hideImages, hideAddToCart, hideUnitPrice, navigateToVfPage;

	@AuraEnabled
	public List<String> quantityList { get; set; }

	@AuraEnabled
	public x7sProductActionSet actionSet;

	public x7sProductListModel()
	{
		this.items              = new List<x7sProductModel>();
		this.totalCount         = 0;
		this.pageSize           = 0;
		this.pageHasMore        = false;
		this.hideImages         = false;
		this.hideAddToCart      = false;
		this.hideUnitPrice      = false;
		this.navigateToVfPage   = false;
		this.quantityList       = null;
		this.actionSet = null;
	}

	public x7sProductModel addProduct(x7sProductModel item)
	{
		this.items.add(item);
		return item;
	}

	public void updateSettings(x7sProductSettings settings)
	{
		this.hideAddToCart      = settings.hideAddToCart;
		this.hideImages         = settings.hideImages;
		this.hideUnitPrice      = settings.hideUnitPrice;
		this.navigateToVfPage   = settings.navigateToVfPage;
		this.quantityList       = settings.quantityList;
		this.actionSet          = settings.actionSet;
	}

	public void Dump(String title) {
		System.debug('Dump pc_listModel: ' + title);
		System.debug('  model.total        : ' + this.totalCount);
		System.debug('  model.pageSize     : ' + this.pageSize);
		System.debug('  model.hasMore      : ' + this.pageHasMore);
		System.debug('  model.items        : ' + this.items.size());
		System.debug('  model.quantityList : ' + this.quantityList);

		if (this.actionSet != null)
		{
			this.actionSet.Dump('action list');
		}

		if (items.size() > 0)
		{
			Integer count = 1;

			for(x7sProductModel item: this.items)
			{
				item.dumpModel('item ' + count);
				count += 1;
			}
		}
	}
}