public class Pentair_ERICO_Claims {
    public String sourcesystem {get; set;}
    public String id {get; set;}
    public String oid {get; set;}
    public String clid {get; set;}
    public String cid {get; set;}
    
    public class ClaimMainHeaderRecord
    {
        public String claimNumber{set; get;}
        public String claimDate {set; get;}
        public String primaryReasonCode {set; get;}
        public String rma {set; get;}
        public String status {set; get;}
        public String onAccount {set; get;}
    }
    
    public class ClaimHeader
    {
        public String claimNumber {set; get;}
        public String companyName  {set; get;}
        public String customerNumber  {set; get;}
        public String contact  {set; get;}
        public String claimDate  {set; get;}
        public String sourceDocument  {set; get;}
        public String enteredBy {set; get;}
        public String location  {set; get;}
        public String assignedTo  {set; get;}
    }
    
    public class ClaimLineItems
    {
        public String reasonCode {set; get;}
        public String release {set; get;}
        public String shipment {set; get;}
        public String lineNumber {set; get;}
        public String partNumber {set; get;}
        public String partDescription {set; get;}
    }
    
    public String getSfIdQueryParam() {
        String sfId = ApexPages.currentPage().getParameters().get('id');
        return sfId == null ? '' : '&id=' + sfId;
    }
    
    Pentair_ERICOWebService webServiceController = new Pentair_ERICOWebService();
    
    public list<ClaimHeader> lstClaimDetail {set; get;}
    public list<ClaimLineItems> lstClaimLineItems {set; get;}
    public list<ClaimMainHeaderRecord> lstClaimMainHeader{set; get;}
    
    public void getClaimHeaderRecord()
    {
        this.cid = ApexPages.CurrentPage().getParameters().get('cid');
        this.sourcesystem = ApexPages.CurrentPage().getParameters().get('ssid');
         
        lstClaimMainHeader = new list<ClaimMainHeaderRecord>();
        
        XmlDom.Element xmlClaimHeader = webServiceController.getClaimHeader(cid, sourcesystem, null, null, null, null);
        
        if (xmlClaimHeader != null)
        {
            XmlDom.Element[] elements = xmlClaimHeader.getElementsByTagName('Header');
            if (elements != null) {
                for (XmlDom.Element lineElement : elements)
                {
                    ClaimMainHeaderRecord xLine = new ClaimMainHeaderRecord();
                    
                    xLine.claimNumber = lineElement.getValue('claimNumber');
                    xLine.claimDate = lineElement.getValue('claimDate');
                    xLine.primaryReasonCode = lineElement.getValue('primaryReasonCode');
                    xLine.Status = lineElement.getValue('Status');
                    xLine.Status = lineElement.getValue('rma');
                    xLine.onAccount = lineElement.getValue('onAccount');
                    
                    lstClaimMainHeader.add(xLine);
                }                 
            }
        }
    }
    
    public void getClaimDetails()
    {
        lstClaimDetail = new list<ClaimHeader>();
        lstClaimLineItems = new list<ClaimLineItems>();
       
        this.clid = ApexPages.CurrentPage().getParameters().get('clid');
        this.id = ApexPages.CurrentPage().getParameters().get('id');
        
        List<String> lstMACPAC;
        if (clid.contains(';'))
        {
        	lstMACPAC = clid.split(';');
            System.debug('lstMACPAC Fields: ' + lstMACPAC);
        }
        else
            lstMACPAC[0] = clid;
        
        System.debug('clid Fields: ' + clid);
        
        for (String singleId : lstMACPAC) 
        {        
            List<String> lstRecords;
            lstRecords = singleId.split('\\.');
            
            System.debug('Record 0: ' + singleId);
            
            sourcesystem = lstRecords[1];    
        
        XmlDom.Element xmlClaimHeader = webServiceController.getClaimDetail(lstRecords[0]);
        
        if (xmlClaimHeader != null)
        {
            ClaimHeader cHeader = new ClaimHeader();
            
            System.debug('xx Claim Number: ' + xmlClaimHeader.getValue('ClaimNumber'));
            
        	cHeader.claimNumber = xmlClaimHeader.getValue('ClaimNumber');
            cHeader.companyName = xmlClaimHeader.getValue('CompanyName');
            cHeader.customerNumber = xmlClaimHeader.getValue('CustomerNumber');
            cHeader.contact = xmlClaimHeader.getValue('Contact');
            cHeader.claimDate = xmlClaimHeader.getValue('ClaimDate');
            cHeader.sourceDocument = xmlClaimHeader.getValue('SourceDocument');
            cHeader.enteredBy = xmlClaimHeader.getValue('OpenedBy');
            cHeader.location = xmlClaimHeader.getValue('OpenedLocation');
            cHeader.assignedTo = xmlClaimHeader.getValue('AssignedTo');
            
            lstClaimDetail.add(cHeader);
            
            XmlDom.Element[] lines = xmlClaimHeader.getElementsByTagName('Line');
            if (lines != null) {
                for (XmlDom.Element lineElement : lines) {
                    ClaimLineItems xLine = new ClaimLineItems();
                    
                    xLine.reasonCode = lineElement.getValue('ReasonCode');
                    xLine.release = lineElement.getValue('Release');
                    xLine.shipment = lineElement.getValue('Shipment');
                    xLine.lineNumber = lineElement.getValue('LineNumber');
                    xLine.partNumber = lineElement.getValue('PartNumber');
                    xLine.partDescription = lineElement.getValue('PartDescription');
                    
                    lstClaimLineItems.add(xLine);
                }
            }
        }        
        }
    }
       
	public PageReference cancel()
 	{       
        System.debug('CID: ' + ApexPages.CurrentPage().getParameters().get('cid'));
        System.debug('id: ' + ApexPages.CurrentPage().getParameters().get('id'));
        System.debug('ssid: ' + ApexPages.CurrentPage().getParameters().get('ssid'));
        
        PageReference redirectPage = Page.Pentair_ERICOMain;
        redirectPage.getParameters().put('cid',ApexPages.CurrentPage().getParameters().get('cid'));
        redirectPage.getParameters().put('id',ApexPages.CurrentPage().getParameters().get('id'));
        redirectPage.getParameters().put('ssid',ApexPages.CurrentPage().getParameters().get('ssid'));
    	redirectPage.setRedirect(true);
    	return redirectPage;
    }

}