/**
 * Created By ankitSoni on 05/28/2020
 */
public with sharing class x7S_WishListItemModel {
    @AuraEnabled
	public String id, productId, productName, productSkuCode , productDescription;
    
    @AuraEnabled
	public List<String>imageUrl;

	@AuraEnabled
    public Decimal quantity;
    
}