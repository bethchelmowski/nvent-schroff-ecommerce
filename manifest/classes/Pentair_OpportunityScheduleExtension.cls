public with sharing class Pentair_OpportunityScheduleExtension {
    public Opportunity objOpp;
    public list<OpportunityLineItemSchedule> lstOLIS {get;set;}
    public list<OpportunityLineItem> lstOpportuntyProducts {get;set;}
    public Integer rowNum{get;set;}
	public Id RecordToDelete {get; set;}
    
    public Pentair_OpportunityScheduleExtension(ApexPages.StandardController controller) {
        this.objOpp= (Opportunity)controller.getRecord();
    }
    
    public void getAllList()
    {
        getOLIS();
        getOpportunityProducts();
    }
       
    public void getOLIS(){
        lstOLIS = new list<OpportunityLineItemSchedule >();
        
        for(OpportunityLineItemSchedule objOLIS:[Select Id, ScheduleDate, Revenue,
                                                 OpportunityLineItem.Product2.Name,
                                                 Description
                                                 from OpportunityLineItemSchedule 
                                                 where OpportunityLineItem.OpportunityId =:objOpp.Id
                                                 order by scheduledate ASC] ){
            lstOLIS.add(objOLIS);    
                                                 }                                          
    }     
    
    public void getOpportunityProducts(){
        lstOpportuntyProducts = new list<OpportunityLineItem>();
        
        for(OpportunityLineItem objOLIS:[Select Id, ServiceDate,
                                                 Product2.Name,
                                                 TotalPrice
                                         from OpportunityLineItem
                                         where OpportunityId =:objOpp.Id
                                         order by ServiceDate ASC] ){
            lstOpportuntyProducts.add(objOLIS);                                             
        }
    } 
       
    public void UpdateProductScheduleDate()
    {
        List<AggregateResult> getRecordProducts = [SELECT OpportunityLineItemId
                                                   FROM OpportunityLineItemSchedule
                                                   where OpportunityLineItem.OpportunityId =:objOpp.Id
                                                   GROUP BY OpportunityLineItemId];
        
        System.debug('x Opp ID: ' + objOpp.Id );
        
        if (getRecordProducts.size() > 0)
        {
            for (AggregateResult xProducts : getRecordProducts)
            {
                String xProductId = (String)xProducts.get('OpportunityLineItemId');
                System.debug('x OpportunityLineItemId: ' + xProductId);
                
				List<OpportunityLineItemSchedule> updateRecordDate = [SELECT ScheduleDate FROM OpportunityLineItemSchedule
                                                       		  WHERE OpportunityLineItem.Id =:xProductId
                                                              ORDER BY ScheduleDate ASC];
        
                
                OpportunityLineItem updateOpp = new OpportunityLineItem(ID=xProductId);
                updateOpp.ServiceDate = updateRecordDate[0].ScheduleDate;
                update updateOpp;
            }
        }
        
    }
    
    public void SaveForm()
    {
        if (lstOLIS.size() > 0)
        	upsert lstOLIS;
    }
    
    public PageReference SaveAndRefresh() {
        SaveForm();

        UpdateProductScheduleDate();
                   
        PageReference pr = ApexPages.currentPage(); 
        pr.setRedirect(true);
        return pr;
    }
    
	public PageReference RemoveItems(){        

        System.debug('Record ID: ' + RecordToDelete);

        List<OpportunityLineItem> getRecord = [SELECT ID FROM OpportunityLineItem 
                                                     WHERE ID =: RecordToDelete];

		if (getRecord.size() > 0)     
        {
            OpportunityLineItem deleteRecords = [SELECT ID FROM OpportunityLineItem 
                                                         WHERE ID =: RecordToDelete];
            delete deleteRecords; 
        }
        
        PageReference pr = ApexPages.currentPage(); 
		pr.setRedirect(true);
		return pr;
    } 
}