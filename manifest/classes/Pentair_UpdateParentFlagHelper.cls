public class Pentair_UpdateParentFlagHelper 
{
    Private Set<Parent_Opportunity__c> SetofParenttoUpdate = new set<Parent_Opportunity__c>();
    Public void InsertredOrDeletedOpportunity(List<Opportunity> lstofOpportunity)
    {
        list<Parent_Opportunity__c> lstofParenttoUpdate = new list<Parent_Opportunity__c>();
        for(Opportunity newOpportunity:lstofOpportunity)
        {
            if (newOpportunity.Parent_Opportunity__c != null)
            {
                updatetheParentObject(newOpportunity.Parent_Opportunity__c);
            }
        }
        lstofParenttoUpdate.addAll(SetofParenttoUpdate);
        Update lstofParenttoUpdate;
    }
    Public void UpdatedOpportunity(List<Opportunity> lstNewOpportunity,Map<id,Opportunity> mapofOldOpportunity)
    {
        list<Parent_Opportunity__c> lstofParenttoUpdate = new list<Parent_Opportunity__c>();
        for (Opportunity newOpportunity: lstNewOpportunity)
        {       
            //This will check to see if the amount has changed
            Opportunity OldOpportunity = mapofOldOpportunity.get(newOpportunity.ID);
            if((newOpportunity.Amount != OldOpportunity.Amount) || 
               (newOpportunity.Channel_Duplicate__c != OldOpportunity.Channel_Duplicate__c) ||
               (newOpportunity.Parent_Opportunity__c != OldOpportunity.Parent_Opportunity__c) || (newOpportunity.StageName != OldOpportunity.StageName))
            {
                if (newOpportunity.Parent_Opportunity__c != null)
                {
                    updatetheParentObject(newOpportunity.Parent_Opportunity__c);
                }
                if((newOpportunity.Parent_Opportunity__c != OldOpportunity.Parent_Opportunity__c)
                   &&(OldOpportunity.Parent_Opportunity__c != null))
                {
                    updatetheParentObject(OldOpportunity.Parent_Opportunity__c);
                }
            }
        }
        lstofParenttoUpdate.addAll(SetofParenttoUpdate);
        update lstofParenttoUpdate;
    }
    Private void updatetheParentObject(id ParentOpportunityIDtoUpdate)
    {
        Parent_Opportunity__c aux  = new Parent_Opportunity__c(Id = ParentOpportunityIDtoUpdate); 
        aux.Update_Totals__c = true; 
        SetofParenttoUpdate.add(aux);
    }
    
}