@isTest
public class MergeDuplicateAccountBatchtest {
	public static integer scope = 4;
    public static list<Account> CreateData()
    { 
        integer i;
        integer No_of_Records_to_be_Created = scope;
  		list<Account> Create_Accounts = new list<Account>();
        
        for(i=0; i<=No_of_Records_to_be_Created;i++)
            Create_Accounts.add(new Account(name='acc'+i,BillingCountry='India',BillingCity='City'+i,BillingState='Goa',BillingStreet='Street'+i,BillingPostalCode = 'Postcode'+i));
        insert Create_Accounts;
        
        
        
        list<Account_Connection__c> Create_AccountConn = new list<Account_Connection__c>();
        for(Account accinst : Create_Accounts)
        {
            Create_AccountConn.add(new Account_Connection__c(Connected_from_Account__c=accinst.id,Connected_to_Account__c=accinst.id));
        }
        insert Create_AccountConn;
        
        list<Account_Plan__c> Create_AccountPlan = new list<Account_Plan__c>();
        for(Account accinst : Create_Accounts)
        {
            Create_AccountPlan.add(new Account_Plan__c(Account__c=accinst.id,Plan_Year__c='2018'));
        }
        insert Create_AccountPlan;
        
        /*list<Account_Role__c> Create_AccountRole = new list<Account_Role__c>();
        i=0;
        for(Account accinst : Create_Accounts)
        {
            Create_AccountRole.add(new Account_Role__c(Account__c=accinst.id,Account_Role_on_Opportunity__c='Contractor',Opportunity__c=create_opportunity[i].id));
            i++;
        }
        insert Create_AccountRole;*/
        
        list<AccountTeamMember> Create_AccountTeamMember = new list<AccountTeamMember>();
        for(Account accinst : Create_Accounts)
        {
            Create_AccountTeamMember.add(new AccountTeamMember(AccountId=accinst.id,userid=userinfo.getUserId()));
        }
        insert Create_AccountTeamMember;
        
        list<Case> Create_Case = new list<Case>();
        for(Account accinst : Create_Accounts)
        {
            Create_Case.add(new Case(AccountId=accinst.id,Electrical_Contractor_Account__c=accinst.id,General_Contractor_Account__c=accinst.id,Service_Partner__c=accinst.id,Tile_Sanitary_Contractor_Account__c=accinst.id));
        }
        insert Create_Case;
        
        list<Certification__c> Create_Certification = new list<Certification__c>();
        for(Account accinst : Create_Accounts)
        {
            Create_Certification.add(new Certification__c(Account__c=accinst.id));
        }
        insert Create_Certification;
        
        list<Contact> Create_Contact = new list<Contact>();
        for(Account accinst : Create_Accounts)
        {
            Create_Contact.add(new Contact(AccountId=accinst.id,lastname=userinfo.getLastName()));
        }
        insert Create_Contact;
        
        list<Event> Create_Event = new list<Event>();
        for(Account accinst : Create_Accounts)
        {
            Create_Event.add(new Event(Whatid=accinst.id,DurationInMinutes=10000+i,ActivityDateTime=datetime.now()));
        }
        insert Create_Event;
        
        list<RFQ__c> Create_RFQ = new list<RFQ__c>();
        for(Account accinst : Create_Accounts)
        {
            Create_RFQ.add(new RFQ__c(Account__c=accinst.id));
        }
        insert Create_RFQ;
        
        list<Task> Create_Task = new list<Task>();
        for(Account accinst : Create_Accounts)
        {
            Create_Task.add(new Task(Whatid=accinst.id));
        }
        insert Create_Task;
        
        return Create_Accounts; 
    }
    public static void createCustomsettings(List<Account>  Create_Accounts)
    {
        List<Duplicate_Accounts__c> DuplicateAccnt = new List<Duplicate_Accounts__c>();
        for(integer i=0; i<=scope;i++)
        {
            system.debug('value in i'+i+' value in No_of_Records_to_be_Created/2'+(scope/2));
            if(i<=(scope/2))
            {
            	DuplicateAccnt.add(new Duplicate_Accounts__c(name=Create_Accounts[i].id,Locked__c=False,Master_Account_ID__c=Create_Accounts[scope-i].id));
            }
        }
        insert DuplicateAccnt;
    }
    public  static  testMethod void mergeRecordsPageTest()
    {        
        list<Account> createdaccountid = CreateData();
        createCustomsettings(createdaccountid);
        Test.startTest();
        
        MergeDuplicateAccountBatch excBatch = new MergeDuplicateAccountBatch();
		Database.executeBatch(excBatch,scope);
        
        Test.stopTest();
    }
    public  static  testMethod void mergeRecordswithnoData()
    {        
        CreateData();
        Test.startTest();
        
        MergeDuplicateAccountBatch excBatch = new MergeDuplicateAccountBatch();
		Database.executeBatch(excBatch,scope);
        
        Test.stopTest();
    }
    public  static  testMethod void mergeRecordsPageTestfromScheduler()
    {        
        list<Account> createdaccountid = CreateData();
        createCustomsettings(createdaccountid);
        Test.startTest();
        
        MergeDuplicateAccBatchSch batchSch = new MergeDuplicateAccBatchSch();
        system.schedule('Account De Dupe', '0 0 13 * * ?', batchSch);
        
        Test.stopTest();
    }
}