//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: Tag_Dup_Account_Batch	                                                                                    //
// 	Purpose: Batch Class for Tagging Duplicates Account																		//				                       
//  Created By: Tredence																									//			
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//


global class Tag_Dup_Account_Batch implements Database.Batchable<Tag_Dup_Account_Iterable.DuplicatesList>,Database.Stateful {
    global integer passed=0;
    global integer failed=0;
    global integer total=0;
    global string finalstr= 'Record Id, Error Status, Error Message, Error Fields \n';
    
    global Iterable<Tag_Dup_Account_Iterable.DuplicatesList> start(Database.BatchableContext bc) 
    {
        return new Tag_Dup_Account_Call_Iterable();
    }
    
    global void execute(Database.BatchableContext bc,List<Tag_Dup_Account_Iterable.DuplicatesList> records )
    {
        
        //Creating set of Ids of Accounts and Master Accounts
        set<string> DupIds=new set<string>();
        for(Tag_Dup_Account_Iterable.DuplicatesList d: records)
        {
            DupIds.add(d.dupAccountId);
            DupIds.add(d.MasterAccounttId);
        }
        
        List<Account> accountIds=new List<Account>();
        
        accountIds=[Select Id,Name,Master_Account__c,IsDuplicate__c,DeDuped_on__c,IsPartner from Account where Id In:DupIds];
        
        List<Account> accounts=new List<Account>();
        
        //Creating map of Accounts and their Master Accounts
        Map<Id,Account> accountsmap=new map<Id,Account>(); 
        For(Account acc: accountIds)
        {
            accountsmap.put(acc.Id,acc);
        }
        
        //Returning a list of accounts after tagging using method UpdateAccounts of helper class Tag_Dup_Account_Helper
        accounts=Tag_Dup_Account_Helper.UpdateAccounts(records,accountsmap);
        total=total+accounts.size();
        
        List<Duplicate_Accounts__c> dups =[Select Name, Master_Account_ID__c,Tagged__c from Duplicate_Accounts__c where Name In:DupIds];
      
        List<Duplicate_Accounts__c> duptoupdates=new list<Duplicate_Accounts__c>();
        
        //Creating a map of custom setting
        map<string,Duplicate_Accounts__c> duplicatesmap=new map<string,Duplicate_Accounts__c>();
        for(Duplicate_Accounts__c d: dups)
        {
            duplicatesmap.put(d.Name,d);
        }
        
        
        try
        {
        //Updating the list of accounts which are tagged 
        database.saveResult[] results=database.update(accounts,false);
        
        //Checking the status of each update
        for(integer i=0;i<accounts.size();i++)
        {
           
            database.SaveResult s=results[i];
            //Tag_Dup_Account_Iterable.DuplicatesList Ids=records[i];
            Account a1=accounts[i];
            if(!s.isSuccess())
            {
                failed++;
                for(Database.Error err : s.getErrors())
                {
                    
                    string recordString = '"'+a1.Id+'","'+err.getStatusCode()+'","'+err.getMessage().escapecsv()+'","'+err.getFields() +'"\n';
                    finalstr = finalstr +recordString;
                    
                }
            }
            else
            {
                
                //Updating Tagged flag in custom setting as True
                //Duplicate_Accounts__c d1=duplicatesmap.get(Ids.dupAccountId);
                Duplicate_Accounts__c d1=duplicatesmap.get(a1.id);
                d1.Tagged__c=True;
                duptoupdates.add(d1);
                passed++;
            }
            
            
        }
        }
        catch(Exception ex)
        {
            string recordString = '"'+ +'","'+ex.getTypeName()+'","'+ex.getMessage().escapecsv()+'","'+ex.getStackTraceString() +'"\n';
            finalstr = finalstr +recordString;
            failed=failed+accounts.size();
            //failed++;
        }
        try
        {
        	if(duptoupdates.size()>0)
        	update duptoupdates;
         }
        catch(Exception ex)
        {
            system.debug('****** ERROR: '+ex.getMessage()+' Line Number '+ex.getLineNumber());
        }
        
    }
    
    global void finish(Database.BatchableContext bc)
    {      
        //Sending notification email using helper class Tag_Dup_Account_Helper
        Tag_Dup_Account_Helper.Email_StatusOfbatch(finalstr,passed,failed,total);    
    }
}