/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/2/19.
 */

public with sharing class x7sProductModel
{
	@AuraEnabled
	public String id, name, description, longDescription, productCode, productType, productCategory;

	@AuraEnabled
	public Decimal unitPrice;

	@AuraEnabled
	public List<String> imageUrl;

	@AuraEnabled
	public String externalProductUrl;

	@AuraEnabled
	public Boolean featured;

	@AuraEnabled
	public Integer sequence;

	public x7sProductModel()
	{
		this.featured           = false;
		this.sequence           = 0;
		this.externalProductUrl = '';
		this.productType        = '';
		this.imageUrl           = new List<String>();
	}

	// Utility functions
	public void dumpModel(String title)
	{
		System.debug('Dump pc_model: ' + title);
		System.debug('  id               : ' + this.id);
		System.debug('  name             : ' + this.name);
		System.debug('  product code     : ' + this.productCode);
		System.debug('  product type     : ' + this.productType);
		System.debug('  price            : ' + this.unitPrice);
		System.debug('  imageUrl         : ' + this.imageUrl);
	}
}