/**
 * Created By ankitSoni on 05/06/2020
 * Description : Response Wrapper Class for x7S_PricingRequestCallout
 */
public class x7S_PricingResponse {
    
    public String errorFlag;
	public Object errorMessage;
	public List<Items> items;

    public class Items {
		public String itemNumber;
		public String quantity;
		public String unitOfMeasure;
		public Price listPrice;
		public Price netPrice;
		public String isAvailableFlag;
		public String leadTime;
		public String discount;
		public List<Components> components;
    }
    
	public class Components {
		public String itemNumber;
		public String quantity;
		public String unitOfMeasure;
		public Price listPrice;
		public Price netPrice;
		public String discount;
	}
		
	public class Price {
		public String value;
		public String currencyCode;
    }
 
    public static x7S_PricingResponse parse(String json) {
		return (x7S_PricingResponse) System.JSON.deserialize(json, x7S_PricingResponse.class);
	}
}