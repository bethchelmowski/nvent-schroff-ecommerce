/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/4/19.
 */

public with sharing class x7sProductSpecCollection
{
	@AuraEnabled
	public List<x7sProductSpecList> specLists { get; set; }

	public x7sProductSpecCollection()
	{
		this.specLists       = new List<x7sProductSpecList>();
	}

	public void addSpec(String groupName, x7sProductSpec spec)
	{
		x7sProductSpecList targetList = containsGroup(groupName);

		if (targetList == null)
		{
			targetList = new x7sProductSpecList(groupName);
			specLists.add(targetList);
		}

		targetList.addSpec(spec);
	}

	public void sortSpecs()
	{
		specLists.sort();

		for(x7sProductSpecList specList : specLists)
		{
			specList.sortSpecs();
		}
	}
	@TestVisible
	private x7sProductSpecList containsGroup(String groupName)
	{
		x7sProductSpecList found = null;

		for (x7sProductSpecList specList : specLists)
		{
			if (specList.groupName == groupName)
			{
				found = specList;
				break;
			}
		}

		return found;
	}

	public void Dump(String title)
	{
		System.debug('x7sProductSpecCollection: ' + title);

		Integer counter = 0;
		for (x7sProductSpecList specList : specLists)
		{
			if (specList.items.size() > 0)
			{
				specList.Dump('Spec List ' + counter++);
			}
		}
	}
}