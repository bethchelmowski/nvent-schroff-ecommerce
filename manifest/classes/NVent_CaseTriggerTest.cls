@isTest
public class  NVent_CaseTriggerTest
{
	@testsetup
	public static void createData()
	{
		Profile prflst = [Select Id from Profile where Name =: 'System Administrator' limit 1];
        
        User objUser = new User();
        objUser.FirstName = 'fNameTest';
        objUser.LastName = 'lNameTest';
        objUser.Email = 'aptest@test.com';
        objUser.isActive = true;
        objUser.ProfileId = prflst.Id;
        objUser.Username = 'aptest@test.com' + System.currentTimeMillis();
        objUser.CompanyName = 'testComp';
        objUser.Title = 'Developer';
        objUser.Alias = 'testal';
        objUser.TimeZoneSidKey = 'America/Los_Angeles';
        objUser.EmailEncodingKey = 'UTF-8';
        objUser.LanguageLocaleKey = 'en_US';
        objUser.LocaleSidKey = 'en_US';
        objUser.SAP_Id__c = 'S000';
        insert objUser;
		
        Account objAccount = new Account();
        objAccount.Name = 'testCompany';
        objAccount.Subvertical__c = 'Oil and Gas';
        objAccount.BillingCountryCode = 'FR';
        objAccount.BillingStreet = 'test';
        objAccount.BillingCity = 'Ttest';
        insert objAccount;
		
        AccountTeamMember objAM1 = new AccountTeamMember();
        objAM1.AccountId = objAccount.ID;
        objAM1.TeamMemberRole = 'Hoffman Customer Care';
        objAM1.UserId = objUser.ID;
        insert objAM1;
		
        AccountTeamMember objAM2 = new AccountTeamMember();
        objAM2.AccountId = objAccount.ID;
        objAM2.TeamMemberRole = 'Schroff Customer Care';
        objAM2.UserId = objUser.ID;
        insert objAM2;

        AccountTeamMember objAM3 = new AccountTeamMember();
        objAM3.AccountId = objAccount.ID;
        objAM3.TeamMemberRole = 'Customer Care';
        objAM3.UserId = objUser.ID;
        insert objAM3;
		
		Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enclosures CC Case').getRecordTypeId();
		
		String FirstName1 = 'Test'; 
        String LastName1 = 'Con';
		
		Contact con1 = new Contact();
        con1.AccountId = objAccount.Id;
        con1.FirstName = FirstName1;
        con1.LastName=LastName1;
        con1.MailingCountry = 'Sweden';
        insert con1;
		Contact con2 = new Contact();
        con2.AccountId = objAccount.Id;
        con2.FirstName = FirstName1;
        con2.LastName=LastName1;
        con2.MailingCountry = 'Sweden';
        insert con2;
        Contact con3 = new Contact();
        con3.AccountId = objAccount.Id;
        con3.FirstName = FirstName1;
        con3.LastName=LastName1;
        con3.MailingCountry = 'Sweden';
        insert con3;
        Case caseObj1 = new Case();
        caseObj1.SuppliedName = 'Hey dude';
        caseObj1.SuppliedEmail ='anvi.kanodra@nvent.com';
        caseObj1.ContactId = con1.Id; 
        caseObj1.RecordTypeId = recordTypeId;
        caseObj1.Origin = 'Email';
		Case caseObj2 = new Case();
        caseObj2.SuppliedName = 'Hey dude';
        caseObj2.SuppliedEmail ='anvi.kanodra@nvent.com';
        caseObj2.ContactId = con1.Id; 
        caseObj2.RecordTypeId = recordTypeId;
        caseObj2.Origin = 'Email';
        Case caseObj3 = new Case();
        caseObj3.SuppliedName = 'Hey dude';
        caseObj3.SuppliedEmail ='anvi.kanodra@nvent.com';
        caseObj3.ContactId = con1.Id; 
        caseObj3.RecordTypeId = recordTypeId;
        caseObj3.Origin = 'Email';
        List<case> ListOfcases = new List<case>();
        ListOfcases.add(caseObj1);
        ListOfcases.add(caseObj2);
        ListOfcases.add(caseObj3);
        insert ListOfcases;
		
		
	}
	
	@isTest 
	public static void testCaseTrigger()
    {
        Contact con = [Select Id from Contact Limit 1];
		Case csObj = [Select Id, SuppliedName, SuppliedEmail,AccountId From Case Limit 1];
        map<Id, SObject> mapCom = new map<Id, SObject>([Select Id, SuppliedName, SuppliedEmail,AccountId From Case Limit 1]);
        NVent_CaseTriggerHandler obj = new NVent_CaseTriggerHandler();
        obj.beforeDelete(mapCom);
        obj.afterDelete(mapCom);
        obj.afterUndelete(mapCom);
        system.debug('CASe New '+csObj);
    }
}