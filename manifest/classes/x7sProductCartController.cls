/**
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

public with sharing class x7sProductCartController
{
	public static final String CART_UPDATE = 'update';
	public static final String CART_ADD    = 'add';
	public static final String CART_REMOVE = 'remove';
	public static final String CART_CLEAR  = 'clear';

	@AuraEnabled
	public static x7sProductCartList getActiveCart(String setting, String userId)
	{
		x7sProductCartList model = new x7sProductCartList();
		if (string.isBlank(userId))
		{
			userId = UserInfo.getUserId();
		}
        
		System.debug('getActiveCart: ' + userId +' ' +  ccrz.ccApi.CURRENT_VERSION);
		Map<String, Object> inputData = new Map<String, Object> {
			ccrz.ccApi.API_VERSION    => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccApiCart.ACTIVECART => true,
            ccrz.ccApiCart.BYOWNER    => userId,
			ccrz.ccApiCart.BYUSER     => userId,
			ccrz.ccApi.SIZING         => new Map<String, Object>
				{ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL}}
		};

		try {
			Map<String, Object> outputData = ccrz.ccApiCart.fetch(inputData);
			System.debug('outputData ' + outputData); 
			if (outputData.get(ccrz.ccApiCart.CART_OBJLIST) != null) {
				List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) outputData.get(ccrz.ccApiCart.CART_OBJLIST);
				
				if (outputCartList.size() > 0)
				{
					Decimal itemCount     = 0;
					model.cartEncId       = (String) outputCartList[0].get('encryptedId');
					model.totalPrice      = (Decimal) outputCartList[0].get('totalAmount');
					model.totalQuantity   = (Decimal) outputCartList[0].get('totalQuantity');

					// collect the items in the cart
					for(Map<String, Object> cartEntry : outputCartList)
					{
						List<Map<String, Object>> items = (List<Map<String, Object>>)cartEntry.get('ECartItemsS');
						for (Map<String, Object> item : items)
						{
							x7sProductCartModel cartItem = new x7sProductCartModel();

							cartItem.id         = (String)item.get('sfid');
							cartItem.productId  = (String)item.get('product');
							cartItem.quantity   = (Decimal)item.get('quantity');
							cartItem.unitPrice  = (Decimal)item.get('price');
							cartItem.unitTotal  = (Decimal)item.get('price') * (Decimal)item.get('quantity');

							if ((String)item.get('cartItemType') == 'Major')
							{
								itemCount += (Decimal) item.get('quantity');
							}

							model.items.add(cartItem);
						}
					}

					model.itemCount = itemCount;
					model.status    = true;
				}

				// resolve the products in the cart
				List<Map<String, Object>> productList = (List<Map<String, Object>>)outputData.get(ccrz.ccAPIProduct.PRODUCTLIST);
				for(Map<String, Object> productEntry : productList)
				{
					x7sProductCartModel cartItem = model.findCartItem((String)productEntry.get('sfid'));
					if (cartItem != null)
					{
						cartItem.name        = (String)productEntry.get('sfdcName');
						cartItem.productCode = (String)productEntry.get('SKU');

						List<Map<String,Object>> mediaList = (List<Map<String,Object>>)productEntry.get('EProductMediasS');
						cartItem.imageUrl  = x7sProductUtility.getImageSrc(mediaList, 'Product Search Image');
					}
					else {
						cartItem.name = 'not found';
						System.debug('cart product not found in product list: ' + (String)productEntry.get('sfid'));
					}

					System.debug('Product: ' + productEntry.get('sfid') + ' ' + productEntry.get('sfdcName'));
					System.debug('entry: ' + productEntry);
				}
			}
		} catch (Exception ex) { model.message = 'getActiveCart Exception: ' + ex.getMessage();}

		model.dump('getActiveCart', true);
		return model;
	}

	@AuraEnabled
	public static x7sProductCartList updateCartItems(
		String setting,
		List<String> skuList,
		List<String> itemList,
		List<String> oldValues,
		List<String> newValues,
		String action)
	{
		String  userId = UserInfo.getUserId();
		x7sProductCartList model = getActiveCart(setting, userId);
		Boolean success = false;

		System.debug('updateCartItems: ' + action + ', ' + skuList + ', ' + newValues + ', ' + oldValues);

		List<Map<String, Object>> newLineItems = new List<Map<String, Object>>();
		List<ccrz.ccApiCart.LineData> oldLineItems = new List<ccrz.ccApiCart.LineData>();

		if (action == CART_ADD || action == CART_UPDATE)
		{
			if (skuList != null && skuList.size() > 0)
			{
				Integer pos = 0;
				for (String sku : skuList)
				{
					newLineItems.add(new Map<String, Object>
					{
						ccrz.ccApiCart.LINE_DATA_SKU      => sku,
						ccrz.ccApiCart.LINE_DATA_QUANTITY => Integer.valueOf(newValues[pos++])
					});
				}
				System.debug('New line items: ' + newLineItems);
			}
		}

		if (action == CART_REMOVE || action == CART_UPDATE)
		{
			if (itemList != null && itemList.size() > 0)
			{
				for (String item : itemList)
				{
					ccrz.ccApiCart.LineData lineItem = new ccrz.ccApiCart.LineData();
					lineItem.sfid = item;
					oldLineItems.add(lineItem);
				}
				System.debug('Old line items: ' + oldLineItems);
			}
		}

		Map<String, Object> inputData = new Map<String, Object>
		{
			ccrz.ccApi.API_VERSION    => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccApiCart.CART_ENCID => model.cartEncId
		};

		if (action == CART_CLEAR)
		{
			Map<String, Object> clearResults = ccrz.ccApiCart.removeCart(inputData);
			success = (Boolean) clearResults.get(ccrz.ccApi.SUCCESS);

			System.debug('Clear cart: ' + success);
			System.debug('Clear messages: ' + (List<ccrz.cc_bean_Message>) clearResults.get(ccrz.ccApi.MESSAGES));
		}

		if (action == CART_UPDATE || action == CART_REMOVE)
		{
			inputData.put(ccrz.ccApiCart.LINE_DATA, oldLineItems);

			System.debug('Remove Old items: ' + inputData);

			Map<String, Object> removeResults = ccrz.ccApiCart.removeFrom(inputData);
			success = (Boolean) removeResults.get(ccrz.ccApi.SUCCESS);

			System.debug('Remove old items: ' + success);
			System.debug('Remove messages : ' + (List<ccrz.cc_bean_Message>) removeResults.get(ccrz.ccApi.MESSAGES));
		}

		if (action == CART_ADD || action == CART_UPDATE)
		{
			inputData.put(ccrz.ccApiCart.ISREPRICE,true);
			inputData.put(ccrz.ccApiCart.LINE_DATA, newLineItems);

			System.debug('Add/Update input: ' + inputData);

			Map<String, Object> addResults = ccrz.ccApiCart.addTo(inputData);
			success = (Boolean) addResults.get(ccrz.ccApi.SUCCESS);

			System.debug('Add/Update items   : ' + success);
			System.debug('Add/Update messages: ' + (List<ccrz.cc_bean_Message>)addResults.get(ccrz.ccApi.MESSAGES));
		}

		if (success)
		{
			System.debug('SUCCESS');
			model = getActiveCart(setting, userId);
		}

		return model;
	}
}