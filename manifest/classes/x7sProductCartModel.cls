/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 12/30/19.
 */

public with sharing class x7sProductCartModel
{
	@AuraEnabled
	public String id, productId, externalUrl, name, family, productCode;
	@AuraEnabled
	public List<String>imageUrl;

	@AuraEnabled
	public Decimal quantity, unitPrice, unitTotal;

	public x7sProductCartModel()
	{
	}

	public void dump(String title)
	{
		System.debug('Cart item dump: ' + title);
		System.debug('id         : ' + id);
		System.debug('product id : ' + productId);
		System.debug('name       : ' + name);
		System.debug('sku        : ' + productCode);
		System.debug('qty        : ' + quantity);
		System.debug('price      : ' + unitPrice);
		System.debug('total      : ' + unitTotal);
		System.debug('images     : ' + imageUrl);
	}
}