@isTest(SeeAllData=true)
public class Pentair_Test_RFRecords {   
    static testMethod void myUnitTest() {
        
        Test.startTest();
        
        //Sample Test Account Record
        Account newAccount = new Account(
            name = 'Test Account',
            ShippingCountryCode = 'US',
            BillingCity = 'Example',
            BillingStreet = '1111 Example',
            BillingCountryCode = 'US'
        );
        Insert newAccount;
        
        //Sample Test Account Record
        Contact newContact = new Contact(
            firstname = 'Test First',
            lastname = 'Test Last Name',
            AccountId = newAccount.Id
        );
        Insert newContact;
        
        Id recordTypeQuoteId = [select Id from RecordType where DeveloperName = 'Quote' limit 1].Id;
        Id recordTypeRFQId = [select Id from RecordType where DeveloperName = 'RFQ' limit 1].Id;
        Id recordTypeP3Id = [select Id from RecordType where DeveloperName = 'P3' limit 1].Id;
        
        
        
        //Schroff RFQ Record
        RFQ__c newSchroffRFQRecord = new RFQ__c(
            name = 'Test RFQ Record',
            RecordTypeId = recordTypeRFQId,
            Account__c = newAccount.Id,
            Contact__c = newContact.Id,
            Sales_Team__c = 'Schroff',
            Type__c = 'Customized',
            RFQ_Received__c = Date.today(),
            Requested_By__c = Date.today()
        );
        insert newSchroffRFQRecord;
        
         newSchroffRFQRecord.ERP_Number__c = '11111111';
        update newSchroffRFQRecord;
        
        //Schroff Clone RFQ Record
        RFQ__c newSchroffRFQRecordClone = new RFQ__c(
            name = 'Test RFQ Record',
            RecordTypeId = recordTypeRFQId,
            Account__c = newAccount.Id,
            Contact__c = newContact.Id,
            Sales_Team__c = 'Schroff',
            Proposal_Number__c = 'US-0000001',
            Type__c = 'Customized',
            RFQ_Received__c = Date.today(),
            Requested_By__c = Date.today()
        );
        insert newSchroffRFQRecordClone;
        
        //Hoffman RFQ Record
        RFQ__c newHoffmanRFQRecord = new RFQ__c(
            name = 'Test RFQ Record #2',
            RecordTypeId = recordTypeRFQId,
            Account__c = newAccount.Id,
            Contact__c = newContact.Id,
            Sales_Team__c = 'Hoffman',
            Type__c = 'Standard',
            RFQ_Received__c = Date.today(),
            Requested_By__c = Date.today()
        );
        insert newHoffmanRFQRecord;
        
        //BIS RFQ Record
        RFQ__c newBISRFQRecord = new RFQ__c(
            name = 'Test RFQ Record #3',
            RecordTypeId = recordTypeP3Id,
            Account__c = newAccount.Id,
            Contact__c = newContact.Id,
            Sales_Team__c = 'BIS',
            Type__c = 'Standard',
            Value__c = 5000,
            RFQ_Received__c = Date.today(),
            Requested_By__c = Date.today()
        );
        insert newBISRFQRecord;
        
        //Update the Resources Included & Product Type
        newBISRFQRecord.Resources_Includes__c = 'Engineering';
        update newBISRFQRecord;
        
    newBISRFQRecord.Resources_Includes__c = 'Installation';
        update newBISRFQRecord;

    newBISRFQRecord.Product_Type__c = 'Standard';
        update newBISRFQRecord;      
        
        newBISRFQRecord.Product_Type__c = 'Modified';
        update newBISRFQRecord; 
        
        newBISRFQRecord.Product_Type__c = 'Customized';
        update newBISRFQRecord; 
        
        newBISRFQRecord.Product_Type__c = 'Engineered';
        newBISRFQRecord.Status__c = 'Quoted';
        update newBISRFQRecord;
        
        //Create Line Item to a RFQ Record
        RFQ_Line_Item__c newRFQLineItem = new RFQ_Line_Item__c(
            Name = 'Widget #1',
            Material_Number__c = '001-383948',
            Quantity__c = 10,
            Price_Gross__c = 100.00,
            Price_List__c = 90.00,
            RFQ__c = newSchroffRFQRecord.Id
        );

        insert newRFQLineItem;
        
        //Update the RFQ Record
        RFQ_Line_Item__c updateRFQLineItem = new RFQ_Line_Item__c(ID=newRFQLineItem.Id);
        updateRFQLineItem.Quantity__c = 20;
        update updateRFQLineItem;     
        
        //Create End to End Record
        End_to_End__c endToEnd = new End_to_End__c(
            Name = 'End to End Test',
            RFQ__c = newSchroffRFQRecord.Id,
            Request_Type__c = 'Calculations'
        );        
        insert endToEnd;
        
        //Update the End to End Record
        List<QueueSobject> getRFQQueue = [SELECT QueueId, Id, SobjectType FROM QueueSobject WHERE SobjectType ='End_to_End__c' LIMIT 1 ];
        if (getRFQQueue.size() > 0)
        {
            endToEnd.OwnerId = getRFQQueue[0].QueueId;
            endToEnd.Notes__c = 'Test Notes';
            update endToEnd;   
            
            endToEnd.OwnerId = UserInfo.getUserId();
            endToEnd.Notes__c = 'Test Notes dfdf';
            update endToEnd;   
        }
        
        //Update RFQ Record
        List<QueueSobject> getRFQQueue2 = [SELECT QueueId, Id, SobjectType FROM QueueSobject WHERE SobjectType ='RFQ__c' LIMIT 1 ];
        if (getRFQQueue2.size() > 0)
        {
            newSchroffRFQRecord.OwnerId = getRFQQueue2[0].QueueId;
            update newSchroffRFQRecord;    
        }
        
                List<RFQ_XML_COND__C> multipleConditions = new LIst<RFQ_XML_COND__C>(); 
        
        RFQ_XML_COND__C single1 = new RFQ_XML_COND__C(
            NAME = '0020149371',
            DOCUMENT_CONDITION__C = '0004496344',
            CONDITION_ITEM_NUMBER__C = '000040',
            CONDITION_TYPE__C = 'ZCUS',  
            CONDITION_USED_FOR_STAT__C = null,
            CAL_TYPE_FOR_CONDITION__C = 'A',
            CONDITION_IS_INACTIVE__C = null,
            CONDITION_PRICING_UNIT__C = '1',
            STEP_NUMBER__c = '070',
            CONDITION_BASE_VALUE__c = '10000.00',
            RATE__c = '7.81',
            CURRENCY_KEY__c = 'EUR',
            CONDITION_VALUE__c = '7810.00');
        
        multipleConditions.add(single1);   
        
        RFQ_XML_COND__C single2 = new RFQ_XML_COND__C(
            NAME = '0020149371',
            DOCUMENT_CONDITION__C = '0004496344',
            CONDITION_ITEM_NUMBER__C = '000040',
            CONDITION_TYPE__C = 'ZPAC',  
            CONDITION_USED_FOR_STAT__C = null,
            CAL_TYPE_FOR_CONDITION__C = 'A',
            CONDITION_IS_INACTIVE__C = null,
            CONDITION_PRICING_UNIT__C = '1',
            STEP_NUMBER__c = '070',
            CONDITION_BASE_VALUE__c = '10000.00',
            RATE__c = '7.81',
            CURRENCY_KEY__c = 'EUR',
            CONDITION_VALUE__c = '7810.00');
        
        multipleConditions.add(single2); 
        
        RFQ_XML_COND__C single3 = new RFQ_XML_COND__C(
            NAME = '0020149371',
            DOCUMENT_CONDITION__C = '0004496344',
            CONDITION_ITEM_NUMBER__C = '000040',
            CONDITION_TYPE__C = 'AMIZ',  
            CONDITION_USED_FOR_STAT__C = null,
            CAL_TYPE_FOR_CONDITION__C = 'A',
            CONDITION_IS_INACTIVE__C = null,
            CONDITION_PRICING_UNIT__C = '1',
            STEP_NUMBER__c = '070',
            CONDITION_BASE_VALUE__c = '10000.00',
            RATE__c = '7.81',
            CURRENCY_KEY__c = 'EUR',
            CONDITION_VALUE__c = '7810.00');
        
        multipleConditions.add(single3);
        
        RFQ_XML_COND__C single4 = new RFQ_XML_COND__C(
            NAME = '0020149371',
            DOCUMENT_CONDITION__C = '0004496344',
            CONDITION_ITEM_NUMBER__C = '000040',
            CONDITION_TYPE__C = 'ZINS',  
            CONDITION_USED_FOR_STAT__C = null,
            CAL_TYPE_FOR_CONDITION__C = 'A',
            CONDITION_IS_INACTIVE__C = null,
            CONDITION_PRICING_UNIT__C = '1',
            STEP_NUMBER__c = '070',
            CONDITION_BASE_VALUE__c = '10000.00',
            RATE__c = '7.81',
            CURRENCY_KEY__c = 'EUR',
            CONDITION_VALUE__c = '7810.00');
        
        multipleConditions.add(single4);
        
        RFQ_XML_COND__C single5 = new RFQ_XML_COND__C(
            NAME = '0020149371',
            DOCUMENT_CONDITION__C = '0004496344',
            CONDITION_ITEM_NUMBER__C = '000040',
            CONDITION_TYPE__C = 'ZFRG',  
            CONDITION_USED_FOR_STAT__C = null,
            CAL_TYPE_FOR_CONDITION__C = 'A',
            CONDITION_IS_INACTIVE__C = null,
            CONDITION_PRICING_UNIT__C = '1',
            STEP_NUMBER__c = '070',
            CONDITION_BASE_VALUE__c = '10000.00',
            RATE__c = '7.81',
            CURRENCY_KEY__c = 'EUR',
            CONDITION_VALUE__c = '7810.00');
        
        multipleConditions.add(single5);
    
    RFQ_XML_COND__C single6 = new RFQ_XML_COND__C(
            NAME = '0020149371',
            DOCUMENT_CONDITION__C = '0004496344',
            CONDITION_ITEM_NUMBER__C = '000040',
            CONDITION_TYPE__C = 'ZFRG',  
            CONDITION_USED_FOR_STAT__C = null,
            CAL_TYPE_FOR_CONDITION__C = 'A',
            CONDITION_IS_INACTIVE__C = null,
            CONDITION_PRICING_UNIT__C = '1',
            STEP_NUMBER__c = '040',
            CONDITION_BASE_VALUE__c = '10000.00',
            RATE__c = '7.81',
            CURRENCY_KEY__c = 'EUR',
            CONDITION_VALUE__c = '7810.00');
        
    multipleConditions.add(single6);
    
    insert multipleConditions;
    
    //Test XML File        
        RFQ_XML__c xmlFileInsert = new RFQ_XML__c(
            BASE_UNIT__c = '', BILLTO_ADDR_LINE1__c = 'Example', BILLTO_ADDR_LINE2__c = 'Example',
            BILLTO_ADDR_LINE3__c = 'Example', BILLTO_CITY__c = 'DUBLIN', BILLTO_COUNTRY__c = 'US', BILLTO_CUSTOMER__c = '',
            BILLTO_NAME__c = 'Example', BILLTO_STATE__c = 'OH', BILLTO_ZIP__c = '43016', CONTACT_EMAIL__c = '',
            CONTACT_FAX__c = '', CONTACT_NAME__c = '',  CONTACT_PHONE__c = '', CONTRACTOR__c = '',
            CURRENCY__c = 'USD', CurrencyIsoCode = 'USD', CUSTOMER_REFERENCE_ADDR_LINE1__c = 'Example',
            CUSTOMER_REFERENCE_ADDR_LINE2__c = 'Example', CUSTOMER_REFERENCE_ADDR_LINE3__c = 'Example',
            CUSTOMER_REFERENCE_CITY__c = '', CUSTOMER_REFERENCE_COUNTRY__c = '',
            CUSTOMER_REFERENCE_EMAIL__c = '', CUSTOMER_REFERENCE_FAX__c = '',
            CUSTOMER_REFERENCE_NAME__c = '', CUSTOMER_REFERENCE_PHONE__c = '',
            CUSTOMER_REFERENCE_STATE__c = '', CUSTOMER_REFERENCE_ZIP__c = '',
            DESCRIPTION__c = '', DESCRIPTION_LINE__c = '',
            DESCRIPTION_NA__c = '', Discount__c = 0,
            EFFECTIVE_FROM__c = '20170925', EFFECTIVE_TO__c = '20170925',
            ERP_QUOTE_NUMBER__c = '0020149371BC', FREIGHT_TERMS__c = '0',
            Incoterms__c = '', Incoterms_Description__c = '',
            INVOICED_DATE__c = '', Line__c = 1,
            LIST_PRICE__c = '10.00',  MATERIAL_NUMBER__c = '0034212423',
            NAME__c = '0020149371', OPPORTUNITY_ID__c = '',
            ORDER_DETAIL_AMOUNT__c = '0', ORDER_DISCOUNT__c = '0',
            ORDER_FREIGHT_AMOUNT__c = '0', ORDER_PRE_FREIGHT_AMOUNT__c = '0',
            ORDER_TOTAL_AMOUNT__c = '0', ORDER_TOTAL_TAX__c = '0',
            OWNER_NAME__c = '',  OWNER_NUMBER__c = '',
            OWNER_QUOTE__c = 'S5749', OWNER_QUOTE_NAME__c = '',
            Packaging_and_Insurance_Costs__c = '', PAYMENT_TERMS__c = '',
            POTENTIAL_CUSTOMER__c = 'XXXXXXXXX', PRICE_LIST__c = '90',
            PROJECT_NAME__c = '',  QUANTITY__c = '1',
            QUANTITY_LINE__c = '1', Name = '0020149371',
            QUOTE_NUMBER__c = '0020149371',
            SHAREPOINT_SUBFOLDER_NAME__c = '', SHIPMENT_METHOD__c = '',
            SHIPTO_ADDR_LINE1__c = 'Example', SHIPTO_ADDR_LINE2__c = 'Example',
            SHIPTO_ADDR_LINE3__c = 'Example', SHIPTO_CITY__c = '',
            SHIPTO_COUNTRY__c = '', SHIPTO_NAME__c = '',
            SHIPTO_STATE__c = '',  SHIPTO_ZIP__c = '',
            TITLE__c = '', TOTAL_WITHOUT_TAX__c = '90',
            Type__c = 'Quote', UNIT_PRICE__c = '90', 
            CONDITION_ITEM_NUMBER__C = '000040', SALES_ORGANIZATION__c = '1090',
            SALES_DOC_ITEM__C = '000040', OUTPUT_LANGUAGE__c ='DE', DOCUMENT_CURRENCY__C ='USD'
        );
		
        RFQ_XML__c xmlFileInsert2 = new RFQ_XML__c(
            BASE_UNIT__c = '', BILLTO_ADDR_LINE1__c = '', BILLTO_ADDR_LINE2__c = '',
            BILLTO_ADDR_LINE3__c = '', BILLTO_CITY__c = '', BILLTO_COUNTRY__c = '', BILLTO_CUSTOMER__c = '',
            BILLTO_NAME__c = '', BILLTO_STATE__c = '', BILLTO_ZIP__c = '', CONTACT_EMAIL__c = '',
            CONTACT_FAX__c = '', CONTACT_NAME__c = '',  CONTACT_PHONE__c = '', CONTRACTOR__c = '',
            CURRENCY__c = 'USD', CurrencyIsoCode = 'USD', CUSTOMER_REFERENCE_ADDR_LINE1__c = '',
            CUSTOMER_REFERENCE_ADDR_LINE2__c = '', CUSTOMER_REFERENCE_ADDR_LINE3__c = '',
            CUSTOMER_REFERENCE_CITY__c = '', CUSTOMER_REFERENCE_COUNTRY__c = '',
            CUSTOMER_REFERENCE_EMAIL__c = '', CUSTOMER_REFERENCE_FAX__c = '',
            CUSTOMER_REFERENCE_NAME__c = '', CUSTOMER_REFERENCE_PHONE__c = '',
            CUSTOMER_REFERENCE_STATE__c = '', CUSTOMER_REFERENCE_ZIP__c = '',
            DESCRIPTION__c = '', DESCRIPTION_LINE__c = '',
            DESCRIPTION_NA__c = '', Discount__c = 0,
            EFFECTIVE_FROM__c = '20170925', EFFECTIVE_TO__c = '20170925',
            ERP_QUOTE_NUMBER__c = '0020149371', FREIGHT_TERMS__c = '0',
            Incoterms__c = '', Incoterms_Description__c = '',
            INVOICED_DATE__c = '', Line__c = 2,
            LIST_PRICE__c = '20',  MATERIAL_NUMBER__c = '34212423',
            NAME__c = '0020149371', OPPORTUNITY_ID__c = '99999',
            ORDER_DETAIL_AMOUNT__c = '0', ORDER_DISCOUNT__c = '0',
            ORDER_FREIGHT_AMOUNT__c = '0', ORDER_PRE_FREIGHT_AMOUNT__c = '0',
            ORDER_TOTAL_AMOUNT__c = '0', ORDER_TOTAL_TAX__c = '0',
            OWNER_NAME__c = '',  OWNER_NUMBER__c = '',
            OWNER_QUOTE__c = 'S5749', OWNER_QUOTE_NAME__c = '',
            Packaging_and_Insurance_Costs__c = '', PAYMENT_TERMS__c = '',
            POTENTIAL_CUSTOMER__c = '1090925378', PRICE_LIST__c = '90',
            PROJECT_NAME__c = '',  QUANTITY__c = '1',
            QUANTITY_LINE__c = '1', Name = '0020149371',
            QUOTE_NUMBER__c = '0020149371',
            SHAREPOINT_SUBFOLDER_NAME__c = '', SHIPMENT_METHOD__c = '',
            SHIPTO_ADDR_LINE1__c = '', SHIPTO_ADDR_LINE2__c = '',
            SHIPTO_ADDR_LINE3__c = '', SHIPTO_CITY__c = '',
            SHIPTO_COUNTRY__c = '', SHIPTO_NAME__c = '',
            SHIPTO_STATE__c = '',  SHIPTO_ZIP__c = '',
            TITLE__c = '', TOTAL_WITHOUT_TAX__c = '90',
            Type__c = 'Order', UNIT_PRICE__c = '90'
        );
        insert xmlFileInsert2;
         
        Pentair_RFQ_XML_COND.AdditionalLines(multipleConditions, 'EUR', 'DE', newSchroffRFQRecord.id); 
        
        Pentair_RFQXMLParse.RFQXMLLines('0020149371', 'Quote', newSchroffRFQRecord.id);
		
		updateRFQXML urx = new updateRFQXML();
        Id batchId = Database.executeBatch(urx,1);
        
        Test.stopTest();

    }
}