public class Pentair_ERICO_ShipmentStatus {   
    public String cid {get; set;}
    public String sourcesystem {get; set;}
    public String id {get; set;}
    public String oid {get; set;}
    public String clid {get; set;}
    
    public String orderKey = '';
    
    public class ShipmentHeader {
    
        public String orderNumber {get;set;}        
        public String orderDate {get;set;}
        public String customerNumber {get;set;}
        public String purchaseOrder {get;set;}
        public String status {get;set;}
        public String companyName {get;set;}
        public String addr1 {get;set;}
        public String addr2 {get;set;}
        public String addr3 {get;set;}
        public String city {get;set;}
        public String state {get;set;}
        public String postalCode {get;set;}
        public List<Shipment> shipments {get;set;}
    
        public ShipmentHeader() { }
        
        public ShipmentHeader(XmlDom.Element xml) {
            if (xml == null)
                return;
            this.orderNumber = xml.getValue('OrderNumber');
            this.orderDate = xml.getValue('OrderDate');
            this.customerNumber = xml.getValue('CustomerNumber');
            this.purchaseOrder = xml.getValue('PurchaseOrder');
            this.status = xml.getValue('HeaderStatus');
            this.companyName = xml.getValue('CompanyName');
            this.addr1 = xml.getValue('Address1');
            this.addr2 = xml.getValue('Address2');
            this.addr3 = xml.getValue('Address3');
            this.city = xml.getValue('City');
            this.state = xml.getValue('StateProvince');
            this.postalCode = xml.getValue('PostalCode');
            this.shipments = new List<Shipment>();        
            
            XmlDom.Element[] lines = xml.getElementsByTagName('Shipment');
            if (lines != null) {
                for (XmlDom.Element shipmentElement : lines) {
                    this.shipments.add(new Shipment(shipmentElement));
                }
            }            
        }                   
    }
    
    public class Shipment {
        
        public String shipmentDate {get;set;}
        public String warehouse {get;set;}
        public String carrier {get;set;}
        public String trackingCode {get;set;}
        public String trackingUrl {get;set;}
        public List<ShipmentLineItem> lineItems {get;set;}
        
        public Shipment() {
            this.lineItems = new List<ShipmentLineItem>();
        }
        public Shipment(XmlDom.Element xml) {
            this.shipmentDate = xml.getValue('ShipmentDate');
            this.warehouse = xml.getValue('Warehouse');
            this.carrier = xml.getValue('Carrier');
            this.trackingCode = xml.getValue('TrackingCode');
            this.trackingUrl = xml.getValue('TrackingUrl');
            
            this.lineItems = new List<ShipmentLineItem>();
            
            XmlDom.Element[] lines = xml.getElementsByTagName('Line');
            if (lines != null) {
                for (XmlDom.Element lineElement : lines) {
                    this.lineItems.add(new ShipmentLineItem(lineElement));
                }
            }        
        }
    }
    
    public class ShipmentLineItem {
        public String lineNumber {get;set;}
        public String partNumber {get;set;}
        public String quantityShipped {get;set;}
  
        public ShipmentLineItem() {
        }
        public ShipmentLineItem(XmlDom.Element xml) {
            if (xml == null)
                return;
                
            this.lineNumber = xml.getValue('LineNumber');
            this.partNumber = xml.getValue('PartNumber');
            this.quantityShipped = xml.getValue('QuantityShipped');
        }                  
    }
    
    Pentair_ERICOWebService webServiceController = new Pentair_ERICOWebService();
    
    public ShipmentHeader shipmentHeader {set; get;}
    
    
    public void getShipmentStatusHeader()
    {        
        this.shipmentHeader = new ShipmentHeader();
        
        this.oid = ApexPages.CurrentPage().getParameters().get('oid').replace(' ', '');
        this.sourcesystem = ApexPages.CurrentPage().getParameters().get('ssid');
        this.id = ApexPages.CurrentPage().getParameters().get('id');
        
        if (oid != null) {
            
            if(sourcesystem == null)
                sourceSystem = '01';
            
            XmlDom.Element xmlShipmentHeader = webServiceController.getShipment(oid, sourcesystem);

            if (xmlShipmentHeader != null)
            { 
                this.shipmentHeader = new ShipmentHeader(xmlShipmentHeader);    
            } 
        }         
    }
    
    
    public String getSfIdQueryParam() {
        String sfId = ApexPages.currentPage().getParameters().get('id');
        return sfId == null ? '' : '&id=' + sfId;
    }
    
    public PageReference cancel()
   {       
        System.debug('CID: ' + ApexPages.CurrentPage().getParameters().get('cid'));
        System.debug('id: ' + ApexPages.CurrentPage().getParameters().get('id'));
        System.debug('ssid: ' + ApexPages.CurrentPage().getParameters().get('ssid'));
        
        PageReference redirectPage = Page.Pentair_ERICOMain;
        redirectPage.getParameters().put('cid',ApexPages.CurrentPage().getParameters().get('cid')+'.'+ApexPages.CurrentPage().getParameters().get('ssid')+';');
        redirectPage.getParameters().put('id',ApexPages.CurrentPage().getParameters().get('id'));
        redirectPage.getParameters().put('ssid',ApexPages.CurrentPage().getParameters().get('ssid'));
      redirectPage.setRedirect(true);
      return redirectPage;
    }
}