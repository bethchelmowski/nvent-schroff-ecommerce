@isTest(SeeAllData=true)
private class Pentair_Test_ControllerParentOpp2 {
    static testMethod void TestMethodOpportunity2()
    {  
        //Create Test Account Account
        Account newAccount = new Account(
            Name='Test Account 123456789',
            ShippingCountryCode = 'US',
            BillingCountryCode = 'US',
            BillingCity = 'Example',
            BillingStreet = '1111 Example'
        );
        insert newAccount;
        
        List<PriceBook2> getPriceBook = [SELECT Id FROM PriceBook2 LIMIT 1 ];
        
            List<Opportunity> lstOpportunity = new List<Opportunity>();
     
            Opportunity testOpportunity = new Opportunity();
            testOpportunity.Name='Test Opp1' ;
            testOpportunity.CloseDate = Date.today();
            testOpportunity.First_Revenue_Date__c = Date.today();
            testOpportunity.StageName = 'Establish';
            testOpportunity.AccountId = newAccount.Id;
            testOpportunity.Sales_Team__c = 'BIS';
            testOpportunity.Probability = 25;
            testOpportunity.Amount = 5000;
            testOpportunity.CurrencyIsoCode = 'USD';
            lstOpportunity.add(testOpportunity);
            
            insert lstOpportunity;
        
        PriceBook2 xPriceBook2 = [SELECT ID, Name FROM PriceBook2 WHERE Name = 'BIS' LIMIT 1];
        PriceBookEntry xOppEntryId = [SELECT ID, Name FROM PriceBookEntry 
                                      WHERE PriceBook2Id =: xPriceBook2.Id 
                                      AND CurrencyIsoCode = 'USD' 
                                      AND ISActive = true LIMIT 1];
        
        OpportunityLineItem line1 = new OpportunityLineItem(
            Number_of_Installments__c = 3,
            Schedule_Type__c = 'Divide Amount into multiple installments',
            UnitPrice = 5000,
            Installment_Period__c = 'Monthly',
            Quantity = 1,
            ServiceDate = Date.today(),
            PricebookEntryId = xOppEntryId.Id,
            OpportunityId = lstOpportunity[0].Id
        );
        insert line1;
        
        OpportunityLineItem line2 = new OpportunityLineItem(
            Number_of_Installments__c = 3,
            Schedule_Type__c = 'Divide Amount into multiple installments',
            UnitPrice = 5000,
            Installment_Period__c = 'Monthly',
            Quantity = 1,
            ServiceDate = Date.today(),
            PricebookEntryId = xOppEntryId.Id,
            OpportunityId = lstOpportunity[0].Id
        );
        //insert line2;
        
        OpportunityLineItemSchedule schedule1 = new OpportunityLineItemSchedule(
            OpportunityLineItemId = line1.Id,
            Revenue = 5000,
            ScheduleDate = Date.today(),
            Type = 'Revenue'
        );
        
        insert schedule1;
             
        Test.startTest();
                    

        
        //Pentair_ProductScheduleCreate
        ApexPages.StandardController sc2 = new ApexPages.StandardController(line1); 
        Pentair_ProductScheduleCreate controller2 = new Pentair_ProductScheduleCreate(sc2);
        PageReference pageRef = Page.Pentair_ScheduleUtility;
        pageRef.getParameters().put('id', String.valueOf(line1.Id));
        Test.setCurrentPage(pageRef);
        controller2.save();
        controller2.CreateSchedule();
        controller2.SaveAndRedirect();
        controller2.editSchedule();
        controller2.DeleteLineItems();
        controller2.masterList();
        
        //Pentair_OpportunityScheduleExtension
        ApexPages.StandardController sc1 = new ApexPages.StandardController(lstOpportunity[0]); 
        Pentair_OpportunityScheduleExtension controller1 = new Pentair_OpportunityScheduleExtension(sc1);
        PageReference pageRef1 = Page.Pentair_EditSchedule;
        pageRef1.getParameters().put('index', String.valueOf(0));
        Test.setCurrentPage(pageRef1);
        
        controller1.objOpp.Id = lstOpportunity[0].Id;
        controller1.getAllList();
        controller1.UpdateProductScheduleDate();
        controller1.SaveForm();
        controller1.SaveAndRefresh();
        
        controller1.RecordToDelete = line1.Id;
        controller1.RemoveItems();        
            
        Test.stopTest(); 
    }
}