/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 9/21/19.
 */

public with sharing class x7sProductUtility
{
	// Parse source string into key, value pairs
	//
	//	source string format
	//	    field1:value1;field2:value1;
	public static final String FIELD_SEPARATOR = ';';
	public static final String VALUE_SEPARATOR = ':';
	public static final String VALUE_DELIMITER = ',';
	public static final String RANGE_SEPARATOR = '-';

	final static String NAMESPACE    = 'CCRZ';

	public static List<String> getPicklistValues(String objName, String fieldName)
	{
		List<String> options = new List<String>();

		Schema.SObjectType objType                  = Schema.getGlobalDescribe().get(objName);
		Schema.DescribeSObjectResult objDescribe    = objType.getDescribe();
		Map<String, Schema.SObjectField> fieldMap   = objDescribe.fields.getMap();
		List<Schema.PicklistEntry> values           = fieldMap.get(fieldName).getDescribe().getPicklistValues();

		for (Schema.PicklistEntry a : values)
		{
			options.add(a.getLabel());
		}
		return options;
	}

	public static List<String> getImageSrc(List<Map<String,Object>> mediaList, String imageTarget)
	{
		List<String> imageSrc = new List<String>();
		//System.debug('Media source size: ' + mediaList.size());

		for (Integer mediaPos = 0; mediaPos < mediaList.size(); mediaPos++)
		{
			String mediaType = (String) mediaList[mediaPos].get('mediaType');
			//System.debug('    media type: ' + mediaType);

			if (mediaType == imageTarget)
			{
				//System.debug('    == found product search image');
				String mediaSource = (String) mediaList[mediaPos].get('productMediaSource');

				switch on mediaSource
				{
					when 'Static Resource'
					{
						imageSrc.add('/resource/' + NAMESPACE + '__'
							+ (String) mediaList[mediaPos].get('staticResourceName')
							+ '/' + (String) mediaList[mediaPos].get('filePath'));
					}
					when 'Attachment'
					{
						//Attachment
					}
					when else
					{
						// URI
						imageSrc.add((String) mediaList[mediaPos].get('URI'));
					}
				}
			}
		}

		return imageSrc;
	}

}