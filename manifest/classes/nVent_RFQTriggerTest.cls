/* ********************************************
Developer -     Apurva Prasade
Class -         nVent_RFQTriggerTest
Description -   Test class to cover RFQ trigger

Developer   Date        Version
Apurva P    23-08-2019  V1.0

********************************************** */
@isTest
public class nVent_RFQTriggerTest {
    @TestSetup
    static void createData()
    {

        Profile prflst = [Select Id from Profile where Name =: 'System Administrator' limit 1];
        
        User objUser = new User();
        objUser.FirstName = 'fNameTest';
        objUser.LastName = 'lNameTest';
        objUser.Email = 'aptest@test.com';
        objUser.isActive = true;
        objUser.ProfileId = prflst.Id;
        objUser.Username = 'aptest@test.com' + System.currentTimeMillis();
        objUser.CompanyName = 'testComp';
        objUser.Title = 'Developer';
        objUser.Alias = 'testal';
        objUser.TimeZoneSidKey = 'America/Los_Angeles';
        objUser.EmailEncodingKey = 'UTF-8';
        objUser.LanguageLocaleKey = 'en_US';
        objUser.LocaleSidKey = 'en_US';
        objUser.SAP_Id__c = 'S000';
        objUser.Primary_Sales_Team__c = 'Schroff';
        insert objUser;
        
        RFQ_Setting_H__c settings = RFQ_Setting_H__c.getOrgDefaults();
settings.Thermal_Sales_Org_IDs__c = '1244';
        settings.Enclosures_Sales_Org_IDs__c = '6320';
        settings.Enclosures_Default_Owner__c = objUser.Id;
insert settings;

        Account objAccount = new Account();
        objAccount.Name = 'testCompany';
        objAccount.Subvertical__c = 'Oil and Gas';
        objAccount.BillingCountryCode = 'FR';
        objAccount.BillingStreet = 'test';
        objAccount.BillingCity = 'Ttest';
        objAccount.SAP__c = '70080076';
        insert objAccount;
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'testABCCC';
        objAccount1.Subvertical__c = 'Oil and Gas';
        objAccount1.BillingCountryCode = 'FR';
        objAccount1.BillingStreet = 'test';
        objAccount1.BillingCity = 'Ttest';
        objAccount1.SAP__c = '70080075';
        insert objAccount1;

        AccountTeamMember objAM1 = new AccountTeamMember();
        objAM1.AccountId = objAccount.ID;
        objAM1.TeamMemberRole = 'Schroff Rep';
        objAM1.UserId = objUser.ID;
        insert objAM1;
		
        AccountTeamMember objAM2 = new AccountTeamMember();
        objAM2.AccountId = objAccount.ID;
        objAM2.TeamMemberRole = 'BIS Rep';
        objAM2.UserId = objUser.ID;
        insert objAM2;

        AccountTeamMember objAM3 = new AccountTeamMember();
        objAM3.AccountId = objAccount.ID;
        objAM3.TeamMemberRole = 'IHS Rep';
        objAM3.UserId = objUser.ID;
        insert objAM3;

        AccountTeamMember objAM = new AccountTeamMember();
        objAM.AccountId = objAccount.ID;
        objAM.TeamMemberRole = 'Hoffman Rep';
        objAM.UserId = objUser.ID;
        insert objAM;

        Id p3vRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('P3 Readonly').getRecordTypeId();
        RFQ__c objRFQ1 = new RFQ__c();
        objRFQ1.Account__c = objAccount.Id;
        objRFQ1.RecordTypeId = p3vRecordTypeId;
        objRFQ1.Resources_Includes__c = 'Engineering';
        //objRFQ1.Product_Type__c = 'Standard';
        objRFQ1.Linked_External_System__c = 'test';
        objRFQ1.Proposal_Number__c = '1234';
        //objRFQ1.Owner_Doc__c = 'S000';
        objRFQ1.RFQ_Received__c = Date.today();
        //objRFQ1.SAP_Sales_Org__c = '6320';
        insert objRFQ1;

        RFQ__c objRFQ2 = new RFQ__c();
        objRFQ2.Account__c = objAccount.Id;
        objRFQ2.RecordTypeId = p3vRecordTypeId;
        objRFQ2.Resources_Includes__c = 'Installation';
        //objRFQ2.Product_Type__c = 'Modified';
        objRFQ2.RFQ_Received__c = Date.today();
        objRFQ2.Bill_To_Name__c = 'check tmermm';
        objRFQ2.SAP_Sales_Org__c = '1244';
        objRFQ2.Bill_To_City__c = 'test';
            objRFQ2.Bill_To_Country__c = 'FR';
           //objRFQ2
        insert objRFQ2;
        
        RFQ__c objRFQ3 = new RFQ__c();
        objRFQ3.Account__c = objAccount.Id;
        objRFQ3.RecordTypeId = p3vRecordTypeId;
        objRFQ3.Resources_Includes__c = 'Engineering';
        //objRFQ3.Product_Type__c = 'Customized';
        objRFQ3.RFQ_Received__c = Date.today();
        insert objRFQ3;

        RFQ__c objRFQ4 = new RFQ__c();
        objRFQ4.Account__c = objAccount.Id;
        objRFQ4.RecordTypeId = p3vRecordTypeId;
        objRFQ4.Resources_Includes__c = 'Engineering';
        objRFQ4.Product_Type__c = 'Engineered';
        objRFQ4.RFQ_Received__c = Date.today();
        insert objRFQ4;
        // Sales_Team__c = Schroff ----------------------------------------------------------------------------------------------------
        Id rfqvRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        RFQ__c objRFQ5 = new RFQ__c();
        objRFQ5.Account__c = objAccount.Id;
        objRFQ5.RecordTypeId = rfqvRecordTypeId;
        objRFQ5.SAP_Sales_Org__c = '6320';
        //objRFQ5.Product_Type__c = 'Standard';
        objRFQ5.ERP_Number__c = 'ABDC1234';
        objRFQ5.RFQ_Received__c = Date.today();
        objRFQ5.Sales_Team__c = 'Schroff';
        //objRFQ5.Type__c = 'Customized';
        objRFQ5.Sold_To_Number__c = '70080075';
        objRFQ5.Owner_Doc__c = 'S000';
        objRFQ5.Sales_Group__c = 'S000';
        insert objRFQ5;

        RFQ__c objRFQ6 = new RFQ__c();
        objRFQ6.Account__c = objAccount.Id;
        objRFQ6.RecordTypeId = rfqvRecordTypeId;
        //objRFQ6.Product_Type__c = 'Standard';
        objRFQ6.ERP_Number__c = 'ABDC1234';
        objRFQ5.SAP_Sales_Org__c = '6320';
        objRFQ6.RFQ_Received__c = Date.today();
        objRFQ6.Sales_Team__c = 'Schroff';
        //objRFQ6.Type__c = 'Standard';
        objRFQ6.Sold_To_Number__c = '70080075';
        insert objRFQ6;

        /*RFQ__c objRFQ7 = new RFQ__c();
        objRFQ7.Account__c = objAccount.Id;
        objRFQ7.RecordTypeId = rfqvRecordTypeId;
        objRFQ7.Product_Type__c = 'Standard';
        objRFQ7.ERP_Number__c = 'ABDC1234';
        objRFQ7.RFQ_Received__c = Date.today();
        objRFQ7.Sales_Team__c = 'Schroff';
        objRFQ7.Type__c = 'Off-platform';
        insert objRFQ7;*/
        // Sales_Team__c = Schroff ------------------END----------------------------------------------------------------------------------

        // Sales_Team__c = Hoffman ---------------------------------------------------------------------------------------------------
        RFQ__c objRFQ8 = new RFQ__c();
        objRFQ8.Account__c = objAccount.Id;
        objRFQ8.RecordTypeId = rfqvRecordTypeId;
        objRFQ8.SAP_Sales_Org__c = '6320';
        objRFQ8.Sold_To_Number__c = '70080075';
        //objRFQ8.Product_Type__c = 'Standard';
        objRFQ8.ERP_Number__c = 'ABDC1234';
        objRFQ8.RFQ_Received__c = Date.today();
        objRFQ8.Sales_Team__c = 'Hoffman';
        objRFQ8.Type__c = 'Modified';
        insert objRFQ8;

        RFQ__c objRFQ9 = new RFQ__c();
        objRFQ9.Account__c = objAccount.Id;
        objRFQ9.RecordTypeId = rfqvRecordTypeId;
        //objRFQ9.Product_Type__c = 'Standard';
        objRFQ9.ERP_Number__c = 'ABDC1234';
        objRFQ9.RecordTypeId = rfqvRecordTypeId;
        objRFQ9.SAP_Sales_Org__c = '6320';
        objRFQ9.Sold_To_Number__c = '70080075';
        objRFQ9.RFQ_Received__c = Date.today();
        objRFQ9.Sales_Team__c = 'Hoffman';
        objRFQ9.Type__c = 'Standard';
        insert objRFQ9;

        RFQ__c objRFQ10 = new RFQ__c();
        objRFQ10.Account__c = objAccount.Id;
        objRFQ10.RecordTypeId = rfqvRecordTypeId;
       // objRFQ10.Product_Type__c = 'Standard';
        objRFQ10.ERP_Number__c = 'ABDC1234';
        objRFQ10.SAP_Sales_Org__c = '6320';
        objRFQ10.Sold_To_Number__c = '70080075';
        objRFQ10.RFQ_Received__c = Date.today();
        objRFQ10.Sales_Team__c = 'Hoffman';
        objRFQ10.Type__c = 'Off-platform';
        insert objRFQ10;
        
        
        //1244
        Id quoteRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('Quote').getRecordTypeId();
        RFQ__c objRFQ11 = new RFQ__c();
        objRFQ11.Account__c = objAccount.Id;
        objRFQ11.RecordTypeId = quoteRecordTypeId;
       // objRFQ10.Product_Type__c = 'Standard';
        objRFQ11.ERP_Number__c = 'ABDC1234';
        objRFQ11.SAP_Sales_Org__c = '1244';
        objRFQ11.Sold_To_Number__c = '70080075';
        objRFQ11.RFQ_Received__c = Date.today();
        objRFQ11.Sales_Team__c = 'BIS';
        objRFQ11.Type__c = 'Standard';
        insert objRFQ11;
        
        RFQ__c objRFQ12 = new RFQ__c();
        objRFQ12.Account__c = objAccount.Id;
        objRFQ12.RecordTypeId = quoteRecordTypeId;
       // objRFQ10.Product_Type__c = 'Standard';
        objRFQ12.ERP_Number__c = 'ABDC1234';
        objRFQ12.SAP_Sales_Org__c = '1244';
        objRFQ12.Ship_To_Number__c = '70080075';
        objRFQ12.RFQ_Received__c = Date.today();
        objRFQ12.Sales_Team__c = 'BIS';
        objRFQ12.Type__c = 'Standard';
        insert objRFQ12;
        
        RFQ__c objRFQ13 = new RFQ__c();
        objRFQ13.Account__c = objAccount.Id;
        objRFQ13.RecordTypeId = quoteRecordTypeId;
       // objRFQ10.Product_Type__c = 'Standard';
        objRFQ13.ERP_Number__c = 'ABDC1234';
        objRFQ13.SAP_Sales_Org__c = '1244';
        objRFQ13.Sold_To_Number__c = '700';
        objRFQ13.RFQ_Received__c = Date.today();
        objRFQ13.Sales_Team__c = 'BIS';
        objRFQ13.Type__c = 'Standard';
        insert objRFQ13;
        
        // Sales_Team__c = Hoffman ---------------------------END------------------------------------------------------------------------
    }
    public static testMethod void testBeforeScope()
    {   
        Id p3vRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('P3 Readonly').getRecordTypeId();
        list<RFQ__c> listRFQ = [Select Id, Scope__c from RFQ__c];
        system.debug('listRFQ '+listRFQ);
        //System.assertEquals(listRFQ[0].Scope__c ,'EP');
        
        RFQ__c objRFQ = [Select Id, Scope__c,Product_Type__c,Resources_Includes__c,Linked_External_System__c,ERP_Number__c from RFQ__c where Product_Type__c = 'Standard' and RecordTypeId =: p3vRecordTypeId limit 1];
        objRFQ.Product_Type__c = 'Modified';
        objRFQ.Linked_External_System__c = 'testUpdate';
        objRFQ.ERP_Number__c = 'PQRS1234';
        update objRFQ;
        
    }
    
    public static testMethod void testBeforeSHDate()
    {
        Id rfqvRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        list<RFQ__c> listRFQ = [Select Id,Type__c, Scope__c,Product_Type__c,Resources_Includes__c,Linked_External_System__c,ERP_Number__c from RFQ__c where RecordTypeId =: rfqvRecordTypeId and Type__c = 'Customized'];
        //System.assertEquals(listRFQ[0].Product_Type__c ,'Customized');
        list<RFQ__c> listRFQ1 = new list<RFQ__c>();
        for(RFQ__c obj : [Select Id,Type__c, Sales_Team__c,Product_Type__c,Resources_Includes__c from RFQ__c where Sales_Team__c = 'Schroff' and RecordTypeId =: rfqvRecordTypeId])
        {
            obj.Sales_Team__c = 'Hoffman';
            listRFQ1.add(obj);
        }
        for(RFQ__c obj : [Select Id,Type__c, Sales_Team__c,Product_Type__c,Resources_Includes__c from RFQ__c where Sales_Team__c = 'Hoffman' and RecordTypeId =: rfqvRecordTypeId])
        {
            obj.Sales_Team__c = 'Schroff';
            listRFQ1.add(obj);
        }
        update listRFQ1;
    }
    public static testMethod void testRecoType()
    {
		Id rfqvRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        Id quoteRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('Quote').getRecordTypeId();
        RFQ__c objRec = [Select Id,Type__c,ERP_Number__c, Account__c,Sales_Team__c,Product_Type__c,Resources_Includes__c, RecordTypeId from RFQ__c where Sales_Team__c = 'Schroff' and RecordTypeId =: rfqvRecordTypeId limit 1];
        objRec.RecordTypeId = quoteRecordTypeId;
        objRec.SAP_Sales_Org__c = '1244';
        update objRec;
        
        objRec.RecordTypeId = rfqvRecordTypeId;
        objRec.SAP_Sales_Org__c = '6320';
        update objRec;
        
        Account accobj = [Select Id,SAP__C from Account Limit 1];
        Pentair_RFQController.GetERPNumber(accobj.Id);
        Pentair_RFQController.SetProposalNumer(objRec.ERP_Number__c,objRec.Id);
        Pentair_RFQController.runOnceRFQNUmberTrigger();
        Pentair_RFQController.runOnceRFQTrigger();
        Pentair_RFQController.addDate(System.now(),3); 
    }
	
   /* public static testMethod void testAfterAddItem()
    {
        
    }*/
    
}