//**********************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  	//
// 	Class Name: Pentair_UtilityClass																		// 								
//	Purpose of the Class: This is a UtilityClass															//
//	Created By: Tredence																					// 		
//	Created Date: 11/20/2018									  											//
//----------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose						//
//	||	1.0		||	11/20/2018		||	Tredence	||	A-0215		||	Initial Creation					//
//**********************************************************************************************************//	
public class Pentair_UtilityClass 
{
    //A-0215 The AllowToDeleteLineitemsonChangeofERPNum is used to skip deleting of RFQ line item when there is changes in ERP Number
	public static boolean AllowToDeleteLineitemsonChangeofERPNum = True;
}