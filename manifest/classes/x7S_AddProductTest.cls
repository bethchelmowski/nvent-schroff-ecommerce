/**
* Created By ankitSoni on 05/22/2020
*/
@isTest
public class x7S_AddProductTest {
    
    @isTest
    static void testAddProductSKU(){
        Profile communityUserProfile = [SELECT Id FROM Profile WHERE Name='nVent Customer Community Plus Login User Profile' LIMIT 1];
        Map<String,Object> retData =
            ccrz.ccApiTestData.setupData(new Map<String,Map<String,Object>>{
                ccrz.ccApiTestData.ACCOUNT_DATA => new Map<String,Object>{
                    ccrz.ccApiTestData.ACCOUNT_LIST => new List<Map<String,Object>>{
                        new Map<String,Object>{
                            'name' => 'testAccount1',
                                'ccrz__dataId__c' => 'testAccount1'
                                }
                    }
                },
                    ccrz.ccApiTestData.CONTACT_DATA => new Map<String,Object>{
                        ccrz.ccApiTestData.CONTACT_LIST => new List<Map<String,Object>>{
                            new Map<String,Object>{
                                'ccrz__dataId__c' => 'testContact1',
                                    'account' => new Account(ccrz__dataId__c = 'testAccount1'),
                                    'email' => 'testcontact1.ccrz@cloudcraze.com',
                                    'lastName' => 'User1',
                                    'firstName' => 'Test1'
                                    }
                        }
                    },
                        ccrz.ccApiTestData.USER_DATA => new Map<String,Object>{
                            ccrz.ccApiTestData.USER_LIST => new List<Map<String,Object>>{
                                new Map<String,Object>{
                                    'ccrz__dataId__c' => 'testUser1',
                                        'alias' => 'defusr1',
                                        'email' => 'test.ccrz1@cloudcraze.com',
                                        'lastName' => 'User1',
                                        'firstName' => 'Test1',
                                        'languageLocaleKey' => 'fr',
                                        'localeSIDKey' => 'fr_FR',
                                        'emailEncodingKey' => 'UTF-8',
                                        'profileId' => communityUserProfile.Id,
                                        'username' => System.currentTimeMillis() + 'test1@cloudcraze.com',
                                        'ccrz__CC_CurrencyCode__c' => 'EUR',
                                        'contact' => new Contact(ccrz__dataId__c = 'testContact1'),
                                        'timezoneSIDKey' => 'GMT'
                                        }
                            }
                        }
            });
        Map<String,Object> theData = (Map<String,Object>)retData.get(ccrz.ccApiTestData.USER_DATA);
        List<sObject> theList = (List<sObject>)theData.get(ccrz.ccApiTestData.USER_LIST); 
        User theUser = (User)theList.get(0);
        
        //CC Category Test Data
        Map<String,Object> retCatData = ccrz.ccApiTestData.setupData(new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.CATEGORY_DATA => new Map<String,Object>{
                ccrz.ccApiTestData.CATEGORY_LIST => new List<Map<String,Object>>{
                    new Map<String,Object>{
                        'ccrz__dataId__c' => 'testBaseCategory',
                            'Name' => 'All Products',
                            'ccrz__CategoryID__c' => '0',
                            'ccrz__EndDate__c' => Date.today().addDays(10),
                            'ccrz__LongDescRT__c' => 'Some long description',
                            'ccrz__SEOId__c' => 'basecat',
                            'ccrz__Sequence__c' => 0,
                            'ccrz__ShortDescRT__c' => 'Some short description',
                            'ccrz__StartDate__c' => Date.today().addDays(-10)
                            }
                }
            }
        });
        Map<String,Object> theCatData = (Map<String,Object>)retCatData.get(ccrz.ccApiTestData.CATEGORY_DATA);
        List<sObject> theCatList = (List<sObject>)theCatData.get(ccrz.ccApiTestData.CATEGORY_LIST);        
        ccrz__E_Category__c theCat = (ccrz__E_Category__c)theCatList.get(0);
        
        
        //CC Price List Test Data
        Map<String,Object> retPLData = ccrz.ccApiTestData.setupData(new Map<String,Map<String,Object>>{
            ccrz.ccApiTestData.PRICELIST_DATA => new Map<String,Object>{
                ccrz.ccApiTestData.PRICELIST_LIST => new List<Map<String,Object>>{
                    new Map<String,Object>{
                        'name' => 'Enterprise',
                            'ccrz__dataId__c' => 'Enterprise',
                            'ccrz__desc__c' => 'Enterprise pricelist',
                            'ccrz__Enabled__c' => true,
                            'ccrz__StartDate__c' => Date.today().addDays(-10),
                            'ccrz__EndDate__c' => Date.today().addDays(10),
                            'ccrz__Storefront__c' => 'DefaultStore',
                            'ccrz__CurrencyISOCode__c' => 'USD',
                            'ccrz__PricelistId__c' => 'Enterpise'
                            }
                }
            }
        });        
        Map<String,Object> thePLData = (Map<String,Object>)retPLData.get(ccrz.ccApiTestData.PRICELIST_DATA);
		List<sObject> thePLList = (List<sObject>)thePLData.get(ccrz.ccApiTestData.PRICELIST_LIST);
		ccrz__E_Pricelist__c thePL = (ccrz__E_Pricelist__c)thePLList.get(0);
        
        System.runAs(theUser) {
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new x7S_PricingRequestCalloutMock());
            x7S_AddProduct.addProductSKU('11527789','1');
            Test.stopTest();
        }
    }
}