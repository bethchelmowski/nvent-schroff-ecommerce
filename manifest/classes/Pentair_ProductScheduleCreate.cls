public class Pentair_ProductScheduleCreate {    
    OpportunityLineItem lineItem;

    public Pentair_ProductScheduleCreate(ApexPages.StandardController controller) {
		this.lineItem = (OpportunityLineItem)controller.getRecord();
    }
    
    public void save() {
        DeleteLineItems();
        
        try{
        	update lineItem;
        }
        catch (Exception ed)
        {
            
        }
        
        CreateSchedule();
    }
    
    public PageReference SaveAndRedirect()
 	{
        save();
        
        PageReference redirectPage = Page.Pentair_EditProductSchedule;
    	redirectPage.setRedirect(true);
    	redirectPage.getParameters().put('id',lineItem.Id);
    	return redirectPage;
    }
    
	public PageReference editSchedule() {      
        PageReference redirectPage = Page.Pentair_EditProductSchedule;
    	redirectPage.setRedirect(true);
    	redirectPage.getParameters().put('id',lineItem.Id);
    	return redirectPage;
	}
    
	public PageReference masterList() {      
        PageReference redirectPage = Page.Pentair_EditSchedule;
    	redirectPage.setRedirect(true);
    	redirectPage.getParameters().put('id',lineItem.OpportunityId);
    	return redirectPage;
	}
    
    public void DeleteLineItems()
    {
        System.debug('Product ID ' + lineItem.Id );
        
        List<OpportunityLineItemSchedule> delobj = new List<OpportunityLineItemSchedule>();
    	delobj = [select id FROM OpportunityLineItemSchedule 
                  where OpportunityLineItem.Id =:lineItem.Id];
        if(delobj.size()>0){
            System.debug('Found Delete Items ' + delobj.size() );
        	delete delobj;
    	}
    }
    
    public void CreateSchedule()
    {
        String aid = System.currentPageReference().getParameters().get('id');
        List<OpportunityLineItemSchedule> addObj = new List<OpportunityLineItemSchedule>();
        
        List<OpportunityLineItem> objOLIS = [Select 
                                                    Id, Schedule_Type__c, UnitPrice, TotalPrice,
                                             		Installment_Period__c,
                                                    Number_of_Installments__c
                                               from OpportunityLineItem 
                                               where Id =:aid];
        
        Decimal vNumberOfInstallment = 0.0;
        String vScheduleType = '';
        Decimal vUnitPrice = 0.0;
        String vInstallmentPeriod = '';
        
        if(objOLIS.size()>0){
            vNumberOfInstallment = objOLIS[0].Number_of_Installments__c;
            vScheduleType = objOLIS[0].Schedule_Type__c;
            vUnitPrice = objOLIS[0].TotalPrice;
            vInstallmentPeriod = objOLIS[0].Installment_Period__c;
        }
        
        for (Integer i=0; i<vNumberOfInstallment; i++)
        {
            OpportunityLineItemSchedule c = new OpportunityLineItemSchedule();
            c.OpportunityLineItemId = Id.valueOf(aid);
            c.Type = 'Revenue';
            
            System.debug('Schedule Type ' + vScheduleType );
            System.debug('Unit Price ' + vUnitPrice );
            System.debug('Installment Period ' + vInstallmentPeriod);
            System.debug('Number of Installments ' + vNumberOfInstallment );
            
            if (vScheduleType == 'Divide Amount into multiple installments')
            {
                c.Revenue = vUnitPrice / vNumberOfInstallment;
            }
            else if (vScheduleType == 'Enter custom revenue amounts')
            {
                c.Revenue = 0;
            }
            else if (vScheduleType == 'Repeat Amount for each installment')
            {
                c.Revenue = vUnitPrice;
            }
            else 
            {
                c.Revenue = vUnitPrice;
            }
            
            if (vInstallmentPeriod == 'Daily')
            {
                Date serviceDate = lineItem.ServiceDate.addDays(i);
                c.ScheduleDate = serviceDate;
            }

            if (vInstallmentPeriod == 'Weekly')
            {
                Date serviceDate = lineItem.ServiceDate.addDays(i*7);
                c.ScheduleDate = serviceDate;
            }
            
            if (vInstallmentPeriod == 'Monthly')
            {
                Date serviceDate = lineItem.ServiceDate.addMonths(i);
                c.ScheduleDate = serviceDate;
            }
            
            if (vInstallmentPeriod == 'Quarterly')
            {                
                Date serviceDate = lineItem.ServiceDate.addMonths(i*3);
                c.ScheduleDate = serviceDate;
            }
            
            if (vInstallmentPeriod == 'Yearly')
            {
                Date serviceDate = lineItem.ServiceDate.addYears(i);
                c.ScheduleDate = serviceDate;
            }
            
            System.debug('c Quantity ' + c.Quantity );
            System.debug('c Revenue ' + c.Revenue );
            System.debug('c Schedule Date ' + c.ScheduleDate);
            System.debug('c Type ' + c.Type );
            
            addObj.add(c);
        }
        
        insert addObj;
    }
}