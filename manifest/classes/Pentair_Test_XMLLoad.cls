@isTest(SeeAllData=true)
public class Pentair_Test_XMLLoad {
    static testmethod void CreateSampleDataAndTest() {
        //Test XML File        
        RFQ_XML__c xmlFileInsert = new RFQ_XML__c(
            BASE_UNIT__c = '', BILLTO_ADDR_LINE1__c = 'Example', BILLTO_ADDR_LINE2__c = 'Example',
            BILLTO_ADDR_LINE3__c = 'Example', BILLTO_CITY__c = 'DUBLIN', BILLTO_COUNTRY__c = 'US', BILLTO_CUSTOMER__c = '',
            BILLTO_NAME__c = 'Example', BILLTO_STATE__c = 'OH', BILLTO_ZIP__c = '43016', CONTACT_EMAIL__c = '',
            CONTACT_FAX__c = '', CONTACT_NAME__c = '',  CONTACT_PHONE__c = '', CONTRACTOR__c = '',
            CURRENCY__c = 'USD', CurrencyIsoCode = 'USD', CUSTOMER_REFERENCE_ADDR_LINE1__c = 'Example',
            CUSTOMER_REFERENCE_ADDR_LINE2__c = 'Example', CUSTOMER_REFERENCE_ADDR_LINE3__c = 'Example',
            CUSTOMER_REFERENCE_CITY__c = '', CUSTOMER_REFERENCE_COUNTRY__c = '',
            CUSTOMER_REFERENCE_EMAIL__c = '', CUSTOMER_REFERENCE_FAX__c = '',
            CUSTOMER_REFERENCE_NAME__c = '', CUSTOMER_REFERENCE_PHONE__c = '',
            CUSTOMER_REFERENCE_STATE__c = '', CUSTOMER_REFERENCE_ZIP__c = '',
            DESCRIPTION__c = '', DESCRIPTION_LINE__c = '',
            DESCRIPTION_NA__c = '', Discount__c = 0,
            EFFECTIVE_FROM__c = '20170925', EFFECTIVE_TO__c = '20170925',
            ERP_QUOTE_NUMBER__c = '0020149371BC', FREIGHT_TERMS__c = '0',
            Incoterms__c = '', Incoterms_Description__c = '',
            INVOICED_DATE__c = '', Line__c = 1,
            LIST_PRICE__c = '10.00',  MATERIAL_NUMBER__c = '0034212423',
            NAME__c = '0020149371', OPPORTUNITY_ID__c = '',
            ORDER_DETAIL_AMOUNT__c = '0', ORDER_DISCOUNT__c = '0',
            ORDER_FREIGHT_AMOUNT__c = '0', ORDER_PRE_FREIGHT_AMOUNT__c = '0',
            ORDER_TOTAL_AMOUNT__c = '0', ORDER_TOTAL_TAX__c = '0',
            OWNER_NAME__c = '',  OWNER_NUMBER__c = '',
            OWNER_QUOTE__c = 'S5749', OWNER_QUOTE_NAME__c = '',
            Packaging_and_Insurance_Costs__c = '', PAYMENT_TERMS__c = '',
            POTENTIAL_CUSTOMER__c = 'XXXXXXXXX', PRICE_LIST__c = '90',
            PROJECT_NAME__c = '',  QUANTITY__c = '1',
            QUANTITY_LINE__c = '1', Name = '0020149371',
            QUOTE_NUMBER__c = '0020149371',
            SHAREPOINT_SUBFOLDER_NAME__c = '', SHIPMENT_METHOD__c = '',
            SHIPTO_ADDR_LINE1__c = 'Example', SHIPTO_ADDR_LINE2__c = 'Example',
            SHIPTO_ADDR_LINE3__c = 'Example', SHIPTO_CITY__c = '',
            SHIPTO_COUNTRY__c = '', SHIPTO_NAME__c = '',
            SHIPTO_STATE__c = '',  SHIPTO_ZIP__c = '',
            TITLE__c = '', TOTAL_WITHOUT_TAX__c = '90',
            Type__c = 'Quote', UNIT_PRICE__c = '90', 
            CONDITION_ITEM_NUMBER__C = '000040', SALES_ORGANIZATION__c = '1090',
            SALES_DOC_ITEM__C = '000040', OUTPUT_LANGUAGE__c ='DE', DOCUMENT_CURRENCY__C ='USD'
        );
        
        insert xmlFileInsert;
        
        RFQ_XML__c xmlFileInsertA = new RFQ_XML__c(
            BASE_UNIT__c = '', BILLTO_ADDR_LINE1__c = 'Example', BILLTO_ADDR_LINE2__c = 'Example',
            BILLTO_ADDR_LINE3__c = 'Example', BILLTO_CITY__c = 'DUBLIN', BILLTO_COUNTRY__c = 'US', BILLTO_CUSTOMER__c = '',
            BILLTO_NAME__c = 'Example', BILLTO_STATE__c = 'OH', BILLTO_ZIP__c = '43016', CONTACT_EMAIL__c = '',
            CONTACT_FAX__c = '', CONTACT_NAME__c = '',  CONTACT_PHONE__c = '', CONTRACTOR__c = '',
            CURRENCY__c = 'USD', CurrencyIsoCode = 'USD', CUSTOMER_REFERENCE_ADDR_LINE1__c = 'Example',
            CUSTOMER_REFERENCE_ADDR_LINE2__c = 'Example', CUSTOMER_REFERENCE_ADDR_LINE3__c = 'Example',
            CUSTOMER_REFERENCE_CITY__c = '', CUSTOMER_REFERENCE_COUNTRY__c = '',
            CUSTOMER_REFERENCE_EMAIL__c = '', CUSTOMER_REFERENCE_FAX__c = '',
            CUSTOMER_REFERENCE_NAME__c = '', CUSTOMER_REFERENCE_PHONE__c = '',
            CUSTOMER_REFERENCE_STATE__c = '', CUSTOMER_REFERENCE_ZIP__c = '',
            DESCRIPTION__c = '', DESCRIPTION_LINE__c = '',
            DESCRIPTION_NA__c = '', Discount__c = 0,
            EFFECTIVE_FROM__c = '20170925', EFFECTIVE_TO__c = '20170925',
            ERP_QUOTE_NUMBER__c = '66666666', FREIGHT_TERMS__c = '0',
            Incoterms__c = '', Incoterms_Description__c = '',
            INVOICED_DATE__c = '', Line__c = 1,
            LIST_PRICE__c = '10',  MATERIAL_NUMBER__c = '0034212423',
            NAME__c = '0020149371', OPPORTUNITY_ID__c = '',
            ORDER_DETAIL_AMOUNT__c = '0', ORDER_DISCOUNT__c = '0',
            ORDER_FREIGHT_AMOUNT__c = '0', ORDER_PRE_FREIGHT_AMOUNT__c = '0',
            ORDER_TOTAL_AMOUNT__c = '0', ORDER_TOTAL_TAX__c = '0',
            OWNER_NAME__c = '',  OWNER_NUMBER__c = '',
            OWNER_QUOTE__c = 'S5749', OWNER_QUOTE_NAME__c = '',
            Packaging_and_Insurance_Costs__c = '', PAYMENT_TERMS__c = '',
            POTENTIAL_CUSTOMER__c = 'XXXXXXXXX', PRICE_LIST__c = '90',
            PROJECT_NAME__c = '',  QUANTITY__c = '1',
            QUANTITY_LINE__c = '1', Name = '4353453453',
            QUOTE_NUMBER__c = '00201234549371',
            SHAREPOINT_SUBFOLDER_NAME__c = '', SHIPMENT_METHOD__c = '',
            SHIPTO_ADDR_LINE1__c = 'Example', SHIPTO_ADDR_LINE2__c = 'Example',
            SHIPTO_ADDR_LINE3__c = 'Example', SHIPTO_CITY__c = '',
            SHIPTO_COUNTRY__c = '', SHIPTO_NAME__c = '',
            SHIPTO_STATE__c = '',  SHIPTO_ZIP__c = '',
            TITLE__c = '', TOTAL_WITHOUT_TAX__c = '90',
            Type__c = 'Quote', UNIT_PRICE__c = '90',
            CONDITION_ITEM_NUMBER__C = '000040', SALES_ORGANIZATION__c = '3000',
            SALES_DOC_ITEM__C = '000040', OUTPUT_LANGUAGE__c ='DE', DOCUMENT_CURRENCY__C ='USD'
        );
        
        insert xmlFileInsertA;
		
		update xmlFileInsertA;
        
        RFQ_XML__c xmlFileInsert2 = new RFQ_XML__c(
            BASE_UNIT__c = '', BILLTO_ADDR_LINE1__c = '', BILLTO_ADDR_LINE2__c = '',
            BILLTO_ADDR_LINE3__c = '', BILLTO_CITY__c = '', BILLTO_COUNTRY__c = '', BILLTO_CUSTOMER__c = '',
            BILLTO_NAME__c = '', BILLTO_STATE__c = '', BILLTO_ZIP__c = '', CONTACT_EMAIL__c = '',
            CONTACT_FAX__c = '', CONTACT_NAME__c = '',  CONTACT_PHONE__c = '', CONTRACTOR__c = '',
            CURRENCY__c = 'USD', CurrencyIsoCode = 'USD', CUSTOMER_REFERENCE_ADDR_LINE1__c = '',
            CUSTOMER_REFERENCE_ADDR_LINE2__c = '', CUSTOMER_REFERENCE_ADDR_LINE3__c = '',
            CUSTOMER_REFERENCE_CITY__c = '', CUSTOMER_REFERENCE_COUNTRY__c = '',
            CUSTOMER_REFERENCE_EMAIL__c = '', CUSTOMER_REFERENCE_FAX__c = '',
            CUSTOMER_REFERENCE_NAME__c = '', CUSTOMER_REFERENCE_PHONE__c = '',
            CUSTOMER_REFERENCE_STATE__c = '', CUSTOMER_REFERENCE_ZIP__c = '',
            DESCRIPTION__c = '', DESCRIPTION_LINE__c = '',
            DESCRIPTION_NA__c = '', Discount__c = 0,
            EFFECTIVE_FROM__c = '20170925', EFFECTIVE_TO__c = '20170925',
            ERP_QUOTE_NUMBER__c = '0020149371', FREIGHT_TERMS__c = '0',
            Incoterms__c = '', Incoterms_Description__c = '',
            INVOICED_DATE__c = '', Line__c = 1,
            LIST_PRICE__c = '20',  MATERIAL_NUMBER__c = '34212423',
            NAME__c = '0020149371', OPPORTUNITY_ID__c = '99999',
            ORDER_DETAIL_AMOUNT__c = '0', ORDER_DISCOUNT__c = '0',
            ORDER_FREIGHT_AMOUNT__c = '0', ORDER_PRE_FREIGHT_AMOUNT__c = '0',
            ORDER_TOTAL_AMOUNT__c = '0', ORDER_TOTAL_TAX__c = '0',
            OWNER_NAME__c = '',  OWNER_NUMBER__c = '',
            OWNER_QUOTE__c = 'S5749', OWNER_QUOTE_NAME__c = '',
            Packaging_and_Insurance_Costs__c = '', PAYMENT_TERMS__c = '',
            POTENTIAL_CUSTOMER__c = '1090925378', PRICE_LIST__c = '90',
            PROJECT_NAME__c = '',  QUANTITY__c = '1',
            QUANTITY_LINE__c = '1', Name = '0020149371',
            QUOTE_NUMBER__c = '0020149371',
            SHAREPOINT_SUBFOLDER_NAME__c = '', SHIPMENT_METHOD__c = '',
            SHIPTO_ADDR_LINE1__c = '', SHIPTO_ADDR_LINE2__c = '',
            SHIPTO_ADDR_LINE3__c = '', SHIPTO_CITY__c = '',
            SHIPTO_COUNTRY__c = '', SHIPTO_NAME__c = '',
            SHIPTO_STATE__c = '',  SHIPTO_ZIP__c = '',
            TITLE__c = '', TOTAL_WITHOUT_TAX__c = '90',
            Type__c = 'Order', UNIT_PRICE__c = '90'
        );
        
        insert xmlFileInsert2;
		
		update xmlFileInsert2;
        
		Decimal itemNumber = 1.00;        
        Pentair_RFQXMLParse.ProcessXMLFile(xmlFileInsert, itemNumber);
		
		Test.startTest();
        updateRFQXML urx = new updateRFQXML();
        Id batchId = Database.executeBatch(urx,1);
        Test.stopTest();
    }
}