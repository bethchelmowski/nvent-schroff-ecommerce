/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/2/19.
 */

public with sharing class x7sProductSpec implements Comparable
{
	@AuraEnabled
	public String id, name, filterType, minValue, maxValue;

	@AuraEnabled
	public List<String> values, displayValues;

	public x7sProductSpec()
	{
		this.id         = '';
		this.name       = '';
		this.filterType = '';
		this.minValue   = '';
		this.maxValue   = '';
		this.values     = new List<String>();
	}

	public x7sProductSpec(String id, String name, String filterType, String value)
	{
		this.id         = id;
		this.name       = name;
		this.filterType = filterType;
		this.values     = new List<String>{value};
	}

	public x7sProductSpec(String id, String name, String filterType, List<String> values)
	{
		this.id         = id;
		this.name       = name;
		this.filterType = filterType;


		if (filterType == 'Slider')
		{
			// sort and keep string and decimal synced
			Map<Decimal, String> sliderMap    = new Map<Decimal, String>();
			List<Decimal>        sliderValues = new List<Decimal>();

			for (String valueString : values)
			{
				Decimal sliderValue = Decimal.valueOf(valueString);

				sliderMap.put(sliderValue, valueString);
				sliderValues.add(sliderValue);
			}

			// numeric sort
			sliderValues.sort();

			this.displayValues = new List<String>();
			this.values        = new List<String>();

			for (Decimal valueDecimal : sliderValues)
			{
				this.displayValues.add(String.valueOf(valueDecimal));
				this.values.add(sliderMap.get(valueDecimal));
			}

			this.minValue = String.valueOf(sliderValues[0]);
			this.maxValue = String.valueOf(sliderValues[sliderValues.size() - 1]);
		}
		else
		{
			this.values = values;
			this.values.sort();
		}
	}

	public Integer compareTo(Object param1)
	{
		return this.name.compareTo((String)((x7sProductSpec)param1).name);
	}

	public void Dump(String title)
	{
		System.debug('X7SProductSpec: ' + title);
		System.debug('  id      : ' + this.id);
		System.debug('  name    : ' + this.name);
		System.debug('  type    : ' + this.filterType);
		System.debug('  values  : ' + this.values);
		System.debug('  display : ' + this.displayValues);
	}
}