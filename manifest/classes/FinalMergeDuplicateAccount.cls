//**********************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  	//
// 	Class Name: FinalMergeDuplicateAccount																	// 								
//	Purpose of the Class: Used to Merge Accounts															//
//	Created By: Tredence																					// 		
//	Created Date: 24/08/2018										  										//
//----------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose						//
//	||	1.0		||	24/08/2018		||	Tredence	||				||	Initial Creation					//
//**********************************************************************************************************//																

global class FinalMergeDuplicateAccount implements Database.Batchable<Sobject> 
{
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
       return Database.getQueryLocator('SELECT Id,IsDuplicate__c,Master_Account__c FROM Account WHERE IsDuplicate__c = True AND Master_Account__c != NULL');
	}
   	global void execute(Database.BatchableContext bc, List<Account> scopedAccountFiles)
    {
        
        set<id> masterids =  new set<id>();
        set<id> dupids = new set<id>();
        for(Account actid : scopedAccountFiles)
        {
            masterids.add(actid.Master_Account__c);
            dupids.add(actid.id);
        }
                
        List<AccountContactRelation> IndirectRelations = [Select ID,AccountId,IsDirect from AccountContactRelation Where AccountId IN : dupids AND IsDirect = False];
        delete IndirectRelations;
        
        Account MasterRec = [Select Id From Account where id IN:masterids];
        Account DupRec = [Select Id From Account where id IN:dupids];
        
        merge MasterRec DupRec;
        
    }
     global void finish(Database.BatchableContext bc)
    {
        
    }
}