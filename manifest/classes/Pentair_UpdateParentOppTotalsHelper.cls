public class Pentair_UpdateParentOppTotalsHelper 
{
    Private map<id,Parent_Opportunity__c> mapofParentOpportunities = new map<id,Parent_Opportunity__c>();
    Private map<string,Decimal> currencyMap = new map<string,Decimal>();
    
    Public void CheckforCurrencyChange(List<Parent_Opportunity__c> lstofnewParentOpportunities,Map<id,Parent_Opportunity__c> mapofoldParentOpportunities)
    {	
        list<Parent_Opportunity__c> lstofParenttoUpdate = new list<Parent_Opportunity__c>();
        for(Parent_Opportunity__c parentObject: lstofnewParentOpportunities)
        {
            system.debug(' ----- Inside the For ----');
            Parent_Opportunity__c oldparentObject = mapofoldParentOpportunities.get(parentObject.ID);
            system.debug(' ----- before the if------'+oldparentObject);
            system.debug('------- the values in ParentObject ------'+parentObject);
            if (oldparentObject.CurrencyIsoCode != parentObject.CurrencyIsoCode)
            {
                Parent_Opportunity__c aux  = new Parent_Opportunity__c(Id = parentObject.Id); 
                aux.Update_Totals__c = true;                  
                //update aux;
                lstofParenttoUpdate.add(aux);
            }
        }
        system.debug('------ Updating the Parent Object ------'+lstofParenttoUpdate);
        Update lstofParenttoUpdate;
    }
    Public void CalculateTotalAmount(List<Parent_Opportunity__c> lstofnewParentOpportunities,Map<id,Parent_Opportunity__c> mapofoldParentOpportunities)
    {
        
        System.debug(' ------ Inside the CalculateTotalAmount ---------');
        Set<ID> ParentOpportunityIDs = mapofoldParentOpportunities.keySet(); // adding all Parent Ids to the set
        
        List<CurrencyType> currencyList = [SELECT Id, IsoCode, ConversionRate FROM CurrencyType];
        
        for(CurrencyType currencyvalue:currencyList)
        {
            currencyMap.put(currencyvalue.IsoCode,currencyvalue.ConversionRate);
        }
        system.debug('---------- Prepared the map of currency  -----------'+currencyMap);
        for(Opportunity childOpportunities:[SELECT id,Amount, CurrencyIsoCode,Parent_Opportunity__C,Parent_Opportunity__r.CurrencyIsoCode,Parent_Opportunity__r.Update_Totals__c,Channel_Duplicate__c,IsWon,IsClosed FROM Opportunity 
                                            WHERE Parent_Opportunity__C IN :ParentOpportunityIDs])
        {
            //Parent_Opportunity__c parentOpportunity = new Parent_Opportunity__c(Id = childOpportunities.Parent_Opportunity__C);
            // Open childOpportunities
            if(childOpportunities.Parent_Opportunity__r.Update_Totals__c != false)
            {
                Decimal AmountofOpenOpportunities = 0.0;
                Decimal AmountofClosedOpportunities = 0.0;
                Decimal ChannelAmount = 0.0;
                system.debug('---------- Inside For Before data Conversion  -----------'+childOpportunities);
                if(childOpportunities.IsClosed != True && childOpportunities.Channel_Duplicate__c != True)
                {
                    AmountofOpenOpportunities = returnConvertedAmount(childOpportunities.Parent_Opportunity__r.CurrencyIsoCode,childOpportunities.CurrencyIsoCode,childOpportunities.Amount);
                }
                else if(childOpportunities.IsClosed == True && childOpportunities.Channel_Duplicate__c != True)
                {
                    AmountofClosedOpportunities = returnConvertedAmount(childOpportunities.Parent_Opportunity__r.CurrencyIsoCode,childOpportunities.CurrencyIsoCode,childOpportunities.Amount);
                }
                else if(childOpportunities.IsClosed != True && childOpportunities.Channel_Duplicate__c == True)
                {
                    ChannelAmount = returnConvertedAmount(childOpportunities.Parent_Opportunity__r.CurrencyIsoCode,childOpportunities.CurrencyIsoCode,childOpportunities.Amount);
                }
                Parent_Opportunity__c parentOpportunity = new Parent_Opportunity__c();
                if(mapofParentOpportunities.containsKey(childOpportunities.Parent_Opportunity__C))
                {
                    parentOpportunity = mapofParentOpportunities.get(childOpportunities.Parent_Opportunity__C);
                    parentOpportunity.Channel_Duplicate_Amount__c += ChannelAmount;
                    parentOpportunity.Total_Amount_of_Closed_Opportunities__c += AmountofClosedOpportunities;
                    parentOpportunity.Total_Amount_of_Open_Opportunities__c += AmountofOpenOpportunities;
                    parentOpportunity.Total_Amount__c += (AmountofOpenOpportunities + AmountofClosedOpportunities);
                    parentOpportunity.Update_Totals__c = False;
                    mapofParentOpportunities.put(childOpportunities.Parent_Opportunity__C, parentOpportunity);
                }
                else
                {
                    parentOpportunity = new Parent_Opportunity__c(Id = childOpportunities.Parent_Opportunity__C);
                    parentOpportunity.Channel_Duplicate_Amount__c = ChannelAmount;
                    parentOpportunity.Total_Amount_of_Closed_Opportunities__c = AmountofClosedOpportunities;
                    parentOpportunity.Total_Amount_of_Open_Opportunities__c = AmountofOpenOpportunities;
                    parentOpportunity.Total_Amount__c = (AmountofOpenOpportunities + AmountofClosedOpportunities);
                    parentOpportunity.Update_Totals__c = False;
                    mapofParentOpportunities.put(childOpportunities.Parent_Opportunity__C, parentOpportunity);
                }
                
            }
        }
        List<Parent_Opportunity__c> lsttoinsert = mapofParentOpportunities.values();
        update lsttoinsert;
    }
    Private Decimal returnConvertedAmount(String ParrentOppCurrencyCode,String OppCurrencyCode,Decimal Amount)
    {
        if(ParrentOppCurrencyCode != OppCurrencyCode && ParrentOppCurrencyCode != 'USD')
        {
            return (Amount * currencyMap.get(ParrentOppCurrencyCode));
        }
        if(ParrentOppCurrencyCode == 'USD')
        {
            return (Amount / currencyMap.get(OppCurrencyCode)); 
        }
        else
        {
            return Amount;
        }
    }
}