/* ********************************************
Developer -     Apurva Prasade
Class -         nVent_MultipleAccTeamMemClassTest
Description -   Test class for nVent_MultipleAccMemClass

Developer   Date           Version
Apurva P    03/01/2020     V1.0

********************************************** */

@isTest
public with sharing class nVent_MultipleAccTeamMemClassTest {
    @TestSetup
    static void createData()
    {
        Profile prflst = [Select Id from Profile where Name =: 'System Administrator' limit 1];
        
        User objUser = new User();
        objUser.FirstName = 'fNameTest';
        objUser.LastName = 'lNameTest';
        objUser.Email = 'aptest@test.com';
        objUser.isActive = true;
        objUser.ProfileId = prflst.Id;
        objUser.Username = 'aptest@test.com' + System.currentTimeMillis();
        objUser.CompanyName = 'testComp';
        objUser.Title = 'Developer';
        objUser.Alias = 'testal';
        objUser.TimeZoneSidKey = 'America/Los_Angeles';
        objUser.EmailEncodingKey = 'UTF-8';
        objUser.LanguageLocaleKey = 'en_US';
        objUser.LocaleSidKey = 'en_US';
        objUser.SAP_Id__c = 'S000';
        insert objUser;

        Account objAccount = new Account();
        objAccount.Name = 'testCompany';
        objAccount.Subvertical__c = 'Oil and Gas';
        objAccount.BillingCountryCode = 'FR';
        objAccount.BillingStreet = 'test';
        objAccount.BillingCity = 'Ttest';
        insert objAccount;

        AccountTeamMember objAM1 = new AccountTeamMember();
        objAM1.AccountId = objAccount.ID;
        objAM1.TeamMemberRole = 'Schroff Rep';
        objAM1.UserId = objUser.ID;
        insert objAM1;
		
        AccountTeamMember objAM2 = new AccountTeamMember();
        objAM2.AccountId = objAccount.ID;
        objAM2.TeamMemberRole = 'Schroff Rep';
        objAM2.UserId = objUser.ID;
        insert objAM2;

        AccountTeamMember objAM3 = new AccountTeamMember();
        objAM3.AccountId = objAccount.ID;
        objAM3.TeamMemberRole = 'IHS Rep';
        objAM3.UserId = objUser.ID;
        insert objAM3;

        AccountTeamMember objAM4 = new AccountTeamMember();
        objAM4.AccountId = objAccount.ID;
        objAM4.TeamMemberRole = 'BIS Rep';
        objAM4.UserId = objUser.ID;
        insert objAM4;

        Id rfqvRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        RFQ__c objRFQ5 = new RFQ__c();
        objRFQ5.Account__c = objAccount.Id;
        objRFQ5.RecordTypeId = rfqvRecordTypeId;
        //objRFQ5.SAP_Sales_Org__c = '6320';
        //objRFQ5.Product_Type__c = 'Standard';
        objRFQ5.ERP_Number__c = 'ABDC1234';
        objRFQ5.RFQ_Received__c = Date.today();
        objRFQ5.Sales_Team__c = 'Schroff';
        objRFQ5.Type__c = 'Customized';
        objRFQ5.Sold_To_Number__c = '70080075';
        insert objRFQ5;
    }

    public static testMethod void testMH1()
    {
        Id rfqvRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        RFQ__c objRFQ = [Select Id from RFQ__c where RecordTypeId =: rfqvRecordTypeId Limit 1 ];
        
        String msg = nVent_MultipleAccTeamMemClass.getAccounMeminfo(objRFQ.Id);
    }
}