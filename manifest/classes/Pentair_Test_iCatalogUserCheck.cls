@isTest
private class Pentair_Test_iCatalogUserCheck {
    @isTest static void iCatalogUserCheckTest(){
        //Create Test Account Account
        Account newAccount = new Account(
            Name='Test Account 123456789',
            BillingCountryCode = 'US',
            BillingCity = 'Example',
            BillingStreet = '1111 Example'
        );
        insert newAccount;
        
        Contact contactObject = new Contact();
        contactObject.LastName = 'Test Contact Last Name';
        contactObject.AccountId = newAccount.Id;
        contactObject.Email = 'test@hello.com';
        insert contactObject;
        
        //Test 1
        Lead leadObject = new Lead();
        leadObject.FirstName = 'Testing iCatalog';
        leadObject.LastName = 'iCatalog Trigger';
        leadObject.Email = 'test@hello.com';
        leadObject.Company = 'iCatalog Trigger Test Account';
        leadObject.Catalog_User__c = 'nvent=pentair@example.com';
        leadObject.Create_Follow_Up_Tasks__c = true;
        leadObject.Sales_Team__c = 'Schroff';
        
        insert leadObject;
        
        //Test 2
        Lead leadObject2 = new Lead();
        leadObject2.FirstName = 'Testing iCatalog 2';
        leadObject2.LastName = 'iCatalog Trigger 2';
        leadObject2.Email = 'test22@hello.com';
        leadObject2.Company = 'iCatalog Trigger Test Account 2';
        leadObject2.Catalog_User__c = 'nvent=pentair@example.com';
        leadObject2.Create_Follow_Up_Tasks__c = true;
        leadObject2.Sales_Team__c = 'Hoffman';
        
        insert leadObject2;
    }
}