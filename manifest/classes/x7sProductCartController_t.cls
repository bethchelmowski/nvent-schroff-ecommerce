/**
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

@IsTest
private class x7sProductCartController_t
{
	@IsTest
	static void testCartList()
	{
		x7sProductCartList cartList = new x7sProductCartList();
		System.assertNotEquals(null, cartList);
		System.assertNotEquals(null, cartList.items);
	}
}