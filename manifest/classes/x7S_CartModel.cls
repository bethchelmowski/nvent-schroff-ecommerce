/**
* Created By ankitSoni on 05/27/2020
*/
public with sharing class x7S_CartModel {
    
    @AuraEnabled
    public String cartId,cartEncId,currencyISOCode;
    
	@AuraEnabled
	public Decimal totalQuantity, totalPrice, itemCount;

	@AuraEnabled
	public List<x7S_CartItemModel> items;

    public x7S_CartModel() {
        cartId = '';
        cartEncId = '';
		items  = new List<x7S_CartItemModel>();
    }

    public x7S_CartItemModel findCartItem(String productId)
	{
		for (x7S_CartItemModel item : items)
		{
			if (item.productId == productId)
			{
				return item;
			}
		}

		return null;
	}
}