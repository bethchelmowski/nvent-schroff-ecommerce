/**********************************************************************
Name:  NVent_ITriggerHandler
Developed by: Anvi Kanodra
=======================================================================
Req: 
US000001 - Use Trigger Framework
=======================================================================
Purpose:                                                            
This is an interface. It provides a framework to all Trigger
Handlers. All trigger handlers should implement this interface.
=======================================================================
History                                                            
-------                                                            
VERSION  AUTHOR            DATE              DETAIL
1.0 -                                  Initial Development
***********************************************************************/


public interface NVent_ITriggerHandler{
    void beforeInsert(List<SObject> newItems);
    void beforeUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
    void beforeDelete(Map<Id, SObject> oldItems);
    void afterInsert(Map<Id, SObject> newItems);
    void afterUpdate(Map<Id, SObject> newItems, Map<Id, SObject> oldItems);
    void afterDelete(Map<Id, SObject> oldItems);
    void afterUndelete(Map<Id, SObject> oldItems);
   
}