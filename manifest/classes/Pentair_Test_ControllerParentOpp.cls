@isTest(SeeAllData=true)
private class Pentair_Test_ControllerParentOpp {
    static testMethod void TestMethodOpportunity1()
    {  
        //Create Test Account Account
        Account newAccount = new Account(
            Name='Test Account 123456789',
            ShippingCountryCode = 'US',
            BillingCountryCode = 'US',
            BillingCity = 'Example',
            BillingStreet = '1111 Example'
        );
        insert newAccount;
        
        List<PriceBook2> getPriceBook = [SELECT Id FROM PriceBook2 LIMIT 1 ];
        
        //Parent Opportunity Record
        Parent_Opportunity__c parentOpportunity = new Parent_Opportunity__c(
            Name = 'Test Parent Opportunity',
            CurrencyIsoCode = 'USD'
        );
        insert parentOpportunity;
        
        //Opportunity Records
		List<Opportunity> lstOpportunity = new List<Opportunity>();     
        Opportunity testOpportunity = new Opportunity(
        	Name='Test Opp1',
           	CloseDate = Date.today(),
            First_Revenue_Date__c = Date.today(),
            StageName = 'Establish',
            AccountId = newAccount.Id,
            Sales_Team__c = 'BIS',
            Probability = 25,
            Amount = 5000,
            CurrencyIsoCode = 'USD'
		);
        
        Opportunity testOpportunity2 = new Opportunity(
        	Name='Test Opp2',
           	CloseDate = Date.today(),
            First_Revenue_Date__c = Date.today(),
            StageName = 'Establish',
            AccountId = newAccount.Id,
            Sales_Team__c = 'Schroff',
            Probability = 25,
            Channel_Duplicate__c = true,
            Amount = 5000,
            CurrencyIsoCode = 'USD'
		);
        
        lstOpportunity.add(testOpportunity);   
        lstOpportunity.add(testOpportunity2); 
        insert lstOpportunity;  
               
        //Link the Parent Opportunity to the record
        testOpportunity2.Parent_Opportunity__c = parentOpportunity.Id;
        update testOpportunity2;
        
        testOpportunity.Parent_Opportunity__c = parentOpportunity.Id;
        update testOpportunity;
        
        //Update the currency code
        parentOpportunity.CurrencyIsoCode = 'CAD';
        update parentOpportunity;
        
        parentOpportunity.CurrencyIsoCode = 'USD';
        update parentOpportunity;

        //Price Book Records
        PriceBook2 xPriceBook2 = [SELECT ID, Name FROM PriceBook2 WHERE Name = 'BIS' LIMIT 1];
        PriceBookEntry xOppEntryId = [SELECT ID, Name FROM PriceBookEntry 
                                      WHERE PriceBook2Id =: xPriceBook2.Id 
                                      AND CurrencyIsoCode = 'USD' 
                                      AND ISActive = true LIMIT 1];
        
        OpportunityLineItem line1 = new OpportunityLineItem(
            Number_of_Installments__c = 3,
            Schedule_Type__c = 'Divide Amount into multiple installments',
            UnitPrice = 5000,
            Installment_Period__c = 'Monthly',
            Quantity = 1,
            ServiceDate = Date.today(),
            PricebookEntryId = xOppEntryId.Id,
            OpportunityId = lstOpportunity[0].Id
        );
        insert line1;
        
        OpportunityLineItem line2 = new OpportunityLineItem(
            Number_of_Installments__c = 3,
            Schedule_Type__c = 'Divide Amount into multiple installments',
            UnitPrice = 5000,
            Installment_Period__c = 'Monthly',
            Quantity = 1,
            PricebookEntryId = xOppEntryId.Id,
            OpportunityId = lstOpportunity[0].Id
        );
        insert line2;
        
        OpportunityLineItemSchedule schedule1 = new OpportunityLineItemSchedule(
            OpportunityLineItemId = line1.Id,
            Revenue = 5000,
            ScheduleDate = Date.today(),
            Type = 'Revenue'
        );
        
        insert schedule1;
             
        Test.startTest();
        

        
        //Pentair_ProductScheduleExtension
        ApexPages.StandardController sc3 = new ApexPages.StandardController(line1); 
        Pentair_ProductScheduleExtension controller3 = new Pentair_ProductScheduleExtension(sc3);
        PageReference pageRef3 = Page.Pentair_EditProductSchedule;
        pageRef3.getParameters().put('index', String.valueOf(1));
        pageRef3.getParameters().put('id', String.valueOf(schedule1.Id));
        Test.setCurrentPage(pageRef3);
        
        controller3.objProduct = line1;

        controller3.getOLIS(); 
        controller3.UpdateFirstRevenueDate();
        controller3.masterList(); 
        controller3.SaveAndRefresh();
        controller3.SaveForm();  
        controller3.addNewLine();
        
        controller3.RecordToDelete = line1.Id;
        controller3.RemoveItems();
            
        Test.stopTest();        
    }
    

}