/* ********************************************
Developer -     Apurva Prasade
Class -         nVent_RFQ_TriggerHandler
Description -   Trigger handler for RFQ trigger

Developer   Date        Version
Apurva P    15-07-2019  V1.0

********************************************** */
public class nVent_RFQ_TriggerHandler {

    public static void beforeinsertUtil(list<RFQ__c> listNewRFQ)
    {
        beforeInsertupdatep3Scope(listNewRFQ);
        //beforeInserSHDate(listNewRFQ);
        beforeInsertSetRT(listNewRFQ);
        beforeInsertPopulateSAP(listNewRFQ);
        //
        beforeInsertPopulateDummyValues(listNewRFQ);
    }

    public static void beforeUpdateUtil(list<RFQ__c> listNewRFQ,map<Id, RFQ__c> mapOldRFQ)
    {
        beforeUpdateSetRT(listNewRFQ,mapOldRFQ);
        beforeUpdateupdatep3Scope(listNewRFQ,mapOldRFQ);
        beforeUpdateSHChange(listNewRFQ,mapOldRFQ);
    }

    public static void afterinsertUtil(list<RFQ__c> listNewRFQ)
    {
        //afterInsertAddLineItems(listNewRFQ);
        afterInsertRFQNumber(listNewRFQ);
    }

    public static void afterUpdateUtil(list<RFQ__c> listNewRFQ,map<Id, RFQ__c> mapOldRFQ)
    {
        //afterUpdateAddLineItems(listNewRFQ,mapOldRFQ);
        afterUpdateShareRecord(listNewRFQ,mapOldRFQ);
    }
    //Handler method for Pentair_P3_Scope trigger - before insert
    /**
    Update scope field
     */
    public static void beforeInsertupdatep3Scope(list<RFQ__c> listNewRFQ)
    {
        for (RFQ__c rfqObject : listNewRFQ)
            nVent_RFQ_TriggerHelper.concateScope(rfqObject);
    }
    /**
        Populate salesteam,Assignto, Proposal number on RFQ
    */
    public static void beforeInsertPopulateSAP(list<RFQ__c> listNewRFQ)
    {
        list<String> listSAPIDs = new list<String>();
        list<RFQ__c> listRFQs = new list<RFQ__c>();
        list<String> listSalesGroups = new list<String>();
        list<Id> listAccIds = new list<Id>();
        Id RFQRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        Id QuoteRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('Quote').getRecordTypeId();
        RFQ_Setting_H__c csRFQSetting = RFQ_Setting_H__c.getOrgDefaults();
        map<String, RFQ__c> mapODRfq = new map<String, RFQ__c>();
        for(RFQ__c obj : listNewRFQ)
        {   
            //List of RFQs created by integration ??? Tricky condition
            if(obj.Owner_Doc__c != null)
            {
                listSAPIDs.add('%'+obj.Owner_Doc__c+'%');
                listSalesGroups.add(obj.Sales_Group__c);
                mapODRfq.put(obj.Owner_Doc__c,obj);
            }
            //list of RFQs created manually
            else 
            {
                listRFQs.add(obj);
                listAccIds.add(obj.Account__c);
            }
        } 
        //RFQs created by integration
        if(listSAPIDs.size() > 0)
        {
            for(User objUser : [select Id,Primary_Sales_Team__c,SAP_ID__c from User Where SAP_ID__c like: listSAPIDs])
            {
                System.debug('objUser '+objUser);
                //if(objUser.SAP_ID__c.contains(','))
                //{
                    list<String> listTemp = new list<String>();
                    System.debug('objUser.SAP_ID__c '+objUser.SAP_ID__c);
                    if(objUser.SAP_ID__c.contains(','))
                        listTemp.addAll(objUser.SAP_ID__c.split(','));
                    else
                        listTemp.add(objUser.SAP_ID__c);
                    System.debug('listTemp '+objUser);
                    if(listTemp.size() > 0)
                    {
                        for(String str : listTemp)
                        {
                            if(mapODRfq.containskey(str))
                            {
                                System.debug('mapODRfq.get(str) '+mapODRfq.get(str));
                                RFQ__c objRFQ = mapODRfq.get(str);
                                
                                objRFQ.Proposal_Owner__c = objUser.Id;
                                //objRFQ.Type__c = 'Standard';
                                if(objRFQ.RFQ_Received__c != null)
                                    objRFQ.Scheduled_Completion__c = Pentair_RFQController.SetDateByBusinessDays(objRFQ.RFQ_Received__c, 2);
                                System.debug('objRFQ '+objRFQ);
                            }
                        }
                    }
                //}
            }
        }
        if(listSalesGroups.size()> 0)
        {
            for(User objUser : [select Id,Primary_Sales_Team__c,SAP_ID__c from User Where SAP_ID__c in: listSalesGroups])
            {
                for(RFQ__c obj : listNewRFQ)
                {
                    if(obj.SAP_Sales_Org__c != null)
                    {
                        if(obj.Sales_Group__c == objUser.SAP_ID__c)
                        {
                            obj.Assigned_To__c = objUser.Id;
                            obj.Sales_Team__c = objUser.Primary_Sales_Team__c;
                            if(csRFQSetting.Enclosures_Sales_Org_IDs__c.contains(obj.SAP_Sales_Org__c))
                                obj.Type__c = 'Standard';
                        }
                    }
                }
            }
        }
        // RFQs created manually
        if(listRFQs.size() > 0)
            nVent_RFQ_TriggerHelper.populateProposalAssign(listNewRFQ, listAccIds);
    }

    /**
    Update scope field
    Update record type on change of Linked External system
    EPR number update if changed
     */
    public static void beforeUpdateupdatep3Scope(list<RFQ__c> listNewRFQ,map<Id, RFQ__c> mapOldRFQ)
    {
        Id p3vRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('P3 Readonly').getRecordTypeId();
        for (RFQ__c rfqObject : listNewRFQ)
        {
            if (rfqObject.Resources_Includes__c != mapOldRFQ.get(rfqObject.Id).Resources_Includes__c || 
                rfqObject.Product_Type__c != mapOldRFQ.get(rfqObject.Id).Product_Type__c)
            {
                //String multiSelect = rfqObject.Resources_Includes__c;
                nVent_RFQ_TriggerHelper.concateScope(rfqObject);
            }
            
            if (rfqObject.Linked_External_System__c != null && 
                rfqObject.Linked_External_System__c != mapOldRFQ.get(rfqObject.Id).Linked_External_System__c )
            {
                if(p3vRecordTypeId != null)   
                    rfqObject.RecordTypeId = p3vRecordTypeId;
            }
            //Update EPR Number if changed
            /*if(mapOldRFQ.get(rfqObject.id).ERP_Number__c != rfqObject.ERP_Number__c)
            {
                rfqObject.Value__c = 0.00;
            }*/
        }
    }
    /**
        Populate recordtypes
    */
    public static void beforeInsertSetRT(list<RFQ__c> listNewRFQ)
    {
        Id RFQRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        Id QuoteRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('Quote').getRecordTypeId();
        RFQ_Setting_H__c csRFQSetting = RFQ_Setting_H__c.getOrgDefaults();
        system.debug('csRFQSetting '+csRFQSetting);
        
        for(RFQ__c obj : listNewRFQ)
        {
            if(obj.SAP_Sales_Org__c != null)
            {
                
                if(csRFQSetting.Enclosures_Sales_Org_IDs__c.contains(obj.SAP_Sales_Org__c))
                {
                    system.debug('obj.SAP_Sales_Org__c '+obj.SAP_Sales_Org__c);
                    obj.RecordTypeId = RFQRecordTypeId;
                    //obj.Type__c = 'Standard';
                }
                if(csRFQSetting.Thermal_Sales_Org_IDs__c.contains(obj.SAP_Sales_Org__c))
                {
                    system.debug('obj.SAP_Sales_Org__c '+obj.SAP_Sales_Org__c);
                    obj.RecordTypeId = QuoteRecordTypeId;
                }
            }
        }
        system.debug(listNewRFQ);
    }
    
    /**
    Popuate dummy account and dummy contacts
    */
    public static void beforeInsertPopulateDummyValues(list<RFQ__c> listNewRFQ)
    {
        RFQ_Setting_H__c csRFQSetting = RFQ_Setting_H__c.getOrgDefaults();
        Id RFQRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        Id QuoteRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('Quote').getRecordTypeId();
        map<String, Account> mapAccount = new map<String, Account>(); 
        map<String, RFQ__c> mapThermalRFQs = new map<String, RFQ__c>();
        list<Account> listAccountInsert = new list<Account>();
        map<String,RFQ__c> mapSAPRFQ = new map<String,RFQ__c>();
		map<String, RFQ__c> mapAccNames = new map<String, RFQ__c>();
		
        for(RFQ__c obj : listNewRFQ)
        {
            if(obj.SAP_Sales_Org__c != null)
            {
                if(csRFQSetting.Enclosures_Sales_Org_IDs__c.contains(obj.SAP_Sales_Org__c))
                {
                    obj.Account__c = csRFQSetting.Enclosures_Default_Account__c;
                    obj.Contact__c = csRFQSetting.Enclosures_Default_Contact__c;
                    obj.OwnerId = csRFQSetting.Enclosures_Default_Owner__c;
                    ///obj.Type__c = 'Standard';
                }
                else if(csRFQSetting.Thermal_Sales_Org_IDs__c.contains(obj.SAP_Sales_Org__c))
                {
					System.debug('obj.Ship_To_Number__c  '+obj.Ship_To_Number__c);
					if(obj.Sold_To_Number__c != null)
					{
						mapThermalRFQs.put('%'+obj.Sold_To_Number__c+'%',obj);
						mapSAPRFQ.put(obj.Sold_To_Number__c,obj);
					}
					else if(obj.Ship_To_Number__c != null)
					{
						mapThermalRFQs.put('%'+obj.Ship_To_Number__c+'%',obj);
						mapSAPRFQ.put(obj.Ship_To_Number__c,obj);
					}
					else if(obj.Bill_To_Name__c != null)
						mapAccNames.put(obj.Bill_To_Name__c, obj);
                }
                    
            }
        }
        System.debug('mapThermalRFQs  '+mapThermalRFQs);
        System.debug('mapSAPRFQ  '+mapSAPRFQ);
        //if(mapThermalRFQs.size() > 0)
        //{
            System.debug('mapThermalRFQs IF *****  '+mapThermalRFQs);
			list<Account> listAccountSAPs = new list<Account>();
			list<Account> listAccountName = new list<Account>();
			listAccountSAPs = [Select Id, SAP__c from Account Where SAP__c like: mapThermalRFQs.keyset()];
			listAccountName = [Select Id, NAme, SAP__c from Account Where Name in: mapAccNames.keyset()];
			System.debug('listAccountName IF *****  '+listAccountName);
			if(listAccountSAPs.size() > 0)
			{
				for(Account obj : listAccountSAPs)
					mapAccount.put(obj.SAP__c,obj);
			}
			System.debug('mapAccount  *****  '+mapAccount);
            
                for(String key : mapThermalRFQs.keySet())
                {
                    String rfqSAP = key.remove('%');
                    System.debug('rfqSAP  *****  '+rfqSAP);
                    Integer count = 0;
                    if(mapAccount.size() > 0)
                    {
                        for(String accKey : mapAccount.keySet())
                        {
                            if(accKey.contains(rfqSAP))
                            {
                                RFQ__c obj = mapThermalRFQs.get(key);
                                obj.Account__c = mapAccount.get(accKey).Id;
                                count++;
                            }
                        }
                    }
                    System.debug('count  *****  '+count);
                    if(count == 0)
                    {
						//Fine account using name else create new account
						if(listAccountName.size() > 0)
						{
							for(Account obj : listAccountName)
								mapAccount.put(obj.Name,obj);
						}
						
						for(String nm : mapAccNames.keyset())
						{
							Integer count1 = 0;
							if(mapAccount.size() > 0)
							{
								for(String accKey : mapAccount.keySet())
								{
									if(nm == acckey)
									{
										RFQ__c obj = mapAccNames.get(nm);
										obj.Account__c = mapAccount.get(accKey).Id;
										count1++;
									}
								}
							}
							if(count1 == 0)
							{
								//create Account
								RFQ__c obj = mapThermalRFQs.get(key);
								Account newAccount = new Account(
									Name = obj.Bill_To_Name__c,
									SAP__C = obj.Sold_To_Number__c,
									BillingStreet = 'Street',
									BillingCity = obj.Bill_To_City__c,
									BillingPostalCode = obj.Bill_To_Postal_Code__c,
									BillingCountryCode = obj.Bill_To_Country__c
								);
								listAccountInsert.add(newAccount);
							}
						}  
                    }
                }
			//}
            
        //}
        if(listAccountInsert.size() > 0)
            insert listAccountInsert;
        map<Id, Account> mapAccountsIns = new map<Id, Account>(listAccountInsert);

        for(Id accId : mapAccountsIns.keySet())
        {
            if(mapSAPRFQ.containskey(mapAccountsIns.get(accId).SAP__c))
            {
                RFQ__c obj = mapSAPRFQ.get(mapAccountsIns.get(accId).SAP__c);
                obj.Account__c = accId;
            }
        }
        
    }
        
    /**
    If salesteam is Scroff or Hoffman, update few fields - 
    If RFQ type is 'Customized'
    Scheduled competition should be RFQ_Received__c + 5 wkend excluded
    else
    Scheduled competition should be RFQ_Received__c + 2 wkend excluded

     */

    public static void beforeUpdateSHChange(list<RFQ__C> listNewRFQ, map<Id, RFQ__c> mapOldRFQ)
    {
        for (RFQ__c rfqObject: listNewRFQ)
        {        
            //Only features done for Schroff and Hoffman //Apurva
            if ((rfqObject.RFQ_Received__c != mapOldRFQ.get(rfqObject.Id).RFQ_Received__c && rfqObject.RFQ_Received__c != null) || 
                (rfqObject.Sales_Team__c != mapOldRFQ.get(rfqObject.Id).Sales_Team__c) || (rfqObject.Type__c != mapOldRFQ.get(rfqObject.Id).Type__c))
            {
                nVent_RFQ_TriggerHelper.populateScheduledDate(rfqObject);
            }
        }
    }

    public static void afterInsertRFQNumber(list<RFQ__C> listNewRFQ)
    {
        list<RFQ__C> listRFQUpdate = new list<RFQ__C>();
        String quoteId;
        String quoteNumber;
        String proposalNumber;
        String opportunityId = '';
        Decimal revisionNumber = 0;
        String shipToCode = '';
        RFQ_Setting_H__c csRFQSetting = RFQ_Setting_H__c.getOrgDefaults();
        for (RFQ__c quoteObject: listNewRFQ)
        {
            quoteId = quoteObject.Id;
            quoteNumber = quoteObject.RFQ_Auto_Number__c;
            proposalNumber = quoteObject.Proposal_Number__c;
            revisionNumber = quoteObject.Revision_Number__c;
            shipToCode = quoteObject.Ship_To_Country__c;
        
            if (proposalNumber == null)
            {
                
                RFQ__c aux  = new RFQ__c(Id = quoteId); 
                if(quoteObject.SAP_Sales_Org__c != null)
                {
                    if(csRFQSetting.Enclosures_Sales_Org_IDs__c.contains(quoteObject.SAP_Sales_Org__c))
                    {
                        aux.Proposal_Number__c = nVent_RFQ_TriggerHelper.SetProposalNumer(quoteNumber, quoteId, shipToCode);
                    }
                    else if(csRFQSetting.Thermal_Sales_Org_IDs__c.contains(quoteObject.SAP_Sales_Org__c))
                    {
                        aux.Proposal_Number__c = nVent_RFQ_TriggerHelper.SetProposalNumer(quoteNumber, quoteId,'');
                    }
                }
                else {
                    aux.Proposal_Number__c = nVent_RFQ_TriggerHelper.SetProposalNumer(quoteNumber, quoteId,'');
                }
                
                //update aux;
                listRFQUpdate.add(aux);
            }
            else
            {
                //Clone Record
                System.debug('Is Cloning');
                RFQ__c aux2  = new RFQ__c(Id = quoteId); 
                aux2.Revision_Number__c = revisionNumber + 1;
                aux2.RFQ_Received__c = Date.valueOf(date.today());
                aux2.Requested_By__c = Date.valueOf(date.today());
                //update aux2;
                listRFQUpdate.add(aux2);
            }
        }
        if(listRFQUpdate.size() > 0)
            update listRFQUpdate;
    }
    /**
        Populate recordtypes
    */
    public static void beforeUpdateSetRT(list<RFQ__c> listNewRFQ, map<Id, RFQ__c> mapOldRFQ)
    {
        Id RFQRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        Id QuoteRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('Quote').getRecordTypeId();
        RFQ_Setting_H__c csRFQSetting = RFQ_Setting_H__c.getOrgDefaults();
        system.debug('csRFQSetting '+csRFQSetting);
        
        for(RFQ__c obj : listNewRFQ)
        {
            if(obj.RecordTypeId != mapOldRFQ.get(obj.Id).RecordTypeId)
            {
                if(obj.SAP_Sales_Org__c != null)
                {
                    if(csRFQSetting.Enclosures_Sales_Org_IDs__c.contains(obj.SAP_Sales_Org__c))
                    {
                        system.debug('obj.SAP_Sales_Org__c '+obj.SAP_Sales_Org__c);
                        obj.RecordTypeId = RFQRecordTypeId;
                    }
                    if(csRFQSetting.Thermal_Sales_Org_IDs__c.contains(obj.SAP_Sales_Org__c))
                    {
                        system.debug('obj.SAP_Sales_Org__c '+obj.SAP_Sales_Org__c);
                        obj.RecordTypeId = QuoteRecordTypeId;
                    }
                }
            }
            
        }
        system.debug(listNewRFQ);
    }
    /******
        Need to bulkify - Apurva
    ********/
    public static void afterUpdateShareRecord(list<RFQ__C> listNewRFQ, map<Id, RFQ__c> mapOldRFQ)
    {
        list<RFQ__Share> listRFQShare = new list<RFQ__Share>();
        if (Pentair_RFQController.runOnce())
        {
            for(RFQ__c rfqObject : listNewRFQ)
            {
                RFQ__c oldRFQ = mapOldRFQ.get(rfqObject.ID);
                if (oldRFQ.OwnerId != rfqObject.OwnerId && rfqObject.CreatedById != rfqObject.OwnerId)
                {
                    //Boolean isCreated = Pentair_ManualSharingRecord.manualShareRFQRead(rfqObject.Id, rfqObject.CreatedById);
                    // Create new sharing object for the custom object Job.
                    RFQ__Share jobShr  = new RFQ__Share();
                    // Set the ID of record being shared.
                    jobShr.ParentId = rfqObject.Id;
        
                    // Set the ID of user or group being granted access.
                    jobShr.UserOrGroupId = rfqObject.CreatedById;
                    // Set the access level.
                    jobShr.AccessLevel = 'Read';
                    jobShr.RowCause = Schema.RFQ__Share.RowCause.Manual;
                    listRFQShare.add(jobShr);
                        //if (!isCreated)
                            //System.debug('Failed on sharing record for ' + rfqObject.CreatedById + ' on record: ' + rfqObject.Id);
                }
            }
            if(listRFQShare.size() > 0)
            {
                for(Database.SaveResult res : Database.insert(listRFQShare, false))
                {
                    if(!res.isSuccess())
                    {
                        // Get first save result error.
                        Database.Error err = res.getErrors()[0];
                        if(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  
                        err.getMessage().contains('AccessLevel')){
                            // Indicates success.
                            //return true;
                            //Do something
                        }
                    }
                    else
                    {
                        //Do something
                    }
                }
            }
        }
    }    
}