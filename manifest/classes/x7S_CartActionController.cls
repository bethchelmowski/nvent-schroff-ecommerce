/**
* Created By ankitSoni on 05/27/2020
*/
public with sharing class x7S_CartActionController {
    
    public static final String CART_UPDATE = 'update';
	public static final String CART_ADD    = 'add';
	public static final String CART_DELETE = 'delete';
    
    /**
     * @return : current cart model of the logged in User.
     */
    @AuraEnabled
    public static x7S_CartModel getCurrentCart(){
        String cartId = ''; // sfid of the current logged in user's Cart
        String userId = UserInfo.getUserId(); // sfid of the logged in user
        System.debug('getCurrentCart()');

        x7S_CartModel model = new x7S_CartModel();
        
        Map<String, Object> inputData = new Map<String, Object> {
			ccrz.ccApi.API_VERSION    => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccApiCart.ACTIVECART => true,
			ccrz.ccApiCart.CARTTYPE   => 'Cart',
            ccrz.ccApiCart.BYOWNER    => userId,
			ccrz.ccApiCart.BYUSER     => userId,
			ccrz.ccApi.SIZING         => new Map<String, Object>
				{ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL}}
		};
		try {
			Map<String, Object> outputData = ccrz.ccApiCart.fetch(inputData);
			
			if (outputData.get(ccrz.ccApiCart.CART_OBJLIST) != null) {
				List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) outputData.get(ccrz.ccApiCart.CART_OBJLIST);
                //outputCartList contains all the details of the current cart
                System.debug('CloudCraze Cart Response-'+outputCartList);

				if (outputCartList.size() > 0)
				{
                    Decimal itemCount     = 0;

                    //collect the items in the cart
                    for(Map<String,Object> cartEntry : outputCartList){
                        cartId = (String)cartEntry.get('sfid');

                        model.cartId        = (String)  cartEntry.get('sfid');
                        model.cartEncId     = (String)  cartEntry.get('encryptedId');
                        model.totalPrice    = (Decimal) cartEntry.get('totalAmount');
                        model.totalQuantity = (Decimal) cartEntry.get('totalQuantity');
                        model.currencyISOCode = (String) cartEntry.get('currencyISOCode');
                        // get cart items in the cart
                        List<Map<String, Object>> cartItems = (List<Map<String, Object>>)cartEntry.get('ECartItemsS');
                        for (Map<String, Object> item : cartItems)
						{
							x7S_CartItemModel cartItem = new x7S_CartItemModel();

							cartItem.id         = (String)item.get('sfid');
                            cartItem.productId  = (String)item.get('product');
							cartItem.quantity   = (Decimal)item.get('quantity');
							cartItem.listPrice  = (Decimal)item.get('price');
							cartItem.subTotal   = (Decimal)item.get('price') * (Decimal)item.get('quantity');

                            if ((String)item.get('cartItemType') == 'Major')
							{
								itemCount += (Decimal) item.get('quantity');
                            }
                            
							model.items.add(cartItem);
                        }   
                    }
                }
                // resolve the products in the cart
				List<Map<String, Object>> productList = (List<Map<String, Object>>)outputData.get(ccrz.ccAPIProduct.PRODUCTLIST);
				for(Map<String, Object> productEntry : productList)
				{
					x7S_CartItemModel cartItem = model.findCartItem((String)productEntry.get('sfid'));
					if (cartItem != null)
					{
						cartItem.productName        = (String)productEntry.get('sfdcName');
						cartItem.productSkuCode     = (String)productEntry.get('SKU');

						List<Map<String,Object>> mediaList = (List<Map<String,Object>>)productEntry.get('EProductMediasS');
						cartItem.imageUrl  = x7S_ProductImageUtility.getImageSrc(mediaList, 'Product Search Image');
					}
					else {
						cartItem.productName = 'not found';
						System.debug('Cart Item product not found in product list: ' + (String)productEntry.get('sfid'));
					}

					System.debug('Product: ' + productEntry.get('sfid') + ' ' + productEntry.get('sfdcName'));
					System.debug('entry: ' + productEntry);
				}

            }
		} catch (Exception ex) { 
            System.debug('fetch cart Exception: ' + ex.getMessage());
        }

        System.debug('Current Cart Id '+cartId);
        System.debug('Cart Model '+model);

        return model;
	}
	
	/**
	 * Description : Method performing Cart Actions (add/delete/update)
	 * @param : action , itemList(sfid) , newQuantity , oldQuantity
	 * @return : Updated Cart Model
	 */
	public static x7S_CartModel updateCartItems(String action,List<String> itemList, String newQuantity ,String oldQuantity){
		
		//get current cart
		x7S_CartModel model = getCurrentCart();
		Boolean success = false;

		System.debug('updateCartItems: ' + action + ',itemList ' + itemList+ ',newQuantity ' + newQuantity+ ',oldQuantity ' + oldQuantity);

		List<Map<String, Object>> newLineItems = new List<Map<String, Object>>();
		List<ccrz.ccApiCart.LineData> oldLineItems = new List<ccrz.ccApiCart.LineData>();

		Map<String, Object> inputData = new Map<String, Object>{
			ccrz.ccApi.API_VERSION    => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccApiCart.CART_ENCID => model.cartEncId
		};

		//Prepare input Data for delete//
		if (action == CART_DELETE || action == CART_UPDATE){
			if (itemList != null && itemList.size() > 0){
				for (String item : itemList){
					ccrz.ccApiCart.LineData lineItem = new ccrz.ccApiCart.LineData();
					lineItem.sfid = item;
					oldLineItems.add(lineItem);
				}
				System.debug('Old line items: ' + oldLineItems);
			}
		}

		//Prepare input Data for Add or Update//
		if (action == CART_ADD || action == CART_UPDATE){
			if (itemList != null && itemList.size() > 0){
				Integer pos = 0;
				for (String item : itemList){
					newLineItems.add(new Map<String, Object>{
						ccrz.ccApiCart.LINE_DATA_PRODUCT_SFID => Id.valueOf(item),
						ccrz.ccApiCart.LINE_DATA_QUANTITY => Integer.valueOf(newQuantity)
					});
				}
				System.debug('New line items: ' + newLineItems);
			}
		}

		//Perform Delete Action
		if (action == CART_DELETE || action == CART_UPDATE){
			inputData.put(ccrz.ccApiCart.LINE_DATA, oldLineItems);

			System.debug('Remove Old items: ' + inputData);

			Map<String, Object> removeResults = ccrz.ccApiCart.removeFrom(inputData);
			success = (Boolean) removeResults.get(ccrz.ccApi.SUCCESS);

			System.debug('Remove old items: ' + success);
			System.debug('Remove messages : ' + (List<ccrz.cc_bean_Message>) removeResults.get(ccrz.ccApi.MESSAGES));
		}

		//Perform Delete Action
		if (action == CART_ADD || action == CART_UPDATE){
			inputData.put(ccrz.ccApiCart.ISREPRICE,true);
			inputData.put(ccrz.ccApiCart.LINE_DATA, newLineItems);

			System.debug('Add/Update input: ' + inputData);

			Map<String, Object> addResults = ccrz.ccApiCart.addTo(inputData);
			success = (Boolean) addResults.get(ccrz.ccApi.SUCCESS);

			System.debug('Add/Update items   : ' + success);
			System.debug('Add/Update messages: ' + (List<ccrz.cc_bean_Message>)addResults.get(ccrz.ccApi.MESSAGES));
		}

		if (success){
			System.debug('Cart Action :'+action+ 'SUCCESS');
			model = getCurrentCart();
		}

		return model;

	}
}