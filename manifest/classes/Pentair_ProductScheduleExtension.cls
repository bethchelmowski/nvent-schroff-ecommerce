public class Pentair_ProductScheduleExtension {
    public OpportunityLineItem objProduct;
    String objOpportunityId;
    public list<OpportunityLineItemSchedule> lstOLIS {get;set;}
    public Integer rowNum{get;set;}
	public Id RecordToDelete {get; set;}
    
    public Pentair_ProductScheduleExtension(ApexPages.StandardController controller) {
        this.objProduct = (OpportunityLineItem)controller.getRecord();
    }
       
    public void getOLIS(){
        lstOLIS = new list<OpportunityLineItemSchedule>();
        
        for(OpportunityLineItemSchedule objOLIS:[Select Id, ScheduleDate, Revenue,
                                                 OpportunityLineItem.Product2.Name,
                                                 Description,
                                                 OpportunityLineItem.OpportunityId
                                                 from OpportunityLineItemSchedule 
                                                 where OpportunityLineItem.Id =:objProduct.Id
                                                 order by ScheduleDate ASC] )
        {
            
            lstOLIS.add(objOLIS);                                             
        }
        
        if (lstOLIS.size() > 0)
        	objOpportunityId = lstOLIS[0].OpportunityLineItem.OpportunityId;
    }
    
    public void UpdateFirstRevenueDate()
    {
		List<OpportunityLineItemSchedule> updateRecordDate = [SELECT ScheduleDate FROM OpportunityLineItemSchedule
                                                       		  WHERE OpportunityLineItem.Id =:objProduct.Id
                                                              ORDER BY ScheduleDate ASC];
        
        
        
        System.debug('Product ID ' + objProduct.Id);
        System.debug('Schedule Date ' + updateRecordDate[0].ScheduleDate);
        
        OpportunityLineItem updateOpp = new OpportunityLineItem(ID=objProduct.Id);
        updateOpp.ServiceDate = updateRecordDate[0].ScheduleDate;
        update updateOpp;
    }
   
        
    public void SaveForm()
    {           
        if (lstOLIS.size() > 0)
        	upsert lstOLIS;
    }
    
    public PageReference masterList() {      
        PageReference redirectPage = Page.Pentair_EditSchedule;
    	redirectPage.setRedirect(true);
    	redirectPage.getParameters().put('id',objOpportunityId);
    	return redirectPage;
	}
    
    public PageReference SaveAndRefresh() {
        SaveForm();
        
		UpdateFirstRevenueDate();
                   
   		PageReference pr = ApexPages.currentPage(); 
        pr.setRedirect(true);
   		return pr;
	}
    
    public void addNewLine(){
        
        String aid = System.currentPageReference().getParameters().get('id');
        
        System.debug('Product ID ' + aid );
        
    	OpportunityLineItemSchedule c = new OpportunityLineItemSchedule();
        c.OpportunityLineItemId = Id.valueOf(aid);
        c.Type = 'Revenue';
        c.Revenue = 0;
        
    	lstOLIS.add(c);
	}
    
    public PageReference RemoveItems(){       
        System.debug('Record ID: ' + RecordToDelete);

        List<OpportunityLineItemSchedule> getRecord = [SELECT ID FROM OpportunityLineItemSchedule 
                                                     WHERE ID =: RecordToDelete];

		if (getRecord.size() > 0)     
        {
            OpportunityLineItemSchedule deleteRecords = [SELECT ID FROM OpportunityLineItemSchedule 
                                                         WHERE ID =: RecordToDelete];
            delete deleteRecords; 
        }
     
        PageReference pr = ApexPages.currentPage(); 
		pr.setRedirect(true);
		return pr;
    } 
}