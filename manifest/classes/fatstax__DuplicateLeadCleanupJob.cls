/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DuplicateLeadCleanupJob implements Database.Batchable<SObject>, System.Schedulable {
    global DuplicateLeadCleanupJob() {

    }
    global void execute(System.SchedulableContext ctx) {

    }
    global void execute(Database.BatchableContext bCtx, List<Lead> scope) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global static String scheduleMe() {
        return null;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return null;
    }
}
