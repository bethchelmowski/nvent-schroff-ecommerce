/* ********************************************
Developer -     Apurva Prasade
Class -         nVent_RFQ_TriggerHelper
Description -   Class helper for RFQ trigger

Developer   Date        Version
Apurva P    11-12-2019  V1.0

********************************************** */

public class nVent_RFQ_TriggerHelper {
     /**
     * Populate assign to and proposal number to all manually created RFQs
     * */
    public static void populateProposalAssign(list<RFQ__c> listNewRFQ, list<Id> listAccIds)
    {
        Id RFQRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        Id QuoteRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('Quote').getRecordTypeId();
        map<Id, list<AccountTeamMember>> mapAccMems = new map<Id, list<AccountTeamMember>>();
        for(AccountTeamMember aMem : [SELECT AccountId,UserID,TeamMemberRole,User.Division,User.Name FROM AccountTeamMember 
                                WHERE AccountId in: listAccIds AND TeamMemberRole in
                                ('Hoffman Rep','Hoffman Customer Care','nVent Team Member','Schroff Rep','Schroff Customer Care','BIS Rep','IHS Rep')])
        {
            if(mapAccMems.containsKey(aMem.AccountId))
            {
                list<AccountTeamMember> listTemp = mapAccMems.get(aMem.AccountId);
                listTemp.add(aMem);
                mapAccMems.put(aMem.AccountId, listTemp);
            }
            else
            {
                list<AccountTeamMember> listTemp = new list<AccountTeamMember>();
                listTemp.add(aMem);
                mapAccMems.put(aMem.AccountId, listTemp);
            }
        }
        //Populate assigned to && propsal owner
        for(RFQ__c obj : listNewRFQ)
        {
            if(obj.RecordTypeId == QuoteRecordTypeId)
            {
                if(mapAccMems.get(obj.Account__c) != null)
                {
                    for(AccountTeamMember aMem : mapAccMems.get(obj.Account__c))
                    {
                        if(aMem.TeamMemberRole == 'IHS Rep' && obj.Sales_Team__c == 'IHS')
                        {
                            obj.Assigned_To__c = aMem.UserID;
                            obj.Proposal_Owner__c = aMem.UserID;
                        }    
                        if(aMem.TeamMemberRole == 'BIS Rep' && obj.Sales_Team__c == 'BIS')
                        {
                            obj.Assigned_To__c = aMem.UserID;
                            obj.Proposal_Owner__c = aMem.UserID;
                        }
                    }
                }
            }
            
            if(obj.RecordTypeId == RFQRecordTypeId)
            {
                //Add error if there are more than one CC or Rep for one RFQ
                Integer sRep = 0, sCC = 0, hRep = 0, hCC = 0;
                if(mapAccMems.get(obj.Account__c) != null)
                {
                    for(AccountTeamMember aMem : mapAccMems.get(obj.Account__c))
                    {
                        if(aMem.TeamMemberRole == 'Schroff Rep' && obj.Sales_Team__c == 'Schroff')
                            sRep++;
                        if(aMem.TeamMemberRole == 'Schroff Customer Care' && obj.Sales_Team__c == 'Schroff')
                            sCC++;
                        if(aMem.TeamMemberRole == 'Hoffman Customer Care' && obj.Sales_Team__c == 'Hoffman')
                            hCC++;
                        if(aMem.TeamMemberRole == 'Hoffman Rep' && obj.Sales_Team__c == 'Hoffman')
                            hRep++;
                    }
                   /* if(sRep > 1 || sCC > 1 || hCC > 1 || hRep > 1)
                        obj.AddError('There are multiple Sales Reps for this account. Please choose Proposal Owner manually.'); */
                    if(sRep == 1 || sCC == 1 || hCC == 1 || hRep == 1)
                    {
                        for(AccountTeamMember aMem : mapAccMems.get(obj.Account__c))
                        {
                            if(aMem.TeamMemberRole == 'Schroff Rep' && obj.Sales_Team__c == 'Schroff')
                                obj.Proposal_Owner__c = aMem.UserId;
                            if(aMem.TeamMemberRole == 'Schroff Customer Care' && obj.Sales_Team__c == 'Schroff')
                                obj.Assigned_To__c = aMem.UserId;
                            if(aMem.TeamMemberRole == 'Hoffman Customer Care' && obj.Sales_Team__c == 'Hoffman')
                                obj.Assigned_To__c = aMem.UserId;
                            if(aMem.TeamMemberRole == 'Hoffman Rep' && obj.Sales_Team__c == 'Hoffman')
                                obj.Proposal_Owner__c = aMem.UserId;
                        }
                    }
                    
                }
            }
        }
       /* for(RFQ__c obj : listNewRFQ)
        {
            if(obj.Assigned_To__c == null)
                obj.Assigned_To__c = UserInfo.getUserId();
            if(obj.Proposal_Owner__c == null)
                obj.Proposal_Owner__c = UserInfo.getUserId();
        }*/
    }

    public static void concateScope(RFQ__c rfqObject)
    {
        if (rfqObject.Resources_Includes__c != null)
        {
            String scopeField = '';
            if (rfqObject.Resources_Includes__c.contains('Engineering'))
                scopeField = 'E';
            if (!rfqObject.Resources_Includes__c.contains('Engineering'))
                scopeField = '';
                    
            if (rfqObject.Product_Type__c == 'Standard')
                scopeField = scopeField + 'P';
            else if (rfqObject.Product_Type__c == 'Modified')
                scopeField = scopeField + 'Pm';
            else if (rfqObject.Product_Type__c == 'Customized')
                scopeField = scopeField + 'Pc';
            else if (rfqObject.Product_Type__c == 'Engineered')
                scopeField = scopeField + 'Pe';
                    
            if (rfqObject.Resources_Includes__c.contains('Installation') || rfqObject.Resources_Includes__c.contains('Supervision') || 
                rfqObject.Resources_Includes__c.contains('Commissioning') || rfqObject.Resources_Includes__c.contains('Maintenance'))
                    scopeField = scopeField + 'C';
                    rfqObject.Scope__c = scopeField;
        }
    }

    public static String SetProposalNumer(String quoteNumber, String quoteId, String shipToCode)
    {
        String billingCode = null;
        List<RFQ__c> xAccountQuote = [SELECT Account__c From RFQ__c WHERE ID = :quoteId ];
        try
        {
            Account singleAccount = [SELECT BillingCountryCode From Account where ID =: xAccountQuote[0].Account__c];
            billingCode = singleAccount.BillingCountryCode;
        }
        catch (Exception ex)
        {
            System.debug('Exception finding account for billing code ex:' + ex.getMessage());
        }
        
        System.debug('Country Code: ' + billingCode); 
        System.debug('Quote ID: ' + quoteNumber); 
        
        if (billingCode == null)
            billingCode= 'US';

        if(shipToCode != '' && shipToCode != null)
            return shipToCode + '-' + quoteNumber;
        else 
             return billingCode + '-' + quoteNumber;
    }

    public static void populateScheduledDate(RFQ__c rfqObject)
    {
        if (rfqObject.RFQ_Received__c != null)
        {
            if (rfqObject.Sales_Team__c == 'Schroff')
            {         
                //Set the Scheduled Completion Date
                if(rfqObject.Type__c == 'Customized') //add 5 business days 
                    rfqObject.Scheduled_Completion__c = Pentair_RFQController.SetDateByBusinessDays(rfqObject.RFQ_Received__c, 5); 
                    
                else if(rfqObject.Type__c == 'Standard' || rfqObject.Type__c == 'Modified' || rfqObject.Type__c == 'DYS (Design your Standard)')//add 2 business days
                    rfqObject.Scheduled_Completion__c = Pentair_RFQController.SetDateByBusinessDays(rfqObject.RFQ_Received__c, 2);
                
                else if(rfqObject.Type__c == 'Off-platform') //add 10 business days 
                    rfqObject.Scheduled_Completion__c = Pentair_RFQController.SetDateByBusinessDays(rfqObject.RFQ_Received__c, 10); 
                    
                if(rfqObject.Product_Type__c != null) 
                    rfqObject.Product_Type__c = rfqObject.Type__c;
            }
            else if(rfqObject.Sales_Team__c == 'Hoffman') 
            {
                if(rfqObject.Type__c == 'Modified') //add 5 business days
                    rfqObject.Scheduled_Completion__c = Pentair_RFQController.SetDateByBusinessDays(rfqObject.RFQ_Received__c, 5);
                    
                else if(rfqObject.Type__c == 'Standard' || rfqObject.Type__c == 'DYS (Design your Standard)')//add 1 business days
                    rfqObject.Scheduled_Completion__c = Pentair_RFQController.SetDateByBusinessDays(rfqObject.RFQ_Received__c, 1);
                
                else if(rfqObject.Type__c == 'Off-platform') //add 10 business days
                    rfqObject.Scheduled_Completion__c = Pentair_RFQController.SetDateByBusinessDays(rfqObject.RFQ_Received__c, 10);
                if(rfqObject.Product_Type__c != null)
                    rfqObject.Product_Type__c = rfqObject.Type__c;
            }
        }
    }
}