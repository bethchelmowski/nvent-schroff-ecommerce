/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InsightProjectSearch {
    @Deprecated
    @RemoteAction
    global static List<rdcc__ReedProject__c> getAccount(String country, String state, String county, String category, String divisionCode, String lastUpdatedDateRange, String biddingDateRange, String projectValueRange, String searchText, String listView, String srchName) {
        return null;
    }
}
