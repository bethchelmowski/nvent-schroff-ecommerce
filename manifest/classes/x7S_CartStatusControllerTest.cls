/**
 * Created By ankitSoni on 05/01/2020
 * Description : Test Class for x7S_CartStatusController
 */
@isTest
public class x7S_CartStatusControllerTest {
    
    @isTest
    static void fetchCartStatusTest(){
        
       /* Account acc = x7S_TestUtility.createAccount(true);
        Contact con = x7S_TestUtility.createContact(true,1,acc.id);
         
        Profile portalProfileId = [select Id,Name from Profile where Name = 'nVent Customer Community Plus Login User Profile' limit 1];
        User user = x7S_TestUtility.createUser(true,1,portalProfileId.Id,con.Id);
       */ 
        
        Profile communityUserProfile = [SELECT Id FROM Profile WHERE Name='nVent Customer Community Plus Login User Profile' LIMIT 1];
        Map<String,Object> retData =
            ccrz.ccApiTestData.setupData(new Map<String,Map<String,Object>>{
                ccrz.ccApiTestData.ACCOUNT_DATA => new Map<String,Object>{
                    ccrz.ccApiTestData.ACCOUNT_LIST => new List<Map<String,Object>>{
                        new Map<String,Object>{
                            'name' => 'testAccount1',
                            'ccrz__dataId__c' => 'testAccount1'
                        }
                    }
                },
                ccrz.ccApiTestData.CONTACT_DATA => new Map<String,Object>{
                    ccrz.ccApiTestData.CONTACT_LIST => new List<Map<String,Object>>{
                        new Map<String,Object>{
                            'ccrz__dataId__c' => 'testContact1',
                            'account' => new Account(ccrz__dataId__c = 'testAccount1'),
                            'email' => 'testcontact1.ccrz@cloudcraze.com',
                            'lastName' => 'User1',
                            'firstName' => 'Test1'
                        }
                    }
                },
                ccrz.ccApiTestData.USER_DATA => new Map<String,Object>{
                    ccrz.ccApiTestData.USER_LIST => new List<Map<String,Object>>{
                        new Map<String,Object>{
                            'ccrz__dataId__c' => 'testUser1',
                            'alias' => 'defusr1',
                            'email' => 'test.ccrz1@cloudcraze.com',
                            'lastName' => 'User1',
                            'firstName' => 'Test1',
                            'languageLocaleKey' => 'fr',
                            'localeSIDKey' => 'fr_FR',
                            'emailEncodingKey' => 'UTF-8',
                            'profileId' => communityUserProfile.Id,
                            'username' => System.currentTimeMillis() + 'test1@cloudcraze.com',
                            'ccrz__CC_CurrencyCode__c' => 'EUR',
                            'contact' => new Contact(ccrz__dataId__c = 'testContact1'),
                            'timezoneSIDKey' => 'GMT'
                        }
                    }
                }
            });
        Map<String,Object> theData = (Map<String,Object>)retData.get(ccrz.ccApiTestData.USER_DATA);
        List<sObject> theList = (List<sObject>)theData.get(ccrz.ccApiTestData.USER_LIST);
 
        User theUser = (User)theList.get(0);
 
        /*    Map<String, Map<String, Object>> storefrontData = new Map <String, Map<String, Object>> {
              ccrz.ccApiTestData.STOREFRONT_SETTINGS => new Map<String, Object> {
                  'DefaultStore' => new Map<String, Object> {
                      'Allow_Anonymous_Browsing__c' => true,
                      'Currencies__c' => 'USD',
                      'Languages__c' => 'en_US',
                      'Customer_Portal_Account_Name__c' => 'testAccount1',
                      'CustomerPortalAcctGroupName__c' => 'PortalAccount',
                      'DefaultCurrencyCode__c' => 'USD',
                      'DefaultLocale__c' => 'en_US',
                      'InventoryCheckFlag__c' => false,
                      'Quoting_Enabled__c' => true,
                      'Skip_Tax_Calculation__c' => false,
                      'Filter_Orders_Based_on_Owner__c' => true
                  }
              }
            };
            Map<String,Object> storeRetData = ccrz.ccApiTestData.setupData(storefrontData);
        */    
        System.runAs(theUser) {
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__User__c = UserInfo.getUserId();
            cart.ccrz__CartType__c = 'Cart';
            cart.ccrz__ActiveCart__c = TRUE;
            insert cart;
            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, true);

        	List<ccrz__E_Cart__c> cartList =  [SELECT Id , Name , ccrz__ActiveCart__c , ccrz__User__c , ccrz__CartStatus__c , OwnerId FROM ccrz__E_Cart__c];
        	System.debug('Cart query+'+cartList);
            String cartStatus = x7S_CartStatusController.fetchCartStatus();  
                
            System.assertNotEquals(null, cartStatus);
       	}
    }
}