global class MergeDuplicateAccountBatch implements Database.batchable<MergeDuplicateAccountIterable.accountTableList>, Database.Stateful { 
   
   global List<MergeDuplicateAccounts.reParentAccountProcessResultWrap> reParentAcntProcessResult;
   
   global MergeDuplicateAccountBatch() 
   {
       reParentAcntProcessResult = new List<MergeDuplicateAccounts.reParentAccountProcessResultWrap>();
   }
   
   global Iterable<MergeDuplicateAccountIterable.accountTableList> start(Database.batchableContext info)
   { 
       return new AccountCustomIterable(); 
   }
        
   global void execute(Database.batchableContext info, List<MergeDuplicateAccountIterable.accountTableList> scope)
   { 
       system.debug('------------- Data in scope: '+scope);
       //Map<String, String> accountTable = new Map<String, String>();
       Map<id, id> allAcntKeyIds = new Map<id, id>();
       MergeDuplicateAccounts mpe = new MergeDuplicateAccounts();
       
       List<MergeDuplicateAccounts.reParentAccountProcessResultWrap> processResult = new List<MergeDuplicateAccounts.reParentAccountProcessResultWrap>();
       
       for(MergeDuplicateAccountIterable.accountTableList pro : scope)
           allAcntKeyIds.put(pro.dupProjectID, pro.parentProjectID); 
       
       if(!allAcntKeyIds.KeySet().isEmpty()) {
           
           //Map<id, id> allAcntKeyIds = new Map<id, id>(); 
           //code to get accounts which is already merged  
           /*for(Project__c project : [select id,Project_Unique_Id__c,ownerid,Warning_Message_For_Merge__c,merge_action__c,Parent_Project__c,is_merged__c from project__c where id in: accountTable.keyset() and is_merged__c=false]) {
                allProjKeyIds.put(project.id, accountTable.get(String.ValueOf(project.id)));
           }*/
           /*for(Account Acnt : accountTable) {
                //allProjKeyIds.put(, accountTable.get(String.ValueOf(project.id)));
           }*/
            system.debug('---------- value in allAcntKeyIds: '+allAcntKeyIds);   
           if(!allAcntKeyIds.keyset().isEmpty())
               processResult = mpe.reParentAccountChild(allAcntKeyIds);
               
           system.debug('** final processResult - ' + processResult.size());
           system.debug('** final processResult - ' + processResult);    
           
           if(!processResult.isEmpty())
               reParentAcntProcessResult.addall(processResult);
           
           system.debug('** final reParentAcntProcessResult - ' + reParentAcntProcessResult.size());
           system.debug('** final reParentAcntProcessResult - ' + reParentAcntProcessResult);
     
        }
   } 
       
   global void finish(Database.batchableContext info){
   
       system.debug('**reParentAcntProcessResult - ' + reParentAcntProcessResult.size());
       system.debug('**reParentAcntProcessResult - ' + reParentAcntProcessResult);
       
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {'arun.kumar@tredence.com'};
       String[] ccAddresses = new String[] {'nvent_sfdc@tredence.com'};
       mail.settoAddresses(toAddresses);
       mail.setccAddresses(ccAddresses );
       mail.setSenderDisplayName('nVent Account deDup');
       mail.setSubject('Account deDup Completed - '+system.today().format());
       if(reParentAcntProcessResult.isEmpty())
           mail.setPlainTextBody('Hello All, \n\nScheduled Project de duplication is completed. There are no errors during the merge process. \n\nRegards\nnVent SFDC');
       if(!reParentAcntProcessResult.isEmpty())
           mail.setPlainTextBody('Hello All, \n\nScheduled Project de duplication is completed. There are'+ reParentAcntProcessResult.size() +'errors during the merge process, please find the attachment with the errors details. \n\nRegards\nnVent SFDC');    
       Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName('Error Records.txt');
            efa.setBody(blob.valueOf(JSON.serialize(reParentAcntProcessResult)));
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});     
   
   } 
}