/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_ccrz_E_CartTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_ccrz_E_CartTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new ccrz__E_Cart__c());
    }
}