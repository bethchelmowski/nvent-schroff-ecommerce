/* ********************************************
Developer -     Apurva Prasade
Class -         nVent_MultipleAccTeamMemClass
Description -   class for nVent_MultipleAccMemClass

Developer   Date           Version
Apurva P    03/01/2020     V1.0

********************************************** */

public class nVent_MultipleAccTeamMemClass {
    
    @AuraEnabled
    public static String getAccounMeminfo(Id recordId)
    {
        System.debug('getAccounMeminfo'+recordId);
        map<Id, list<AccountTeamMember>> mapAccMems = new map<Id, list<AccountTeamMember>>();
        Id RFQRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        System.debug('RFQRecordTypeId '+RFQRecordTypeId);
        RFQ__c objRFQ = [Select Id,RecordTypeId,Sales_Team__c,Assigned_To__c,Proposal_Owner__c,Account__c from RFQ__c where Id =: recordId];
        System.debug('getAccounMeminfo '+objRFQ);
        for(AccountTeamMember aMem : [SELECT AccountId,UserID,TeamMemberRole,User.Division,User.Name FROM AccountTeamMember 
                            	WHERE AccountId =: objRFQ.Account__c AND TeamMemberRole in
								('Hoffman Rep','Hoffman Customer Care','nVent Team Member','Schroff Rep','Schroff Customer Care','BIS Rep','IHS Rep')])
        {
            if(mapAccMems.containsKey(aMem.AccountId))
            {
                list<AccountTeamMember> listTemp = mapAccMems.get(aMem.AccountId);
                listTemp.add(aMem);
                mapAccMems.put(aMem.AccountId, listTemp);
            }
            else
            {
                list<AccountTeamMember> listTemp = new list<AccountTeamMember>();
                listTemp.add(aMem);
                mapAccMems.put(aMem.AccountId, listTemp);
            }
        }
        System.debug('mapAccMems '+mapAccMems);
        if(objRFQ.RecordTypeId == RFQRecordTypeId)
        {
            System.debug('If Iff 1');
            //Add error if there are more than one CC or Rep for one RFQ
            Integer sRep = 0, sCC = 0, hRep = 0, hCC = 0;
            if(mapAccMems.get(objRFQ.Account__c) != null)
            {
                System.debug('If Iff 2');
                for(AccountTeamMember aMem : mapAccMems.get(objRFQ.Account__c))
                {
                    if(aMem.TeamMemberRole == 'Schroff Rep' && objRFQ.Sales_Team__c == 'Schroff')
                        sRep++;
                    if(aMem.TeamMemberRole == 'Schroff Customer Care' && objRFQ.Sales_Team__c == 'Schroff')
                        sCC++;
                    if(aMem.TeamMemberRole == 'Hoffman Customer Care' && objRFQ.Sales_Team__c == 'Hoffman')
                        hCC++;
                    if(aMem.TeamMemberRole == 'Hoffman Rep' && objRFQ.Sales_Team__c == 'Hoffman')
                        hRep++;
                }
                System.debug('sRep '+sRep);
                if(sRep > 1 || sCC > 1 || hCC > 1 || hRep > 1)
                    return 'There are multiple Sales Reps and/or Customer Care users for this account. Please choose Proposal Owner manually.';
                
                    //obj.AddError('There are multiple Sales Reps for this account. Please choose Proposal Owner manually.');
            }
        }
        return '';
    }
}