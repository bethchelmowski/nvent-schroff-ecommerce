public class Pentair_iCatalogController {   
    public static void CreateTaskRecord(Id ownerId, Id leadId, Id contactId, Boolean createFollowUp)
    {
        Set<ID> leadIdRecords = new Set<ID>();
        leadIdRecords.add(leadId);
        
        Lead[] singleLead = [
            select ID, Name, Email, Status, Application__c, Catalog_Country__c, 
            Catalog_Description__c, Catalog_Market__c, Catalog_Name__c, Catalog_Stack_Notes__c, 
            Catalog_User__c, Catalog_Platform__c, Create_Follow_Up_Tasks__c from Lead 
            WHERE ID IN : leadIdRecords
        ]; 
        
        Task singleTask = new Task();
        singleTask.OwnerId = ownerId;
        if (!createFollowUp)
        {
            singleTask.Subject = 'New literature request';
            singleTask.Status = 'Closed';
        }
        else
        { 
            singleTask.Subject = 'Literature Request Follow Up';
            singleTask.Status = 'Open';
            singleTask.ActivityDate = date.today().addDays(2);
        }
        
        singleTask.Description = 'Description:' + '\n ' + 'Catalog Name: ' + singleLead[0].Catalog_Name__c + '\n' +
        'Catalog Stack Note: ' + singleLead[0].Catalog_Stack_Notes__c + '\n' +
        'Platform: ' + singleLead[0].Catalog_Platform__c + '\n' +
        'Catalog User: ' + singleLead[0].Catalog_User__c + '\n' +
        'Application: ' + singleLead[0].Application__c;
        
        if (contactId == null)
            singleTask.WhoId = leadId;
        else 
            singleTask.WhoId = contactId;

        insert singleTask;
    }
}