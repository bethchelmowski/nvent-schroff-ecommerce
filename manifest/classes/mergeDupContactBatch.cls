//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: mergeDupContactBatch	                                                                                    //
// 	Purpose:To merge Contacts which are tagged as duplicates																//					// 								
//  Created By: Tredence																									//	// 		
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//

global class mergeDupContactBatch implements Database.Batchable<sObject> ,Database.stateful
{
    global Integer totalRecords = 0;
    global Integer successRecords = 0;
    global Integer failedRecords = 0;
    global string finalstr= 'Record Id, Error Status, Error Message, Error Fields \n';
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        string date_today=string.valueOf(system.today());
        String query= 'Select Id,AccountId,FirstName,LastName,Master_Contact__c,IsDuplicate__c,DeDuped_on__c,ToBeMergedOn__c from Contact WHERE IsDuplicate__c = true AND Master_Contact__c!= null AND ToBeMergedOn__c<='+date_today;
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext bc, List<Contact> records)
    {
        totalRecords = totalRecords + records.size();
        
        map<Id,Contact> contactsmap=new map<Id,Contact>();
        
        //Creating a set of Ids of Contacts and their master Contacts
        set<Id> ConIds=new set<Id>();
        for(Contact c: records)
        {
            ConIds.add(c.Id);
            ConIds.add(c.Master_Contact__c);
        }
        
        List<Contact> contacts=new list<Contact>();
        
        contacts=[Select Id,AccountId,FirstName,LastName,Master_Contact__c,IsDuplicate__c,DeDuped_on__c,ToBeMergedOn__c from Contact where ID In:ConIds];
        
        //Creating a map of Contacts and their master Contacts
        for(Contact c:contacts)
        {
            contactsmap.put(c.Id,c);
        }
        List<Duplicate_Contact__c> duplicates=new List<Duplicate_Contact__c>();
        List<Duplicate_Contact__c> Listtoupdate=new List<Duplicate_Contact__c>();
        
        duplicates=[Select Name,Master_Contact_ID__c,Tagged__c,isMerged__c from Duplicate_Contact__c where Name In: ConIds];
        
        Map<string,Duplicate_Contact__c> dupmap = new Map<string,Duplicate_Contact__c>();
        
        //Creating a map of custom setting records
        for(Duplicate_Contact__c dup: duplicates)
        {
            dupmap.put(dup.Name,dup);
        }
        try{
        //merging each contact with their master contact
        for(Contact con : records)
        {   
            Contact masterCont =contactsmap.get(con.Master_Contact__c);
            Contact mergeCont = con;
            Database.MergeResult result = Database.merge(masterCont, mergeCont, false);
            system.debug(result);
            
            if(result.isSuccess())
            {
                successRecords++;
                Duplicate_Contact__c dup = dupmap.get(con.Id);
                dup.isMerged__c = true; 
                Listtoupdate.add(dup);
            }
            else
            {
                
                failedRecords++;
                for(Database.Error err : result.getErrors())
                {
                    
                    string recordString = '"'+con.Id+'","'+err.getStatusCode()+'","'+err.getMessage().escapecsv()+'","'+err.getFields() +'"\n';
                    finalstr = finalstr +recordString;
                    
                }
            }
            
        } 
        }
        catch(Exception ex)
        {
            string recordString = '"'+ +'","'+ex.getTypeName()+'","'+ex.getMessage().escapecsv()+'","'+ex.getStackTraceString() +'"\n';
            finalstr = finalstr +recordString;
            failedRecords=failedRecords+records.size();
        }
        
        try
        {
            //updating merged flag in custom setting as true after record is merged
            if(Listtoupdate.size()>0)
                update Listtoupdate;
        }
        catch (DmlException ex) 
        {
            string recordString = '"'+ +'","'+ex.getTypeName()+'","'+ex.getMessage().escapecsv()+'","'+ex.getStackTraceString() +'"\n';
            finalstr = finalstr +recordString;
            System.debug('An unexpected error has occurred: ' + ex.getMessage());
        }
    }
    
    
    global void finish(Database.BatchableContext bc)
    {
        //Sending notification email about the records processed
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= 'DebugLogs.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        
        
        Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
        string[] toAddress=new string[]{'pranshu.agarwal@tredence.com','arun.kumar@tredence.com','nvent_sfdc@tredence.com'};
        mail.setToAddresses(toAddress);
        mail.setSubject('Status of records processed');
        mail.setPlainTextBody('Hello All, \n\nScheduled Contact merging is completed. \n Total records processed ' + totalRecords + ' with '+ failedRecords +' records failed and ' + successRecords +' records passed .\n\nRegards\nnVent SFDC');
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        
    }    
}