/**
 * Created By ankitSoni on 05/26/2020
 */
public with sharing class x7S_DataTableController {
    
    //Retreive all Products with Details
    /* public static void getProducts(){

        System.debug('getProducts');
        Map<String, Object> inputData = new Map<String, Object>{
            ccrz.ccAPI.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
            ccrz.ccApiProduct.PARAM_INCLUDE_PRICING	=> true,
            ccrz.ccApi.SIZING         => new Map<String, Object>
				{ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL}}
        };
         
        try {
            Map<String, Object> outputData = ccrz.ccAPIProduct.fetch(inputData);
            if (outputData.get(ccrz.ccAPIProduct.PRODUCTLIST) != null) {
                List<Map<String, Object>> outputProductList = (List<Map<String, Object>>) outputData.get(ccrz.ccAPIProduct.PRODUCTLIST);
                
                System.debug('Stringify -'+ JSON.serialize(outputProductList) );
                String productName = (String) outputProductList[0].get('sfdcName');
            }
        } catch (Exception ex) {
            System.debug('Fetch Products Exception : '+ex.getMessage());
        }
    } */

    @AuraEnabled
    public static x7S_WishListModel getWishlistItems(){
        String wishlistCartId = '';

        String userId = UserInfo.getUserId(); // sfid of the logged in user
        System.debug('getWishlistItems()');

        x7S_WishListModel model = new x7S_WishListModel();
        
        List<ccrz__E_Cart__c> wishlist = [SELECT Id, ccrz__ActiveCart__c, ccrz__CartType__c,ccrz__User__c
                                        FROM ccrz__E_Cart__c
                                        WHERE ccrz__User__c=: Userinfo.getUserId()
                                        AND ccrz__ActiveCart__c=true
                                        AND ccrz__CartType__c='WishList'
                                        LIMIT 1];


        for(ccrz__E_Cart__c cart: wishlist){
                wishlistCartId = cart.Id;
        }
        System.debug('Current wishlist Id '+wishlistCartId);

        Map<String, Object> inputData = new Map<String, Object>{
            ccrz.ccAPI.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
            ccrz.ccAPIWishList.ID	=> wishlistCartId,
            ccrz.ccAPIWishList.INCLUDE_ITEMS	=> true,
            ccrz.ccApi.SIZING         => new Map<String, Object>
				{ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL}}
        };
          
        try {
            Map<String, Object> outputData = ccrz.ccAPIWishList.fetch(inputData);

            if (outputData.get(ccrz.ccAPIWishList.WISHLISTS) != null) {
                System.debug('Fetch Wishlist Output data -'+outputData);
                Map<String, Object> wishListMap = (Map<String, Object>) outputData.get(ccrz.ccAPIWishList.WISHLISTS);

                //get the wishlist into a map
                Map<String, Object> wishList1Map = (Map<String, Object>) wishListMap.get(wishlistCartId);

                System.debug('wishListMap'+wishList1Map);

                model.cartId        = (String)  wishList1Map.get('sfid');
                model.cartEncId     = (String)  wishList1Map.get('encryptedId');
                model.totalQuantity = (Decimal) wishList1Map.get('totalQuantity');

                List<Map<String, Object>> cartItems = (List<Map<String, Object>>)wishList1Map.get('ECartItemsS');
                for (Map<String, Object> item : cartItems){
                    x7S_WishListItemModel wishlistItem = new x7S_WishListItemModel();
                    wishlistItem.id         = (String)item.get('sfid');
                    wishlistItem.productId  = (String)item.get('product');
                    wishlistItem.quantity   = (Decimal)item.get('quantity');
                                
                    model.items.add(wishlistItem);
                } 
                
                // resolve the products in the cart(wishlist)
				List<Map<String, Object>> productList = (List<Map<String, Object>>)outputData.get(ccrz.ccAPIProduct.PRODUCTLIST);
				for(Map<String, Object> productEntry : productList)
				{
					x7S_WishListItemModel wishlistItem = model.findWishlistItem((String)productEntry.get('sfid'));
					if (wishlistItem != null)
					{
						wishlistItem.productName        = (String)productEntry.get('sfdcName');
						wishlistItem.productSkuCode     = (String)productEntry.get('SKU');
                        wishlistItem.productDescription = (String)productEntry.get('longDesc');

						List<Map<String,Object>> mediaList = (List<Map<String,Object>>)productEntry.get('EProductMediasS');
						wishlistItem.imageUrl  = x7S_ProductImageUtility.getImageSrc(mediaList, 'Product Search Image');
					}
					else {
						wishlistItem.productName = 'not found';
						System.debug('Wishlist Item product not found in product list: ' + (String)productEntry.get('sfid'));
					}

					System.debug('Wishlist Product: ' + productEntry.get('sfid') + ' ' + productEntry.get('SKU')+ ' '+ productEntry.get('sfdcName') + ' ' + productEntry.get('longDesc'));
                    System.debug('Wishlist Product entry: ' + productEntry);
                    
                }
                
            }
        } catch (Exception e) {
            System.debug('fetch Wishlist cart Exception: ' + e.getMessage());
        }

        System.debug('Wishlist Model '+model);

        return model;
    }

    /**
     * Description : Add wishlist item to the existing cart
     * @param : wishlistId(i.e cart(type wishlist) id) , wishlistItemId
     */
    @AuraEnabled
    public static void addWishlistItemToCart(String wishlistId , String wishlistItemId){

        if( (wishlistId != null && wishlistItemId != null) || (String.isNotBlank(wishlistId) && String.isNotBlank(wishlistItemId)) ){
            
            List<ccrz__E_CartItem__c> wishListItem = [SELECT Id , Name , ccrz__Product__c ,ccrz__Price__c, ccrz__OriginalItemPrice__c , ccrz__Quantity__c
                                                    FROM ccrz__E_CartItem__c 
                                                    WHERE Id =:wishlistItemId AND ccrz__Cart__c =:wishlistId];

            try{
                Map<String,Object> inputData = new Map<String, Object>{
                    ccrz.ccAPI.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
                    ccrz.ccAPIWishList.ID		=> wishlistId,
                    ccrz.ccApi.SIZING         => new Map<String, Object>
                        {ccrz.ccAPIWishList.ENTITYNAME => new Map<String, Object>{ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL}}
                };

                Map<String, Object> outputData = ccrz.ccApiWishList.fetchWishlistItems(inputData);
                System.debug('fetchWishlistItems outputData'+outputData);

                Map<String,Object>	outputCartItemsData = ( Map<String,Object>) outputData.get(ccrz.ccAPIWishList.WISHLIST_ITEMS);
                System.debug('outputCartItemsData'+outputCartItemsData);

                Map<String,Object> cartItemsList = (Map<String,Object>) outputCartItemsData.get(wishlistId);
                System.debug('cartItemsList'+cartItemsList);

            }catch(Exception e){
                System.debug('Exception : '+e.getMessage());
            }
        }
    }
}