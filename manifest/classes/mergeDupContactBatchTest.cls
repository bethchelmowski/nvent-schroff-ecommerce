//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: mergeDupContactBatchTest	                                                                                //
// 	Purpose:To test our mergeDupContactBatch																			    // 								
//  Created By: Tredence																									//	 		
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//

@istest
public class mergeDupContactBatchTest 
{
	@istest static void mergeDupAccountMethodPos_Scenario()
    {
        //creating test data
        Contact conmas=new contact(LastName='Test2');
        insert conmas;
        
        Contact con=new contact(LastName='Dup[Test1]',DeDuped_on__c=Date.today().addDays(-31),Master_Contact__c=conmas.Id,IsDuplicate__c=True);
        insert con;
        
        Duplicate_Contact__c dup=new Duplicate_Contact__c();
        dup.Name=con.Id;
        dup.Master_Contact_ID__c=conmas.Id;
        dup.Tagged__c=True;
        insert dup;
        
        
        Test.startTest();
        mergeDupContactBatch T1=new mergeDupContactBatch();
        database.executeBatch(T1);
        test.stopTest();
        
        
        List<Duplicate_Contact__c> dups=[Select Name,isMerged__c from Duplicate_Contact__c where Name=:con.Id];
        
        System.assertEquals(True,dups[0].isMerged__c);
        
        
   }
}