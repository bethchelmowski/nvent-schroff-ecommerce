//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: mergeDupAccountBatch	                                                                                    //
// 	Purpose:To merge Accounts which are tagged as duplicates																//					// 								
//  Created By: Tredence																									//	// 		
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//

global class mergeDupAccountBatch implements Database.Batchable<sObject> ,Database.stateful
{
    global Integer totalRecords = 0;
    global Integer successRecords = 0;
    global Integer failedRecords = 0;
    global string finalstr= 'Record Id, Error Status, Error Message, Error Fields \n';
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        string date_today=string.valueOf(system.today());
        String query= 'SELECT Id,Name,Master_Account__c,IsDuplicate__c,ToBeMergedOn__c FROM Account WHERE IsDuplicate__c = true AND Master_Account__c!= null AND ToBeMergedOn__c<='+date_today;
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext bc, List<Account> records)
    {
        totalRecords = totalRecords + records.size();
        
        map<Id,Account> accountsmap=new map<Id,Account>();
        set<Id> AccIds=new set<Id>();
        
        //Creating a set of Ids of accounts and their master accounts
        for(Account a: records)
        {
            AccIds.add(a.Id);
            AccIds.add(a.Master_Account__c);
        }
        List<Account> accounts=new list<Account>();
        
        accounts=[SELECT Id,Name,Master_Account__c,IsDuplicate__c,ToBeMergedOn__c FROM Account where ID In:AccIds];
        
        //Creating a map of Accounts and their master accounts
        for(Account a:accounts)
        {
            accountsmap.put(a.Id,a);
        }
        
        List<Duplicate_Accounts__c> duplicates=new List<Duplicate_Accounts__c>();
        List<Duplicate_Accounts__c> Listtoupdate=new List<Duplicate_Accounts__c>();
        
        duplicates=[Select Name,Master_Account_ID__c,Tagged__c,isMerged__c from Duplicate_Accounts__c where Name In: AccIds];
        
        Map<string,Duplicate_Accounts__c> dupmap = new Map<string,Duplicate_Accounts__c>();
        
        //Creating a map of custom setting records
        for(Duplicate_Accounts__c dup: duplicates)
        {
            dupmap.put(dup.Name,dup);
        }
        try{
        //merging each account with their master account
        for(Account acc : records)
        {   
            Account masterAcct =accountsmap.get(acc.Master_Account__c);
            Account mergeAcct = acc;
            Database.MergeResult result = Database.merge(masterAcct, mergeAcct, false);
            
            if(result.isSuccess())
            {
                successRecords++;
                Duplicate_Accounts__c dup = dupmap.get(acc.Id);
                dup.isMerged__c = true; 
                Listtoupdate.add(dup);
            }
            else
            {
                failedRecords++;
                for(Database.Error err : result.getErrors())
                {
                    
                    string recordString = '"'+acc.Id+'","'+err.getStatusCode()+'","'+err.getMessage().escapecsv()+'","'+err.getFields() +'"\n';
                    finalstr = finalstr +recordString;
                    
                }
            }
        }
            
        } 
        catch(Exception ex)
        {
            string recordString = '"'+ +'","'+ex.getTypeName()+'","'+ex.getMessage().escapecsv()+'","'+ex.getStackTraceString() +'"\n';
            finalstr = finalstr +recordString;
            failedRecords=failedRecords+records.size();
        }
        try
        {
            //updating merged flag in custom setting as true after record is merged
            if(Listtoupdate.size()>0)
                update Listtoupdate;
        }
        catch (DmlException ex) 
        {
            string recordString = '"'+ +'","'+ex.getTypeName()+'","'+ex.getMessage().escapecsv()+'","'+ex.getStackTraceString() +'"\n';
            finalstr = finalstr +recordString;
            System.debug('An unexpected error has occurred: ' + ex.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext bc)
    {
        //Sending notification email about the records processed
       
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= 'DebugLogs.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        
        
        Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
        string[] toAddress=new string[]{'pranshu.agarwal@tredence.com','arun.kumar@tredence.com','nvent_sfdc@tredence.com'};
        mail.setToAddresses(toAddress);
        mail.setSubject('Status of records processed');
        mail.setPlainTextBody('Hello All, \n\nScheduled Account merging is completed. \n Total records processed ' + totalRecords + ' with '+ failedRecords +' records failed and ' + successRecords +' records passed. \n\nRegards\nnVent SFDC');
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        
    }    
}