//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: mergeDupAccountBatchTest	                                                                                //
// 	Purpose:To test our mergeDupAccountBatch																			    // 								
//  Created By: Tredence																									//	 		
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//

@istest
public class mergeDupAccountBatchTest 
{
	@istest static void mergeDupAccountMethodPos_Scenario()
    {
        //creating test data
        Account mas=new account(Name='Test2');
        insert mas;
        Contact conmas=new contact(LastName='Test2',AccountId=mas.id);
        insert conmas;
        Opportunity oppmas=new opportunity(Name='Test2',CloseDate=Date.today(),StageName='Prospecting',AccountId=mas.Id);
        insert oppmas;
        
        
        Account acc=new account(Name='Dup[Test1]',IsDuplicate__c=True,DeDuped_on__c=Date.today().addDays(-31),Master_Account__c=mas.Id);
        insert acc;
        Contact con=new contact(LastName='Test1',AccountId=acc.id);
        insert con;
        Opportunity opp=new opportunity(Name='Test1',CloseDate=Date.today(),StageName='Prospecting',AccountId=acc.Id);
        insert opp;
        
        
        Duplicate_Accounts__c dup=new Duplicate_Accounts__c();
        dup.Name=acc.Id;
        dup.Master_Account_ID__c=mas.Id;
        dup.Tagged__c=True;
        insert dup;
        
        Test.startTest();
        mergeDupAccountBatch T1=new mergeDupAccountBatch();
        database.executeBatch(T1);
        test.stopTest();
        
        //retrieving data after running batch class
        List<Contact> contacts=[Select Id,AccountId from Contact where AccountId=:mas.Id];
        List<Opportunity> opportunities=[Select Id,AccountId from opportunity where AccountId=:mas.id];
        List<Duplicate_Accounts__c> dups=[Select Name,isMerged__c from Duplicate_Accounts__c where Name=:acc.Id];
        
        System.assertEquals(2, contacts.size());
        System.assertEquals(2, opportunities.size());
        System.assertEquals(True, dups[0].isMerged__c);
        
        
   }
}