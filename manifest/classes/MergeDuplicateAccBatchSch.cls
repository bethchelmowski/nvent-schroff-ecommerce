global class MergeDuplicateAccBatchSch implements Schedulable
{
	global void execute(SchedulableContext SC)
    {
        MergeDuplicateAccountBatch batchinst = new MergeDuplicateAccountBatch();
        if(test.isRunningTest())
        {
           database.executeBatch(batchinst,MergeDuplicateAccountBatchtest.scope); 
        }
        else
        {
           database.executeBatch(batchinst, integer.valueof(label.AccountDeDupeBatchSize));   
        }
        
    }
}