//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: Tag_Dup_Contact_Test	                                                                                    //
// 	Purpose: Test Class for Batch Class Tag_Dup_Contact_Test																//						                        //
//  Created By: Tredence																									//			
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//


@istest
public class Tag_Dup_Contact_Test 
{
    @istest Static void Dup_Contact_TestMethod1()
    {
        //Creating test data
        Account acc1=new Account(Name='Test1');
        insert acc1;
        Contact con1=new Contact(LastName='Last1',AccountId=acc1.Id);
        insert con1;
        
        Account acc2=new Account(Name='Test2');
        insert acc2;
        Contact con2=new Contact(LastName='Last2',AccountId=acc2.Id);
        insert con2;
        
        Duplicate_Contact__c d1=new Duplicate_Contact__c();
        d1.Name=con1.Id;
        d1.Master_Contact_ID__c=con2.Id;
        d1.Tagged__c=False;
        insert d1;
        
        Test.startTest();
        Tag_Dup_Contact_Batch T1=new Tag_Dup_Contact_Batch();
        database.executeBatch(T1);
        Test.stopTest();
        
        //Retrieving the records after runnning batch class and verifying our test
        contact contact1 = [SELECT NAME,Master_Contact__c,DeDuped_on__c,IsDuplicate__c from contact where id=: con1.Id];
        System.assertEquals('Dup[Last1]', contact1.Name);
        System.assertEquals(con2.Id,contact1.Master_Contact__c);
        System.assertEquals(Date.today(),contact1.DeDuped_on__c);
    }

     @istest Static void Dup_Contact_TestMethod2()
    {
        //Creating test data
        Account acc3=new Account(Name='Test3');
        insert acc3;
        Contact con3=new Contact(LastName='Last3',FirstName='First3',AccountId=acc3.Id);
        insert con3;
        
        Account acc4=new Account(Name='Test4');
        insert acc4;
        Contact con4=new Contact(LastName='Last4',AccountId=acc4.Id);
        insert con4;
        
        Duplicate_Contact__c d2=new Duplicate_Contact__c();
        d2.Name=con3.Id;
        d2.Master_Contact_ID__c=con4.Id;
        d2.Tagged__c=False;
        insert d2;
        
        Test.startTest();
        Tag_Dup_Contact_Batch T1=new Tag_Dup_Contact_Batch();
        database.executeBatch(T1);
        Test.stopTest();
        
        //Retrieving the records after runnning batch class and verifying our test
        contact contact2 = [SELECT NAME,Master_Contact__c,DeDuped_on__c,IsDuplicate__c from contact where id=: con3.Id];
        System.assertEquals('Dup[First3 Last3]', contact2.Name);
        System.assertEquals(con4.Id,contact2.Master_Contact__c);
        System.assertEquals(Date.today(),contact2.DeDuped_on__c);
    }

}