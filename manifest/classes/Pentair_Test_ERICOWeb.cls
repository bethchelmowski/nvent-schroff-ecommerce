@isTest
private class Pentair_Test_ERICOWeb {    
    static testMethod void TestMethodMain()
    {
        String id = '129746';
        
        Pentair_Erico_Main controllerMain = new Pentair_Erico_Main();        
             
        PageReference pageRefMain = Page.Pentair_EricoMain;
        pageRefMain.getParameters().put('id', String.valueOf(id));
        Test.setCurrentPage(pageRefMain);
        
        controllerMain.backMethod(); 
    }    
    
    static testMethod void TestMethodClaims()
    { 
        Test.startTest(); 
        
        Pentair_ERICOWebService webServiceController = new Pentair_ERICOWebService();
        webServiceController.callRESTService('url');
        
        String cid = '129746.02;';
        String clid = '129746.02;';
        String sourcesystem = '02';
        
        Pentair_ERICO_Claims controllerClaims = new Pentair_ERICO_Claims();        
             
        PageReference pageRefClaims = Page.Pentair_Claims;
        pageRefClaims.getParameters().put('cid', String.valueOf(cid));
        pageRefClaims.getParameters().put('ssid', String.valueOf(sourcesystem));
        Test.setCurrentPage(pageRefClaims);
        
        controllerClaims.getClaimHeaderRecord();  
        
        PageReference pageRefClaimDetail = Page.Pentair_ClaimDetail;
        pageRefClaimDetail.getParameters().put('clid', String.valueOf(clid));
        Test.setCurrentPage(pageRefClaimDetail);

        controllerClaims.getClaimDetails();        
        controllerClaims.cancel();        
        controllerClaims.getSfIdQueryParam();
        
        Test.stopTest();  
    }
    
    static testMethod void TestMethodOrder()
    { 
        Test.startTest(); 
        
        String cid = '129746.02;';
        String clid = '129746.02;';
        String sourcesystem = '02';
        String oid = '129746;';
        String id = '1000';
        
        Pentair_ERICO_OrderStatus controllerOrder = new Pentair_ERICO_OrderStatus();        
             
        PageReference pageRefOrder = Page.Pentair_OrderStatus;
        pageRefOrder.getParameters().put('cid', String.valueOf(cid));
        pageRefOrder.getParameters().put('ssid', String.valueOf(sourcesystem));
        pageRefOrder.getParameters().put('id', String.valueOf(id));
        pageRefOrder.getParameters().put('oid', String.valueOf(oid));
        Test.setCurrentPage(pageRefOrder);
        
        controllerOrder.getOrderStatusHeader();
        
        PageReference pageRefOrderDetail = Page.Pentair_Order;
        pageRefOrderDetail.getParameters().put('oid', String.valueOf(oid));
        pageRefOrderDetail.getParameters().put('cid', String.valueOf(cid));
        pageRefOrderDetail.getParameters().put('ssid', String.valueOf(sourcesystem));
        pageRefOrderDetail.getParameters().put('id', String.valueOf(id));
        Test.setCurrentPage(pageRefOrderDetail);

        controllerOrder.getOrderDetail();       
        controllerOrder.cancel();        
        controllerOrder.getSfIdQueryParam();
        
        Test.stopTest();  
    }
    
    static testMethod void TestMethodShipment()
    { 
        Test.startTest(); 
        
        String cid = '129746.02;';
        String clid = '129746.02;';
        String sourcesystem = '02';
        String oid = '129746;';
        String id = '1000';
        
        Pentair_ERICO_ShipmentStatus controllerShip = new Pentair_ERICO_ShipmentStatus();        
             
        PageReference pageRefShip = Page.Pentair_Shipment;
        pageRefShip.getParameters().put('cid', String.valueOf(cid));
        pageRefShip.getParameters().put('ssid', String.valueOf(sourcesystem));
        pageRefShip.getParameters().put('id', String.valueOf(id));
        pageRefShip.getParameters().put('oid', String.valueOf(oid));
        Test.setCurrentPage(pageRefShip);
        
        controllerShip.getShipmentStatusHeader();
      
        controllerShip.cancel();        
        controllerShip.getSfIdQueryParam();
        
        Test.stopTest();  
    }
}