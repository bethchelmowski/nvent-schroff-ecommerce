public class Pentair_Erico_Main {   
    public String id {get; set;}
    
    public pagereference backMethod()
    {
        this.id = ApexPages.CurrentPage().getParameters().get('id');
        PageReference pg = new PageReference('/'+  id); 
      	pg.setRedirect(true);
      	return pg;
    }
}