/**
 * Created By ankitSoni on 05/08/2020
 * Description : Test Class for x7S_PricingRequestCallout
 */
@isTest
public with sharing class x7S_PricingRequestCalloutTest {
    
    @IsTest
    static void addtoCartCallTest_existingCart(){
        ccrz__E_Cart__c newCart = x7S_TestUtility.createCart(true);
        List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1,newCart.Id,true);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new x7S_PricingRequestCalloutMock());
            String responseMessage = x7S_PricingRequestCallout.addSkuToCartCall();
            System.debug('Test Response Message -'+responseMessage);
        Test.stopTest();
    }
    @IsTest
    static void addtoCartCallTest_newCart(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new x7S_PricingRequestCalloutMock());
            String responseMessage = x7S_PricingRequestCallout.addSkuToCartCall();
            System.debug('Test Response Message -'+responseMessage);
        Test.stopTest();
    }
}