/**
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

public with sharing class x7sProductActionModel
{
	@AuraEnabled
	public String name, label, actionValue, actionType, title;

	@AuraEnabled
	public Boolean newWindow;

	public x7sProductActionModel()
	{
		name        = '';
		label       = '';
		actionValue = '';
		actionType  = '';
		title       = '';
		newWindow   = false;
	}

	public void Dump(String heading)
	{
		System.debug('Dump action mode  : ' + heading);
		System.debug('  name            : ' + name);
		System.debug('  label           : ' + label);
		System.debug('  type            : ' + actionType);
		System.debug('  value           : ' + actionValue);
		System.debug('  tooltip         : ' + title);
		System.debug('  newWindow       : ' + newWindow);
	}
}