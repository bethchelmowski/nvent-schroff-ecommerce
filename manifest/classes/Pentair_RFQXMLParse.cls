//**********************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  	//
// 	Class Name: Pentair_RFQXMLParse																			// 								
//	Purpose of the Class: Creation of Qoute for Thermal when Data Flows from SAP							//
//	Created By: Slalom																						// 		
//	Created Date: 4/4/2018										  											//
//----------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose						//
//	||	1.0		||	4/4/2018		||	Slalom		||				||	Initial Creation					//
//	||	2.1		||	2/11/2018		||	Tredence	||	A-0208		||	Adding More Sales ORGS for Thermal	//
//	||  3.0		|| 19/11/2018		||	Tredence	||	Ad-01		||  Uncommenting the Sales Org Codes	//
//**********************************************************************************************************//																
public class Pentair_RFQXMLParse {    
    public class RFQRecord
    {
        public string erpQuoteNumber{ set; get; }
        public string name { set; get; }
        public string potentialCustomer{ set; get; }
        public string contactName{ set; get; }
        public string contactPhone { set; get; }
        public string contactFax{ set; get; }
        public string contactEmail{ set; get; }
        public string shipToName { set; get; }
        public string shipToAddressLine1{ set; get; }
        public string shipToAddressLine2{ set; get; }
        public string shipToAddressLine3 { set; get; }
        public string shipToCity{ set; get; }
        public string shipToState{ set; get; }
        public string shipToZip { set; get; }
        public string shipToCountry{ set; get; }
        public string billToName{ set; get; }
        public string billToAddressLine1 { set; get; }
        public string billToAddressLine2{ set; get; }
        public string billToAddressLine3{ set; get; }
        public string billToCity { set; get; }
        public string billToState{ set; get; }
        public string billToZip{ set; get; }
        public string billToCountry { set; get; }
        public string projectName{ set; get; }
        public string quoteNumber{ set; get; }
        public string descriptionNA { set; get; }
        public string invoiceDate{ set; get; }
        public string freightAmount{ set; get; }
        public string ownerQuote { set; get; }
        public string ownerQuoteName{ set; get; }
        public string contractor{ set; get; }
        public string refName { set; get; }
        public string refAddressLine1{ set; get; }
        public string refAddressLine2{ set; get; }
        public string refAddressLine3 { set; get; }
        public string refCity{ set; get; }
        public string refState{ set; get; }
        public string refZip { set; get; }
        public string refFax{ set; get; }
        public string refCountry{ set; get; }
        public string refPhone { set; get; }
        public string refEmail{ set; get; }
        public string effectiveFrom{ set; get; }
        public string effectiveTo { set; get; }
        public string currencyList{ set; get; }
        public string pricelist{ set; get; }
        public string sharepoint { set; get; }
        public string shipmentMethod{ set; get; }
        public string paymentTerms{ set; get; }
        public string freightTerms { set; get; }
        public string detailAmount{ set; get; }
        public string quantity{ set; get; }
        public string discount { set; get; }
        public string preFreightAmount{ set; get; }
        public string totalTax{ set; get; }
        public string totalAmount { set; get; }
        public string title{ set; get; }
        public string description { set; get; }
        public string opportuntyId{ set; get; }
        public string incoterms{ set; get; }
        public string incotermsDescription{ set; get; }
        public string packagingandInsuranceCosts{ set; get; }
        public string salesOrganization {set;get;}
        public string materialNumber{ set; get; }
        public string unitPrice{ set; get; }
        public string listPrice{ set; get; }
        public string salesDocItem {set; get;}
        public string subTotalLine {set; get; }
        public string descriptionLine {set; get;}
    }
    
    public static String stripCDATA(String str) {
        Pattern p = Pattern.compile('<!\\[CDATA\\[(.*?)\\]\\]>');
        Matcher m = p.matcher(str);
        while(m.find()) {
            str = str.replace(m.group(), m.group(1).escapeXml());
        }
        return str;
    }
    
    public static void ProcessOrderXMLFile(RFQ_XML__c rfqXmlFile)
    {
        //This will close the Quote to Won
        System.debug('XML Node Record ID: ' + rfqXmlFile.Id);
               
        if (rfqXmlFile.QUOTE_NUMBER__c != null)
        {
            String rfqId = Pentair_RFQXMLParse.IsERPNumberOnRFQ(rfqXmlFile.QUOTE_NUMBER__c);
            
            if (rfqId != null)
            {
                RFQ__c aux = new RFQ__c(ID=rfqId);
                aux.Status__c = 'Quoted';
                update aux;
            }
        }
    }
    
    public static Boolean CreateQuoteHeader(String sapOrgId)
    {
        Boolean createHeader = false;
        
        if (sapOrgId == '1007') //Tracer Industries-CA
            createHeader = true;
        else if (sapOrgId == '1090') //Pentair Thermal Mgmt
            createHeader = true;
        else if (sapOrgId == '3400') //Erico Lightning AU
            createHeader = true;
        else if (sapOrgId == '8550') //Pentair Thermal  CA
            createHeader = true;
        //------------- Ad-01-- Uncommented the sales ORG Changes which were rolled back on 10/10 --- Changes Starts here----
        else if (sapOrgId == '1200') //PTS-Switzerland
            //createHeader = false;
              createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal -- 10/10 Roll back Changes
         else if (sapOrgId == '1210') //A-0208 PTS-Czech (Added by Tredence)
            createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal
        else if (sapOrgId == '1220') //PTS-Belgium/Italy (Added by Tredence)
            createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal
        else if (sapOrgId == '1221') //PTS-Italy (Added by Tredence)
            createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal -- 10/10 Roll back Changes*/
        else if (sapOrgId == '1222') //Schroff Italy
            createHeader = false;
        else if (sapOrgId == '1230') //PTM-Sweden
              //createHeader = false;
              createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal-- 10/10 Roll back Changes
        else if (sapOrgId == '1235') //PTM-Germany
              //createHeader = false;
              createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal-- 10/10 Roll back Changes
        else if (sapOrgId == '1236') //PTM-UK
              //createHeader = false;
              createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal-- 10/10 Roll back Changes
        else if (sapOrgId == '1237') //PTM-FRANCE
              //createHeader = false;
              createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal-- 10/10 Roll back Changes
        else if (sapOrgId == '1238') //Schroff UK
            createHeader = false;
        else if (sapOrgId == '1242') //PTM-Norway
              //createHeader = false;
              createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal-- 10/10 Roll back Changes
        else if (sapOrgId == '1243') //PTM-Netherlands
              //createHeader = false;
              createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal-- 10/10 Roll back Changes
        else if (sapOrgId == '1244') //PTM-Belgium
              //createHeader = false;
              createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal-- 10/10 Roll back Changes
        else if (sapOrgId == '1250') //PNR- Finland
              //createHeader = false;
              createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal-- 10/10 Roll back Changes
        else if (sapOrgId == '1260') //A-0208 PTS-Turkey (Added by Tredence)
            createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal-- 10/10 Roll back Changes
        else if (sapOrgId == '1270') //PTM-Poland
              //createHeader = false;
              createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal-- 10/10 Roll back Changes
        else if (sapOrgId == '1295') //A-0208 PTS-Romania (Added by Tredence)
            //createHeader = false;
            createHeader = true; // Tredence - A-0208 Turning the value to true as its part of Thermal-- 10/10 Roll back Changes
        else if (sapOrgId == '3000') //PTM Shanghai
            createHeader = false;
        else if (sapOrgId == '6310') //PTS Germany
            createHeader = false;
        else if (sapOrgId == '6320') //PTS France
            createHeader = false;
        else if (sapOrgId == '') //Blank
            createHeader = false;  
         //------------- Ad-01-- Uncommented the sales ORG Changes which were rolled back on 10/10 --- Changes Ends here----
        return createHeader;
    }
    
    public static void ProcessXMLFile(RFQ_XML__c rfqXmlFile, Decimal lineItem)
    {
        System.debug('XML Node Record ID: ' + rfqXmlFile.Id);
            
        /**
        RFQ_XML__c rfqXmlFile = [SELECT ID, Name, NAME__c, POTENTIAL_CUSTOMER__c, CONTACT_NAME__c, CONTACT_PHONE__c,ERP_QUOTE_NUMBER__c,
                                 CONTACT_FAX__c, CONTACT_EMAIL__c, SHIPTO_NAME__c,SHIPTO_ADDR_LINE1__c, SHIPTO_ADDR_LINE2__c,
                                 SHIPTO_CITY__c, SHIPTO_ADDR_LINE3__c, SHIPTO_STATE__c, SHIPTO_ZIP__c,
                                 SHIPTO_COUNTRY__c, BILLTO_NAME__c, BILLTO_ADDR_LINE1__c, BILLTO_ADDR_LINE2__c,
                                 BILLTO_ADDR_LINE3__c, BILLTO_CITY__c, BILLTO_STATE__c, BILLTO_ZIP__c, BILLTO_COUNTRY__c,
                                 PROJECT_NAME__c, QUOTE_NUMBER__c, DESCRIPTION_NA__c, INVOICED_DATE__c, ORDER_FREIGHT_AMOUNT__c,
                                 OWNER_QUOTE__c, OWNER_QUOTE_NAME__c, CONTRACTOR__c, CUSTOMER_REFERENCE_NAME__c, 
                                 CUSTOMER_REFERENCE_ADDR_LINE1__c, CUSTOMER_REFERENCE_ADDR_LINE2__c, 
                                 CUSTOMER_REFERENCE_ADDR_LINE3__c, CUSTOMER_REFERENCE_CITY__c, CUSTOMER_REFERENCE_STATE__c, 
                                 CUSTOMER_REFERENCE_ZIP__c, CUSTOMER_REFERENCE_COUNTRY__c, CUSTOMER_REFERENCE_PHONE__c, 
                                 CUSTOMER_REFERENCE_FAX__c, CUSTOMER_REFERENCE_EMAIL__c, EFFECTIVE_FROM__c, EFFECTIVE_TO__c, 
                                 CURRENCY__c, PRICE_LIST__c, SHAREPOINT_SUBFOLDER_NAME__c, SHIPMENT_METHOD__c, PAYMENT_TERMS__c,
                                 FREIGHT_TERMS__c, ORDER_DETAIL_AMOUNT__c, QUANTITY__c, ORDER_DISCOUNT__c, DESCRIPTION__c,
                                 ORDER_PRE_FREIGHT_AMOUNT__c, ORDER_TOTAL_TAX__c, ORDER_TOTAL_AMOUNT__c, TITLE__c, OPPORTUNITY_ID__c ,
                                 Incoterms__c, Incoterms_Description__c, Packaging_and_Insurance_Costs__c
                                 FROM RFQ_XML__c 
                                 WHERE ID = :RFQXML_ID];
            **/            
            Pentair_RFQXMLParse.RFQRecord singleRFQ = new Pentair_RFQXMLParse.RFQRecord();
            //Get the value for the variables
            singleRFQ.erpQuoteNumber = rfqXmlFile.ERP_QUOTE_NUMBER__c;
            singleRFQ.name = rfqXmlFile.Name;
            singleRFQ.potentialCustomer = rfqXmlFile.POTENTIAL_CUSTOMER__c;
            singleRFQ.contactName = rfqXmlFile.CONTACT_NAME__c;
            singleRFQ.contactPhone = rfqXmlFile.CONTACT_PHONE__c;
            singleRFQ.contactFax = rfqXmlFile.CONTACT_FAX__c;
            singleRFQ.contactEmail = rfqXmlFile.CONTACT_EMAIL__c;
            singleRFQ.shipToName = rfqXmlFile.SHIPTO_NAME__c;
            singleRFQ.shipToAddressLine1 = rfqXmlFile.SHIPTO_ADDR_LINE1__c;
            singleRFQ.shipToAddressLine2 = rfqXmlFile.SHIPTO_ADDR_LINE2__c;
            singleRFQ.shipToAddressLine3 = rfqXmlFile.SHIPTO_ADDR_LINE3__c;
            singleRFQ.shipToCity = rfqXmlFile.SHIPTO_CITY__c;
            singleRFQ.shipToState = rfqXmlFile.SHIPTO_STATE__c;
            singleRFQ.shipToZip = rfqXmlFile.SHIPTO_ZIP__c;
            singleRFQ.shipToCountry = rfqXmlFile.SHIPTO_COUNTRY__c;
            singleRFQ.billToName = rfqXmlFile.BILLTO_NAME__c;
            singleRFQ.billToAddressLine1 = rfqXmlFile.BILLTO_ADDR_LINE1__c;
            singleRFQ.billToAddressLine2 = rfqXmlFile.BILLTO_ADDR_LINE2__c;
            singleRFQ.billToAddressLine3 = rfqXmlFile.BILLTO_ADDR_LINE3__c;
            singleRFQ.billToCity = rfqXmlFile.BILLTO_CITY__c;
            singleRFQ.billToState = rfqXmlFile.BILLTO_STATE__c;
            singleRFQ.billToZip = rfqXmlFile.BILLTO_ZIP__c;
            singleRFQ.billToCountry = rfqXmlFile.BILLTO_COUNTRY__c;
            singleRFQ.projectName = rfqXmlFile.PROJECT_NAME__c;
            singleRFQ.quoteNumber = rfqXmlFile.QUOTE_NUMBER__c;
            singleRFQ.descriptionNA = rfqXmlFile.DESCRIPTION_NA__c;
            singleRFQ.invoiceDate = rfqXmlFile.INVOICED_DATE__c;
            
            singleRFQ.ownerQuote = rfqXmlFile.OWNER_QUOTE__c;
            singleRFQ.ownerQuoteName = rfqXmlFile.OWNER_QUOTE_NAME__c;
            singleRFQ.contractor = rfqXmlFile.CONTRACTOR__c;
            singleRFQ.refName = rfqXmlFile.CUSTOMER_REFERENCE_NAME__c;
            singleRFQ.refAddressLine1 = rfqXmlFile.CUSTOMER_REFERENCE_ADDR_LINE1__c;
            singleRFQ.refAddressLine2 = rfqXmlFile.CUSTOMER_REFERENCE_ADDR_LINE2__c;
            singleRFQ.refAddressLine3 = rfqXmlFile.CUSTOMER_REFERENCE_ADDR_LINE3__c;
            singleRFQ.refCity = rfqXmlFile.CUSTOMER_REFERENCE_CITY__c;
            singleRFQ.refState = rfqXmlFile.CUSTOMER_REFERENCE_STATE__c;
            singleRFQ.refZip = rfqXmlFile.CUSTOMER_REFERENCE_ZIP__c;
            singleRFQ.refCountry = rfqXmlFile.CUSTOMER_REFERENCE_COUNTRY__c;
            singleRFQ.refPhone = rfqXmlFile.CUSTOMER_REFERENCE_PHONE__c;
            singleRFQ.refFax = rfqXmlFile.CUSTOMER_REFERENCE_FAX__c;
            singleRFQ.refEmail = rfqXmlFile.CUSTOMER_REFERENCE_EMAIL__c;
            singleRFQ.effectiveFrom = rfqXmlFile.EFFECTIVE_FROM__c;
            singleRFQ.effectiveTo = rfqXmlFile.EFFECTIVE_TO__c;
            singleRFQ.currencyList = rfqXmlFile.CURRENCY__c;
            singleRFQ.pricelist = rfqXmlFile.PRICE_LIST__c;
            singleRFQ.sharepoint = rfqXmlFile.SHAREPOINT_SUBFOLDER_NAME__c;
            singleRFQ.shipmentMethod = rfqXmlFile.SHIPMENT_METHOD__c;
            singleRFQ.paymentTerms = rfqXmlFile.PAYMENT_TERMS__c;
            singleRFQ.freightTerms = rfqXmlFile.FREIGHT_TERMS__c;
            singleRFQ.detailAmount = rfqXmlFile.ORDER_DETAIL_AMOUNT__c;
            singleRFQ.quantity = rfqXmlFile.QUANTITY__c;
            singleRFQ.discount = rfqXmlFile.ORDER_DISCOUNT__c;
        
            singleRFQ.preFreightAmount = '0'; //rfqXmlFile.ORDER_PRE_FREIGHT_AMOUNT__c;
            singleRFQ.freightAmount = rfqXmlFile.ORDER_FREIGHT_AMOUNT__c;
        
            singleRFQ.totalTax = rfqXmlFile.ORDER_TOTAL_TAX__c;
            singleRFQ.totalAmount = rfqXmlFile.ORDER_TOTAL_AMOUNT__c;
            singleRFQ.title = rfqXmlFile.TITLE__c;
            singleRFQ.description = rfqXmlFile.DESCRIPTION__c;
            singleRFQ.opportuntyId = rfqXmlFile.OPPORTUNITY_ID__c;     
            singleRFQ.incoterms = rfqXmlFile.Incoterms__c; 
            singleRFQ.incotermsDescription = rfqXmlFile.Incoterms_Description__c; 
            singleRFQ.packagingandInsuranceCosts = rfqXmlFile.Packaging_and_Insurance_Costs__c;
            singleRFQ.salesOrganization = rfqXmlFile.SALES_ORGANIZATION__c;
        
            singleRFQ.materialNumber = rfqXmlFile.MATERIAL_NUMBER__c;
            singleRFQ.unitPrice = rfqXmlFile.UNIT_PRICE__c;
            singleRFQ.listPrice = rfqXmlFile.LIST_PRICE__c;
            singleRFQ.salesDocItem = rfqXmlFile.SALES_DOC_ITEM__c;
            singleRFQ.subTotalLine = rfqXmlFile.TOTAL_WITHOUT_TAX__c;
            singleRFQ.descriptionLine = rfqXmlFile.DESCRIPTION_LINE__c;
        
            System.debug('ERP ID: ' + singleRFQ.erpQuoteNumber);
                    
            String rfqId = Pentair_RFQXMLParse.IsERPNumberOnRFQ(singleRFQ.erpQuoteNumber);
            String salesTeam = Pentair_RFQXMLParse.GetSalesTeam(singleRFQ.ownerQuote);
            Boolean createSAPHeaderRecord = Pentair_RFQXMLParse.CreateQuoteHeader(singleRFQ.salesOrganization);
        
            System.debug('rfqId ID: ' + rfqId);
            System.debug('salesTeam: ' + salesTeam);
            
            //Create Header Record and line items
            Pentair_RFQXMLParse.CreateUpdateRFQRecord(singleRFQ, rfqId, salesTeam, createSapHeaderRecord, lineItem);
                
    }
    
    public static void RFQXMLLines(String erpNumber, String typeRecord, String rfqId)
    {        
        String xErpNumber= '%'+erpNumber+'%';
               
        List<RFQ_XML__c> rfqLineItems = [SELECT ERP_QUOTE_NUMBER__C, SALES_DOC_ITEM__C, OWNER_NUMBER__c, OWNER_NAME__c, DOCUMENT_CURRENCY__C, 
                                                ORDER_TOTAL_AMOUNT__C, CURRENCY__c, OUTPUT_LANGUAGE__c, ORDER_PRE_FREIGHT_AMOUNT__c,
                                                ORDER_FREIGHT_AMOUNT__c, MATERIAL_NUMBER__c, DESCRIPTION_LINE__c, QUANTITY_LINE__c, BASE_UNIT__c,
                                                UNIT_PRICE__c, LIST_PRICE__c, HIGH_LVL_ITEM_BOM__C, NET_VALUE__C, CU_ORD_QTY_IN_SU__C,
                                                NET_PRICE__C, SUBTOTAL_1__C, CONFIGURATION__C, MATERIAL_ENTERED__C
                                         FROM RFQ_XML__c
                                         WHERE ERP_QUOTE_NUMBER__C like :xErpNumber
                                         AND Type__c =:typeRecord];
        
        List<RFQ_XML_COND__C> rfqLineItemConds = [SELECT ID, NAME, DOCUMENT_CONDITION__C, CONDITION_ITEM_NUMBER__C, CONDITION_TYPE__C, 
                                                         CAL_TYPE_FOR_CONDITION__C, CONDITION_USED_FOR_STAT__C, CONDITION_IS_INACTIVE__C, 
                                                         CONDITION_PRICING_UNIT__C, STEP_NUMBER__c, CONDITION_BASE_VALUE__c,
                                                         RATE__c, CURRENCY_KEY__c, CONDITION_VALUE__c
                                                  FROM RFQ_XML_COND__C
                                                  WHERE NAME like :xErpNumber
                                                  ];
        
        System.debug('rfqLineItems group count' + rfqLineItems.size());
        
        if (rfqLineItems.size() > 0)
        {           
            //for (RFQ_XML__c lineItems : rfqLineItems)
            //{                
            //  Pentair_RFQ_XML_COND.ProcessLineItems(erpNumber, lineItems.SALES_DOC_ITEM__C, rfqId, rfqLineItemConds);
            //}
                
                Pentair_RFQ_XML_COND.ProcessLineItems(erpNumber, rfqLineItems, rfqId, rfqLineItemConds);
                Pentair_RFQ_XML_COND.AdditionalLines(rfqLineItemConds, rfqLineItems[0].DOCUMENT_CURRENCY__C, 
                                                     rfqLineItems[0].OUTPUT_LANGUAGE__c, rfqId);
                                                     
            List<RFQ__C> getRecordLineItem = [SELECT Line_Item_Rollup__c FROM RFQ__C WHERE ID =: rfqId];
            
            try
            {
                Boolean isNotValidate = false;
                if (Decimal.valueof(rfqLineItems[0].ORDER_TOTAL_AMOUNT__C) <> getRecordLineItem[0].Line_Item_Rollup__c)
                    isNotValidate = true;
                
                RFQ__C updateRFQTotal = new RFQ__C(
                    ID=rfqId,
                    Value__C = Decimal.valueof(rfqLineItems[0].ORDER_TOTAL_AMOUNT__C),
                    Freight_Amount__c = Decimal.valueof(rfqLineItems[0].ORDER_FREIGHT_AMOUNT__c),
                    Pre_Freight_Amount__c = 0, //Decimal.valueof(rfqLineItems[0].ORDER_FREIGHT_AMOUNT__c),
                    CurrencyIsoCode = rfqLineItems[0].CURRENCY__c,
                    Is_Not_Validate__c = isNotValidate
                );
                update updateRFQTotal;
            }
            catch (Exception ex)
            {
                System.Debug('Error updating total: ex: ' + ex.getMessage());
            }
        }
    }
    
    public static String GetOwnerId(String ownerId)
    {
        String returnOwnerId = '';
        List<User> userList = [SELECT ID, SAP_ID__c, ProfileId FROM User 
                               WHERE SAP_ID__c LIKE :('%' + ownerId + '%')];
        
        if (userList.size() > 0)
        {
            returnOwnerId = userList[0].Id;
        }
        
        return returnOwnerId;
    }
    
    public static String GetSalesTeam(String ownerId)
    {
        string profileName = '';
        List<User> userList = [SELECT ID, SAP_ID__c, ProfileId FROM User 
                               WHERE SAP_ID__c LIKE :('%' + ownerId + '%')];
        
        if (userList.size() > 0)
        {
            List<Profile> profileList = [SELECT ID, Name FROM Profile 
                                        WHERE ID =:userList[0].ProfileId];
            
            if (profileList.size() > 0)
            {
                if (profileList[0].Name.contains('BIS'))
                    profileName = 'BIS';
                else if (profileList[0].Name.contains('CADDY'))
                    profileName = 'CADDY';
                else if (profileList[0].Name.contains('DNS'))
                    profileName = 'DNS';
                else if (profileList[0].Name.contains('ERICO'))
                    profileName = 'ERICO';
                else if (profileList[0].Name.contains('ERIFLEX'))
                    profileName = 'ERIFLEX';
                else if (profileList[0].Name.contains('Hoffman'))
                    profileName = 'Hoffman';
                else if (profileList[0].Name.contains('IHS'))
                    profileName = 'IHS';
                else if (profileList[0].Name.contains('LENTON'))
                    profileName = 'LENTON';
                else if (profileList[0].Name.contains('Rail'))
                    profileName = 'Rail';
                else if (profileList[0].Name.contains('Schroff'))
                    profileName = 'Schroff';
            }
        }
        
        return profileName;
    }
    
    public static String IsERPNumberOnRFQ(String erpNumber)
    {
        String rfqId = null;
        String xErpNumber= '%'+erpNumber+'%';
        List<RFQ__c> rfqRecord = [SELECT ID FROM RFQ__c 
                                  WHERE ERP_Number__c like :xErpNumber];
        
        if (rfqRecord.size() > 0)
            rfqId = rfqRecord[0].Id;
        
        return rfqId;
    }
    
    public static void CreateUpdateRFQRecord(RFQRecord singleRFQRecord, String rfqId, String salesTeam, 
                                             Boolean createSapHeaderRecord, Decimal lineItem)
    {
        Id recordTypeId = [select Id from RecordType where DeveloperName = 'Quote' AND sobjecttype = 'RFQ__C' limit 1].Id;
        Id currentRecordType;
        
        Boolean createRecord = true;
        Boolean isNewRecord = false;
        RFQ__c rfqNewRecord = new RFQ__c();
        
        System.debug('Quote RFQ Type ID ' + recordTypeId);
        System.debug('SAP Header ' + createSapHeaderRecord + ', RFQ ID: ' + rfqId + ', lineItem: ' + lineItem);
  
        if (createSapHeaderRecord && rfqId == null && lineItem == 1)
        {       
            String addressBilling = '';
            if (singleRFQRecord.billToAddressLine1 != null)
            {
                addressBilling = singleRFQRecord.billToAddressLine1;
                if (singleRFQRecord.billToAddressLine2 != null)
                    addressBilling = addressBilling + ' ' + singleRFQRecord.billToAddressLine2;
                if (singleRFQRecord.billToAddressLine3 != null)
                    addressBilling = addressBilling + ' ' + singleRFQRecord.billToAddressLine3;
            }
            
            String addressShipping = '';
            if (singleRFQRecord.shipToAddressLine1 != null)
            {
                addressShipping = singleRFQRecord.shipToAddressLine1;
                if (singleRFQRecord.shipToAddressLine2 != null)
                    addressShipping = addressShipping + ' ' + singleRFQRecord.shipToAddressLine2;
                if (singleRFQRecord.shipToAddressLine3 != null)
                    addressShipping = addressShipping + ' ' + singleRFQRecord.shipToAddressLine3;
            }
            
            String addressRef = '';
            if (singleRFQRecord.refAddressLine1 != null)
            {
                addressRef = singleRFQRecord.refAddressLine1;
                if (singleRFQRecord.refAddressLine2 != null)
                    addressRef = addressRef + ' ' + singleRFQRecord.refAddressLine2;
                if (singleRFQRecord.refAddressLine3 != null)
                    addressRef = addressRef + ' ' + singleRFQRecord.refAddressLine3;
            }
            
            rfqNewRecord.RecordTypeId = recordTypeId;
            rfqNewRecord.Name = singleRFQRecord.name;
            rfqNewRecord.Sales_Team__c = salesTeam;
                
            //Check the account record
            if (SearchAccountRecord(singleRFQRecord.potentialCustomer, singleRFQRecord.billToName).length() > 0)
                rfqNewRecord.Account__c = SearchAccountRecord(singleRFQRecord.potentialCustomer, singleRFQRecord.billToName);
            else
            {
                try
                {
                rfqNewRecord.Account__c = CreateAccount(singleRFQRecord.potentialCustomer, addressBilling,
                                                            singleRFQRecord.billToCity, singleRFQRecord.billToState,
                                                            singleRFQRecord.billToZip, singleRFQRecord.billToCountry,
                                                            singleRFQRecord.billToName, singleRFQRecord.ownerQuote);
                }
                catch (Exception ex)
                {
                    System.debug('Error creating account');
                }
            }
            if(singleRFQRecord.opportuntyId != '') {
                //Check the Opportunity Record
                if (SearchOpportunityRecord(singleRFQRecord.opportuntyId).length() > 0)
                    rfqNewRecord.Opportunity__c = SearchOpportunityRecord(singleRFQRecord.opportuntyId);
            }
            
            rfqNewRecord.Contact_Name__c = singleRFQRecord.contactName;
            rfqNewRecord.Contact_Phone__c = singleRFQRecord.contactPhone;
            rfqNewRecord.Contact_Email__c = singleRFQRecord.contactEmail;
            rfqNewRecord.Contact_Fax__c = singleRFQRecord.contactFax;
            rfqNewRecord.Ship_To_Name__c = singleRFQRecord.shipToName;
            rfqNewRecord.Ship_To_Address__c = addressShipping;
            rfqNewRecord.Ship_To_City__c = singleRFQRecord.shipToCity;
            rfqNewRecord.Ship_To_State__c = singleRFQRecord.shipToState;
            rfqNewRecord.Ship_To_Postal_Code__c = singleRFQRecord.shipToZip;
            rfqNewRecord.Ship_To_Country__c = singleRFQRecord.shipToCountry;
            rfqNewRecord.Bill_To_Name__c = singleRFQRecord.billToName;
            rfqNewRecord.Bill_To_Address__c = addressBilling;
            rfqNewRecord.Bill_To_City__c = singleRFQRecord.billToCity;
            rfqNewRecord.Bill_To_State__c = singleRFQRecord.billToState;
            rfqNewRecord.Bill_To_Postal_Code__c = singleRFQRecord.billToZip;
            rfqNewRecord.Bill_To_Country__c = singleRFQRecord.billToCountry;
            rfqNewRecord.Description__c = singleRFQRecord.descriptionNA;
            
            if (GetOwnerId(singleRFQRecord.ownerQuote).length() > 0)
                rfqNewRecord.OwnerId = GetOwnerId(singleRFQRecord.ownerQuote);
            else 
                System.debug('Owner Name from SAP: ' + singleRFQRecord.ownerQuoteName);
            
            rfqNewRecord.Customer_Reference_Address__c = addressRef;
            rfqNewRecord.Customer_Reference_City__c = singleRFQRecord.refCity;
            rfqNewRecord.Customer_Reference_Country__c = singleRFQRecord.refCountry;
            rfqNewRecord.Customer_Reference_Fax__c = singleRFQRecord.refFax;
            rfqNewRecord.Customer_Reference_Name__c = singleRFQRecord.refName;
            rfqNewRecord.Customer_Reference_Phone__c = singleRFQRecord.refPhone;
            rfqNewRecord.Customer_Reference_Postal_Code__c = singleRFQRecord.refZip;
            rfqNewRecord.Customer_Reference_State__c = singleRFQRecord.refState;
            rfqNewRecord.Customer_Reference_Email__c = singleRFQRecord.refEmail;
            
            if (singleRFQRecord.effectiveFrom.length() > 0)
                rfqNewRecord.Effective_From__c = Date.valueOf(formatDate(singleRFQRecord.effectiveFrom));
            
            if (singleRFQRecord.effectiveTo.length() > 0)
                rfqNewRecord.Effective_To__c = Date.valueOf(formatDate(singleRFQRecord.effectiveTo));
            
            if (singleRFQRecord.currencyList.length() > 0)
                rfqNewRecord.CurrencyIsoCode = singleRFQRecord.currencyList;
         
            rfqNewRecord.Shipping_Method__c = singleRFQRecord.shipmentMethod;
            rfqNewRecord.Freight_Terms__c = singleRFQRecord.freightTerms;
            rfqNewRecord.Payment_Method__c = singleRFQRecord.paymentTerms;
            
            rfqNewRecord.ERP_Number__c = singleRFQRecord.erpQuoteNumber;
            
            if (singleRFQRecord.freightAmount.length() > 0)
                rfqNewRecord.Freight_Amount__c = Decimal.valueOf(singleRFQRecord.freightAmount);
            if (singleRFQRecord.detailAmount.length() > 0)
                rfqNewRecord.Detail_Amount__c = Decimal.valueOf(singleRFQRecord.detailAmount);
            if (singleRFQRecord.discount.length() > 0)
                rfqNewRecord.Discount__c = Decimal.valueOf(singleRFQRecord.discount);
            if (singleRFQRecord.preFreightAmount.length() > 0)
                rfqNewRecord.Pre_Freight_Amount__c = 0; //Decimal.valueOf(singleRFQRecord.preFreightAmount);
            if (singleRFQRecord.totalTax.length() > 0)
                rfqNewRecord.Total_Tax__c = Decimal.valueOf(singleRFQRecord.totalTax);
            if (singleRFQRecord.totalAmount.length() > 0)
                rfqNewRecord.Value__c = Decimal.valueOf(singleRFQRecord.totalAmount);    
            
            isNewRecord = true;

        }
        else
        {
            if (rfqId != null)
            {
                rfqNewRecord.Id = rfqId;
                
                if (singleRFQRecord.freightAmount.length() > 0)
                {
                    rfqNewRecord.Freight_Amount__c = Decimal.valueOf(singleRFQRecord.freightAmount);
                    rfqNewRecord.Pre_Freight_Amount__c = 0;
                }
                
                rfqNewRecord.Shipping_Method__c = singleRFQRecord.shipmentMethod;
                rfqNewRecord.Freight_Terms__c = singleRFQRecord.freightTerms;
                rfqNewRecord.Payment_Method__c = singleRFQRecord.paymentTerms;
                rfqNewRecord.Incoterms__c = singleRFQRecord.incoterms;
                rfqNewRecord.Incoterms_Description__c = singleRFQRecord.incotermsDescription;
                rfqNewRecord.Packaging_and_Insurance_Costs__c = singleRFQRecord.packagingandInsuranceCosts;
                if (singleRFQRecord.totalAmount.length() > 0)
                rfqNewRecord.Value__c = Decimal.valueOf(singleRFQRecord.totalAmount);  
                
                //Check the Opportunity Record
                if (SearchOpportunityRecord(singleRFQRecord.opportuntyId).length() > 0)
                    rfqNewRecord.Opportunity__c = SearchOpportunityRecord(singleRFQRecord.opportuntyId);
            }
            else 
                createRecord = false;
        }
        
        if (createRecord && singleRFQRecord.erpQuoteNumber != null && singleRFQRecord.name.length() > 1)
        {
            upsert rfqNewRecord;
        }
        
        if (rfqId == null && rfqNewRecord.Id != null)
            rfqId = rfqNewRecord.Id;
        
        if (createSapHeaderRecord && rfqId != null)
        {
            //NuHeat Line Items
            CreateUpdateRFQLineItems(singleRFQRecord.materialNumber, singleRFQRecord.descriptionLine, singleRFQRecord.quantity,
                                    singleRFQRecord.unitPrice, singleRFQRecord.listPrice, singleRFQRecord.salesDocItem,
                                    singleRFQRecord.currencyList, singleRFQRecord.subTotalLine, rfqId);
        }
        else
        {            
            //Schroff and Hoffman Line Items
            if(singleRFQRecord.erpQuoteNumber != null && rfqId != null) 
                RFQXMLLines(singleRFQRecord.erpQuoteNumber, 'Quote', rfqId);
        }
    }
        
    // Used for BIS, IHS Quotes
    public static void CreateUpdateRFQLineItems(String materialNumber, String itemDescription, String quantity, 
                                                String unitPrice, String listPrice, String saleDocItem, 
                                                String currencyCodeValue, String subTotalLine, String rfqID)
    {        
        Integer lineNumber = Integer.valueof(saleDocItem);
        List<RFQ_Line_Item__c> lineItem = [SELECT ID FROM RFQ_Line_Item__c 
                                           WHERE Material_Number__c =:materialNumber
                                           AND Position__c =:lineNumber
                                           AND RFQ__C =: rfqID];
        if (lineItem.size() > 0)
        {
            RFQ_Line_Item__c lineItems = new RFQ_Line_Item__c(Id = lineItem[0].Id);
            if(materialNumber != null)
                lineItems.Material_Number__c = materialNumber.replaceFirst('^0+','');
            lineItems.Position__c = lineNumber;
            lineItems.Name = itemDescription;
            if(quantity != null)
                lineItems.Quantity__c =  Math.roundToLong(Integer.valueof(quantity));
            if (unitPrice != null)
                lineItems.Price_List__c = Decimal.valueof(unitPrice);
            if (listPrice != null)
                lineItems.Price_Gross__c = Decimal.valueof(listPrice);
            if (lineItems.Price_Gross__c == null)
                lineItems.Price_Gross__c = Decimal.valueof(unitPrice);
            
            lineItems.Value__c = Decimal.valueof(subTotalLine);
            lineItems.CurrencyIsoCode = currencyCodeValue;
            update lineItems;
        }
        else 
        {
            //New Record
            RFQ_Line_Item__c newLineItems = new RFQ_Line_Item__c();
            if(materialNumber != null)
                newLineItems.Material_Number__c = materialNumber.replaceFirst('^0+','');
            newLineItems.Position__c = lineNumber;
            newLineItems.Name = itemDescription;
            if(quantity != null)
                newLineItems.Quantity__c =  Math.roundToLong(Integer.valueof(quantity));
            if (unitPrice != null)
                newLineItems.Price_List__c = Decimal.valueof(unitPrice);
            
            if (listPrice != null)
                newLineItems.Price_Gross__c = Decimal.valueof(listPrice);
            
            if (newLineItems.Price_Gross__c == null)
                newLineItems.Price_Gross__c = Decimal.valueof(unitPrice);
                
            newLineItems.RFQ__c = rfqID;
            
            if(subTotalLine != null)
                newLineItems.Value__c = Decimal.valueof(subTotalLine);
            newLineItems.CurrencyIsoCode = currencyCodeValue;
            if(newLineItems.RFQ__c != null)
                insert newLineItems;
        }
    }
    
    public static String SearchOpportunityRecord(String opportunityId)
    {
        String crmOpportuntyId = '';
        
        List<Opportunity> xOpp = [SELECT ID FROM Opportunity 
                                  WHERE ID =:opportunityId];
        
        if (xOpp.size() > 0)
            crmOpportuntyId = xOpp[0].Id;
        
        return crmOpportuntyId;
    }
    
    public static String SearchAccountRecord(String accountNumber, String xName)
    {
        String crmAccountId = '';
        
        List<Account> xAccount = [SELECT ID FROM Account 
                                  WHERE SAP__c like :('%' + accountNumber + '%') OR Name =: xName LIMIT 1];
        
        if (xAccount.size() > 0)
            crmAccountId = xAccount[0].Id;
        
        return crmAccountId;
    }
    
    public static String CreateAccount(String accountNumber, String street, String city, String stateCode,
                                       String zipCode, String countryCode, String name, String ownerQuote)
    {        
        Account newAccount = new Account(
            Name = name,
            SAP__C = accountNumber,
            BillingStreet = street,
            BillingCity = city,
            BillingPostalCode = zipCode,
            BillingCountryCode = countryCode
        );
        
        if (GetOwnerId(ownerQuote).length() > 0)
            newAccount.OwnerId = GetOwnerId(ownerQuote);
        

        insert newAccount;
        
        return newAccount.Id;
    }
    
    public static String formatDate(String d) {
        string year = d.substring(0,4);
        string month = d.substring(4,6);
        string day = d.substring(6,8);
        
        System.debug('Day: ' + day + ', Month ' + month + ', ' + year);
        
        return year + '-' + month + '-' + day;
    }
}