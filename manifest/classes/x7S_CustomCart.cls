public with sharing class x7S_CustomCart {
    
    public static void customFetch(){
        String userId = '005P0000004UEnqIAG';
        Set<String> cartIdList = new Set<String>{'a3kP0000000OvibIAC'};

        if (string.isBlank(userId))
		{
			userId = UserInfo.getUserId();
		}
        
		Map<String, Object> inputData = new Map<String, Object> {
			ccrz.ccApi.API_VERSION    => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccApiCart.ACTIVECART => true,
            ccrz.ccApiCart.BYOWNER    => userId,
			ccrz.ccApiCart.BYUSER     => userId,
			ccrz.ccApi.SIZING         => new Map<String, Object>
				{ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL}}
        };
        
        System.debug('Customer no. : ' + userId);
        
        try{
            Map<String,Object> outputData = ccrz.ccApiCart.fetch(inputData);
            System.debug('Output Data -'+outputData);
            if (outputData.get(ccrz.ccAPICart.CART_OBJLIST) != null) {
                List<Map<String, Object>> outputCartList = (List<Map<String, Object>>) outputData.get(ccrz.ccAPICart.CART_OBJLIST);
                System.debug('Output Cart Data -'+outputCartList);
          
                if(outputCartList.size() > 0){
                    for(Map<String, Object> cartEntry : outputCartList)
					{
						List<Map<String, Object>> items = (List<Map<String, Object>>)cartEntry.get('ECartItems');
						for (Map<String, Object> item : items)
						{
                            //System.debug('Product SKU NO.'+item.get('product'));
						}
					}

                } 
            }
    
        }
        
        catch(Exception e){
            System.debug('Exception Message '+e.getMessage());
        }
        
    }
}