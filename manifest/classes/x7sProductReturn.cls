/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/16/19.
 */

public with sharing class x7sProductReturn
{
	@AuraEnabled
	public Boolean status;

	@AuraEnabled
	public String message, value, data;

	public x7sProductReturn()
	{
		status  = false;
		message = '';
		value   = '';
		data    = '';
	}

	public x7sProductReturn(Boolean status, String message)
	{
		this();
		this.status  = status;
		this.message = message;
	}

	public x7sProductReturn (Boolean status, String message, String value)
	{
		this(status, message);
		this.value = value;
	}
}