//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: Tag_Dup_Contact_Scheduler	                                                                                //
// 	Purpose: Scheduler Class for scheduling the Batch Class Tag_Dup_Contact_Batch											//						                        //
//  Created By: Tredence																									//			
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//


global class Tag_Dup_Contact_Scheduler implements schedulable
{
    global void execute(schedulableContext sc)
    {
        Tag_Dup_Contact_Batch BAC=new Tag_Dup_Contact_Batch();
        database.executeBatch(BAC);
    }
}