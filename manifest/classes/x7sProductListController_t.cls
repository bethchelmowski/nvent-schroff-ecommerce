/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/11/19.
 */

@IsTest (SeeAllData=true)
private class x7sProductListController_t
{
	@IsTest
	public static void testGetInitialSetup()
	{
		Map<String, Object> setup = x7sProductListController.setupInitialInput('myStore', 'nameDesc', 10);
		System.assertNotEquals(null, setup.get(ccrz.ccApi.API_VERSION));
		System.assert(setup.get(ccrz.ccAPIProduct.PRODUCTSTOREFRONT) == 'myStore');
	}


	@IsTest
	public static void testGetProducts()
	{
		// no category
		x7sProductListModel      productList  = x7sProductListController.getProducts('', 20,'', '', '', '', 0, 0, '' );
		System.assertNotEquals(null, productList);

		// 1st category
		List<x7sProductCategory> categories = x7sProductCategoryController.getCategories('');
		x7sProductListModel      listModel  = x7sProductListController.getProducts('', 20,'', '', '', '', 0, 0, categories[0].id );
		System.assertNotEquals(null, listModel);
		listModel.Dump('testGetProducts');
	}

	@IsTest
	public static void testGetProductsById()
	{
		Integer pageSize = 5;
		List<x7sProductCategory> categories = x7sProductCategoryController.getCategories('');
		for (x7sProductCategory categoryItem : categories)
		{
			categoryItem.Dump('category dump');
		}

		x7sProductListModel      listModel  = x7sProductListController.getProducts('', pageSize,'', '', '', '', 0, 0, categories[0].id );
		List<String> idList = new List<String>();
		for(x7sProductModel product : listModel.items)
		{
			idList.add(product.id);
		}

		x7sProductListModel idListModel = x7sProductListController.getProductsById('', idList);
		System.assertNotEquals(null, idListModel);
		idListModel.Dump('testGetProductsById');
	}

	@IsTest
	public static void testSearchProducts()
	{
		Integer pageSize = 5;
		List<x7sProductCategory> categories = x7sProductCategoryController.getCategories('');
		System.debug('Categories: ' + categories);
		System.debug('1st category: ' + categories[0]);

		x7sProductListModel      listModel  = x7sProductListController.getProducts('', pageSize,'', '', '', '', 0, 0, categories[0].id );
		listModel.Dump('testSearchProducts');
		String searchTerm = listModel.items[0].productCode;

		x7sProductListModel      searchModel = x7sProductListController.getProducts('', pageSize, searchTerm, '', '', '', 0, 0, categories[0].id);
		System.assertNotEquals(null, searchModel);
		searchModel.Dump('testSearchProducts');
	}

	@IsTest
	public static void testProductTypeList()
	{
		List<String> typeList = x7sProductListController.getProductTypeList();
		System.assertNotEquals(null, typeList);
		System.debug('typeList: ' + typeList);
	}

	@IsTest
	public static void testGetSortOrder()
	{
		Map<String, Object> dataMap   = new Map<String, Object>();

		Map<String, Object> sortOrder = x7sProductListController.getSortOrder(dataMap, 'nameAsc');
		System.assert(sortOrder.get(ccrz.ccService.ORDERBY) == ccrz.ccAPIProductIndex.BY_NAME);
		System.assert(sortOrder.get(ccrz.ccService.SORTDESC) == false);

		sortOrder = x7sProductListController.getSortOrder(dataMap, 'nameDesc');
		System.assert(sortOrder.get(ccrz.ccService.ORDERBY) == ccrz.ccAPIProductIndex.BY_NAME);
		System.assert(sortOrder.get(ccrz.ccService.SORTDESC) == true);

		sortOrder = x7sProductListController.getSortOrder(dataMap, 'priceAsc');
		System.assert(sortOrder.get(ccrz.ccService.ORDERBY) == ccrz.ccAPIProductIndex.BY_PRICE);
		System.assert(sortOrder.get(ccrz.ccService.SORTDESC) == false);

		sortOrder = x7sProductListController.getSortOrder(dataMap, 'priceDesc');
		System.assert(sortOrder.get(ccrz.ccService.ORDERBY) == ccrz.ccAPIProductIndex.BY_PRICE);
		System.assert(sortOrder.get(ccrz.ccService.SORTDESC) == true);
	}

	@IsTest
	public static void testGetActiveCart()
	{
		x7sProductReturn returnValue = x7sProductListController.getActiveCart('');
		System.assertNotEquals(null, returnValue);
	}

	@IsTest
	public static void testAddToCart()
	{
		List<x7sProductCategory> categories = x7sProductCategoryController.getCategories('');
		x7sProductListModel      listModel  = x7sProductListController.getProducts('', 20,'', '', '', '', 0, 0, categories[0].id );
		System.debug('Product list model items: ' + listModel.items);

		List<String> skuList  = new List<String>{listModel.items[0].productCode};
		List<String> valueList = new List<String>{'1'};

		x7sProductReturn returnValue = x7sProductListController.addItemsToCart('', skuList, valueList);
		System.assertNotEquals(null, returnValue);
	}

	@IsTest
	public static void testGetFeaturedProducts()
	{
		x7sProductListModel listModel = x7sProductListController.getProductsFeatured('');
		System.assertNotEquals(null, listModel);
	}

	@IsTest
	public static void testSpecFilter()
	{
		String filterString = 'specId1:value1;specId2:value1,value2';
		Map<String, Object> initialData = new Map<String, Object>();

		Map<String, Object> returnMap = x7sProductListController.setupSpecFilter(initialData, filterString);
		System.assertNotEquals(null, returnMap);
	}
}