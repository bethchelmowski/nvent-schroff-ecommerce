/**
 * Created By ankitSoni on 05/20/2020
 */
public with sharing class x7S_AddProduct {
    
    public static void addProductSKU(String skuNumber , String quantity){

        //x7S_AddProduct.addProductSKU('60110456','1');

        String customerNumber = '7000021793'; // HardCoded .. will get it from Acquia
        String countryOfUser = '';
        String currencyIsoCode = '';
        String listPrice = null;
        Set<String> productIdSet = new Set<String>();

        x7S_PricingResponse prResponse = new x7S_PricingResponse();
        Map < String, Object > res1 = new Map < String, Object > (); // to get currencyIsoCode
        
        List<ccrz__E_Product__c> addProductList = new List<ccrz__E_Product__c>();
        List<ccrz__E_PriceListItem__c> addPriceListItemList = new List<ccrz__E_PriceListItem__c>();
        List<ccrz__E_ProductCategory__c> addProductCategoryList = new List<ccrz__E_ProductCategory__c>();


        List < x7S_PricingRequest.Items > itemList = new List < x7S_PricingRequest.Items > ();
        x7S_PricingRequest.Items skuItem = new  x7S_PricingRequest.Items();
        skuItem.itemNumber = skuNumber;
        skuItem.quantity = quantity;
        skuItem.unitOfMeasure = '';

        itemList.add(skuItem);

        // We get the Pricing from our Mulesoft API Callout

        HttpResponse pricingResponse = new HttpResponse();
        try{
            pricingResponse = x7S_PricingRequestCallout.getPricingInfo(customerNumber, countryOfUser, itemList);
        }
        catch(Exception e){
            System.debug('Api Callout Exception - '+e.getMessage());
        }
        
        if (pricingResponse.getStatusCode() == 200) {
            prResponse = x7S_PricingResponse.parse(pricingResponse.getBody());

            res1 = (Map < String, Object > ) JSON.deserializeUntyped(pricingResponse.getBody());
            for (String str: res1.keySet()) {
                if (str == 'items') {
                    List < Object > tmpList = (List < Object > ) res1.get('items');
                    Map < String, Object > temp = (Map < String, Object > ) tmpList[0];
                    if (temp.containsKey('listPrice')) {
                        Map < String, Object > temp1 = (Map < String, Object > ) temp.get('listPrice');
                        System.debug('Currency ISO Code - ' + temp1.get('currency'));
                        if (temp1.get('currency') != null) {
                            currencyIsoCode = String.valueOf(temp1.get('currency')); // Get CurrencyISOCode
                        }
                    }
                }
            }
        } else {
            System.debug('HTTP error code : ' + pricingResponse.getStatusCode() + ' HTTP error status : ' + pricingResponse.getStatus());
        }

        if(prResponse != null){

            for(x7S_PricingResponse.Items item : prResponse.items){
                
                //Add CC Product Record
                ccrz__E_Product__c addProduct = new ccrz__E_Product__c();
                addProduct.ccrz__SKU__c = item.itemNumber;
                addProduct.Name = 'Response Product '+item.itemNumber;
                addProduct.ccrz__Quantityperunit__c = Double.valueOf(quantity);
                addProduct.ccrz__UnitOfMeasure__c = item.unitOfMeasure;
                addProduct.ccrz__Storefront__c = 'DefaultStore';
                addProduct.ccrz__ProductType__c = 'Product';
                addProduct.ccrz__ProductStatus__c = 'Released';
                addProduct.ccrz__ProductIndexStatus__c = 'Not Current';
                addProduct.ccrz__LeadTime__c = Double.valueOf(item.leadTime);
                /*if (item.isAvailableFlag == 'false') {
                    addProduct.ccrz__AvailabilityMessageRT__c = 'Not Available';
                    addProduct.ccrz__AvailabilityMessage__c = 'Not Available';
                } else if (item.isAvailableFlag == 'true') {
                    addProduct.ccrz__AvailabilityMessageRT__c = 'Available';
                    addProduct.ccrz__AvailabilityMessage__c = 'Available';
                } else {
                    addProduct.ccrz__AvailabilityMessageRT__c = '';
                }*/
                listPrice = item.listPrice.value;

                addProductList.add(addProduct);
            }
            if(!addProductList.isEmpty()){
                try{
                    insert addProductList;
                    productIdSet.add(addProductList[0].Id);
                    System.debug('Product Inserted Successfully');
                }
                catch(DmlException e){
                    System.debug('Product insert DML Exception - '+e.getMessage());
                }
            }

            // Get All Product Category
            String categoryId = null;
            Map<String, Object> inputDataCategory = new Map<String, Object>{
                ccrz.ccAPI.API_VERSION => ccrz.ccApi.CURRENT_VERSION ,
                ccrz.ccApiCategory.CATEGORYNAME => 'All Products'
            };
             
            try {
                Map<String, Object> outputData = ccrz.ccAPICategory.fetch(inputDataCategory);
                if (outputData.get(ccrz.ccAPICategory.CATEGORYLIST) != null) {
                    List<Map<String, Object>> outputCategoryList = (List<Map<String, Object>>) outputData.get(ccrz.ccAPICategory.CATEGORYLIST);
                    categoryId = (String) outputCategoryList[0].get('sfid');
                    if(outputCategoryList[0].get('sfid') != null){
                        ccrz__E_ProductCategory__c prodCat = new ccrz__E_ProductCategory__c();
                        prodCat.ccrz__Product__c = addProductList[0].Id;
                        prodCat.ccrz__Category__c = categoryId;
                        addProductCategoryList.add(prodCat);
                    }
                    if(!addProductCategoryList.isEmpty()){
                        try{
                            insert addProductCategoryList;
                            System.debug('Product Category Inserted Successfully');
                        }
                        catch(DmlException e){
                            System.debug('Product Category insert DML Exception - '+e.getMessage());
                        }
                    }

                }
            } catch (Exception e) {
                System.debug('Category fetch exception: ' + e.getMessage());
            }

            // Get Enterprise pricelist
            Set<String> priceList = new Set<String>();
            String priceListId = '';
            Map<String, Object> inputData = new Map<String, Object>{
                ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
                ccrz.ccApiPriceList.Name => 'Enterprise',
                ccrz.ccApi.SIZING => new Map<String, Object> {ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object>{ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_XL}}
            };

            try{
                Map<String,Object> outputData = ccrz.ccApiPriceList.fetch(inputData);
                if(outputData.get(ccrz.ccApiPriceList.PRICELISTS) != null){
                    Map<String, Map<String,Object>> priceListsMap = (Map<String, Map<String,Object>>) outputData.get(ccrz.ccApiPriceList.PRICELISTS);
                    priceList = priceListsMap.keySet();
                }
            }
            catch(Exception e){
                System.debug('Price List fetch exception: ' + e.getMessage());
            }

            for(String keyItem : priceList){
                priceListId = keyItem;
            }
            
 
            Map<String, Object> inputProductData = new Map<String, Object>{
                ccrz.ccAPIProduct.PRODUCTIDLIST => productIdSet,
                ccrz.ccAPI.API_VERSION => ccrz.ccApi.CURRENT_VERSION
            };
            
            try {
                Map<String, Object> outputData = ccrz.ccAPIProduct.fetch(inputProductData);
                if (outputData.get(ccrz.ccAPIProduct.PRODUCTLIST) != null) {
                    List<Map<String, Object>> outputProductList = (List<Map<String, Object>>) outputData.get(ccrz.ccAPIProduct.PRODUCTLIST);
                    if(outputProductList[0].get('sfid') != null){
                        ccrz__E_PriceListItem__c priceListItem  = new ccrz__E_PriceListItem__c();
                        priceListItem.ccrz__Pricelist__c = priceListId;
                        priceListItem.ccrz__Product__c = addProductList[0].Id;
                        priceListItem.ccrz__Price__c = Decimal.valueOf(listPrice);
                        addPriceListItemList.add(priceListItem);
                    }

                    if(!addPriceListItemList.isEmpty()){
                        try{
                            insert addPriceListItemList;
                            System.debug('Price List Item Inserted Successfully');
                        }
                        catch(DmlException e){
                            System.debug('Price List Item insert DML Exception - '+e.getMessage());
                        }
                    }
                }
            } catch (Exception e) {
                System.debug('Product fetch exception: ' + e.getMessage()); 
            }
            
        }
    }
}