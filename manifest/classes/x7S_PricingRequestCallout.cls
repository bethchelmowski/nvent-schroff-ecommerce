/**
 * Created By ankitSoni on 05/06/2020
 */

public class x7S_PricingRequestCallout {

    /*
     >TCD Headers<
     Client_Id - fc7229a635104f37ad444ba8249bbadc
     Client_Secret - 016AA80E8D6144dca41086bFC826AE45
     URL - https://api-np.nvent.com/wcs-rfq-order-dev/v1/pricing/quote
    */

    private static String clientId = System.Label.Client_Id; //7b404180fbb84b099179a4126a9576ff
    private static String clientSecret = System.Label.Client_Secret; //e241f4aa3eca4b2a8FA67A2FEF5754F8
    private static String baseURL = System.Label.Pricing_URL_TCD;
    //'https://anypoint.mulesoft.com/mocking/api/v1/sources/exchange/assets/0414febf-3fbf-4fe4-97c6-f546f182aa00/e-wcs-quote/1.0.1/m/pricing/quote';
    public static String currencyIsoCode = '';

    public static HttpResponse getPricingInfo(String customerNumber, String countryOfUser, List < x7S_PricingRequest.Items > itemList) {

        /** PricingRequest Wrapper */
        x7S_PricingRequest pr = new x7S_PricingRequest();

        if (itemList.size() > 0) {
            pr.customerNumber = customerNumber;
            pr.countryOfUser = countryOfUser;
            pr.items = itemList;
            pr.items.get(0).components = itemList[0].components;
        }


        x7S_PricingResponse prResponse = new x7S_PricingResponse();
        String bodyString = JSON.serialize(pr);
        System.debug('Request Body - ' + bodyString);

        /** HTTP Callout to Mulesoft 'POST' /pricing/quote */
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(baseURL);
        req.setMethod('POST');
        req.setHeader('client_id', clientId);
        req.setHeader('client_secret', clientSecret);
        req.setHeader('accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setBody(bodyString);

        HttpResponse response = http.send(req);
        Map < String, Object > errorMap = (Map < String, Object > ) JSON.deserializeUntyped(response.getBody());
        Map < String, Object > res1 = new Map < String, Object > (); // to get currencyIsoCode
        String iso = '';
        // Success Response
        if (response.getStatusCode() == 200) {
            System.debug('Success Response Body ->' + response.getBody());
            prResponse = x7S_PricingResponse.parse(response.getBody());

            res1 = (Map < String, Object > ) JSON.deserializeUntyped(response.getBody());
            for (String str: res1.keySet()) {
                if (str == 'items') {
                    List < Object > tmpList = (List < Object > ) res1.get('items');
                    Map < String, Object > temp = (Map < String, Object > ) tmpList[0];
                    if (temp.containsKey('listPrice')) {
                        Map < String, Object > temp1 = (Map < String, Object > ) temp.get('listPrice');
                        if (temp1.get('currency') != null) {
                            iso = String.valueOf(temp1.get('currency'));
                            prResponse.items.get(0).listPrice.currencyCode = iso; // parse CurrencyISOCode explicitly
                        }
                    }
                }
            }
            System.debug('ISO - ' + iso);
            System.debug('Pricing Response Wrapper - ' + prResponse); // API response in form of Wrapper
        }
        // Error Responses from API Callout
        else {
            System.debug('HTTP Status : ' + response.getStatus() +
                ' HTTP Status code : ' + response.getStatusCode() +
                ' Message : ' + errorMap.get('message'));
        }


        return response;
    }

    /** onClick for 'Add To Cart'button from Acquia*/
    // req -> customerNumber , countryOfUser , quantity , sku (List)
    private static String addSkuToCart(HttpRequest req) {
        String customerNumber = '7000021793'; // HardCoded .. will get it from Acquia
        String countryOfUser = '';
        String addToCartMessage = '';

        x7S_PricingResponse prResponse = new x7S_PricingResponse();
        List < ccrz__E_Cart__c > newCartList = new List < ccrz__E_Cart__c > ();
        List < ccrz__E_CartItem__c > exisitingCartItems = new List < ccrz__E_CartItem__c > ();
        List < ccrz__E_CartItem__c > cartItemList = new List < ccrz__E_CartItem__c > ();
        List < ccrz__E_CartItem__c > minorCartItemList = new List < ccrz__E_CartItem__c > ();
        List < Id > exisitingCartIdList = new List < Id > ();
    
        Map < String, Object > res1 = new Map < String, Object > (); // to get currencyIsoCode

        //Static Item list  as of now 
        List < x7S_PricingRequest.Items > itemList = new List < x7S_PricingRequest.Items > ();
        List < x7S_PricingRequest.Components > componentList = new List < x7S_PricingRequest.Components > ();

        x7S_PricingRequest.Components component0 = new x7S_PricingRequest.Components();
        x7S_PricingRequest.Components component1 = new x7S_PricingRequest.Components();
        component0.itemNumber = '60110456';
        component0.quantity = '1.000';
        component0.unitOfMeasure = '';

        component1.itemNumber = '10630009';
        component1.quantity = '10.000';
        component1.unitOfMeasure = '';

        componentList.add(component0);
        componentList.add(component1);

        x7S_PricingRequest.Items item = new x7S_PricingRequest.Items();
        item.itemNumber = 'NOVASTAR';
        item.quantity = '1.000';
        item.unitOfMeasure = '';
        item.components = componentList;
        itemList.add(item);

        /*
        x7S_PricingRequest.Components component0 = new x7S_PricingRequest.Components();
        x7S_PricingRequest.Components component1 = new x7S_PricingRequest.Components();
        component0.itemNumber = '64560001';
        component0.quantity = '2.000';
        component0.unitOfMeasure = '';

        component1.itemNumber = '34561752';
        component1.quantity = '1.000';
        component1.unitOfMeasure = '';

        componentList.add(component0);
        componentList.add(component1);

        x7S_PricingRequest.Items item = new x7S_PricingRequest.Items();
        item.itemNumber = '11527789';
        item.quantity = '1.000';
        item.unitOfMeasure = '';
        item.components = componentList;
        itemList.add(item);
*/
        //get pricing info 
        HttpResponse pricingResponse = getPricingInfo(customerNumber, countryOfUser, itemList);

        if (pricingResponse.getStatusCode() == 200) {
            prResponse = x7S_PricingResponse.parse(pricingResponse.getBody());

            res1 = (Map < String, Object > ) JSON.deserializeUntyped(pricingResponse.getBody());
            for (String str: res1.keySet()) {
                if (str == 'items') {
                    List < Object > tmpList = (List < Object > ) res1.get('items');
                    Map < String, Object > temp = (Map < String, Object > ) tmpList[0];
                    if (temp.containsKey('listPrice')) {
                        Map < String, Object > temp1 = (Map < String, Object > ) temp.get('listPrice');
                        System.debug('Currency ISO Code - ' + temp1.get('currency'));
                        if (temp1.get('currency') != null) {
                            currencyIsoCode = String.valueOf(temp1.get('currency')); // Get CurrencyISOCode
                        }
                    }
                }
            }
        } else {
            System.debug('HTTP error code : ' + pricingResponse.getStatusCode() + ' HTTP error status : ' + pricingResponse.getStatus());
        }

        if (prResponse != null) {

            System.debug('Current user - ' + UserInfo.getUserId());
            List < ccrz__E_Cart__c > activeCartList = [SELECT Id FROM ccrz__E_Cart__c WHERE ccrz__User__c =: UserInfo.getUserId() AND ccrz__CartStatus__c = 'Open' AND ccrz__ActiveCart__c = true];
            for (ccrz__E_Cart__c cart: activeCartList) {
                exisitingCartIdList.add(cart.Id);
            }
            System.debug('exisitingCartIdList' + exisitingCartIdList);

            if (!exisitingCartIdList.isEmpty()) {
                Set < Id > cartItems = new Set < Id > ();
                exisitingCartItems = [SELECT Id, ccrz__Cart__c FROM ccrz__E_CartItem__c WHERE ccrz__Cart__c =: exisitingCartIdList];
                if (!exisitingCartItems.isEmpty()) {
                    try {
                        delete exisitingCartItems;
                    } catch (DmlException e) {
                        System.debug('Delete existing items DML Exception -' + e.getMessage());
                    }
                }
            } else {
                // Add a new Cart
                ccrz__E_Cart__c newCart = new ccrz__E_Cart__c();
                newCart.ccrz__CartType__c = 'Cart';
                newCart.ccrz__CartStatus__c = 'Open';
                newCart.ccrz__CurrencyISOCode__c = currencyIsoCode;
                newCart.ccrz__User__c = UserInfo.getUserId();
                newCartList.add(newCart);
                if (!newCartList.isEmpty()) {
                    try {
                        insert newCartList;
                    } catch (DmlException e) {
                        System.debug('Insert new cart DML Exception -' + e.getMessage());
                    }
                }
            }

            for (x7S_PricingResponse.Items items: prResponse.items) {
                System.debug('addSkuToCart item - ' + items);

                /** Add a Cart Item */
                ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c();
                cartItem.ccrz__Cart__c = (exisitingCartIdList.size() > 0) ? exisitingCartIdList[0] : newCartList[0].Id;
                cartItem.ccrz__Quantity__c = Double.valueOf(items.quantity);
                cartItem.ccrz__UnitOfMeasure__c = items.unitOfMeasure;
                cartItem.ccrz__OriginalItemPrice__c = Decimal.valueOf(items.listPrice.value);
                cartItem.ccrz__Price__c = Decimal.valueOf(items.netPrice.value);
                cartItem.CurrencyIsoCode = currencyIsoCode;
                cartItem.ccrz__AbsoluteDiscount__c = Decimal.valueOf(items.discount);
                if (items.isAvailableFlag == 'false') {
                    cartItem.ccrz__ItemStatus__c = 'Invalid';
                } else if (items.isAvailableFlag == 'true') {
                    cartItem.ccrz__ItemStatus__c = 'Available';
                } else {
                    cartItem.ccrz__ItemStatus__c = '';
                }

                cartItemList.add(cartItem);
            }

            if (!cartItemList.isEmpty()) {
                try {
                    insert cartItemList;
                    addToCartMessage = 'The item(s) was successfully added to the cart.';
                    System.debug(addToCartMessage);
                } catch (DmlException e) {
                    addToCartMessage = 'The item(s) was not successfully added to the cart.';
                    System.debug('DML Exception -' + e.getMessage());
                }
            }


            for (x7S_PricingResponse.Items items: prResponse.items) {
                if (items.components != null) {
                    for (x7S_PricingResponse.Components comp: items.components) {
                        System.debug('addSkuToCart Component - ' + comp);

                        // Add a component (minor) item for the above major Cart item
                        ccrz__E_CartItem__c minorCartItem = new ccrz__E_CartItem__c();
                        minorCartItem.ccrz__Cart__c = (exisitingCartIdList.size() > 0) ? exisitingCartIdList[0] : newCartList[0].Id;
                        minorCartItem.ccrz__cartItemType__c = 'Minor';
                        minorCartItem.ccrz__ParentCartItem__c = cartItemList[0].Id;
                        minorCartItem.ccrz__Quantity__c = Double.valueOf(comp.quantity);
                        minorCartItem.ccrz__UnitOfMeasure__c = comp.unitOfMeasure;
                        minorCartItem.ccrz__OriginalItemPrice__c = Decimal.valueOf(comp.listPrice.value);
                        minorCartItem.ccrz__Price__c = Decimal.valueOf(comp.netPrice.value);
                        minorCartItem.CurrencyIsoCode = currencyIsoCode;
                        minorCartItem.ccrz__AbsoluteDiscount__c = Decimal.valueOf(comp.discount);
                        if (items.isAvailableFlag == 'false') {
                            minorCartItem.ccrz__ItemStatus__c = 'Invalid';
                        } else if (items.isAvailableFlag == 'true') {
                            minorCartItem.ccrz__ItemStatus__c = 'Available';
                        } else {
                            minorCartItem.ccrz__ItemStatus__c = '';
                        }
                        minorCartItemList.add(minorCartItem);
                    }
                }
            }
            if (!minorCartItemList.isEmpty()) {
                try {
                    insert minorCartItemList;
                    System.debug('The Component item(s) was successfully added to the cart.');
                } catch (DmlException e) {
                    System.debug('The Component item(s) was not successfully added to the cart.');
                    System.debug('DML Exception -' + e.getMessage());
                }

            }

        }

        return addToCartMessage;
    }

    /** Temporary method call to  addSkuToCart() until implementation of 'Add to Cart' button */
    public static String addSkuToCartCall() {

        HttpRequest req = new HttpRequest();
        req.setEndpoint(baseURL);
        req.setMethod('POST');
        req.setHeader('client_id', clientId);
        req.setHeader('client_secret', clientSecret);
        req.setHeader('accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');

        /** trigger addSkuToCart() */

        String addToCartMessage = addSkuToCart(req);

        return addToCartMessage;
    }
}