/**
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

@IsTest
private class x7sProductActionSet_t
{
	static testMethod void testActionModel()
	{
		x7sProductActionModel model = new x7sProductActionModel();
		System.assertNotEquals(null, model);
		model.Dump('testActionModel');
	}

	static testMethod void testActionSet()
	{
		x7sProductActionSet actionSet = new x7sProductActionSet();
		System.assertNotEquals(null, actionSet);

		x7sProductActionModel model = new x7sProductActionModel();
		model.name       = 'testModel1';
		model.label      = 'testLabel1';
		model.actionType = 'Flow';
		actionSet.items.add(model);

		model = new x7sProductActionModel();
		model.name       = 'testModel2';
		model.label      = 'testLabel2';
		model.actionType = 'URL';
		actionSet.items.add(model);

		System.assertEquals(2, actionSet.items.size());
		actionSet.Dump('testActionList');
	}
}