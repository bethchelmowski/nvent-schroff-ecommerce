global class AccountCustomIterable implements iterable<MergeDuplicateAccountIterable.accountTableList>{
   global Iterator<MergeDuplicateAccountIterable.accountTableList> Iterator(){
      return new MergeDuplicateAccountIterable();
   }
}