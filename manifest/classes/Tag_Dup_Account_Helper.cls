//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: Tag_Dup_Account_Helper	                                                                                    //
// 	Purpose: Helper Class for Batch Class Tag_Dup_Account_Batch																//						                        //
//  Created By: Tredence																									//			
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//


public class Tag_Dup_Account_Helper 
{
    //Method for Updating Fields in Account which are tagged as duplicate
    public static List<Account> UpdateAccounts(List<Tag_Dup_Account_Iterable.DuplicatesList> records,map<Id,Account> accountsmap)
    {
        List<Account> accounts=new List<Account>();
        
        for(Tag_Dup_Account_Iterable.DuplicatesList dup: records)
        {
            Account acc=accountsmap.get(dup.dupAccountId);
            Account mas=accountsmap.get(dup.MasterAccounttId);
            if(acc.IsPartner==False && mas.IsPartner==False)
            {
            string Nametoupdate=acc.Name;
            acc.Name='Dup['+Nametoupdate+']';
            acc.DeDuped_on__c=Date.today();
            acc.IsDuplicate__c=True;
            acc.Master_Account__c=dup.MasterAccounttId;
            accounts.add(acc);
            }
        }
        return accounts;
    }
    
    //Method for sending notification email about records processed
    public static void Email_StatusOfbatch(string finalstr,integer passed,integer failed, integer total)
    {
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= 'DebugLogs.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        
        
        Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
        string[] toAddress=new string[]{'pranshu.agarwal@tredence.com','arun.kumar@tredence.com','nvent_sfdc@tredence.com'};
        mail.setToAddresses(toAddress);
        mail.setSubject('Status of records processed');
        mail.setPlainTextBody('Hello All, \n\nScheduled Account De-Duplication is completed. \n Total records processed ' + total + ' with '+ failed +' records failed and ' + passed +' records passed. \n\nRegards\nnVent SFDC');
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        
    }
}