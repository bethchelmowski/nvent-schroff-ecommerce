@isTest(SeeAllData=true)
public class test_opportunity_contact_required {

	public static testMethod void TestOpportunityRequired1()
    {   
           //create account
            Account Acc = new Account(
            	Name='Testing',
                ShippingCountryCode = 'US',
            	BillingCity = 'Example',
            	BillingStreet = '1111 Example',
            	BillingCountryCode = 'US');
        	insert Acc;
        
            Contact newContact = new Contact(
            	firstname = 'Test First',
            	lastname = 'Test Last Name',
            	AccountId = Acc.Id
            );
        	Insert newContact;      
        
            //create oppty                          
            Opportunity newOpportunity = new Opportunity
            (
				Name='nick_test',
                StageName='Establish',
                First_Revenue_Date__c = Date.today(),
                AccountId = Acc.Id, 
                Sales_Team__c = 'BIS', 
                Probability = 25,
                CurrencyIsoCode = 'USD',
                Primary_Contact__c = null,
                Amount = 5000, 
                CloseDate=Date.today()
            );
            
            insert newOpportunity;
        
        	OpportunityContactRole ocr = new OpportunityContactRole(
            	Role='Business User',
                OpportunityId = newOpportunity.id,
                ContactId = newContact.id,
                IsPrimary=True
            );
        	insert ocr;
            
        	//Update to another stage
      		newOpportunity.StageName='Establish';     
        	newOpportunity.Primary_Contact__c = newContact.Id;
        
         	map<Id, Opportunity> oppy_map = new map<Id, Opportunity>();
            oppy_map.put(newOpportunity.Id,newOpportunity);

			Test.startTest();

			try {	
			    update newOpportunity;			    
			    Opportunity sampleTest = [Select Id, Contact_Required__c, Primary_Contact__c From Opportunity where Id = :newOpportunity.id];
			    
                System.assert([SELECT count() FROM Opportunity WHERE Id IN :oppy_map.keySet()] == 1);
                
			
			} 
        	catch(System.DmlException e) 
            {		
			    System.assert(e.getMessage().contains('No Primary Contact Exists.'));			
			}

    		Test.stopTest();
    }
}