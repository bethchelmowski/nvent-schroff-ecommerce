public with sharing class Pentair_RFQConvertController extends PageControllerBase {
    // This is the lead that is to be converted
    public  Lead leadToConvert {get; set;}
    
    // Constructor for this controller
    public Pentair_RFQConvertController(ApexPages.StandardController stdController) 
    {
        //get the ID to query for the Lead fields
        Id leadId = stdController.getId();
        
        leadToConvert = [SELECT 
                         	Id, Status, OwnerId, Name, Company, 
                         	Sales_Team__c, CurrencyIsoCode, Web_Comments__c,
                         	PostalCode, Street, State, CountryCode, City
                         FROM Lead WHERE Id = :leadId];
    }
    
    public Pentair_RFQConvertCoreController myComponentController { get; set; }

    public override void setComponentController(ComponentControllerBase compController) 
    {
        myComponentController = (Pentair_RFQConvertCoreController)compController;
    }

	/*
  		These are the get methods which override the methods in PageControllerBase.
  		
  		If you add new custom components, a new overridden get method must be added here.
  	*/
    public override ComponentControllerBase getMyComponentController() {

        return myComponentController;
    }   
    
    // This method is called when the user clicks the Convert button on the VF Page
    public PageReference convertLead() {
		
		// This is the lead convert object that will convert the lead 
        Database.LeadConvert leadConvert = new database.LeadConvert();
        
        System.debug('City: ' + leadToConvert.City);
        System.debug('Street: ' + leadToConvert.Street);
        System.debug('CountryCode: ' + leadToConvert.CountryCode);
        
        if ((leadToConvert.City == null || leadToConvert.Street == null || leadToConvert.CountryCode == null) &&
           myComponentController.selectedAccount == 'NEW')
        {
        	PrintError('A Country, City, and Street Address are required on the lead record.');
            return null;
        }
        
        // if Lead Status is not entered show an error  
        if (myComponentController != null && myComponentController.leadConvert.Status == 'NONE'){
            
            PrintError('Please select a Lead Status.');
            return null;
            
        } 
        
        if (leadToConvert.Sales_Team__c == 'CADDY' || leadToConvert.Sales_Team__c == 'ERICO' ||
           leadToConvert.Sales_Team__c == 'ERIFLEX' || leadToConvert.Sales_Team__c == 'LENTON')
        {
            PrintError('The Sales Team you selected on the lead record is not setup for RFQs please change the Sales Team');
            return null;
        }
        
        //set lead ID
        leadConvert.setLeadId(leadToConvert.Id);    
        
        //if the main lead convert component is not set then return
        if (myComponentController == NULL) return null;
        
        //if the Account is not set, then show an error
        if (myComponentController.selectedAccount == 'NONE')
        {
            PrintError('Please select an Account.');
            return null;
            
        }
        
        // otherwise set the account id
        else if (myComponentController != NULL && myComponentController.selectedAccount != 'NEW') {
            leadConvert.setAccountId(myComponentController.selectedAccount);
        }
        
        //set the lead convert status
        leadConvert.setConvertedStatus(myComponentController.leadConvert.Status);
        
        //set the variable to create or not create an opportunity
        leadConvert.setDoNotCreateOpportunity(true);
        
        //set the owner id
        leadConvert.setOwnerId(myComponentController.contactId.ownerID);
        
        //set whether to have a notification email
        leadConvert.setSendNotificationEmail(myComponentController.sendOwnerEmail);
        
        system.debug('leadConvert --> ' + leadConvert);
        
        //convert the lead
        Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
        
        // if the lead converting was a success then create a RFQ
        if (leadConvertResult.success)
        {
        	Id recordTypeRFQId = [select Id from RecordType where DeveloperName = 'RFQ' limit 1].Id;
        	Id recordTypeP3Id = [select Id from RecordType where DeveloperName = 'P3' limit 1].Id;
            
			//Create RFQ Record
            RFQ__C newRFQRecord = new RFQ__c();
            
            if (leadToConvert.Sales_Team__c == 'Hoffman' || leadToConvert.Sales_Team__c == 'Schroff')
            	newRFQRecord.RecordTypeId = recordTypeRFQId;
            else
                newRFQRecord.RecordTypeId = recordTypeP3Id;
            
            newRFQRecord.Contact__c = leadConvertResult.getContactId();
            newRFQRecord.Account__c = leadConvertResult.getAccountId();
            newRFQRecord.Name = myComponentController.rfqID.Name;
            newRFQRecord.Contact__c = myComponentController.rfqID.Contact__c;
            newRFQRecord.RFQ_Received__c = Date.Today();
            newRFQRecord.Requested_By__c = Date.Today().AddDays(1);
            newRFQRecord.Handled_By__c = 'Customer Care';
            newRFQRecord.Description__c = leadToConvert.Web_Comments__c;
            newRFQRecord.Campaign__c = LeadCampain(leadToConvert.Id);
            newRFQRecord.Sales_Team__c = leadToConvert.Sales_Team__c;
            newRFQRecord.CurrencyIsoCode = leadToConvert.CurrencyIsoCode;          
            
            insert newRFQRecord;

            // redirect the user to the newly created Account
            PageReference pageRef = new PageReference('/' + newRFQRecord.Id);
            
            pageRef.setRedirect(true);
            
            return pageRef; 

        }
        else
        {

            //if converting was unsucessful, print the errors to the pageMessages and return null
            System.Debug(leadConvertResult.errors);

            PrintErrors(leadConvertResult.errors);
            
            return null;
        }
        
        return null;

    }
    
    public String LeadCampain(ID LeadId)
    {
        List<CampaignMember> leadCampaign = [SELECT CampaignId FROM CampaignMember WHERE LeadId = :LeadId ORDER BY CreatedDate DESC];
        
        if (leadCampaign.size() > 0)
            return leadCampaign[0].CampaignId;
        else
        	return null;
    }
  	
  	//this method will take database errors and print them to the PageMessages 
    public void PrintErrors(Database.Error[] errors)
    {
        for(Database.Error error : errors)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, error.message);
            ApexPages.addMessage(msg);
        }
    }
    
    //This method will put an error into the PageMessages on the page
    public void PrintError(string error) {
        ApexPages.Message msg = new 
            ApexPages.Message(ApexPages.Severity.ERROR, error);
        ApexPages.addMessage(msg);
    } 
}