/* ********************************************
Developer -     Apurva Prasade
Class -         nVent_OpportunityTriggerTest 
Description -   Test class to cover Opportunity trigger

Developer   Date        Version
Apurva P    07-08-2019  V1.0

********************************************** */

@isTest
public class nVent_OpportunityTriggerTest 
{
    @TestSetup
    static void createData(){

        Account objAccount = new Account();
        objAccount.Name = 'testCompany';
        objAccount.Subvertical__c = 'Oil and Gas';
        objAccount.BillingCountryCode = 'FR';
        objAccount.BillingStreet = 'Arnstadt';
        objAccount.BillingCity = 'Bierweg 2';
        insert objAccount;

        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware');
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();

        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;

        Opportunity objOpp1 = new Opportunity();
        objOpp1.Name = 'TestOpp';
        objOpp1.AccountId = objAccount.Id;
        objOpp1.Amount = 8000;
        objOpp1.PriceBook2 = customPB;
        objOpp1.CloseDate = Date.today()+30;
        objOpp1.StageName = 'Establish';
        objOpp1.Sales_Team__c = 'CADDY';
        objOpp1.Opportunity_Subvertical_1__c = 'Oil and Gas';
        insert objOpp1;
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = objOpp1.Id;
        oli.Product2Id = prod.Id;
        oli.Quantity = 1;
        oli.pricebookentryId = customPrice.Id;
        //oli.TotalPrice = 500;
        //oli.ListPrice = 50;
        oli.UnitPrice = 90;
        insert oli;

    }

    public static testMethod void testafterTrigger()
    {	
        Opportunity opp1 = [Select Id, Opportunity_Subvertical_1__c,StageName from Opportunity Limit 1];
        OpportunityLineItem oli = [Select Id from OpportunityLineItem Where OpportunityId =: opp1.Id ];
        opp1.StageName = 'Closed Won';
        update opp1;
        list<Account> listAcc = [Select Id,Closed_Won_Subverticals__c from Account];
        System.assertEquals(True, listAcc[0].Closed_Won_Subverticals__c != null);
    }
}