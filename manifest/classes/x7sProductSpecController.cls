/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/3/19.
 */

public with sharing class x7sProductSpecController
{
	@AuraEnabled (Cacheable=true)
	public static x7sProductSpecCollection getProductSpecs(String categoryIds)
	{
		x7sProductSpecCollection specCollection = new x7sProductSpecCollection();

		if (String.isBlank(categoryIds))
		{
			List<x7sProductCategory> categories = x7sProductCategoryController.getCategories('');
			categoryIds = categories[0].id;
		}
		List<String> productIdList = getProductIds(categoryIds);

		System.debug('x7sProductSpecCollection getProductSpecs');
		System.debug('Product ids: ' + productIdList);

		Map<String, Object> indicesData = new Map<String, Object>
		{
			ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccApi.SIZING => new Map<String, Object> {ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object> {ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_M}},
			ccrz.ccAPISpec.USE_FOR_FILTER => true,
			ccrz.ccAPISpec.PRODUCT_IDS => productIdList
		};

		List<Map<String, String>> specValues = new List<Map<String, String>>();
		Set<String>               specIdList = new Set<String>();

		Map<String, Object> IdxOutputData = ccrz.ccAPISpec.fetchProductSpecIndices(indicesData);
		if (IdxOutputData.get(ccrz.ccAPISpec.PRODUCT_SPEC_INDICES) != null)
		{
			// get all the spec ids - 'spec' in the map
			Map<String, Object> idxSpecList = (Map<String, Object>) IdxOutputData.get(ccrz.ccAPISpec.PRODUCT_SPEC_INDICES);
			System.debug('Spec indices: ' + idxSpecList);

			if (idxSpecList != null)
			{
				for (String keyString : idxSpecList.keySet())
				{
					Map<String, Object> spec = (Map<String, Object>) idxSpecList.get(keyString);
					if (spec != null)
					{
						String specId = (String) spec.get('spec');
						specIdList.add(specId);
						specValues.add(new Map<String, String> {specId => (String) spec.get('specValue')});
					}
				}
			}

			//specValues.sort();

			System.debug('Spec IDs   : ' + specIdList);
			System.debug('Spec Values: ' + specValues);

			Map<String, Object> inputData = new Map<String, Object>
			{
				ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
				ccrz.ccAPISpec.SPECIDLIST => specIdList,
				ccrz.ccApi.SIZING => new Map<String, Object> {ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object> {ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_M}}
			};

			Map<String, Object> outputData = ccrz.ccAPISpec.fetch(inputData);
			System.debug(outputData.get(ccrz.ccApi.SUCCESS));
			if (outputData.get(ccrz.ccAPISpec.SPECLIST) != null)
			{
				List<Map<String, Object>> specList = (List<Map<String, Object>>) outputData.get(ccrz.ccAPISpec.SPECLIST);
				System.debug('Specs: ' + specList);

				for (Map<String, Object> spec : specList)
				{
					if (spec != null)
					{
						String specIdxId = (String) spec.get('sfid');
						String specGroup = (String) spec.get('specGroup');

						if (String.isNotBlank(specIdxId) && String.isNotBlank(specGroup))
						{
							specCollection.addSpec(
								specGroup,
								new x7sProductSpec(
									specIdxId,
									(String) spec.get('displayName'),
									(String) spec.get('filterType'),
									getSpecValues(specValues, specIdxId)
								)
							);
						}
					}
				}
			}

		}
		specCollection.sortSpecs();

		return specCollection;
	}

	private static List<String> getSpecValues(List<Map<String, String>> specValues, String specId)
	{
		Set<String> values = new Set<String>();

		for(Map<String, String> item : specValues)
		{
			if (item != null && item.containsKey(specId))
			{
				values.add(item.get(specId));
			}
		}

		List<String> valueList = new List<String>(values);
		valueList.sort();

		return valueList;
	}

	@TestVisible
	private static List<String> getProductIds(String categoryIds)
	{
		if (categoryIds == null) { categoryIds = '';}

		List<String> productIds = new List<String>();
		Map<String, Object> inputData = new Map<String, Object>
		{
			ccrz.ccApi.API_VERSION => ccrz.ccApi.CURRENT_VERSION,
			ccrz.ccAPIProduct.CATEGORY_IDS => new List<String> {categoryIds},
			ccrz.ccAPIProduct.INCLUDE_COUNT => true,
			ccrz.ccApi.SIZING => new Map<String, Object> {ccrz.ccAPIProduct.ENTITYNAME => new Map<String, Object> {ccrz.ccApi.SZ_DATA => ccrz.ccApi.SZ_S}}
		};

		Map<String, Object> outputData = ccrz.ccAPIProduct.find(inputData);

		if (outputData.get(ccrz.ccAPIProduct.PRODUCTLIST) != null)
		{
			System.debug('Count: ' + outputData.get(ccrz.ccAPIProduct.COUNT));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) outputData.get(ccrz.ccAPIProduct.PRODUCTLIST);
			System.debug(outputList);

			for (Map<String, Object> productEntry : outputList)
			{
				if (productEntry != null)
				{
					productIds.add((String) productEntry.get('sfid'));
				}
			}
		}
		System.debug('list size: ' + productIds.size());
		System.debug('product Ids' + productIds);

		return productIds;
	}
}