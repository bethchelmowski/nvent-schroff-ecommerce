/**
* Created By ankitSoni on 05/27/2020
*/
public with sharing class x7S_CartItemModel {
    
    @AuraEnabled
	public String id, productId, productName, productSkuCode;
    
    @AuraEnabled
	public List<String>imageUrl;

	@AuraEnabled
    public Decimal listPrice, yourPrice, quantity, subTotal;
    
}