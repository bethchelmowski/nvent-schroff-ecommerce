public class MergeDuplicateAccounts {
    
    public List<reParentAccountProcessResultWrap> reParentAccountChild(Map<Id, Id> allAccountKeyIds) {
        
        List<reParentAccountProcessResultWrap> reParentAccountProcessMasterResult = new List<reParentAccountProcessResultWrap>();
        
        
        if(!allAccountKeyIds.keyset().isEmpty() && !allAccountKeyIds.values().isEmpty()) {
        
            //Savepoint sp = Database.setSavepoint();
            system.debug('----------- Values in allAccountKeyIds'+allAccountKeyIds);
            updateAccntMerge(allAccountKeyIds);
            reParentAccountProcessMasterResult.addall(reParentAcntConnList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntConnList:' + reParentAccountProcessMasterResult.size());
            reParentAccountProcessMasterResult.addall(reParentAcntPlanList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntPlanList:' + reParentAccountProcessMasterResult.size());
            reParentAccountProcessMasterResult.addall(reParentAcntRoleList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntRoleList:' + reParentAccountProcessMasterResult.size());
            reParentAccountProcessMasterResult.addall(reParentAcntTeammemberList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntTeammemberList:' + reParentAccountProcessMasterResult.size());
            reParentAccountProcessMasterResult.addall(reParentAcntCaseList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntCaseList:' + reParentAccountProcessMasterResult.size());
            reParentAccountProcessMasterResult.addall(reParentAcntCertificateList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntCertificateList:' + reParentAccountProcessMasterResult.size());
            reParentAccountProcessMasterResult.addall(reParentAcntContactList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntContactList:' + reParentAccountProcessMasterResult.size());
            reParentAccountProcessMasterResult.addall(reParentAcntEventList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntEventList:' + reParentAccountProcessMasterResult.size());
            //reParentAccountProcessMasterResult.addall(reParentAcntNoteList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntNoteList:' + reParentAccountProcessMasterResult.size());
            reParentAccountProcessMasterResult.addall(reParentAcntOppList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntOppList:' + reParentAccountProcessMasterResult.size());
            reParentAccountProcessMasterResult.addall(reParentAcntRFQList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntRFQList:' + reParentAccountProcessMasterResult.size());
            reParentAccountProcessMasterResult.addall(reParentAcntTaskList(allAccountKeyIds));
                system.debug('** Result Size reParentAcntTaskList:' + reParentAccountProcessMasterResult.size());
            //reParentAccountProcessMasterResult.addall(reParentAcntContentDocumentLinkList(allAccountKeyIds));
            	system.debug('** Result Size reParentAcntContentDocumentLinkList:' + reParentAccountProcessMasterResult.size());
            reParentAccountProcessMasterResult.addall(prefixDuplicatenotation(allAccountKeyIds));
            	system.debug('** Result Size prefixDuplicatenotation:' + reParentAccountProcessMasterResult.size());
            //updateAccntMerge(allAccountKeyIds);
            
            //Database.rollback(sp); 
        }
        

        
        return reParentAccountProcessMasterResult;   
    }

    public List<reParentAccountProcessResultWrap> reParentAcntConnList(Map<id, id> allAccountKeyIds) {
    
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<Account_Connection__c> updateList = new List<Account_Connection__c>();
        
        for(Account_Connection__c At : [select id,Connected_from_Account__c,Connected_to_Account__c from Account_Connection__c where Connected_from_Account__c in : allAccountKeyIds.Keyset() OR Connected_to_Account__c in : allAccountKeyIds.Keyset()])
        {  		
            	boolean addtoList = False;
            	Account_Connection__c A = new Account_Connection__c(id = At.Id);
            	if(string.isNotBlank(allAccountKeyIds.get(At.Connected_from_Account__c))) 
                {
                	A.Connected_from_Account__c = allAccountKeyIds.get(At.Connected_from_Account__c);
                    childProjectIdMap.put(At.Id, At.Connected_from_Account__c);
                    addtoList = true;
                }
            	if(string.isNotBlank(allAccountKeyIds.get(At.Connected_to_Account__c)))
                {
                    A.Connected_to_Account__c = allAccountKeyIds.get(At.Connected_to_Account__c);
                    childProjectIdMap.put(At.Id, At.Connected_to_Account__c);
                    addtoList = true;
                }
            	if(addtoList)
            	{
                	updateList.add(A);
            	}
                
            
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to update in Account_Connection__c'+updateList.size());
            Database.SaveResult[] srList = Database.update(updateList, false);
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    System.debug('**Error updating record. Errors' + JSON.serialize(srList[i].getErrors()));
                    System.debug('**Error updating record. Dup Project' + childProjectIdMap.get(updateList[i].Id));
                    System.debug('**Error updating record. Master Project' + allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)));
                    System.debug('**Error updating record. Object Name' + updateList[i].Id.getSObjectType().getDescribe().getName());
                    
                    reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntPlanList(Map<id, id> allAccountKeyIds) {
    
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<Account_Plan__c> updateList = new List<Account_Plan__c>();
        
        for(Account_Plan__c At : [select id,Account__c from Account_Plan__c where Account__c in : allAccountKeyIds.Keyset()])
        {  
            if(string.isNotBlank(allAccountKeyIds.get(At.Account__c))) {
                Account_Plan__c A = new Account_Plan__c(id = At.Id);
                A.Account__c = allAccountKeyIds.get(At.Account__c);
                childProjectIdMap.put(At.Id, At.Account__c);
                updateList.add(A);

            }
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to update in Account_Plan__c'+updateList.size());
            Database.SaveResult[] srList = Database.update(updateList, false);
            
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    System.debug('**Error updating record. Errors' + JSON.serialize(srList[i].getErrors()));
                    System.debug('**Error updating record. Dup Project' + childProjectIdMap.get(updateList[i].Id));
                    System.debug('**Error updating record. Master Project' + allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)));
                    System.debug('**Error updating record. Object Name' + updateList[i].Id.getSObjectType().getDescribe().getName());
                    
                    reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntRoleList(Map<id, id> allAccountKeyIds) {
    
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<Account_Role__c> updateList = new List<Account_Role__c>();
        
        for(Account_Role__c At : [select id,Account__c from Account_Role__c where Account__c in : allAccountKeyIds.Keyset()])
        {  
            if(string.isNotBlank(allAccountKeyIds.get(At.Account__c))) {
                Account_Role__c a = new Account_Role__c(id = At.Id);
                a.Account__c = allAccountKeyIds.get(At.Account__c);
                childProjectIdMap.put(At.Id, At.Account__c);
                updateList.add(a);
            }
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to update in Account_Role__c'+updateList.size());
            Database.SaveResult[] srList = Database.update(updateList, false);
            
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    System.debug('**Error updating record. Errors' + JSON.serialize(srList[i].getErrors()));
                    System.debug('**Error updating record. Dup Project' + childProjectIdMap.get(updateList[i].Id));
                    System.debug('**Error updating record. Master Project' + allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)));
                    System.debug('**Error updating record. Object Name' + updateList[i].Id.getSObjectType().getDescribe().getName());
                    
                    reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntTeammemberList(Map<id, id> allAccountKeyIds) {
    
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<AccountTeamMember> updateList = new List<AccountTeamMember>();
        
        for(AccountTeamMember At : [select id,AccountId,AccountAccessLevel,CaseAccessLevel,OpportunityAccessLevel,PhotoUrl,TeamMemberRole,Title,UserId from AccountTeamMember where AccountId in : allAccountKeyIds.Keyset()])
        {  
            if(string.isNotBlank(allAccountKeyIds.get(At.AccountId))) {
                //AccountTeamMember A = new AccountTeamMember(id = At.Id);
                AccountTeamMember A = new AccountTeamMember();
                A.AccountId = allAccountKeyIds.get(At.AccountId);
                A.AccountAccessLevel = At.AccountAccessLevel;
                A.CaseAccessLevel = At.CaseAccessLevel;
                A.OpportunityAccessLevel = At.OpportunityAccessLevel;
                A.TeamMemberRole = At.TeamMemberRole;
                A.UserId = At.UserId;
                childProjectIdMap.put(At.Id, At.AccountId);
                updateList.add(A);
            }
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to insert in AccountTeamMember'+updateList.size());
            Database.SaveResult[] srList = Database.insert(updateList, false);
            
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    try{
                    //reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                    reParentAcntProcessResult.add(new reParentAccountProcessResultWrap('','','Account Team Member','', JSON.serialize(srList[i].getErrors()),',\n'));
                    }
                    catch(exception ex)
                    {
                        system.debug('$$$$$$$ ------- The error is:'+ex);
                        System.debug('$$$$$$$ ------ The following error has occurred.'+srList[i].getErrors());                    
                    }
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntCaseList(Map<id, id> allAccountKeyIds) {
    
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<Case> updateList = new list<Case>();
        
        for(Case pt : [select id,AccountId,Electrical_Contractor_Account__c,General_Contractor_Account__c,Service_Partner__c,Tile_Sanitary_Contractor_Account__c from Case where (AccountId in : allAccountKeyIds.Keyset() OR Electrical_Contractor_Account__c in : allAccountKeyIds.Keyset() OR General_Contractor_Account__c in : allAccountKeyIds.Keyset() OR Service_Partner__c in : allAccountKeyIds.Keyset() OR Tile_Sanitary_Contractor_Account__c in : allAccountKeyIds.Keyset())])
        {  
            Boolean addtoList = false;
            Case p = new Case(id = pt.Id);
            if(string.isNotBlank(allAccountKeyIds.get(pt.AccountId)))
            {
                
                p.AccountId = allAccountKeyIds.get(pt.AccountId);
                childProjectIdMap.put(pt.Id, pt.AccountId);
                addtoList = true;
            }
            if(string.isNotBlank(allAccountKeyIds.get(pt.Electrical_Contractor_Account__c)))
            {
                
                p.Electrical_Contractor_Account__c = allAccountKeyIds.get(pt.Electrical_Contractor_Account__c);
                childProjectIdMap.put(pt.Id, pt.Electrical_Contractor_Account__c);
                addtoList = true;
            }
            if(string.isNotBlank(allAccountKeyIds.get(pt.General_Contractor_Account__c)))
            {
                
                p.General_Contractor_Account__c = allAccountKeyIds.get(pt.General_Contractor_Account__c);
                childProjectIdMap.put(pt.Id, pt.General_Contractor_Account__c);
                addtoList = true;
            }
            if(string.isNotBlank(allAccountKeyIds.get(pt.Service_Partner__c)))
            {
                
                p.Service_Partner__c = allAccountKeyIds.get(pt.Service_Partner__c);
                childProjectIdMap.put(pt.Id, pt.Service_Partner__c);
                addtoList = true;
            }
            if(string.isNotBlank(allAccountKeyIds.get(pt.Tile_Sanitary_Contractor_Account__c)))
            {
                
                p.Tile_Sanitary_Contractor_Account__c = allAccountKeyIds.get(pt.Tile_Sanitary_Contractor_Account__c);
                childProjectIdMap.put(pt.Id, pt.Tile_Sanitary_Contractor_Account__c);
                addtoList = true;
            }
            if(addtoList)
            {
             	updateList.add(p);   
            }
            
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to update in Case'+updateList.size());
            Database.SaveResult[] srList = Database.update(updateList, false);
            
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntCertificateList(Map<id, id> allAccountKeyIds) {
    
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<Certification__c> updateList = new List<Certification__c>();
        
        for(Certification__c At : [select id,Account__c from Certification__c where Account__c in : allAccountKeyIds.Keyset()])
        {  
            if(string.isNotBlank(allAccountKeyIds.get(At.Account__c))) {
                Certification__c A = new Certification__c(id = At.Id);
                A.Account__c = allAccountKeyIds.get(At.Account__c);
                childProjectIdMap.put(At.Id, At.Account__c);
                updateList.add(A);
            }
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to update in Certification__c'+updateList.size());
            Database.SaveResult[] srList = Database.update(updateList, false);
            
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntContactList(Map<id, id> allAccountKeyIds) {
    
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<Contact> updateList = new List<Contact>();
        
        for(Contact At : [select id,AccountId from Contact where AccountId in : allAccountKeyIds.Keyset()])
        {  
            if(string.isNotBlank(allAccountKeyIds.get(At.AccountId))) {
                //Note p = new Note(id = pt.Id);
                Contact A = new Contact(id = At.Id);  
                A.AccountId = allAccountKeyIds.get(At.AccountId);
                childProjectIdMap.put(At.Id, At.AccountId);
                updateList.add(A);
            }
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to update in Contact'+updateList.size());
            Database.SaveResult[] srList = Database.update(updateList, false);
            
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    try{
                        reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                    }catch(exception e) {
                        system.debug('**Exception');
                    }
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntEventList(Map<id, id> allAccountKeyIds) {
        
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<Event> updateList = new List<Event>();
        
        for(Event At : [SELECT id,WhatId FROM Event where WhatId in : allAccountKeyIds.Keyset()])
        {  
            if(string.isNotBlank(allAccountKeyIds.get(At.WhatId))) {
                //Attachment p = new Attachment(id = pt.Id);
                Event A = new Event(id = At.Id);
                A.WhatId = allAccountKeyIds.get(At.WhatId);
                childProjectIdMap.put(At.Id, At.WhatId);
                updateList.add(A);
            }
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to update in event'+updateList.size());
            Database.SaveResult[] srList = Database.update(updateList, false);
            
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    try{
                     reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                    }catch(exception e) {
                        system.debug('**Exception');
                    }
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntNoteList(Map<id, id> allAccountKeyIds) {
        
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        /*Map<id, id> childProjectIdMap = new Map<id, id>();
        List<Note> updateList = new List<Note>();
        
        for(Note At : [select id,parentID,Body,IsPrivate,OwnerId from Note where parentID in : allAccountKeyIds.Keyset()])
        {  
            if(string.isNotBlank(allAccountKeyIds.get(At.parentID))) {
                //Note p = new Note(id = pt.Id);
                Note A = new Note();  
                A.Body = At.Body;
                A.IsPrivate = At.IsPrivate;
                A.OwnerId = At.OwnerId;
                A.parentID = allAccountKeyIds.get(At.parentID);
                childProjectIdMap.put(At.Id, At.parentID);
                updateList.add(A);
            }
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to update in note'+updateList.size());
            Database.SaveResult[] srList = Database.insert(updateList, false);
            
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    //reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                    reParentAcntProcessResult.add(new reParentAccountProcessResultWrap('','','Account Team Member','', JSON.serialize(srList[i].getErrors()),',\n'));
                }
            }
        }   
        */
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntOppList(Map<id, id> allAccountKeyIds) {
        
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<Opportunity> updateList = new List<Opportunity>();
        
        for(Opportunity pt : [select id,AccountId,Lost_to_Competitor__c,PartnerAccountId from Opportunity where AccountId in : allAccountKeyIds.Keyset() OR Lost_to_Competitor__c in : allAccountKeyIds.Keyset() OR PartnerAccountId in : allAccountKeyIds.Keyset()])
        {  
            boolean addtoList = false;
            Opportunity p = new Opportunity(id = pt.Id);
            if(string.isNotBlank(allAccountKeyIds.get(pt.AccountId))) 
            {
                p.AccountId = allAccountKeyIds.get(pt.AccountId);
                childProjectIdMap.put(pt.Id, pt.AccountId);
                addtoList = true;
            }
            if(string.isNotBlank(allAccountKeyIds.get(pt.Lost_to_Competitor__c))) 
            {
                p.Lost_to_Competitor__c = allAccountKeyIds.get(pt.Lost_to_Competitor__c);
                childProjectIdMap.put(pt.Id, pt.Lost_to_Competitor__c);
                addtoList = true;
            }
            /*if(string.isNotBlank(allAccountKeyIds.get(pt.PartnerAccountId))) 
            {
                p.PartnerAccountId = allAccountKeyIds.get(pt.PartnerAccountId);
                childProjectIdMap.put(pt.Id, pt.PartnerAccountId);
                addtoList = true;
            }*/
            if(addtoList)
            {
               updateList.add(p);  
            }
           
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to update in Opportunity'+updateList.size());
            Database.SaveResult[] srList = Database.update(updateList, false);
            
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntRFQList(Map<id, id> allAccountKeyIds) {
    
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<RFQ__c> updateList = new List<RFQ__c>();
        
                
        for(RFQ__c At : [select id,Account__c from RFQ__c where Account__c in : allAccountKeyIds.Keyset()])
        {  
            if(string.isNotBlank(allAccountKeyIds.get(At.Account__c))) {
            
                RFQ__c A = new RFQ__c(id = At.Id);
                A.Account__c = allAccountKeyIds.get(At.Account__c);
                childProjectIdMap.put(At.Id, At.Account__c);
                updateList.add(A);
            }
        }
        if(!updateList.isEmpty()) {
            system.debug('-------- value in the static varable :'+Pentair_RFQController.runOnceRFQTrigger());
			system.debug('----------- FinalLst to update in RFQ__c'+updateList.size());
            //system.debug('-------- value in the static varable :'+Pentair_RFQController.runOnceRFQTrigger());
            Database.SaveResult[] srList = Database.update(updateList, false);
            
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntTaskList(Map<id, id> allAccountKeyIds) {
    
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<Task> updateList = new List<Task>();
        
        for(Task pt : [select id,whatId from Task where whatId in : allAccountKeyIds.Keyset()])
        {  
            if(string.isNotBlank(allAccountKeyIds.get(pt.whatId))) {
                Task p = new Task(id = pt.Id);
                p.whatId = allAccountKeyIds.get(pt.whatId);
                childProjectIdMap.put(pt.Id, pt.whatId);
                updateList.add(p);
            }
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to update in Task'+updateList.size());
            Database.SaveResult[] srList = Database.update(updateList, false);
            
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public List<reParentAccountProcessResultWrap> reParentAcntContentDocumentLinkList(Map<id, id> allAccountKeyIds) {
        
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        Map<id, id> childProjectIdMap = new Map<id, id>();
        List<ContentDocumentLink> updateList = new List<ContentDocumentLink>();
        set<id> accountId = allAccountKeyIds.Keyset();
        for(ContentDocumentLink At : [select id,ContentDocumentId,ShareType,Visibility,LinkedEntityId FROM ContentDocumentLink where LinkedEntityId IN :accountId])
        {  
            if(string.isNotBlank(allAccountKeyIds.get(At.LinkedEntityId))) {
                //Note p = new Note(id = pt.Id);
                //ContentDocumentLink A = new ContentDocumentLink(id = At.Id);
                ContentDocumentLink A = new ContentDocumentLink(); 
                A.LinkedEntityId = allAccountKeyIds.get(At.LinkedEntityId);
                A.ContentDocumentId = At.ContentDocumentId;
                A.ShareType = At.ShareType;
                A.Visibility = At.Visibility;
                childProjectIdMap.put(At.Id, At.LinkedEntityId);
                updateList.add(A);
            }
        }
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to ContentDocumentLink in RFQ__c'+updateList.size());
            Database.SaveResult[] srList = Database.insert(updateList, false);
            system.debug('-----**** srList in document: '+srList);
            system.debug('-----**** updateList in document: '+updateList);
            for (Integer i = 0; i < srList.size(); i++) {
                if (srList[i].isSuccess()) {
                    System.debug('Successfully reParented.' + srList[i].getId());
                }
                else {
                    try{
                    //reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(childProjectIdMap.get(updateList[i].Id), allAccountKeyIds.get(childProjectIdMap.get(updateList[i].Id)), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                    reParentAcntProcessResult.add(new reParentAccountProcessResultWrap('','','Account Team Member','', JSON.serialize(srList[i].getErrors()),',\n'));
                    }
                    catch(exception ex)
                    {
                        system.debug('$$$$$$$ ------- The error is:'+ex);
                        System.debug('$$$$$$$ ------ The following error has occurred.'+srList[i].getErrors());                    
                    }
                }
            }
        }   
        
        return reParentAcntProcessResult;
    }
    
    public void updateAccntMerge(Map<id, id> allAccountKeyIds) 
    {
        Set<id> dupAccountIds = new Set<id>();
		dupAccountIds.addall(allAccountKeyIds.keyset());
		System.debug('******* Inside updateAccntMerge values in allAccountKeyIds'+allAccountKeyIds +'and in dupAccountIds' +dupAccountIds);
		if(!dupAccountIds.isEmpty()) 
		{
			// Query the accounts to lock
			List<Account> accts = new List<Account>([SELECT Id from Account WHERE Id IN :dupAccountIds]);
			// Lock the accounts
			System.debug('******* Inside updateAccntMerge values in accts'+accts);
			if(!accts.isEmpty())
			{
				Approval.LockResult[] lrList = Approval.lock(accts, false);
                System.debug('******* Inside updateAccntMerge Locked values in lrList'+lrList);
				List<Duplicate_Accounts__c> SucessIds = new List<Duplicate_Accounts__c>();
				// Iterate through each returned result
				for(Approval.LockResult lr : lrList) 
				{
					if (lr.isSuccess()) 
					{
						// Operation was successful, so get the ID of the record that was processed
						System.debug('*******Successfully locked account with ID: ' + lr.getId());
						Duplicate_Accounts__c dupsSuccess = new Duplicate_Accounts__c();
						dupsSuccess = Duplicate_Accounts__c.getValues(lr.getId());
						if(String.isNotBlank(String.valueof(dupsSuccess.id))) 
						{
							dupsSuccess.Locked__c = True;
							SucessIds.add(dupsSuccess);
						}              
					}
					else 
					{
						// Operation failed, so get all errors                
						for(Database.Error err : lr.getErrors()) 
						{
							System.debug('The following error has occurred.');                    
							System.debug(err.getStatusCode() + ': ' + err.getMessage());
							System.debug('Account fields that affected this error: ' + err.getFields());
						}
					}
				}
				if(!SucessIds.isEmpty())
				update SucessIds;
                System.debug('*******Successfully Updated the records in custom settings' + SucessIds);
			}
		}
        
    }
    
    public void sendNoRecordsNotification() {
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'arun.kumar@tredence.com'};
        String[] ccAddresses = new String[] {'nvent_sfdc@tredence.com'};
        mail.settoAddresses(toAddresses);
        mail.setccAddresses(ccAddresses );
        mail.setSenderDisplayName('nVent Account deDup');
        mail.setSubject('Account deDup - '+system.today().format());
        mail.setPlainTextBody('Hello All, \n\nThere is no Account merge file for today. Administrators are notified regarding the same. \n\nRegards\n nVent SFDC');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    
    }
    
    public class reParentAccountProcessResultWrap {
        
        public Id dupProjectID{get;set;}
        public Id parentProjectID{get;set;}
        public String childObjectName{get;set;}
        public ID chileRecordID{get;set;}
        public String reparentProjectErrors{get;set;}
        public String newLine{get;set;}
        
        public reParentAccountProcessResultWrap(Id dupProjectID, Id parentProjectID, String childObjectName, ID chileRecordID, String reparentProjectErrors, String newLine){
        
            this.dupProjectID = dupProjectID;
            this.parentProjectID = parentProjectID;
            this.childObjectName = childObjectName;
            this.chileRecordID = chileRecordID;
            this.reparentProjectErrors = reparentProjectErrors;
            this.newLine = newLine;
        
        }
    }  
    
    public List<reParentAccountProcessResultWrap> prefixDuplicatenotation(Map<id, id> allAccountKeyIds)
    {
        List<reParentAccountProcessResultWrap> reParentAcntProcessResult = new List<reParentAccountProcessResultWrap>();
        String prefixString='Dup[';
        String suffixString=']';
        list<Account> updateList = new list<Account>();
        //Map<id, id> childProjectIdMap = new Map<id, id>();
        	
        for(Account act : [select id,name from Account where id in : allAccountKeyIds.Keyset()])
        {
            act.name = prefixString+act.name+suffixString;
            act.IsDuplicate__c = true;
            act.Master_Account__c = allAccountKeyIds.get(act.Id);
            updateList.add(act);
            
        }
        
        if(!updateList.isEmpty()) {
			system.debug('----------- FinalLst to prefixDuplicatenotation in Account'+updateList.size());
            Database.SaveResult[] srList = Database.update(updateList, false);
            system.debug('-----**** srList in prefixDuplicatenotation: '+srList);
            system.debug('-----**** updateList in prefixDuplicatenotation: '+updateList);
            for (Integer i = 0; i < srList.size(); i++) 
            {
                if (srList[i].isSuccess()) 
                {
                    System.debug('Successfully prefixDuplicatenotation.' + srList[i].getId());
                }
                else 
                {
                    try
                    {
                    	reParentAcntProcessResult.add(new reParentAccountProcessResultWrap(updateList[i].Id, allAccountKeyIds.get(updateList[i].Id), updateList[i].Id.getSObjectType().getDescribe().getName(), updateList[i].Id, JSON.serialize(srList[i].getErrors()),',\n'));
                    }
                    catch(exception ex)
                    {
                        system.debug('$$$$$$$ ------- The error is:'+ex);
                        System.debug('$$$$$$$ ------ The following error has occurred.'+srList[i].getErrors());                    
                    }
                }
            }
        }   
        
        return reParentAcntProcessResult;
        
    }

}