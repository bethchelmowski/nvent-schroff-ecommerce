/*
 * Copyright (c) 2020. 7Summits Inc. All rights reserved.
 */

/**
 * Created by francoiskorb on 10/16/19.
 */

@IsTest  (SeeAllData=true)
private class x7sProductSpecController_t
{
	@IsTest
	static void testGetProductSpecs()
	{
		List<x7sProductCategory> categories = x7sProductListController.getCategories('');

		x7sProductSpecCollection specs =  x7sProductSpecController.getProductSpecs(categories[0].id);
		System.assertNotEquals(null, specs);
		specs.Dump('testGetProductSpecs');

		specs = x7sProductSpecController.getProductSpecs('');
		System.assertNotEquals(null, specs);
	}

	@IsTest
	static void testProductSpec()
	{
		x7sProductSpec spec = new x7sProductSpec();
		System.assertNotEquals(null, spec);
		spec.Dump('testProductSpec');
	}

	@IsTest
	static void testProductSpecCtor()
	{
		x7sProductSpec spec = new x7sProductSpec('1234', 'name1', 'type1', 'value1');
		System.assertNotEquals(null, spec);
	}

	@IsTest
	static void testSpecList()
	{
		x7sProductSpec spec = new x7sProductSpec('123', 'spec1', 'type1', 'value1');
		x7sProductSpec spec1 = new x7sProductSpec('123', 'spec1', 'type1', 'value2');
		x7sProductSpec spec2 = new x7sProductSpec('234', 'spec2', 'type1', new List<String>{'value1', 'value2'});

		x7sProductSpecList specList = new x7sProductSpecList('group1');
		specList.addSpec(spec);
		specList.addSpec(spec1);
		specList.addSpec(spec2);
		specList.sortSpecs();
		System.assertEquals(2, specList.items.size());

		x7sProductSpecList specList2 = new x7sProductSpecList('group2');
		x7sProductSpecCollection specs = new x7sProductSpecCollection();
		specs.addSpec('group1', spec);
		specs.addSpec('group1', spec1);
		specs.addSpec('group2', spec2);
		specs.sortSpecs();
		System.assertEquals(2, specs.specLists.size());
	}

	@IsTest
	static void testSpecListSlider()
	{
		List<String> specValues = new List<String>{'000022', '000010', '000050'};

		x7sProductSpec spec = new x7sProductSpec('234', 'spec2', 'Slider', specValues);
		System.assertEquals('000010', spec.values[0]);
		System.assertEquals('10', spec.displayValues[0]);
		System.assertEquals('10', spec.minValue);
		System.assertEquals('50', spec.maxValue);
		System.debug(spec.values);

		specValues = new List<String> {'000000000000001000', '000000000000002000', '000000000000000400', '000000000000003000', '000000000000006000'};
		spec = new x7sProductSpec('234', 'spec2', 'Slider', specValues);
		spec.Dump('testSlider');
		System.assertEquals('400', spec.displayValues[0]);
		System.assertEquals('000000000000000400', spec.values[0]);
		System.assertEquals('3000', spec.displayValues[3]);
		System.assertEquals('000000000000003000', spec.values[3]);
	}
}