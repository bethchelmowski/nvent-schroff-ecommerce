//**********************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  	//
// 	Class Name: Pentair_RFQController																		// 								
//	Purpose of the Class: This is a helper class for trigger Pentair_RFQ_SH_Updates							//
//	Created By: Slalom																						// 		
//	Created Date: 4/4/2018										  											//
//----------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose						//
//	||	1.0		||	4/4/2018		||	Slalom		||				||	Initial Creation					//
//	||	2.0		||	2/11/2018		||	Tredence	||	Ad-001		||	Fixed RFQ Assigning Issue			//
//**********************************************************************************************************//	
public class Pentair_RFQController {
	private static boolean run = true;
    private static boolean runRFQTrigger = true;
    public static boolean runOnce(){
        if(Test.isRunningTest())
        {
            run=true;
            return run;
        }
        else
        {
            if(run){
                run=false;
                return true;
            }else{
                return run;
            }
        }
    }
    
	public static boolean runOnceRFQTrigger(){
        /**
        if(Test.isRunningTest())
        {
            runRFQTrigger=true;
            return runRFQTrigger;
        }
        else
        {**/
            if(runRFQTrigger){
                runRFQTrigger=false;
                return true;
            }else{
                return runRFQTrigger;
            }
        //}
    }
    
	public static boolean runOnceRFQNUmberTrigger(){
       
            if(runRFQTrigger){
                runRFQTrigger=false;
                return true;
            }else{
                return runRFQTrigger;
            }
        
    }
    
	public static Date SetDateByBusinessDays(Date currentDate, Integer howManyDays)
    {
        return AddBusinessDays(currentDate, howManyDays);
    }
    
   public static Boolean IsWeekendDay(Date dateParam)
   {
      boolean result = false;
      system.debug('dateParam = '+dateParam); 
      //Recover the day of the week
      Date startOfWeek   = dateParam.toStartOfWeek();
      system.debug('startOfWeek = '+startOfWeek);
      Integer dayOfWeek = startOfWeek.daysBetween(dateParam);
      system.debug('dayOfWeek = '+dayOfWeek);   
      result = dayOfWeek == 0 || dayOfWeek == 6 ? true : false;
      system.debug('result = '+result); 
      return result;
   }    
   
   public static Date AddBusinessDays(Date StartDate, integer BusinessDaysToAdd )
   {
      //Add or decrease in BusinessDaysToAdd days 
      Date finalDate = StartDate;
      system.debug('finaldate = '+finalDate);
      integer direction = BusinessDaysToAdd < 0 ? -1 : 1;
      system.debug('direction = '+direction);
       while(BusinessDaysToAdd != 0)
       {
           finalDate = finalDate.AddDays(direction);
           system.debug('BusinessDaysToAdd = '+BusinessDaysToAdd);            
           system.debug('finaldate = '+finalDate);
           if (!isWeekendDay(finalDate))
           {
               BusinessDaysToAdd -= direction;
           }
       }

       return finalDate;
   }
   public static DateTime addDate(DateTime StartDatedt, integer BusinessDaysToAdd)
	{
        //DateTime StartDatedt = DateTime.newInstance(StartDate.year(), StartDate.month(), StartDate.day(), 00, 00, 00);
    	BusinessHours  defaultHours = [SELECT Id,Name FROM BusinessHours WHERE Name = 'Standard Business Hours'];
		StartDatedt = BusinessHours.nextStartDate(defaultHours.Id, StartDatedt);
		System.debug('pointer '+StartDatedt);
        // make sure you're starting at a Datetime within BusinessHours

        for (Integer elapsed = 0; elapsed < BusinessDaysToAdd; elapsed++)
        {
            StartDatedt = StartDatedt.addDays(1);
            System.debug('pointer '+StartDatedt);
            if (!BusinessHours.isWithin(defaultHours.Id, StartDatedt)){
                System.debug('pointer WITHIN '+StartDatedt);
                StartDatedt = BusinessHours.nextStartDate(defaultHours.Id, StartDatedt);
               // System.debug('pointer iF '+pointer);
            }
        }
		System.debug('pointer '+StartDatedt);
        return StartDatedt;
	} 
    public static String GetERPNumber(String accountId)
    {
        Account getAccount = [SELECT SAP__C FROM ACCOUNT WHERE ID =: accountId];
        return getAccount.SAP__C;
    }
    
   /* public static String GetAssignedto(String accountId, String ownerId, string RecordId)//Ad-001-- Added new parameter RecordTypeId.
    {
        String assigntoUserId = '';
        List<AccountTeamMember> members = [SELECT UserID,User.Division,User.Name FROM AccountTeamMember 
                                     	   WHERE AccountId =: accountId
                                           AND TeamMemberRole = 'Customer Care'];
        /*if (members.size() > 0)Ad-001-- Commneted the previous Code
        {
            assigntoUserId = members[0].UserID; 
        }*/
      /*  RecordType RecordTypeName = [SELECT Id,Name FROM RecordType where id=: RecordId]; //Ad-001-- Querying RecordTypeName based on the ID
		if (members.size() > 0)
        {	// ---- Ad-001--Looping over all the customer care Users to map the right user to the right Qoute
            for(AccountTeamMember AccMember :members)
            {	//---Ad-001-- For Qoute record type the customer care user should belong to Thermal division
                if(RecordTypeName.Name == 'Quote' && AccMember.User.Division == 'Thermal')
                { 
                    assigntoUserId = AccMember.UserID;
                    break;
                }
                //---Ad-001-- For RFQ record type the customer care user should belong to Enclosures division
                if(RecordTypeName.Name == 'RFQ'  && AccMember.User.Division == 'Enclosures')
                {
                    assigntoUserId = AccMember.UserID;
                    break;
                }
            }
            //----Ad-001-- Loop Ends here 
        }
        //else Ad-001--Commented the code
        if(assigntoUserId == '')
        {
            assigntoUserId = ownerId;
        }
            
		return assigntoUserId;
    }*/
    
    public static String SetProposalNumer(String quoteNumber, String quoteId)
    {
        String billingCode = null;
        List<RFQ__c> xAccountQuote = [SELECT Account__c From RFQ__c WHERE ID = :quoteId ];
        try
        {
            Account singleAccount = [SELECT BillingCountryCode From Account where ID =: xAccountQuote[0].Account__c];
            billingCode = singleAccount.BillingCountryCode;
        }
        catch (Exception ex)
        {
            System.debug('Exception finding account for billing code ex:' + ex.getMessage());
        }
        
        System.debug('Country Code: ' + billingCode); 
        System.debug('Quote ID: ' + quoteNumber); 
        
        if (billingCode == null)
            billingCode= 'US';

        return billingCode + '-' + quoteNumber;
    }
}