/**********************************************************************
Name:  NVent_CaseTriggerHelper
Developed by: Anvi Kanodra
=======================================================================
Req: 
Use Trigger Framework for all Triggers
=======================================================================
Purpose:                                                            
This helper class holds all the business logic for Case Trigger.
=======================================================================
History                                                            
-------                                                            
VERSION  AUTHOR            DATE              DETAIL
1.0      Anvi Kanodra      08/29/2019        Added functions to create contact on case saving 

***********************************************************************/
global with sharing class NVent_CaseTriggerHelper{
    
    /*******************************************************************
Purpose:  This method is used to check if the contact exists, if not, create a new contact and link to the case
Parameters: List<SObject>
Returns: void
Throws : None
********************************************************************/
    public static final String COMMA = ', ';  
    
    public static void createContact(list<Case> listOfNewCases)
    {
        Contact ContactObj = new Contact();
        List <String>ListOfEmailAddresses = new List<String>();
        List<Contact> ListOfExistingContacts  = new List<Contact>();
        List<Case> ListOfCasesToUpdate = new List<Case>();
        List<contact> ListOfContactsToCreate = new List<contact>();
        String STRING_EMAIL ='Email';
        String STRING_WEB = 'Web';
        List<Case> ListofCases= new List<case>();
        String firstname;
        string lastname;
        String caseRecordTypeName;
        String EFS_RECORDTYPE ='EFS Case';
        String NUHEAT_RECORDTYPE = 'Nuheat NA Cases';
        String THERMAL_RECORDTYPE ='Thermal Case';
        String THERMAL_NA_RECORDTYPE='Thermal NA Tech Support';
        String Enc_CC_RECORDTYPE = 'Enclosures CC Case';
        
        try
        {
            if(!listOfNewCases.isEmpty())
            {
                //adding email id's to a list
                for(Case caseObj : listOfNewCases)
                {
                    if(!string.isBlank(caseObj.SuppliedEmail))
                    {
                        ListOfEmailAddresses.add(caseObj.SuppliedEmail);
                    }
                }
                
                //Querying to check if the contact already exists
                ListOfExistingContacts =[select id, email from contact where email IN:ListOfEmailAddresses];
                system.debug('ListOfExistingContacts'+ListOfExistingContacts);
                //no results, create a new contact
                if(ListOfExistingContacts.isEmpty())
                {
                    for(Case CaseObj1 : listOfNewCases)
                    {
                        
                       // caseRecordTypeName = caseObj1.RecordTypeid;
                        
                        caseRecordTypeName = Schema.SObjectType.Case.getRecordTypeInfosById().get(CaseObj1.recordtypeid).getName();
                            system.debug('caseRecordTypeId'+caseRecordTypeName);
                        if(!string.isBlank(caseObj1.SuppliedEmail) && !string.isBlank(caseObj1.SuppliedName) && caseRecordTypeName!=EFS_RECORDTYPE)
                        {
                           
                            
                            FirstName = caseObj1.SuppliedName.contains(COMMA) ? caseObj1.SuppliedName.substringAfter(COMMA) : caseObj1.SuppliedName.substringBefore(' '); 
                            LastName = caseObj1.SuppliedName.contains(COMMA) ? caseObj1.SuppliedName.substringBefore(COMMA) : caseObj1.SuppliedName.substringAfter(' '); 
                            ContactObj.FirstName = FirstName;
                            ContactObj.LastName = LastName;
                            ContactObj.Email = caseObj1.SuppliedEmail;
                            ContactObj.Phone = caseObj1.SuppliedPhone;
                            
                            
                            //Adding web comments to determine the origin and record type
                                                        
                            if(caseObj1.Origin == STRING_EMAIL && caseRecordTypeName== NUHEAT_RECORDTYPE )
                            {
                                ContactObj.Web_Comments__c = 'Contact originated from Email, Nuheat'; 
                                ContactObj.AccountId = Label.nVent_Thermal_DummyAccount;
                            }
                            if(caseObj1.Origin == STRING_EMAIL && caseRecordTypeName== THERMAL_RECORDTYPE )
                            {
                                ContactObj.Web_Comments__c = 'Contact originated from Email, Thermal';   
                                ContactObj.AccountId = Label.nVent_Thermal_DummyAccount;
                            }
                            if(caseObj1.Origin == STRING_EMAIL && caseRecordTypeName==THERMAL_NA_RECORDTYPE )
                            {
                                ContactObj.Web_Comments__c = 'Contact originated from Email, Thermal Tech Support';   
                                ContactObj.AccountId = Label.nVent_Thermal_DummyAccount;
                            }
                            //Enc
                            if(caseObj1.Origin == STRING_EMAIL && caseRecordTypeName == Enc_CC_RECORDTYPE )
                            {
                                ContactObj.Web_Comments__c = 'Contact originated from Email, Enclosure Tech Support';   
                                ContactObj.AccountId = Label.nVent_Enclosure_DummyAccount;
                            }
                            if(caseObj1.Origin == STRING_WEB && caseRecordTypeName==NUHEAT_RECORDTYPE)
                            {
                                ContactObj.Web_Comments__c = 'Contact originated from Web, Nuheat';    
                                ContactObj.AccountId = Label.nVent_Thermal_DummyAccount;
                            }
                            
                            if(caseObj1.Origin == STRING_WEB && caseRecordTypeName==THERMAL_RECORDTYPE)
                            {
                                ContactObj.Web_Comments__c = 'Contact originated from Web, Thermal'; 
                                ContactObj.AccountId = Label.nVent_Thermal_DummyAccount;
                            }
                            
                            if(caseObj1.Origin == STRING_WEB && caseRecordTypeName==THERMAL_NA_RECORDTYPE)
                            {
                                ContactObj.Web_Comments__c = 'Contact originated from Web, Thermal Tech Support';   
                                ContactObj.AccountId = Label.nVent_Thermal_DummyAccount;
                            }
                            
                            
                            ListOfContactsToCreate.add(ContactObj);
                            ListofCases.add(caseObj1);
                        }
                    }
                    if(!ListOfContactsToCreate.isEmpty())
                    {
                        insert ListOfContactsToCreate;
                    }
                    
                    //Associate contact with the case
                    if(!ListofCases.isEmpty())
                    {
                        for(case case1 :ListofCases)
                        {
                            case1.contactId = ListOfContactsToCreate[0].id; 
                            ListOfCasesToUpdate.add(case1);
                        }
                    }
                    //update ListOfCasesToUpdate;
                    
                }
            }
            
        }
        catch(Exception e)
        {
            nVent_ExceptionUtility.createLog(e);
        }
        
    }
    
    public static void assignOwnerAccTeam(list<Case> listNewCases)
    {
        list<Id> listConId = new List<Id>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enclosures CC Case').getRecordTypeId();
        system.debug('listNewCases 1 '+listNewCases);    
        for(Case obj : listNewCases)
        {
            if(obj.Origin == 'Email' &&obj.RecordTypeId == recordTypeId)
            {
                
                if(obj.ContactId != null)
                    listConId.add(obj.ContactId);
            }
        }
        
        system.debug('listConId '+listConId);  
        map<Id, Id> mapConAcc = new map<Id, Id>();
        for(Contact obj : [Select Id, AccountId from Contact where Id in: listConId])
            mapConAcc.put(obj.Id, obj.AccountId);
        system.debug('mapConAcc '+mapConAcc); 
        map<Id, map<String,AccountTeamMember>> mapAccMapMem = new map<Id, map<String,AccountTeamMember>>();
        
        for(Account obj : [Select Id, (Select Id, AccountId, TeamMemberRole, UserId from AccountTeamMembers where TeamMemberRole in ('Hoffman Customer Care','Schroff Customer Care','Customer Care')) 
                           from Account where Id in: mapConAcc.values()])
        {
            map<String,AccountTeamMember> mapAccMem = new map<String,AccountTeamMember>();
            for(AccountTeamMember objMem : obj.AccountTeamMembers)
            {
                
                mapAccMem.put(objMem.TeamMemberRole,objMem);
            }
            mapAccMapMem.put(obj.Id,mapAccMem);
        }
        
        system.debug('mapAccMapMem '+mapAccMapMem);
        
        list<Case> listUpdate = new List<Case>();   
        for(Case obj : listNewCases)
        {
            if(obj.ContactId != null && mapAccMapMem.get(mapConAcc.get(obj.ContactId)) != null)
            {
            	map<String,AccountTeamMember> mapAccMem = mapAccMapMem.get(mapConAcc.get(obj.ContactId));
                if(mapAccMem.get('Schroff Customer Care') != null)
                	obj.Assigned_To__c = mapAccMem.get('Schroff Customer Care').UserId;
                else if(mapAccMem.get('Hoffman Customer Care') != null)
                    obj.Assigned_To__c = mapAccMem.get('Hoffman Customer Care').UserId;
                else if(mapAccMem.get('Customer Care') != null)
                    obj.Assigned_To__c = mapAccMem.get('Customer Care').UserId;
            }
            /*else
            {
                obj.OwnerId = '00G17000002uLbL';
                listUpdate.add(obj);
            }*/
        }
        //update listUpdate;
        system.debug('listNewCases '+listNewCases);
    }
    
    public static void assignBusinessHours(list<Case> listNewCases)
    {
        list<Id> listConId = new List<Id>();
        Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enclosures CC Case').getRecordTypeId();
        for(Case obj : listNewCases)
        {
            if(obj.Origin == 'Email' && obj.RecordTypeId == recordTypeId)
            {
                if(obj.ContactId != null)
            		listConId.add(obj.ContactId);
            }
        }
        system.debug('listConId BH '+listConId);
        
        list<Contact> listContact = [Select Id, MailingCountry from Contact where Id in: listConId and MailingCountry in ('Sweden','United Kingdom','Italy')];
        system.debug('listContact BH '+listContact);
        map<Id, Contact> mapContact = new map<Id, Contact>([Select Id, MailingCountry from Contact where Id in: listConId and MailingCountry in ('Sweden','United Kingdom','Italy')]);
        map<String, BusinessHours> mapHours = new map<String, BusinessHours>();
        for(BusinessHours obj : [select id,Name from BusinessHours where Name in ('ENC Italy Office','ENC Sweden Office','ENC UK Office')])
            mapHours.put(obj.Name, obj);
        system.debug('mapContact '+mapContact);
        system.debug('mapHours '+mapHours);
        for(Case obj : listNewCases)
        {
            if(obj.Origin == 'Email' && obj.RecordTypeId == recordTypeId)
            {
                if(mapContact.get(obj.ContactId) != null)
                {
                    String country = mapContact.get(obj.ContactId).MailingCountry;
                    if(country == 'Sweden')
                        obj.BusinessHoursId = mapHours.get('ENC Sweden Office').Id;
                    else If(country == 'United Kingdom')
                        obj.BusinessHoursId = mapHours.get('ENC UK Office').Id;
                    else If(country == 'Italy')
                        obj.BusinessHoursId = mapHours.get('ENC Italy Office').Id;
                }
            }
        }
        
    }
}