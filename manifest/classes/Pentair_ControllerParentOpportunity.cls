public with sharing class Pentair_ControllerParentOpportunity {  
    public ApexPages.StandardSetController OppRecords{
        get {
            if(OppRecords == null) {
                
                OppRecords = new ApexPages.StandardSetController(
                    Database.getQueryLocator(
                        [
                            SELECT 
                            	Name, 
                            	accountid,
                            	closedate,
                            	stagename,
                            	ownerid,
                            	lastmodifieddate,
                            	amount,
                            	Parent_Opportunity__c
                            FROM Opportunity 
                            WHERE Parent_Opportunity__c =: ApexPages.CurrentPage().getParameters().get('id') //'a1W5B000000OsvJUAS' 
                            AND isclosed = false
                        ]));
            }
            return OppRecords;
        }
        private set;
    }
    
    public Pentair_ControllerParentOpportunity(ApexPages.StandardSetController controller) {

    }
    
    public List<Opportunity> getOpportunities(){
        return (List<Opportunity>)OppRecords.getRecords();
    }
    
    public Pagereference save(){
        OppRecords.save();
        return null;
    }
}