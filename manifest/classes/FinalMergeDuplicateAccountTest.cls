//**********************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  	//
// 	Class Name: FinalMergeDuplicateAccountTest																// 								
//	Purpose of the Class: Used as test class for FinalMergeDuplicateAccount									//
//	Created By: Tredence																					// 		
//	Created Date: 24/08/2018										  										//
//----------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose						//
//	||	1.0		||	24/08/2018		||	Tredence	||				||	Initial Creation					//
//**********************************************************************************************************//																

@isTest
public class FinalMergeDuplicateAccountTest {
	public static integer scope = 1;
    public static list<Account> CreateData()
    { 
        integer i;
        integer No_of_Records_to_be_Created = scope;
        List<Account> CreateMasterAccount = new List<Account>();
  		list<Account> Create_Accounts = new list<Account>();
        
        for(i=0; i<No_of_Records_to_be_Created;i++)
            CreateMasterAccount.add(new Account(name='acc'+i,BillingCountry='India',BillingCity='City'+i,BillingState='Goa',BillingStreet='Street'+i,BillingPostalCode = 'Postcode'+i));
        insert CreateMasterAccount;
        
        for(i=0; i<No_of_Records_to_be_Created;i++)
            Create_Accounts.add(new Account(name='acc'+i,BillingCountry='India',BillingCity='BillCity'+i,BillingState='Karnataka',BillingStreet='BillStreet'+i,BillingPostalCode = 'BillPostcode'+i,IsDuplicate__c = true,Master_Account__c=CreateMasterAccount[i].id));
        insert Create_Accounts;
        
        
        
        list<Account_Connection__c> Create_AccountConn = new list<Account_Connection__c>();
        for(Account accinst : Create_Accounts)
        {
            Create_AccountConn.add(new Account_Connection__c(Connected_from_Account__c=accinst.id,Connected_to_Account__c=accinst.id));
        }
        insert Create_AccountConn;
        
        list<Account_Plan__c> Create_AccountPlan = new list<Account_Plan__c>();
        for(Account accinst : Create_Accounts)
        {
            Create_AccountPlan.add(new Account_Plan__c(Account__c=accinst.id,Plan_Year__c='2018'));
        }
        insert Create_AccountPlan;
                
        list<AccountTeamMember> Create_AccountTeamMember = new list<AccountTeamMember>();
        for(Account accinst : Create_Accounts)
        {
            Create_AccountTeamMember.add(new AccountTeamMember(AccountId=accinst.id,userid=userinfo.getUserId()));
        }
        insert Create_AccountTeamMember;
        
        list<Case> Create_Case = new list<Case>();
        for(Account accinst : Create_Accounts)
        {
            Create_Case.add(new Case(AccountId=accinst.id,Electrical_Contractor_Account__c=accinst.id,General_Contractor_Account__c=accinst.id,Service_Partner__c=accinst.id,Tile_Sanitary_Contractor_Account__c=accinst.id));
        }
        insert Create_Case;
        
        list<Certification__c> Create_Certification = new list<Certification__c>();
        for(Account accinst : Create_Accounts)
        {
            Create_Certification.add(new Certification__c(Account__c=accinst.id));
        }
        insert Create_Certification;
        
        list<Contact> Create_Contact = new list<Contact>();
        for(Account accinst : Create_Accounts)
        {
            Create_Contact.add(new Contact(AccountId=accinst.id,lastname=userinfo.getLastName()));
        }
        insert Create_Contact;
        
        list<Event> Create_Event = new list<Event>();
        for(Account accinst : Create_Accounts)
        {
            Create_Event.add(new Event(Whatid=accinst.id,DurationInMinutes=10000+i,ActivityDateTime=datetime.now()));
        }
        insert Create_Event;
        
        list<RFQ__c> Create_RFQ = new list<RFQ__c>();
        for(Account accinst : Create_Accounts)
        {
            Create_RFQ.add(new RFQ__c(Account__c=accinst.id));
        }
        insert Create_RFQ;
        
        list<Task> Create_Task = new list<Task>();
        for(Account accinst : Create_Accounts)
        {
            Create_Task.add(new Task(Whatid=accinst.id));
        }
        insert Create_Task;
        
        return Create_Accounts; 
    }
    
    public  static  testMethod void mergeRecordsPageTest()
    {        
        list<Account> createdaccountid = CreateData();
        //createCustomsettings(createdaccountid);
        Test.startTest();
        List<account> scopesize = [SELECT Id,IsDuplicate__c,Master_Account__c FROM Account WHERE IsDuplicate__c = True AND Master_Account__c != NULL];
        system.debug('scopesize'+scopesize.size());
        FinalMergeDuplicateAccount excBatch = new FinalMergeDuplicateAccount();
		Database.executeBatch(excBatch,scopesize.size());
        
        Test.stopTest();
    }
    
}