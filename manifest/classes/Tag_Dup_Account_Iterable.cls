//**************************************************************************************************************************//
// 									nVent Electrical/Electronic Manufacturing							  					//
// 	Class Name: Tag_Dup_Account_Iterable	                                                                                //
// 	Purpose: Iterable for custom setting Duplicate_Accounts__c											                    //
//  Created By: Tredence																									//			
//	Created Date: 12/7/2018										  															//
//--------------------------------------------------------------------------------------------------------------------------//
//	||	Version	||	Date of Change	||	Editor		||	Enh Req No	||	Change Purpose										//
//	||	1.0		||	12/7/2018		|| Tredence 	||				||	Initial Creation									//
//**************************************************************************************************************************//

global class Tag_Dup_Account_Iterable implements Iterator<DuplicatesList> 
{
    global List<DuplicatesList> duplicates {get;set;}
    global Integer i {get; set;}
    
        global boolean hasNext()
    {
        if(i>=duplicates.size())
            return false;
        else
            return True;
    }
    
    global DuplicatesList next()
    { 
       i++; 
       return duplicates[i-1]; 
   }
    
    public Tag_Dup_Account_Iterable() 
    {   	 
        duplicates = new list<DuplicatesList>();
        Date date_today=Date.today();
        date_today=date_today.addDays(-2);
        list<Duplicate_Accounts__c> dupacc=[Select Name, Master_Account_ID__c,Tagged__c from Duplicate_Accounts__c where Tagged__c=False And CreatedDate>=:date_today];
        i=0;
        
        if(!dupacc.isEmpty())
        {
        	for(Duplicate_Accounts__c dup : dupacc)
            {	 	
                    //adding data to our iterator using constructor
                	duplicates.add(new DuplicatesList(dup.Name,dup.Master_Account_ID__c,dup.Tagged__c));
            }
        } 
    }
        
       //wrapper class
       global class DuplicatesList 
       {
   
        global String dupAccountId{get;set;}
        global String MasterAccounttId{get;set;}
        global boolean Tagged{get;set;}
           
        //creating constructor 
        global DuplicatesList(String dupAccountId, String MasterAccounttId,boolean Tagged)
        {
        
            this.dupAccountId = dupAccountId;
            this.MasterAccounttId = MasterAccounttId; 
            this.Tagged =Tagged;
        }
}
}