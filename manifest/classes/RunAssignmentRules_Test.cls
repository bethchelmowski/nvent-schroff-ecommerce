@isTest
public class RunAssignmentRules_Test {
 
 @testSetup
 static void loadData(){
 Lead l = new Lead(
 LastName='Test',
 Company='Test',
 Sales_team__c ='Hoffman'
 );
 insert l;
 }
 
 @isTest
 static void testLead(){
 //instantiate List to pass to @InvocableMethod
 List<id> ids = new list<id>();
 
 //query for test data and add to List
 Lead l = [SELECT Id, OwnerId FROM Lead];
 ids.add(l.Id);
 
 //call @InvocableMethod
 test.startTest();
 RunAssignmentRules.assignLeads(ids);
 test.stopTest();
 
 //verify that Lead was re-assigned
 Lead res = [SELECT Id, OwnerId FROM Lead];
 System.assert(l.OwnerId != res.OwnerId, res);
 }
}