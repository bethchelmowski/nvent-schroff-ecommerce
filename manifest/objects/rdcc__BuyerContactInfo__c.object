<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>An object to store Buyer Activity Report Contact details.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>rdcc__Average_Job_Size__c</fullName>
        <deprecated>false</deprecated>
        <description>The typical job size for the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The typical job size for the company.</inlineHelpText>
        <label>Average Job Size</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Business_Type__c</fullName>
        <deprecated>false</deprecated>
        <description>The business type for the company, such as Subcontractor, Supplier or Owner.</description>
        <externalId>false</externalId>
        <inlineHelpText>The business type for the company, such as Subcontractor, Supplier or Owner.</inlineHelpText>
        <label>Business Type</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__BuyerContactId__c</fullName>
        <deprecated>false</deprecated>
        <description>The unique ConstructConnect ID for the buyer.</description>
        <externalId>true</externalId>
        <inlineHelpText>The unique ConstructConnect ID for the buyer.</inlineHelpText>
        <label>BuyerContactId</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__BuyerSource__c</fullName>
        <deprecated>false</deprecated>
        <description>The ConstructConnect source of the Buyer Activity Report data.</description>
        <externalId>false</externalId>
        <inlineHelpText>The ConstructConnect source of the Buyer Activity Report data.</inlineHelpText>
        <label>BuyerSource</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Company_Address__c</fullName>
        <deprecated>false</deprecated>
        <description>The address of the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The address of the company.</inlineHelpText>
        <label>Company Address</label>
        <length>500</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>rdcc__Company_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>The name of the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The name of the company.</inlineHelpText>
        <label>Company Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Contact_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>The name of the contact for the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The name of the contact for the company.</inlineHelpText>
        <label>Contact Name</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Email__c</fullName>
        <deprecated>false</deprecated>
        <description>The email address for the company contact.</description>
        <externalId>false</externalId>
        <inlineHelpText>The email address for the company contact.</inlineHelpText>
        <label>Email</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Insured__c</fullName>
        <deprecated>false</deprecated>
        <description>Denotes that the company is insured,  YES for is insured and NO for not insured.</description>
        <externalId>false</externalId>
        <inlineHelpText>Denotes that the company is insured,  YES for is insured and NO for not insured.</inlineHelpText>
        <label>Insured</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Licensed__c</fullName>
        <deprecated>false</deprecated>
        <description>Denotes if the company is licensed for the work performed, YES for is licensed, NO for not licensed.</description>
        <externalId>false</externalId>
        <inlineHelpText>Denotes if the company is licensed for the work performed, YES for is licensed, NO for not licensed.</inlineHelpText>
        <label>Licensed</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__MWBE__c</fullName>
        <deprecated>false</deprecated>
        <description>Indicates if the company is a minority or woman owned company.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates if the company is a minority or woman owned company.</inlineHelpText>
        <label>MWBE</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Phone__c</fullName>
        <deprecated>false</deprecated>
        <description>The phone number for the contact.</description>
        <externalId>false</externalId>
        <inlineHelpText>The phone number for the contact.</inlineHelpText>
        <label>Phone</label>
        <length>15</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Sector__c</fullName>
        <deprecated>false</deprecated>
        <description>The sector in which the company participates, PUBLIC for public, PRIVATE for private, BOTH for both public and private.</description>
        <externalId>false</externalId>
        <inlineHelpText>The sector in which the company participates, PUBLIC for public, PRIVATE for private, BOTH for both public and private.</inlineHelpText>
        <label>Sector</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Union__c</fullName>
        <deprecated>false</deprecated>
        <description>Indicates if the company is a union shop, YES for union and NO for not union.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates if the company is a union shop, YES for union and NO for not union.</inlineHelpText>
        <label>Union</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Year_Established__c</fullName>
        <deprecated>false</deprecated>
        <description>The year the company was established.</description>
        <externalId>false</externalId>
        <inlineHelpText>The year the company was established.</inlineHelpText>
        <label>Year Established</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>BuyerContactInfo</label>
    <listViews>
        <fullName>rdcc__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>Buyer-{0000}</displayFormat>
        <label>Buyer Contact #</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>BuyerContactInfo</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
