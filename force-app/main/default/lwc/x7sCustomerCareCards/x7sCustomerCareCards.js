import { LightningElement, wire, track } from "lwc";

import getRepInformation from "@salesforce/apex/x7S_UserInfoController.getRepInformation";

import { getRecord } from "lightning/uiRecordApi";

import USER_ID from "@salesforce/user/Id";
import CONTACT_ID from "@salesforce/schema/User.ContactId";
import CONTACT_NAME from "@salesforce/schema/Contact.Name";
import ENCLOSURE_SAP from "@salesforce/schema/Contact.Enclosures_SAP__c";

import NVENT_IMAGES from "@salesforce/resourceUrl/nventEmailImages";

const FIELDS = [CONTACT_NAME, ENCLOSURE_SAP];

export default class X7sCustomerCareCards extends LightningElement {
  @wire(getRepInformation)
  wiredRepInformation({ error, data }) {
    if (error) {
      console.error("wiredRepInformation: ", error);
    } else if (data) {
      this.state = JSON.parse(JSON.stringify(data));
    }
  }

  contactId;
  sapNumber;

  @track state = [];

  init = false;

  @wire(getRecord, { recordId: USER_ID, fields: [CONTACT_ID] })
  user({ error, data }) {
    if (error) {
      console.error("getRecord user: ", error);
    } else if (data) {
      console.log("getRecord user: ", data);
      console.log("ContactId: ", data.fields.ContactId.value);

      this.contactId = data.fields.ContactId.value;
    }
  }

  @wire(getRecord, { recordId: "$contactId", fields: FIELDS })
  contact({ error, data }) {
    if (error) {
      console.error("getRecord contact: ", error.body.message);
    } else if (data) {
      console.log("getRecord contact: ", data);

      this.sapNumber = data.fields.Enclosures_SAP__c.value;
    }
  }

  renderedCallback() {
    console.log("this.state: ", this.state);
    if (this.state.length === 2 && this.init === false) {
      console.log("NVENT_IMAGES: ", NVENT_IMAGES + "/nvent-spark.png");
      this.init = true;
      this.state = this.state.map(x => {
        if (
          x &&
          x.smallPhotoUrl === "/nventschroffuserportal/profilephoto/005/T"
        ) {
          x.smallPhotoUrl = NVENT_IMAGES + "/nvent-spark.png";
        }
        if (x.hasOwnProperty('email')) {
          x.emailLink = `mailto:${x.email}`;
        }
        if (x.hasOwnProperty('phone')) {
          x.phoneLink = `tel:${x.phone}`;
        }

        return x;
      });
    }
  }
}