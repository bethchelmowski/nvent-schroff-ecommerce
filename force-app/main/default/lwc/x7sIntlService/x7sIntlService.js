export class X7sIntlService {
  static format(stringToFormat, ...formattingArguments) {
    if (typeof stringToFormat !== "string")
      throw new Error("'stringToFormat' must be a String");
    return stringToFormat.replace(/{(\d+)}/gm, (match, index) =>
      formattingArguments[index] === undefined
        ? ""
        : `${formattingArguments[index]}`
    );
  }
}
