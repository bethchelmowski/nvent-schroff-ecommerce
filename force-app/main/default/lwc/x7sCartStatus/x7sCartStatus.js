import { LightningElement } from "lwc";
import fetchCartStatus from "@salesforce/apex/x7S_CartStatusController.fetchCartStatus";
import status from "@salesforce/label/ccrz.MyCartsInc_Status";

export default class X7sCartStatus extends LightningElement {
  cartStatus;
  statusClass;

  labels = {
    status
  };

  renderedCallback() {
    this.getCartStatus();
  }

  getCartStatus() {
    fetchCartStatus().then(res => {
      console.log('fetchCartStatus: ', res);
      this.cartStatus = res;

      //   this.updateStatusColor(res);
    });
  }

  updateStatusColor(status) {
    switch (status) {
      case "Open":
        this.statusClass = "status-open";
        break;

      default:
        break;
    }
  }
}
