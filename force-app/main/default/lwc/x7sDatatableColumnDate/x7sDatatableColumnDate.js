import { LightningElement, api } from "lwc";

// import {
//   createMessageContext,
//   releaseMessageContext,
//   subscribe
// } from "lightning/messageService";

// import COLUMNMC from "@salesforce/messageChannel/ColumnMessageChannel__c";

export default class X7sDataTableDateColumn extends LightningElement {
  @api lastModifiedDate;
  @api wishlistItemAddedDate;
  @api orderDate;
  @api dateValue;

  dateTransformed;

  // context = createMessageContext();
  // subscription = null;

  connectedCallback() {
    this.transformDate();

    // if (this.subscription) {
    //   return;
    // }
    // this.subscription = subscribe(this.context, COLUMNMC, message => {
    //   this.handleMessage(message);
    // });
  }

  disconnectedCallback() {
    // releaseMessageContext(this.context);
  }

  handleMessage(payload) {
    console.log("handleMessage payload: ", payload);
    const msg = payload.message;

    switch (msg) {
      case "updateDateColumn":
        this.handleUpdateDateColumn();
        break;

      default:
        break;
    }
  }

  transformDate() {
    console.log("orderDate: ", this.orderDate);
    let dateToUse = "";

    if (this.lastModifiedDate) dateToUse = this.lastModifiedDate;
    if (this.wishlistItemAddedDate) dateToUse = this.wishlistItemAddedDate;
    if (this.orderDate) dateToUse = this.orderDate;
    if (this.dateValue) dateToUse = this.dateValue;
    if (dateToUse === "") {
      this.dateTransformed = 'n/a';
      return;
    }

    const date = new Date(dateToUse);
    const dateTimeFormat = new Intl.DateTimeFormat("en-US", {
      year: "numeric",
      month: "short",
      day: "2-digit",
      timeZone: 'UTC'
    });
    let [
      { value: month },
      ,
      { value: day },
      ,
      { value: year }
    ] = dateTimeFormat.formatToParts(date);

    day = Number(day);

    if (day < 10) {
      day = "0" + day.toString();
    } else {
      day.toString();
    }

    this.dateTransformed = `${day} ${month} ${year}`;

    console.log("lastModifiedDate: ", this.dateTransformed);
  }

  handleUpdateDateColumn() {
    console.log("handleUpdateDateColumn: ", this.lastModifiedDate);

    setTimeout(() => {
      this.transformDate();
    }, 0);
  }
}