import { LightningElement, wire, track } from "lwc";

// Pub Sub
import {
  registerListener,
  unregisterAllListeners,
  fireEvent
} from "c/x7sPubSub";

import USER_ID from "@salesforce/user/Id";
import NAME_FIELD from "@salesforce/schema/User.Name";

import { getRecord } from "lightning/uiRecordApi";

import getWishlistItemCount from "@salesforce/apex/x7S_HeaderGreetingController.getWishlistItemCount";
import getOrderCount from "@salesforce/apex/x7S_HeaderGreetingController.getOrderCount";

import headerGreeting from "@salesforce/label/c.nVent_Header_Greeting";
import headerMessage from "@salesforce/label/c.nVent_Header_Message";
import headerOrders from "@salesforce/label/c.nVent_Orders";
import headerFavorites from "@salesforce/label/c.nVent_Favorites";

export default class X7sHeaderGreeting extends LightningElement {
  @track name;
  @track orderCount;
  @track wishlistCount;

  labels = {
    headerGreeting,
    headerMessage,
    headerOrders,
    headerFavorites
  };

  @wire(getOrderCount)
  wiredOrderCount({ error, data }) {
    if (error) {
      console.error(error);
    } else if (data) {
      console.log("data: ", data);

      this.orderCount = data;
    }
  }

  // @wire(getWishlistItemCount)
  // wishlistItemCount({ error, data }) {
  //   if (error) {
  //     console.error(error);
  //   } else if (data) {
  //     console.log("data: ", data);

  //     this.wishlistCount = data;
  //   }
  // }

  @wire(getRecord, {
    recordId: USER_ID,
    fields: [NAME_FIELD]
  })
  wireUser({ error, data }) {
    if (error) {
      console.error(error);
    } else if (data) {
      console.log("data: ", data);

      this.name = data.fields.Name.value.split(" ")[0];
    }
  }

  connectedCallback() {
    this.handleUpdateWishlistItemTotal();
    registerListener("handleHeaderGreetingListener", this.handleMessage, this);
  }

  disconnectedCallback() {
    unregisterAllListeners(this);
  }

  handleMessage(payload) {
    console.log("handleMessage payload: ", payload);
    const msg = payload.message;

    switch (msg) {
      case "updateWishlistItemTotal":
        this.handleUpdateWishlistItemTotal();
        break;
      default:
        break;
    }
  }

  handleUpdateWishlistItemTotal() {
    getWishlistItemCount().then(res => {
      console.log("getWishlistItemCount: ", res);
      if (res) this.wishlistCount = res;
    });
  }
}
