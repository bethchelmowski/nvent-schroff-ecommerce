import { LightningElement, api, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { NavigationMixin } from "lightning/navigation";

// Pub Sub
import {
  registerListener,
  unregisterAllListeners,
  fireEvent
} from "c/x7sPubSub";

// Custom Labels
import emptyDatatableMessage from "@salesforce/label/c.nVent_Empty_Datatable_Message";
import datatableSearch from "@salesforce/label/c.nVent_DataTable_Search";
import datatableSearchCart from "@salesforce/label/c.nVent_DataTable_Search_Cart";
import datatableSearchWishlist from "@salesforce/label/c.nVent_DataTable_Search_Wishlist";
import datatableSearchItems from "@salesforce/label/c.nVent_DataTable_Search_Items";
import datatableSearchQuotes from "@salesforce/label/c.nVent_DataTable_Search_Quotes";
import datatableSearchOrders from "@salesforce/label/c.nVent_DataTable_Search_Orders";
import errorMessage from "@salesforce/label/c.nVent_DataTable_Error_Message";
import noComponentsMessage from "@salesforce/label/c.nVent_DataTable_No_Components";
import addToWishlist from "@salesforce/label/c.nVent_DataTable_Add_To_Wishlist";
import cloneCartMessage from "@salesforce/label/c.nVent_DataTable_Clone_Cart_Message";
import addToCart from "@salesforce/label/c.nVent_DataTable_Add_To_Cart";
import quantityUpdated from "@salesforce/label/c.nVent_DataTable_Update_Quantity";
import success from "@salesforce/label/c.nVent_DataTable_Success";
import successShareCart from "@salesforce/label/c.nVent_DataTable_Success_Share_Cart";
import errorLabel from "@salesforce/label/c.nVent_DataTable_Error";
import cloneWarningMessage from "@salesforce/label/c.nVent_DataTable_Clone_Warning_Message";
import infoLabel from "@salesforce/label/c.nVent_DataTable_Info_Label";
import continueLabel from "@salesforce/label/c.nVent_DataTable_Continue";
import cloneCart from "@salesforce/label/c.nVent_DataTable_Clone_Cart";
import productComponents from "@salesforce/label/c.nVent_DataTable_Product_Components";
import componentsModalMessage from "@salesforce/label/c.nVent_DataTable_Components_Modal_Message";
import closeModal from "@salesforce/label/c.nVent_DataTable_Modal_Close";
import cancel from "@salesforce/label/c.nVent_DataTable_Cancel";
import legalText from "@salesforce/label/c.nVent_DataTable_Legal_Text";
import shareCartLabel from "@salesforce/label/c.nVent_DataTable_Share_Cart";
import shareCartMessage from "@salesforce/label/c.nVent_DataTable_Share_Cart_Message";
import email from "@salesforce/label/c.nVent_DataTable_Email";
import missingEmail from "@salesforce/label/c.nVent_DataTable_Email_Missing";
import sendEmail from "@salesforce/label/c.nVent_DataTable_Email_Send";
import viewCartItems from "@salesforce/label/c.nVent_DataTable_View_Cart_Items";
import moveToCart from "@salesforce/label/c.nVent_DataTable_Move_To_Cart";
import copyToCart from "@salesforce/label/c.nVent_DataTable_Copy_To_Cart";
import deleteLabel from "@salesforce/label/c.nVent_DataTable_Delete";
import showComponents from "@salesforce/label/c.nVent_DataTable_Show_Components";
import obsoleteProductsLabel from "@salesforce/label/c.nVent_DataTable_Obsoslete_Products";
import obsoleteProductsMessageLabel from "@salesforce/label/c.nVent_DataTable_Obsoslete_Products_Message";
import obsoleteProductsInfoMessageLabel from "@salesforce/label/c.nVent_DataTable_Obsoslete_Products_Info_Message";
import cleanMyCart from "@salesforce/label/c.nVent_DataTable_Obsoslete_Products_Clean_Button_Text";
import cannotAddToWishlist from "@salesforce/label/c.nVent_DataTable_Cannot_Add_To_Wishlist";
import nVent_DataTable_Components_Modal_Copy_Message from '@salesforce/label/c.nVent_DataTable_Components_Modal_Copy_Message';
import nVent_DataTable_Components_Modal_Modify_Message from '@salesforce/label/c.nVent_DataTable_Components_Modal_Modify_Message';

// Apex Methods
import getQuotes from "@salesforce/apex/x7S_QuotesController.getQuotes";
import getCurrentCart from "@salesforce/apex/x7S_CartActionController.getCurrentCart";
import cloneSubmittedCart from "@salesforce/apex/x7S_CartActionController.cloneSubmittedCart";
import shareCart from "@salesforce/apex/x7S_CartActionController.shareCart";
import getCartById from "@salesforce/apex/x7S_CartActionController.getCartById";
import getWishlistItems from "@salesforce/apex/x7S_DataTableController.getWishlistItems";
import addCartItemToWishlist from "@salesforce/apex/x7S_DataTableController.addCartItemToWishlist";
import addWishlistItemToCart from "@salesforce/apex/x7S_DataTableController.addWishlistItemToCart";
import updateCartItemQuantity from "@salesforce/apex/x7S_DataTableController.updateCartItemQuantity";
import removeWishListItem from "@salesforce/apex/x7S_DataTableController.removeWishListItem";
import removeCartItem from "@salesforce/apex/x7S_DataTableController.removeCartItem";
import getOrders from "@salesforce/apex/x7S_DataTableController.getOrders";
import getOrderItems from "@salesforce/apex/x7S_DataTableController.getOrderItems";

// Column Imports
import { columns } from "./columns.js";
import { componentColumns } from "./componentColumns.js";
import { columnsOrders } from "./columnsOrders.js";
import { columnsOrderDetail } from "./columnsOrderDetail.js";

// x7s Utilities
import { getQueryParameters } from "c/x7sUtils";
import { X7sIntlService } from "c/x7sIntlService";

export default class X7sDataList extends NavigationMixin(LightningElement) {
  dataCopy = [];
  @track data = [];
  @track components = [];
  @track columns;
  componentColumns = componentColumns;

  @api sortedBy = "id";
  @api sortedDirection;

  @api loadData;
  @api recordId;
  @api showShareCartButton;
  @api showSearchBar;
  @api showCloneCartButton;
  @api showHelpText;

  obsoleteProductsList = [];

  cartId;
  cartIdToClone;
  productNameComponentModal;
  productComponentModalFormattedMessage;

  // Used for View Components modal CTAs
  currentProductComponent = {
    copyUrl: '',
    modifyUrl: '',
    zipUrl: ''
  }

  showSpinner = true;
  showSpinnerModal = false;

  labels = {
    emptyDatatableMessage,
    datatableSearch,
    datatableSearchCart,
    datatableSearchWishlist,
    datatableSearchItems,
    datatableSearchQuotes,
    datatableSearchOrders,
    errorMessage,
    noComponentsMessage,
    addToWishlist,
    addToCart,
    quantityUpdated,
    success,
    successShareCart,
    errorLabel,
    cloneWarningMessage,
    continueLabel,
    cloneCart,
    cancel,
    cloneCartMessage,
    legalText,
    shareCartLabel,
    shareCartMessage,
    email,
    missingEmail,
    sendEmail,
    productComponents,
    closeModal,
    infoLabel,
    componentsModalMessage,
    viewCartItems,
    moveToCart,
    copyToCart,
    deleteLabel,
    showComponents,
    obsoleteProductsLabel,
    obsoleteProductsMessageLabel,
    obsoleteProductsInfoMessageLabel,
    cleanMyCart,
    cannotAddToWishlist,
    nVent_DataTable_Components_Modal_Copy_Message,
    nVent_DataTable_Components_Modal_Modify_Message
  };

  showEmptyState = false;
  showCloneCartModal = false;
  showShareCartModal = false;
  showComponentsModal = false;

  shareEmail = "";
  sapNumber = "";

  get showObsoleteProductCard() {
    return this.obsoleteProductsList.length > 0;
  }

  get searchPlaceholderLabel() {
    let placeholder = "";

    if (this.loadData === "Wish List")
      placeholder = this.labels.datatableSearchWishlist;

    if (this.loadData === "My Cart" || this.loadData === "Shared Cart")
      placeholder = this.labels.datatableSearchCart;

    if (this.loadData === "Submitted Cart")
      placeholder = this.labels.datatableSearchItems;

    if (this.loadData === "Quotes")
      placeholder = this.labels.datatableSearchQuotes;

    if (this.loadData === "Orders")
      placeholder = this.labels.datatableSearchOrders;

    return placeholder;
  }

  connectedCallback() {
    console.log("loadData: ", this.loadData); // Use this to determine what method to call to get data = Wish List, Cart, My Orders, etc.

    this.updateColumns();
    this.loadCartData();
    this.setDefaultSort();

    // Registering pub sub listeners
    registerListener("handleDataListListener", this.handleMessage, this);
  }

  disconnectedCallback() {
    unregisterAllListeners(this);
  }

  handleMessage(payload) {
    console.log("handleMessage payload: ", payload);
    const msg = payload.message;

    switch (msg) {
      case "doneUpdating":
        this.handleDoneUpdating(payload.data);
        break;
      case "showCartSpinner":
        this.handleShowSpinner();
        break;
      case "hideCartSpinner":
        this.handleHideSpinner();
        break;
      case "quoteSubmittedSuccessfully":
        this.handleQuoteSubmitSuccess();
        break;
      default:
        break;
    }
  }

  updateColumns() {
    this.columns = JSON.parse(JSON.stringify(columns));
    let columnsToRemove;
    const productNameColumnIndex = this.columns.findIndex(
      x => x.fieldName === "productName"
    );
    const actionColumnIndex = this.columns.findIndex(x => x.fieldName === "id");
    const quantityColumnIndex = this.columns.findIndex(
      x => x.fieldName === "quantity"
    );

    // Build list of columns to remove based on this condition
    if (this.loadData === "Wish List") {
      columnsToRemove = [
        "action",
        "subTotal",
        "netPrice",
        "listPrice",
        "totalAmount",
        "lastModifiedDate",
        "distinctSKUs",
        "cartId",
        "quantity",
        "quoteNumber",
        "quoteStatus"
      ];

      this.columns[productNameColumnIndex].fixedWidth = 0;
      this.columns[productNameColumnIndex].initialWidth = 500;

      this.columns[actionColumnIndex].typeAttributes.rowActions = [
        { label: this.labels.moveToCart, name: "CART" },
        { label: this.labels.copyToCart, name: "COPYCART" },
        {
          label: this.labels.showComponents,
          name: "COMPONENTS"
        },
        { label: this.labels.deleteLabel, name: "DELETE" }
      ];
    }

    // Build list of columns to remove based on this condition
    if (this.loadData === "My Cart") {
      columnsToRemove = [
        "action",
        "totalAmount",
        "lastModifiedDate",
        "distinctSKUs",
        "cartId",
        "wishlistItemAddedDate",
        "quoteNumber",
        "quoteStatus"
      ];
    }

    // Build list of columns to remove based on this condition
    if (this.loadData === "Submitted Cart") {
      columnsToRemove = [
        "totalAmount",
        "cartId",
        "quoteNumber",
        "lastModifiedDate",
        "wishlistItemAddedDate",
        "distinctSKUs",
        "action",
        "quoteStatus"
      ];

      this.columns[quantityColumnIndex].typeAttributes.showDropdown = false;

      this.columns[actionColumnIndex].typeAttributes.rowActions = [
        { label: this.labels.showComponents, name: "COMPONENTS" }
      ];

      this.columns[productNameColumnIndex].fixedWidth = 0;
      this.columns[productNameColumnIndex].initialWidth = 500;
    }

    // Build list of columns to remove based on this condition
    if (this.loadData === "Shared Cart") {
      columnsToRemove = [
        "action",
        "totalAmount",
        "lastModifiedDate",
        "distinctSKUs",
        "cartId",
        "wishlistItemAddedDate",
        "quoteNumber",
        "quoteStatus"
      ];

      this.columns[actionColumnIndex].typeAttributes.rowActions = [
        { label: showComponents, name: "COMPONENTS" }
      ];

      this.columns[quantityColumnIndex].typeAttributes.showDropdown = false;
    }

    // Build list of columns to remove based on this condition
    if (this.loadData === "Quotes") {
      columnsToRemove = [
        "imageUrl",
        "productSkuCode",
        "productName",
        "listPrice",
        "netPrice",
        "quantity",
        "subTotal",
        "action",
        "wishlistItemAddedDate",
        "cartId"
      ];

      this.columns[actionColumnIndex].typeAttributes.rowActions = [
        { label: this.labels.cloneCart, name: "CLONECART" },
        {
          label: this.labels.viewCartItems,
          name: "VIEWCARTITEMS"
        }
      ];
    }

    // Build list of columns to remove based on this condition
    if (this.loadData === "Orders") {
      this.columns = columnsOrders;
      columnsToRemove = [];
    }

    if(this.loadData === 'Order Detail'){
      this.columns = columnsOrderDetail;
      columnsToRemove = [];
    }

    // Remove the columns built conditionally
    columnsToRemove.forEach(x => {
      const idx = this.columns.findIndex(y => y.fieldName === x);

      console.log("column to remove: ", idx, this.columns[idx]);

      if (idx !== -1) {
        this.columns.splice(idx, 1);
      }
    });

    console.log('this.columns: ', this.columns);
  }

  loadCartData() {
    if (this.loadData === "My Cart") {
      this.executeGetCurrentCart();
    }

    if (this.loadData === "Wish List") {
      this.executeGetWishlistItems();
    }

    if (this.loadData === "Quotes") {
      this.executeGetQuotes();
    }

    if (this.loadData === "Submitted Cart") {
      const cartId = this.recordId.substring(0, 15);
      console.log("Submitted Cart: ", cartId);

      this.handleHideSpinner();
      this.executeGetCartById(cartId, "Submitted Cart");
    }

    if (this.loadData === "Shared Cart") {
      const params = getQueryParameters();
      console.log("Shared Cart: ", params);

      this.handleHideSpinner();
      this.executeGetCartById(params.id, "Shared Cart");
    }

    if (this.loadData === "Orders") {
      console.log("Orders");

      this.handleHideSpinner();
      this.executeGetOrders();
    }

    if (this.loadData === "Order Detail") {
      this.executeGetOrderItems();
    }
  }

  setData(data, setItems = false) {
    if (setItems) {
      this.data = data;
      this.dataCopy = JSON.parse(JSON.stringify(data));
      console.log('this.data: ', this.data);

    } else {
      console.log("setting data in .items");

      this.data = data.items;
      this.dataCopy = JSON.parse(JSON.stringify(data.items));
      this.cartId = data.cartId;
      this.sapNumber = data.customerSAPNumber;
      this.currencyCode = data.currencyISOCode;
    }
  }

  executeGetCurrentCart() {
    getCurrentCart()
      .then(res => {
        console.log("getCurrentCart: ", res);
        const newData = this.checkForObsoleteProducts(res);
        this.setData(newData);

        const obsoleteItemsExistInCart =
          this.data.filter(x => x.isObsolete).length > 0;
        console.log("obsoleteItemsExistInCart: ", obsoleteItemsExistInCart);
      })
      .then(() => {
        this.initCartSummaryTotal();
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {
        this.handleHideSpinner();
        this.checkEmptyState();
      });
  }

  executeGetWishlistItems() {
    getWishlistItems()
      .then(res => {
        console.log("getWishlistItems: ", res);
        this.setData(res);
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {
        this.handleHideSpinner();
        this.checkEmptyState();
      });
  }

  executeGetQuotes() {
    getQuotes()
      .then(res => {
        console.log("getQuotes: ", res);
        this.setData(res, true);
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {
        this.handleHideSpinner();
        this.checkEmptyState();
      });
  }

  executeGetCartById(id, type) {
    console.log("id: ", id, " and type: ", type);
    getCartById({ cartId: id, cartType: type })
      .then(res => {
        console.log("getCartById: ", res);
        const newData = this.checkForObsoleteProducts(res);
        this.setData(newData);
        // publish(this.context, ALTMC, { message: this.cartId });
      })
      .then(() => {
        this.initCartSummaryTotal();
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {
        this.handleHideSpinner();
        this.checkEmptyState();
        this.obsoleteProductsList = []; // empty out array that getter uses to show/hide obsolete product card
      });
  }

  executeGetOrders() {
    getOrders()
      .then(res => {
        console.log("getOrders: ", res);
        this.setData(res, true);
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {
        this.handleHideSpinner();
        this.checkEmptyState();
      });
  }

  executeGetOrderItems() {
    console.log("inside executeGetOrderItems");

    getOrderItems()
      .then(res => {
        console.log("getOrderItems: ", res);
        const parsed = JSON.parse(JSON.stringify(res));
        parsed.shift();
        this.setData(parsed, true);
      })
      .catch(err => {
        console.error(err);
      })
      .finally(() => {
        this.handleHideSpinner();
        this.checkEmptyState();
      });
  }

  checkForObsoleteProducts(data) {
    console.log("data: ", data);
    const copyData = Object.assign({}, data);

    const newData = copyData.items.map(x => {
      let newItem = Object.assign({}, x);

      if (x.listPrice === 0 && x.netPrice === 0) {
        newItem = {
          isObsolete: true,
          quantity: 0,
          ...x
        };

        this.obsoleteProductsList.push(newItem);
      }

      return newItem;
    });

    copyData.items = newData;
    console.log("newData: ", newData);
    console.log("this.obsoleteProductsList: ", this.obsoleteProductsList);

    return copyData;
  }

  // Notify the Cart Summary Component via this message channel to handle the message payload
  initCartSummaryTotal() {
    const payload = {
      message: "initSummaryTotal",
      data: this.data,
      sapNumber: this.sapNumber,
      currencyCode: this.currencyCode
    };
    // publish(this.context, ALTMC, {
    //   message: "initSummaryTotal",
    //   data: this.data,
    //   sapNumber: this.sapNumber,
    //   currencyCode: this.currencyCode
    // });
    fireEvent("handleCartSummaryListener", payload);
  }

  checkEmptyState() {
    if (this.data.length === 0) {
      this.showEmptyState = true;
    }
  }

  handleRowAction(event) {
    let actionName = event.detail.action.name;
    let row = event.detail.row;
    console.log("actionName: ", actionName);
    console.log("row: ", JSON.parse(JSON.stringify(row)));
    switch (actionName) {
      case "DELETE":
        // Call method to delete item from DB based on type of DataList and then remove it from the table
        this.handleRemoveItemFromDB([row], this.loadData);
        break;
      case "WISHLIST":
        // Call method to add item to wishlist and then remove it from the table
        this.handleAddToWishlist(row);
        console.log("add to wishlist");
        break;
      case "CLONECART":
        // Call method to show modal and confirm clone cart
        this.handleCloneCartModal(row);
        console.log("open clone cart modal");
        break;
      case "CART":
        // Call method to add item to cart and then remove it from the table, false as second param is bool for copy control
        this.handleAddToCart(row, false);
        console.log("add to cart");
        break;
      case "COPYCART":
        // Call method to add item to cart and DO NOT remove it from the table, true as second param is bool for copy control
        this.handleAddToCart(row, true);
        console.log("add to cart");
        break;
      case "COMPONENTS":
        this.handleComponentsModal(row);
        console.log("show components modal");
        break;
      case "VIEWCARTITEMS":
        this.navigateToSubmittedCartPage(row);
        break;
      default:
    }
  }

  navigateToSubmittedCartPage(row) {
    const { relatedCartId } = row;
    console.log("relatedCartId: ", relatedCartId);

    const pageReferenceObject = {
      type: "standard__recordPage",
      attributes: {
        recordId: relatedCartId,
        objectApiName: "CC_Cart_Detail__c",
        actionName: "view"
      }
    };

    this[NavigationMixin.Navigate](pageReferenceObject);
  }

  handleComponentsModal(row) {
    const {
      components,
      productName,
      productSkuCode,
      configuratorCopyURL,
      configuratorModifyURL,
      zipURL
    } = row;

    if (components) {
      this.components = components;

      this.currentProductComponent = {
        copyUrl: configuratorCopyURL,
        modifyUrl: configuratorModifyURL,
        zipUrl: zipURL
      }

      this.productNameComponentModal = productName;
      this.productComponentModalFormattedMessage = X7sIntlService.format(
        this.labels.componentsModalMessage,
        productSkuCode
      );

      this.showComponentsModal = true;
    } else {
      const toast = new ShowToastEvent({
        title: this.labels.infoLabel,
        message: this.labels.noComponentsMessage,
        variant: "info",
        messageData: [productName]
      });

      this.dispatchEvent(toast);
    }
  }

  closeCloneCartModal() {
    this.showCloneCartModal = false;
  }

  closeComponentsModal() {
    this.showComponentsModal = false;
    this.components = [];
    this.currentProductComponent = {
      copyUrl: '',
      modifyUrl: '',
      zipUrl: ''
    }
  }

  handleCloneCartModal(row) {
    const { relatedCartId } = row;
    this.cartIdToClone = relatedCartId ? relatedCartId : this.recordId;
    console.log("handleCloneCartModal cartId: ", this.cartIdToClone);

    this.showCloneCartModal = true;
  }

  executeCloneCart() {
    console.log("executeCloneCart");
    this.showSpinnerModal = true;
    let toast;

    cloneSubmittedCart({ submittedCartId: this.cartIdToClone })
      .then(res => {
        console.log("cloneSubmittedCart (): ", res);

        if (res) {
          toast = new ShowToastEvent({
            title: this.labels.success,
            message: this.labels.cloneCartMessage,
            variant: "success",
            messageData: [this.cartIdToClone]
          });
        }
      })
      .catch(err => {
        console.error(err);

        toast = new ShowToastEvent({
          title: this.labels.errorLabel,
          message: this.labels.errorMessage,
          variant: "error",
          messageData: [err]
        });
      })
      .finally(() => {
        this.dispatchEvent(toast);
        this.showCloneCartModal = false;
        this.showSpinnerModal = false;
      });
  }

  handleDeleteRowUi(row) {
    const { id } = row;
    console.log("id: ", id);

    const idx = this.findRowIndexByItemNumber(id);
    if (idx !== -1) {
      this.data = this.data.slice(0, idx).concat(this.data.slice(idx + 1));
    }
  }

  handleRemoveItemFromDB(rows, typeOfDataList) {
    this.handleShowSpinner();
    let ids = rows.map(x => x.id);
    console.log("handleRemoveItemFromDB ids: ", ids);

    if (typeOfDataList === "My Cart") {
      const payload = {
        message: "triggerSpinnerShow"
      };
      fireEvent("handleCartSummaryListener", payload);

      removeCartItem({
        cartId: this.cartId,
        cartItemIdList: ids
      })
        .then(res => {
          console.log("removeCartItem: ", res);

          if (res) {
            rows.forEach(row => {
              this.handleDeleteRowUi(row);
              this.handleUpdateObsoleteProductCart(row);
            });
          }
        })
        .catch(err => {
          console.error(err);
        })
        .finally(() => {
          const obsoleteItemsExistInCart =
            this.data.filter(x => x.isObsolete).length > 0;
          console.log("obsoleteItemsExistInCart: ", obsoleteItemsExistInCart);

          this.handleHideSpinner();

          const payload1 = {
            message: "updateSummaryTotal",
            data: this.data
          };
          fireEvent("handleCartSummaryListener", payload1);

          const payload3 = {
            message: "triggerSpinnerHide"
          };
          fireEvent("handleCartSummaryListener", payload3);

          this.checkEmptyState();
        });
    }

    if (typeOfDataList === "Wish List") {
      console.log(
        "start wish list remove from db condition: ",
        JSON.parse(JSON.stringify(rows))
      );

      // For Wish List get all ids of child components to pass in to method, since CC API doesnt' remove children like CC Cart API does
      rows.forEach(x => {
        if (x.components) {
          x.components.forEach(y => {
            ids.push(y.id);
          });
        }
      });

      console.log("next");

      removeWishListItem({ wishListItemId: ids })
        .then(res => {
          console.log("wishListItemId: ", res);

          if (res) {
            rows.forEach(row => {
              this.handleDeleteRowUi(row);
            });
          }
        })
        .catch(err => {
          console.error(err);
        })
        .finally(() => {
          this.handleHideSpinner();

          // publish(this.context, ALTMC, {
          //   message: "updateSummaryTotal",
          //   data: this.data
          // });

          const payload1 = {
            message: "updateWishlistItemTotal"
          };
          fireEvent("handleHeaderGreetingListener", payload1);

          const payload2 = {
            message: "updateSummaryTotal",
            data: this.data
          };
          fireEvent("handleCartSummaryListener", payload2);
        });
    }
  }

  handleUpdateObsoleteProductCart(row) {
    const { id } = row;
    this.obsoleteProductsList = this.obsoleteProductsList.filter(
      x => x.id !== id
    );
  }

  handleAddToWishlist(row) {
    const { id, productName, quantity, isObsolete } = row;

    console.log("cart id: ", this.cartId);
    console.log("id: ", id);
    console.log("productName: ", productName);
    console.log("quantity: ", quantity);

    let toast;

    if (isObsolete) {
      toast = new ShowToastEvent({
        title: this.labels.errorLabel,
        message: this.labels.cannotAddToWishlist,
        variant: "error",
        messageData: [productName]
      });

      this.dispatchEvent(toast);
    } else {
      this.handleShowSpinner();

      addCartItemToWishlist({
        cartId: this.cartId,
        cartItemId: id
      })
        .then(res => {
          console.log("addCartItemToWishlist(): ", res);

          if (res) {
            this.handleDeleteRowUi(row);

            toast = new ShowToastEvent({
              title: this.labels.success,
              message: this.labels.addToWishlist,
              variant: "success",
              messageData: [productName]
            });
          }
        })
        .catch(err => {
          console.error(err);

          toast = new ShowToastEvent({
            title: this.labels.errorLabel,
            message: this.labels.errorMessage,
            variant: "error",
            messageData: [err]
          });
        })
        .finally(() => {
          this.dispatchEvent(toast);
          this.handleHideSpinner();

          // publish(this.context, ALTMC, {
          //   message: "updateSummaryTotal",
          //   data: this.data
          // });

          const payload = {
            message: "updateSummaryTotal",
            data: this.data
          };
          fireEvent("handleCartSummaryListener", payload);
        });
    }
  }

  /*
  @param Object row = the row with data for the product where action was clicked
  @param Boolean copy = the value which controls whether or not to copy the item and deleting the item from the wishlist
  */
  handleAddToCart(row, copy) {
    this.handleShowSpinner();
    const { id, productName } = row;
    console.log("wishlistId: ", this.cartId);
    console.log("wishlistItemId: ", id);

    let toast;

    addWishlistItemToCart({
      wishlistId: this.cartId,
      wishlistItemId: id,
      isCopyWishlistItem: copy
    })
      .then(res => {
        console.log("addWishlistItemToCart(): ", res);

        if (res) {
          // If we do not want to copy and move it to the cart from wishlist, then remove it from the UI
          // as if we want to copy then the item should remain in the wishlist so user can copy again if they want to
          if (!copy) {
            this.handleDeleteRowUi(row);
          }

          toast = new ShowToastEvent({
            title: this.labels.success,
            message: this.labels.addToCart,
            variant: "success",
            messageData: [productName]
          });
        }
      })
      .catch(err => {
        console.error(err);

        toast = new ShowToastEvent({
          title: this.labels.errorLabel,
          message: this.labels.errorMessage,
          variant: "error",
          messageData: [err]
        });
      })
      .finally(() => {
        this.dispatchEvent(toast);
        this.handleHideSpinner();
      });
  }

  handleCleanCart() {
    console.log("this.obsoleteProductsList: ", this.obsoleteProductsList);
    this.handleRemoveItemFromDB(this.obsoleteProductsList, this.loadData);
  }

  findRowIndexByItemNumber(id) {
    let ret = -1;

    this.data.some((row, index) => {
      if (row.id === id) {
        ret = index;
        return true;
      }
      return false;
    });

    return ret;
  }

  setDefaultSort() {
    this.sortedBy = "id";
    this.sortDirection = "asc";
  }

  doDelete(event) {
    const rowId = event.detail;
    console.log(rowId);
  }

  doUpdateQuantity(event) {
    // publish(this.context, ALTMC, {
    //   message: "triggerSpinnerShow"
    // });

    const payload = {
      message: "triggerSpinnerShow"
    };
    fireEvent("handleCartSummaryListener", payload);

    this.handleShowSpinner();
    let toast;
    const value = event.detail.value;
    const id = event.detail.id;
    console.log("value: ", value);
    console.log("id: ", id);

    const rowIndex = this.data.findIndex(x => x.id === id);
    const row = this.data.filter(x => x.id === id)[0];

    console.log("rowIndex: ", rowIndex);
    console.log("row: ", row);

    updateCartItemQuantity({
      cartItemId: id,
      cartItemQuantity: value
    })
      .then(res => {
        console.log("updateCartItemQuantity: ", res);

        if (res) {
          const quantity = parseFloat(value);
          const newSubtotal = eval(quantity + " * " + res.netPrice);

          row.quantity = quantity;
          row.listPrice = res.listPrice;
          row.netPrice = res.netPrice;
          row.subTotal = Number(Math.round(newSubtotal + "e" + 2) + "e-" + 2); // have to do this to get a number instead of a string when fixing floating point number
          console.log("row.subTotal: ", row.subTotal);

          this.data = this.data
            .slice(0, rowIndex)
            .concat(this.data.slice(rowIndex));

          toast = new ShowToastEvent({
            title: this.labels.success,
            message: this.labels.quantityUpdated,
            variant: "success"
          });

          // publish(this.context, ALTMC, {
          //   message: "updateSummaryTotal",
          //   data: this.data
          // });

          const payload = {
            message: "updateSummaryTotal",
            data: this.data
          };
          fireEvent("handleCartSummaryListener", payload);
        } else {
          const err = "Quantity did not update properly";

          toast = new ShowToastEvent({
            title: this.labels.errorLabel,
            message: this.labels.errorMessage,
            variant: "error",
            messageData: [err]
          });
        }
      })
      .catch(err => {
        console.error(err);

        toast = new ShowToastEvent({
          title: this.labels.errorLabel,
          message: this.labels.errorMessage,
          variant: "error",
          messageData: [err]
        });
      })
      .finally(() => {
        this.dispatchEvent(toast);
        this.handleHideSpinner();
        // publish(this.context, ALTMC, {
        //   message: "triggerSpinnerHide"
        // });

        const payload = {
          message: "triggerSpinnerHide"
        };
        fireEvent("handleCartSummaryListener", payload);
      });
  }

  /**
   * on table column click, send column and direction to sortData()
   * @param event
   */
  doSorting(event) {
    this.sortedBy = event.detail.fieldName;
    this.sortedDirection = event.detail.sortDirection;
    this.sortData(this.sortedBy, this.sortedDirection);
  }

  /**
   * returns filteredData sorted by fieldName and direction
   * @param fieldName
   * @param direction
   */
  sortData(fieldName, direction) {
    console.log("fieldName: ", fieldName);
    console.log("direction: ", direction);

    let parseData = JSON.parse(JSON.stringify(this.data));
    console.log("parseData: ", parseData);
    // Return the value stored in the field
    const keyValue = a => {
      return a[fieldName];
    };
    const getValue = a => {
      return a.value;
    };

    // checking reverse direction
    let isReverse = direction === "asc" ? 1 : -1;

    // sorting data
    parseData.sort((x, y) => {
      x = keyValue(x) ? keyValue(x) : ""; // handling null values
      y = keyValue(y) ? keyValue(y) : "";

      x = typeof x === "object" ? getValue(x) : x;
      y = typeof y === "object" ? getValue(y) : y;

      console.log("--------------------");
      console.log("x is typeof: ", typeof x, x);
      console.log("y is typeof: ", typeof y, y);

      // sorting values based on direction
      return isReverse * ((x > y) - (y > x));
    });

    this.data = parseData;
    console.log("this.data: ", this.data);

    // publish(this.context, COLUMNMC, {
    //   message: "updateDateColumn"
    // });

    const payload = {
      message: "updateDateColumn"
    };
    fireEvent("handleCartSummaryListener", payload);
  }

  handleShareCart() {
    this.showShareCartModal = true;
  }

  closeShareCartModal() {
    this.showShareCartModal = false;
  }

  handleShareEmailChange(event) {
    this.shareEmail = event.target.value;
  }

  executeShareCart() {
    console.log("this.shareEmail: ", this.shareEmail);
    console.log("this.cartId: ", this.cartId);
    let toast;
    this.showSpinnerModal = true;

    shareCart({
      shareEmailAddress: this.shareEmail,
      cartId: this.cartId
    })
      .then(res => {
        if (res) {
          toast = new ShowToastEvent({
            title: this.labels.success,
            message: this.labels.successShareCart,
            variant: "success"
          });

          this.showShareCartModal = false;
          this.shareEmail = "";
        } else {
          toast = new ShowToastEvent({
            title: this.labels.errorLabel,
            variant: "error"
          });
        }
      })
      .catch(err => {
        console.error(err);

        toast = new ShowToastEvent({
          title: this.labels.errorLabel,
          message: this.labels.errorMessage,
          variant: "error",
          messageData: [err]
        });
      })
      .finally(() => {
        this.dispatchEvent(toast);
        this.showSpinnerModal = false;
      });
  }

  filterDataTable(event) {
    const value = event.target.value.toLowerCase();
    console.log("filterDataTable: ", value);
    console.log("this.loadData: ", this.loadData);

    let newData;

    if (this.loadData === "Quotes") {
      newData = this.dataCopy.filter(x => {
        return x.quoteNumber
          .toString()
          .toLowerCase()
          .includes(value);
      });
    }

    if (this.loadData === "Orders") {
      newData = this.dataCopy.filter(x => {
        return x.orderNumber
          .toString()
          .toLowerCase()
          .includes(value);
      });
    }

    if (
      this.loadData === "My Cart" ||
      this.loadData === "Wish List" ||
      this.loadData === "Shared Cart" ||
      this.loadData === "Submitted Cart"
    ) {
      newData = this.dataCopy.filter(x => {
        return (
          x.productName
            .toString()
            .toLowerCase()
            .includes(value) ||
          x.productSkuCode
            .toString()
            .toLowerCase()
            .includes(value)
        );
      });
    }

    this.data = newData;

    console.log("newData: ", newData);
  }

  handleShowSpinner() {
    this.showSpinner = true;
  }

  handleHideSpinner() {
    this.showSpinner = false;
  }

  handleDoneUpdating(data) {
    console.log(data);
  }

  handleQuoteSubmitSuccess() {
    this.data = [];
    this.checkEmptyState();
  }
}