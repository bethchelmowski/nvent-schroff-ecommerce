const columnsOrderDetail = [
  {
    label: "SKU",
    type: "plaintextColumn",
    fieldName: "skuNumber",
    sortable: true,
    fixedWidth: 125,
    typeAttributes: {
      plaintext: { fieldName: "skuNumber" }
    }
  },
  {
    label: "Product Name",
    type: "titleColumn",
    fieldName: "productName",
    sortable: true,
    fixedWidth: 125,
    typeAttributes: {
      productName: { fieldName: "productName" }
    }
  },
  {
    label: "Order Quantity",
    type: "quantityColumn",
    fieldName: "quantityOrder",
    sortable: true,
    fixedWidth: 125,
    typeAttributes: {
      quantity: { fieldName: "quantityOrder" },
      showDropdown: false
    }
  },
  {
    label: "Cancelled Quantity",
    type: "quantityColumn",
    fieldName: "quantityCancelled",
    sortable: true,
    fixedWidth: 125,
    typeAttributes: {
      quantity: { fieldName: "quantityCancelled" },
      showDropdown: false
    }
  },
  {
    label: "Shipped Quantity",
    type: "quantityColumn",
    fieldName: "quantityShipped",
    sortable: true,
    fixedWidth: 150,
    typeAttributes: {
      quantity: { fieldName: "quantityShipped" },
      showDropdown: false
    }
  },
  {
    label: "Promised Date",
    type: "dateColumn",
    fieldName: "promisedDate",
    sortable: true,
    typeAttributes: {
      dateValue: { fieldName: "promisedDate" }
    },
    cellAttributes: {
      class: "nvent-datatable-date-column"
    },
    fixedWidth: 100
  },
  {
    label: "Ship Date",
    type: "dateColumn",
    fieldName: "shipDate",
    sortable: true,
    typeAttributes: {
      dateValue: { fieldName: "shipDate" }
    },
    cellAttributes: {
      class: "nvent-datatable-date-column"
    },
    fixedWidth: 100
  },
  {
    label: "Status",
    type: "plaintextColumn",
    fieldName: "status",
    sortable: true,
    typeAttributes: {
      plaintext: { fieldName: "status" }
    }
  },
  {
    label: "Tracking Number",
    type: "plaintextColumn",
    fieldName: "trackingNumber",
    sortable: true,
    typeAttributes: {
      plaintext: { fieldName: "trackingNumber" }
    }
  }
];

export { columnsOrderDetail };