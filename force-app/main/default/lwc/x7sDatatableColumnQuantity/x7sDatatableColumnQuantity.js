import { LightningElement, api } from "lwc";

const NUMBER_OF_QUANTITY_OPTIONS = 20;

export default class X7sDatatableColumnQuantity extends LightningElement {
  @api rowId;
  @api quantity;
  options = [];

  connectedCallback() {
    this.setOptions();
    this.setQuantityValue();
  }

  setOptions() {
    for (let i = 1; i <= NUMBER_OF_QUANTITY_OPTIONS; i++) {
      this.options.push({
        label: i.toString(),
        value: i.toString(),
        selected: false
      });
    }
  }

  setQuantityValue() {
    this.quantity = this.quantity ? this.quantity.toString() : "";

    for (let i = 0; i < this.options.length; i++) {
      if (this.options[i].value === this.quantity) {
        this.options[i].selected = true;
        break;
      }
    }
  }

  handleQuantityChange(event) {
    const quantity = event.target.value;
    const id = this.rowId;
    const updateQuantityEvent = CustomEvent("updatequantity", {
      composed: true,
      bubbles: true,
      cancelable: true,
      detail: {
        value: quantity,
        id: id
      }
    });

    this.quantity = quantity;
    this.dispatchEvent(updateQuantityEvent);
  }
}
