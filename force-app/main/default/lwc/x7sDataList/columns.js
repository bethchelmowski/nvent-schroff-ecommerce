import itemNumber from "@salesforce/label/c.nVent_DataTable_Headings_Item_Number";
import itemDescription from "@salesforce/label/c.nVent_DataTable_Headings_Item_Description";
import listPrice from "@salesforce/label/c.nVent_DataTable_Headings_List_Price";
import yourPrice from "@salesforce/label/c.nVent_DataTable_Headings_Your_Price";
import quantity from "@salesforce/label/c.nVent_DataTable_Headings_Quantity";
import actions from "@salesforce/label/c.nVent_DataTable_Headings_Actions";
import subtotal from "@salesforce/label/c.nVent_DataTable_Headings_Subtotal";
import cartId from "@salesforce/label/c.nVent_DataTable_Headings_Cart_Id";
import quoteNumber from "@salesforce/label/c.nVent_DataTable_Headings_Quote_Number";
import date from "@salesforce/label/c.nVent_DataTable_Headings_Date";
import dateSubmitted from "@salesforce/label/c.nVent_DataTable_Headings_Date_Submitted";
import distinctSKUs from "@salesforce/label/c.nVent_DataTable_Headings_Distinct_SKUs";
import totalAmount from "@salesforce/label/c.nVent_DataTable_Headings_Total_Amount";
import deleteLabel from "@salesforce/label/c.nVent_DataTable_Delete";
import moveToWishlist from "@salesforce/label/c.nVent_DataTable_Move_to_Wishlist";
import showComponents from "@salesforce/label/c.nVent_DataTable_Show_Components";
import quoteStatus from "@salesforce/label/c.nVent_DataTable_Quote_Status";

const columns = [
  {
    label: '',
    type: "imageColumn",
    fieldName: "imageUrl",
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      imageUrl: { fieldName: "imageUrl" }
    },
    cellAttributes: { alignment: "center" },
    fixedWidth: 75
  },
  {
    label: itemNumber,
    type: "titleColumn",
    fieldName: "productSkuCode",
    sortable: true,
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      productSkuCode: { fieldName: "productSkuCode" }
    },
    fixedWidth: 100
  },
  {
    label: itemDescription,
    type: "titleColumn",
    fieldName: "productName",
    sortable: true,
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      productName: { fieldName: "productName" },
      productLeadTime: { fieldName: "productLeadTime" }
    },
    wrapText: true,
    fixedWidth: 150
  },
  {
    label: cartId,
    fieldName: "cartId",
    sortable: true,
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      cartId: { fieldName: "cartId" }
    }
  },
  {
    label: quoteNumber,
    type: "titleColumn",
    fieldName: "quoteNumber",
    sortable: true,
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      cartId: { fieldName: "cartId" },
      cartNumber: { fieldName: "cartNumber" },
      quoteNumber: { fieldName: "quoteNumber" },
      quoteId: { fieldName: "quoteId" }
    }
  },
  {
    label: listPrice,
    type: "priceColumn",
    fieldName: "listPrice",
    sortable: true,
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      listPrice: { fieldName: "listPrice" },
      currencyISOCode: { fieldName: "currencyISOCode" }
    },
    fixedWidth: 90
  },
  {
    label: yourPrice,
    type: "priceColumn",
    fieldName: "netPrice",
    sortable: true,
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      netPrice: { fieldName: "netPrice" },
      currencyISOCode: { fieldName: "currencyISOCode" }
    },
    fixedWidth: 90
  },
  {
    label: dateSubmitted,
    type: "dateColumn",
    fieldName: "lastModifiedDate",
    sortable: true,
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      lastModifiedDate: { fieldName: "lastModifiedDate" }
    },
    cellAttributes: {
      class: "nvent-datatable-date-column"
    }
  },
  {
    label: date,
    type: "dateColumn",
    fieldName: "wishlistItemAddedDate",
    sortable: true,
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      wishlistItemAddedDate: { fieldName: "wishlistItemAddedDate" }
    },
    cellAttributes: {
      class: "nvent-datatable-date-column"
    }
  },
  {
    label: quoteStatus,
    fieldName: "quoteStatus",
    sortable: true,
    fixedWidth: 150
  },
  {
    label: distinctSKUs,
    fieldName: "distinctSKUs",
    sortable: true,
    fixedWidth: 150
  },
  {
    label: totalAmount,
    type: "priceColumn",
    fieldName: "totalAmount",
    sortable: true,
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      totalAmount: { fieldName: "totalAmount" },
      currencyISOCode: { fieldName: "currencyISOCode" }
    }
  },
  {
    label: quantity,
    type: "quantityColumn",
    fieldName: "quantity",
    sortable: true,
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      quantity: { fieldName: "quantity" },
      moq: { fieldName: "MOQ" },
      dls: { fieldName: "DLS" },
      showDropdown: true
    },
    fixedWidth: 85
  },
  {
    label: actions,
    type: "action",
    fieldName: "id",
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      rowActions: [
        { label: moveToWishlist, name: "WISHLIST" },
        { label: showComponents, name: "COMPONENTS" },
        { label: deleteLabel, name: "DELETE" }
      ],
      id: { fieldName: "id" }
    },
    cellAttributes: { alignment: "center" },
    fixedWidth: 70
  },
  {
    label: subtotal,
    type: "subtotalColumn",
    fieldName: "subTotal",
    sortable: true,
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" },
      subTotal: { fieldName: "subTotal" },
      currencyISOCode: { fieldName: "currencyISOCode" }
    },
    fixedWidth: 125
  },
  {
    label: actions,
    type: "actionColumn",
    fieldName: "action",
    typeAttributes: {
      isObsolete: {fieldName: "isObsolete"},
      id: { fieldName: "id" }
    }
  }
];

export { columns };