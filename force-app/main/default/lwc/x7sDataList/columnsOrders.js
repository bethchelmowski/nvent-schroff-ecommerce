const columnsOrders = [
  {
    label: "Order Number",
    type: "titleColumn",
    fieldName: "orderNumber",
    sortable: true,
    typeAttributes: {
      orderNumber: { fieldName: "orderNumber" },
      orderId: { fieldName: "orderId" }
    },
    fixedWidth: 125
  },
  {
    label: "Status",
    fieldName: "orderStatus",
    sortable: true,
    fixedWidth: 100
  },
  {
    label: "Total SKUs",
    fieldName: "distinctSKU",
    sortable: true,
    fixedWidth: 100
  },
  {
    label: "Delivery Address",
    fieldName: "deliveryAddress"
  },
  {
    label: "Order Date",
    type: "dateColumn",
    fieldName: "orderDate",
    sortable: true,
    typeAttributes: {
      orderDate: { fieldName: "orderDate" }
    },
    cellAttributes: {
      class: "nvent-datatable-date-column"
    },
    fixedWidth: 100
  }
];

export { columnsOrders };