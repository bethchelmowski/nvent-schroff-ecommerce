import itemNumber from "@salesforce/label/c.nVent_DataTable_Headings_Item_Number";
import itemDescription from "@salesforce/label/c.nVent_DataTable_Headings_Item_Description";
import quantity from "@salesforce/label/c.nVent_DataTable_Headings_Quantity";

const componentColumns = [
  {
    label: itemNumber,
    type: "titleColumn",
    fieldName: "productSkuCode",
    typeAttributes: {
      id: { fieldName: "id" },
      productSkuCode: { fieldName: "productSkuCode" }
    }
  },
  {
    label: itemDescription,
    type: "titleColumn",
    fieldName: "productDescription",
    typeAttributes: {
      id: { fieldName: "id" },
      productDescription: { fieldName: "productDescription" }
    },
    wrapText: true
  },
  {
    label: quantity,
    type: "quantityColumn",
    fieldName: "quantity",
    typeAttributes: {
      id: { fieldName: "id" },
      quantity: { fieldName: "quantity" },
      moq: { fieldName: "MOQ" },
      dls: { fieldName: "DLS" }
    },
    fixedWidth: 85
  }
];

export { componentColumns };