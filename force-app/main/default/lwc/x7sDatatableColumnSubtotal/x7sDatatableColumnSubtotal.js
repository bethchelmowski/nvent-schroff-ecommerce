import { LightningElement, api } from 'lwc';

export default class X7sDatatableColumnSubtotal extends LightningElement {
    @api subTotal;
    @api currencyCode;
}