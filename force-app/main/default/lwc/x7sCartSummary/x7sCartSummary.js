/**
 * Created by might on 5/19/2020.
 */

import { LightningElement, wire, api } from "lwc";
import { getRecord } from "lightning/uiRecordApi";
import { ShowToastEvent } from "lightning/platformShowToastEvent";

// Pub Sub
import {
  registerListener,
  unregisterAllListeners,
  fireEvent
} from "c/x7sPubSub";

// Apex Methods
import requestQuote from "@salesforce/apex/x7S_CartActionController.requestQuote";

// Custom Labels
import yourPrice from "@salesforce/label/ccrz.ProductDetails_HRelated_ProductWidget_Your_Price";
import listPrice from "@salesforce/label/ccrz.ProductDetails_ListPrice";
import requestQuoteLabel from "@salesforce/label/c.nVent_Request_Quote";
import generateQuoteLabel from "@salesforce/label/c.nVent_Generate_Quote";
import requestQuoteModalMessage from "@salesforce/label/c.nVent_Request_Quote_Modal_Message";
import generateQuoteModalMessage from "@salesforce/label/c.nVent_Generate_Quote_Modal_Message";
import errorLabel from "@salesforce/label/c.nVent_DataTable_Error";
import errorQuoteMessage from "@salesforce/label/c.nVent_DataTable_Error_Quote_Message";

// User Imports
import USER_ID from "@salesforce/user/Id";
import EMAIL_FIELD from "@salesforce/schema/User.Email";

// Utility for Replacing Placeholder in Custom Label
import { X7sIntlService } from "c/x7sIntlService";

export default class X7SCartSummary extends LightningElement {
  @api recordId;
  @api showQuoteButton;

  yourPriceValue;
  yourPriceCurrency;
  listPriceValue;
  listPriceCurrency;
  userEmail;
  currencyCode;

  isModalOpen = false;
  showSpinner = true;
  showSAPVersion = false;

  labels = {
    yourPrice,
    listPrice,
    requestQuoteLabel,
    generateQuoteLabel,
    requestQuoteModalMessage,
    generateQuoteModalMessage,
    errorLabel,
    errorQuoteMessage
  };

  @wire(getRecord, {
    recordId: USER_ID,
    fields: [EMAIL_FIELD]
  })
  wireuser({ error, data }) {
    if (error) {
      console.error("wireuser: ", error);
    } else if (data) {
      this.userEmail = data.fields.Email.value;
    }
  }

  get formattedGenerateQuoteModalMessage() {
    return X7sIntlService.format(
      this.labels.generateQuoteModalMessage,
      this.userEmail
    );
  }

  connectedCallback() {
    registerListener("handleCartSummaryListener", this.handleMessage, this);
  }

  disconnectedCallback() {
    unregisterAllListeners(this);
  }

  handleMessage(payload) {
    console.log("handleMessage payload: ", payload);
    const msg = payload.message;
    const data = payload.data;
    const sapNumber = payload.sapNumber;
    const obsValue = payload.obsoleteValue;

    this.currencyCode = payload.currencyCode;

    console.log("cart summary obsValue: ", obsValue);

    switch (msg) {
      case "triggerSpinnerShow":
        this.displaySpinner();
        break;
      case "triggerSpinnerHide":
        this.hideSpinner();
        break;
      case "initSummaryTotal":
        this.handleInitUpdateSummaryTotal(data, sapNumber);
        break;
      case "updateSummaryTotal":
        this.handleUpdateSummaryTotal(data);
        break;
      default:
        break;
    }
  }

  displaySpinner() {
    this.showSpinner = true;
  }

  hideSpinner() {
    this.showSpinner = false;
  }

  handleInitUpdateSummaryTotal(data, sapNumber) {
    console.log("----------handleInitUpdateSummaryTotal data: ", data);

    if (data) {
      console.log("sapNumber: ", sapNumber);

      this.calcSumOfSubTotals(data);
      this.calcListPrice(data);
      this.handleSAPNumber(sapNumber);
      this.hideSpinner();
    }

    if (data.length === 0) {
      this.showQuoteButton = false;
    }
  }

  handleUpdateSummaryTotal(data) {
    console.log("----------handleUpdateSummaryTotal data: ", data);
    this.calcSumOfSubTotals(data);
    this.calcListPrice(data);
    // publish(this.context, SAMPLEMC, { message: "doneUpdating", data: data });
    const payload = {
      message: "doneUpdating",
      data: data
    };
    fireEvent("handleDataListListener", payload);
  }

  calcSumOfSubTotals(data) {
    let sum = 0;
    let currency = "USD";

    data.forEach(item => {
      console.log("item.subtotal: ", item.subTotal);

      sum += item.subTotal ? item.subTotal : 0;
    });

    sum = Number(Math.round(sum + "e" + 2) + "e-" + 2); // have to do this to get a number instead of a string when fixing floating point number

    console.log("calcSumOfSubTotals sum: ", sum);
    this.yourPriceValue = sum.toString();
    this.yourPriceCurrency = currency;
  }

  calcListPrice(data) {
    let sum = 0;
    let currency = "USD";

    data.forEach(item => {
      const qty = item.quantity;
      const price = item.listPrice ? parseFloat(item.listPrice) : 0;

      sum += price * qty;
    });

    sum = Number(Math.round(sum + "e" + 2) + "e-" + 2); // have to do this to get a number instead of a string when fixing floating point number

    console.log("calcListPrice sum: ", sum);
    this.listPriceValue = sum.toString();
    this.listPriceCurrency = currency;
  }

  handleSAPNumber(sapNumber) {
    if (sapNumber !== "") {
      this.showSAPVersion = true;
    }
  }

  handleClick() {
    this.displaySpinner();
    // publish(this.context, SAMPLEMC, { message: "showCartSpinner" });
    const payload = {
      message: "showCartSpinner"
    };
    fireEvent("handleDataListListener", payload);

    requestQuote()
      .then(res => {
        console.log("requestQuote: ", res);
        if (res) {
          this.isModalOpen = true;
          this.listPriceValue = 0;
          this.yourPriceValue = 0;

          // publish(this.context, SAMPLEMC, {
          //   message: "quoteSubmittedSuccessfully"
          // });
          const payload = {
            message: "quoteSubmittedSuccessfully"
          };
          fireEvent("handleDataListListener", payload);
        }
      })
      .catch(err => {
        console.error(err);
        const toast = new ShowToastEvent({
          title: this.labels.errorLabel,
          message: this.labels.errorQuoteMessage,
          variant: "error"
        });

        this.dispatchEvent(toast);
      })
      .finally(() => {
        this.hideSpinner();
        // publish(this.context, SAMPLEMC, { message: "hideCartSpinner" });
        const payload = {
          message: "hideCartSpinner"
        };
        fireEvent("handleDataListListener", payload);
      });
  }

  closeModal() {
    this.isModalOpen = false;
  }
}
