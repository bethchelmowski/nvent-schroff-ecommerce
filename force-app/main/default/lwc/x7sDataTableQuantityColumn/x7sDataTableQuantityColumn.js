import { LightningElement, api } from "lwc";

const NUMBER_OF_QUANTITY_OPTIONS = 100;

export default class X7sDataTableQuantityColumn extends LightningElement {
  @api cartRowId;
  @api quantity;
  @api moq;
  @api dls;
  @api showDropdown;
  @api isObsolete;

  options = [];

  connectedCallback() {
    this.setOptions();
    this.setQuantityValue();
  }

  setOptions() {
    console.log("in set options");
    console.log("showDropdown: ", this.showDropdown);

    if (this.moq === 0) {
      console.log("moq = 0");

      for (let i = 1; i <= NUMBER_OF_QUANTITY_OPTIONS; i++) {
        this.options.push({
          label: i.toString(),
          value: i.toString(),
          selected: false
        });
      }
    } else if (this.moq) {
      console.log("moq != 0");
      let increment = this.dls;

      for (let i = 1; i <= NUMBER_OF_QUANTITY_OPTIONS; i++) {
        if (i === 1) {
          // console.log("increment: ", increment);

          this.options.push({
            label: this.moq.toString(),
            value: this.moq.toString(),
            selected: false
          });
        } else {
          // console.log("increment: ", increment);
          const limit = this.moq + increment;

          if (limit <= 100) {
            this.options.push({
              label: (this.moq + increment).toString(),
              value: (this.moq + increment).toString(),
              selected: false
            });
          }
          increment = increment + this.dls;
        }
      }
    }
  }

  get quantityTransformed() {
    return (this.quantity) ? this.quantity : 'n/a';
  }

  setQuantityValue() {
    if (this.quantity) {
      this.quantity = this.quantity ? this.quantity.toString() : "";

      for (let i = 0; i < this.options.length; i++) {
        if (this.options[i].value === this.quantity) {
          this.options[i].selected = true;
          break;
        }
      }
    }
  }

  handleChange(event) {
    console.log("inside x7sDataTableQuantityColumn handleChange");
    console.log("this.cartRowId: ", this.cartRowId);
    const qty = event.target.value;
    this.quantity = qty;
    const id = this.cartRowId;
    const e = CustomEvent("updatequantity", {
      composed: true,
      bubbles: true,
      cancelable: true,
      detail: {
        value: qty,
        id: id
      }
    });

    this.dispatchEvent(e);
  }
}