/**
 * Created by might on 5/13/2020.
 */

import { LightningElement, api } from "lwc";

export default class X7sDataTableImageColumn extends LightningElement {
  @api cartRowId;
  @api imageId = "";
  @api isObsolete;
}
