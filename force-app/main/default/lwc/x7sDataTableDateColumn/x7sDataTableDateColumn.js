import { LightningElement, api } from "lwc";

export default class X7sDataTableDateColumn extends LightningElement {
  @api lastModifiedDate;
  @api wishlistItemAddedDate;
  @api orderDate;
  @api dateValue;

  get dateTransformed() {
    console.log("orderDate: ", this.orderDate);
    let dateToUse = "";

    if (this.lastModifiedDate) dateToUse = this.lastModifiedDate;
    if (this.wishlistItemAddedDate) dateToUse = this.wishlistItemAddedDate;
    if (this.orderDate) dateToUse = this.orderDate;
    if (this.dateValue) dateToUse = this.dateValue;
    if (dateToUse === "") {
      return "n/a";
    }

    const date = new Date(dateToUse);
    const dateTimeFormat = new Intl.DateTimeFormat("en-US", {
      year: "numeric",
      month: "short",
      day: "2-digit",
      timeZone: "UTC"
    });
    let [
      { value: month },
      ,
      { value: day },
      ,
      { value: year }
    ] = dateTimeFormat.formatToParts(date);

    day = Number(day);

    if (day < 10) {
      day = "0" + day.toString();
    } else {
      day.toString();
    }

    return `${day} ${month} ${year}`;
  }
}
