/**
 * Created by might on 5/18/2020.
 */

import { LightningElement, api } from "lwc";

export default class X7sDataTableSubtotalColumn extends LightningElement {
  @api subtotal;
  @api currencyCode;
  @api isObsolete;

  strikeThroughClass = "";

  connectedCallback() {
    console.log("subtotal: ", this.subtotal);
    if (this.isObsolete) this.strikeThroughClass = "x7s-line-through";

    console.log("X7sDataTableSubtotalColumn: currencyCode", this.currencyCode);
  }
}
