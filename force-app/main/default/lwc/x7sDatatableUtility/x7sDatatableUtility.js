import LightningDatatable from "lightning/datatable";

import imageColumn from "./imageColumn.html";
import titleColumn from "./titleColumn.html";
import priceColumn from "./priceColumn.html";
import quantityColumn from "./quantityColumn.html";
import subtotalColumn from "./subtotalColumn.html";
import dateColumn from "./dateColumn.html";

export default class X7sDatatableUtility extends LightningDatatable {
  static customTypes = {
    imageColumn: {
      template: imageColumn,
      typeAttributes: ["imageUrl", "isObsolete"],
      standardCellLayout: true
    },
    titleColumn: {
      template: titleColumn,
      typeAttributes: [
        "productSkuCode",
        "productName",
        "productDescription",
        "cartNumber",
        "productLeadTime",
        "relatedCartId",
        "orderNumber",
        "orderId",
        "isObsolete",
        "quoteNumber",
        "quoteId"
      ],
      standardCellLayout: true
    },
    priceColumn: {
      template: priceColumn,
      typeAttributes: [
        "id",
        "listPrice",
        "netPrice",
        "totalAmount",
        "currencyISOCode",
        "isObsolete"
      ],
      standardCellLayout: true
    },
    dateColumn: {
      template: dateColumn,
      typeAttributes: ["id",
      "lastModifiedDate",
      "wishlistItemAddedDate",
      "isObsolete",
      "orderDate",
      "dateValue"],
      standardCellLayout: true
    },
    quantityColumn: {
      template: quantityColumn,
      typeAttributes: [
        "id",
        "quantity",
        "moq",
        "dls",
        "showDropdown",
        "isObsolete",
        "quantityOrder"
      ],
      standardCellLayout: true
    },
    subtotalColumn: {
      template: subtotalColumn,
      typeAttributes: ["id", "subTotal", "currencyISOCode", "isObsolete"],
      standardCellLayout: true
    }
  };
}
