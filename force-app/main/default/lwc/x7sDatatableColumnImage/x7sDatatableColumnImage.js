import { LightningElement, api } from "lwc";

export default class X7sDatatableColumnImage extends LightningElement {
  @api imageUrl;
  @api isObsolete;
}
