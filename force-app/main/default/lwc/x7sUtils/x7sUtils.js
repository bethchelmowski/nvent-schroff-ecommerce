const getQueryParameters = () => {
    const search = location.search.substring(1);
    let params = {};

    if (search) {
        params = JSON.parse('{"' + search
            .replace(/&/g, '","')
            .replace(/=/g, '":"') + '"}', (key, value) => {
            return key === "" ? value : decodeURIComponent(value)
        });
    }

    return params;
};

export {
    getQueryParameters
}