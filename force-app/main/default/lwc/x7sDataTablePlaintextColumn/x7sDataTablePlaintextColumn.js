/**
 * Created by jeff.schrab on 9/21/20.
 */

import {LightningElement, api} from 'lwc';

export default class X7SDataTablePlaintextColumn extends LightningElement {
	@api plaintext;

	get plaintextTransformed() {
		return (this.plaintext) ? this.plaintext : 'n/a';
	}
}