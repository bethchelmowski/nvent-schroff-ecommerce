import { LightningElement, api } from "lwc";

import { NavigationMixin } from "lightning/navigation";

import { X7sIntlService } from "c/x7sIntlService";

import leadTimeMessage from "@salesforce/label/c.nVent_DataTable_Lead_Time_Message";

const BASE_SCHROFF_URL = "https://schroff.nvent.com/en/schroff/";

export default class X7sDataTableTitleColumn extends NavigationMixin(
  LightningElement
) {
  @api productCode;
  @api productName;
  @api cartNumber;
  @api productLeadTime;
  @api relatedCartId;
  @api quoteNumber;
  @api quoteId;
  @api isObsolete;
  @api productDescription;
  @api orderNumber;
  @api orderId;

  strikeThroughClass = "";

  labels = {
    leadTimeMessage
  };

  connectedCallback() {
    console.log("productName: ", this.productName);

    if (this.isObsolete) this.strikeThroughClass = "x7s-line-through";
  }

  get leadTime() {
    return this.productLeadTime
      ? X7sIntlService.format(this.labels.leadTimeMessage, this.productLeadTime)
      : "";
  }

  handleClick() {
    const pageReferenceObject = {
      type: "standard__recordPage",
      attributes: {
        recordId: this.quoteId,
        objectApiName: "RFQ_Detail__c",
        actionName: "view"
      }
    };

    console.log("pageReferenceObject: ", pageReferenceObject);

    this[NavigationMixin.Navigate](pageReferenceObject);
  }

  handleClickProductCode() {
    const hasHypehn = this.productCode.toLowerCase().indexOf("-");
    let transformedProductCode;
    console.log("hasHypehn: ", hasHypehn);

    if (hasHypehn === -1) {
      transformedProductCode =
        this.productCode.slice(0, 5) + "-" + this.productCode.slice(5);
    }

    this[NavigationMixin.Navigate]({
      type: "standard__webPage",
      attributes: {
        url: `${BASE_SCHROFF_URL}${transformedProductCode}`
      }
    });
  }

  handleClickOrderNumber() {
    console.log("handleClickOrderNumber orderNumber: ", this.orderNumber);
    console.log("handleClickOrderNumber orderId: ", this.orderId);

    const pageReferenceObject = {
      type: "standard__recordPage",
      attributes: {
        recordId: this.orderId,
        objectApiName: "CC_Order_Detail__c",
        actionName: "view"
      }
    };

    this[NavigationMixin.Navigate](pageReferenceObject);
  }
}
