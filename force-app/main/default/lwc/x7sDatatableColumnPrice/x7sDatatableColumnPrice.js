import { LightningElement, api } from "lwc";

export default class X7sDatatableColumnPrice extends LightningElement {
  @api cartRowId;
  @api listPrice;
  @api netPrice;
  @api totalAmount;
  @api currencyCode;
  @api isObsolete;

  showListPrice = false;
  showNetPrice = false;
  showTotalAmount = false;
  strikeThroughClass = "";

  connectedCallback() {
    if (this.listPrice || this.listPrice === 0) {
      this.showListPrice = true;
    }

    if (this.netPrice || this.netPrice === 0) {
      this.showNetPrice = true;
    }

    if (this.totalAmount || this.totalAmount === 0) {
      this.showTotalAmount = true;
    }

    if (this.isObsolete) this.strikeThroughClass = "x7s-line-through";
  }
}
