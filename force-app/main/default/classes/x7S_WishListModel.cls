/**
 * Created By ankitSoni on 05/28/2020
 */
public with sharing class x7S_WishListModel {
    
    @AuraEnabled
    public String cartId,cartEncId;
    
	@AuraEnabled
	public Decimal totalQuantity;

	@AuraEnabled
	public List<x7S_WishListItemModel> items;

    public x7S_WishListModel() {
        cartId = '';
        cartEncId = '';
		items  = new List<x7S_WishListItemModel>();
    }
	
	public x7S_WishListItemModel findParentCartItem(String parentCartItemId){
		for (x7S_WishListItemModel item : items){
			if (item.id == parentCartItemId){
				return item;
			}
		}
		
		return null;
	}

}