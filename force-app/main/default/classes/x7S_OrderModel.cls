public class x7S_OrderModel {
    
    @AuraEnabled
    public String orderId , orderStatus , deliveryAddress;
    
    @AuraEnabled
    public Decimal orderNumber , distinctSKU , totalQuantity;

    @AuraEnabled
    public Date orderDate;
    
}
