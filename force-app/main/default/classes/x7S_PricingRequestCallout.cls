/**
 * Created By ankitSoni on 05/06/2020
 */

public class x7S_PricingRequestCallout {

    /*
     >TCD Headers<
     Client_Id - fc7229a635104f37ad444ba8249bbadc
     Client_Secret - 016AA80E8D6144dca41086bFC826AE45
     URL - https://api-np.nvent.com/wcs-rfq-order-dev/v1/pricing/quote
    */

    private static String clientId = System.Label.nVent_Pricing_Client_Id; //23830fdb7e8b457aa337e4336d4efd6a
    private static String clientSecret = System.Label.nVent_Pricing_Client_Secret; //0a4fC51F4Aa3498F8375935b02c4E934
    private static String baseURL = System.Label.nVent_Pricing_URL; //https://api-np.nvent.com/test/order2cash/v1/pricing/quote

    /**
     * Description : Get Pricing Info from API Callout to Mulesoft
     * @param : requestBodyJSON
     * @return : JSON response in String
     */
    public static String getPricingInfo(String requestBodyString) {

        String responseBody='',responseString = '';
        String httpStatusMessage = '';

        System.debug('Pricing Request Body - ' + requestBodyString);

        /** PricingResponse Wrapper */
        x7S_PricingResponse prResponse = new x7S_PricingResponse();

        /** HTTP Callout to Mulesoft 'POST' /pricing/quote */
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(baseURL);
        req.setMethod('POST');
        req.setHeader('client_id', clientId);
        req.setHeader('client_secret', clientSecret);
        req.setHeader('accept', 'application/json');
        req.setHeader('Content-Type', 'application/json');
        req.setBody(requestBodyString);

        HttpResponse response = http.send(req);
        Map < String, Object > errorMap = (Map < String, Object > ) JSON.deserializeUntyped(response.getBody());

        // Success Response
        if (response.getStatusCode() == 200) {
            responseBody = response.getBody();
            responseString = responseBody;//.replaceAll('\\n','');

            prResponse = x7S_PricingResponse.parse(responseBody);
            System.debug('Pricing Response Body Wrapper->'+ prResponse); // API response in form of Wrapper
        }
        // Error Responses from API Callout
        else {
            responseString = 'Callout Error';
            httpStatusMessage = 'HTTP Status : ' + response.getStatus() +
                                ' HTTP Status code : ' + response.getStatusCode() +
                                ' Message : ' + errorMap.get('message');
            System.debug(httpStatusMessage);
        }

        System.debug('Pricing Response Body JSON->' + responseString);

        return responseString;
    }

}