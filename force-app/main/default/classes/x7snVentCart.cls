/*
 * Created by 7Summits, Inc.
 * Last modified 06.05.2020
*/
@RestResource(urlMapping='/nVent/cart/*')
global with sharing class x7snVentCart {
    
    @httpGet
    global static void getCart(){
        //get the customerNumber from the request body
        RestRequest req = RestContext.request;
        String customerNumber = req.params.get('customerNumber');
        String cartType = req.params.get('cartType');
        String userId = req.params.get('userId');
        
        String getCartMessage , getCartId;
        Integer getCartItemCount;
        x7S_GetCartResponseWrapper getCartResponse = new x7S_GetCartResponseWrapper();

        //call method that fetches user's cart details
        List<ccrz__E_Cart__c> myCarts = [SELECT Id,of_Distinct_SKUs__c 
                                        FROM ccrz__E_Cart__c 
                                        WHERE ccrz__CartType__c =:cartType AND ccrz__User__c =:userId AND ccrz__ActiveCart__c = TRUE];
        //List<ccrz__E_Cart__c> myCarts = [SELECT of_Distinct_SKUs__c, ccrz__ActiveCart__c, ccrz__CartStatus__c, ccrz__TotalQuantity__c, ccrz__CartType__c, ccrz__TotalAmount__c,ccrz__User__c FROM ccrz__E_Cart__c WHERE ccrz__User__c ='005P0000004nIWO'];
        
        if(myCarts.size()>0){
            for(ccrz__E_Cart__c cart : myCarts){
                getCartId = cart.Id;
            }

            // Count Major cart items
            List<AggregateResult> aggregateResultList = [SELECT count(Id) itemCount FROM ccrz__E_CartItem__c WHERE ccrz__Cart__c =:getCartId AND ccrz__cartItemType__c = 'Major'];
            if(aggregateResultList != null && aggregateResultList.size() > 0){
                for(AggregateResult aggr : aggregateResultList){
                    getCartItemCount = (Integer) aggr.get('itemCount');
                }
        
            }

            getCartResponse.numberOfItems = getCartItemCount;

            getCartMessage = JSON.serializePretty(getCartResponse);

        } else{
            //no active cart
            getCartMessage='';
        }
        
        
        //put JSON in response body in order to prevent string encoding and escaping quotations in response
		RestContext.response.responseBody = getCartMessage!=null && getCartMessage!=''? Blob.valueOf(getCartMessage):null;
        
    }

    @httpPost
    global static void addToCart(){
        //get the cart items payload from the request body
        RestRequest req = RestContext.request;
    	Blob body = req.requestBody;
    	String requestString = body.toString();
        system.debug('Cart REQUEST >>> ' + requestString);

        String badRequestMessage = System.Label.nVent_Request_addToCart_Failure;
        String addToCartMessage;
        if(!Test.isRunningTest()){
            //call addSkuToCart method in x7S_PricingRequestCallout method that fetches pricing and adds the item to cart

            //validate the request
            if( x7S_RequestValidator.isValidRequest(requestString , 'cart') ){
                addToCartMessage  = x7S_AddToActionController.addSkuToCart(requestString);
            }
            else{
                addToCartMessage = '{"success":'+false+',"message":"'+badRequestMessage+'"}';
            }

        } else{
            Test.setMock(HttpCalloutMock.class, new x7S_PricingRequestCalloutMock());
            addToCartMessage = x7S_AddToActionController.addSkuToCartCall();
        }

        System.debug('addToCartMessage'+addToCartMessage);
        //put JSON in response body in order to prevent string encoding and escaping quotations in response
        RestContext.response.addHeader('Content-Type', 'application/json'); 
        RestContext.response.responseBody = addToCartMessage!=null && addToCartMessage!=''? Blob.valueOf(addToCartMessage):null;
        
    }
}