/**
* Created By ankitSoni on 05/27/2020
*/
public with sharing class x7S_CartItemModel {
    
    @AuraEnabled
	public String id, productId, productName, productSkuCode, currencyISOCode;
    
    @AuraEnabled
	public String imageUrl , itemURL;

	@AuraEnabled
    public Decimal listPrice, netPrice, quantity, subTotal ,MOQ ,DLS , productLeadTime;

    @AuraEnabled
    public Boolean isAvailable;

    //KIM Products item fields
    @AuraEnabled
    public String configuratorCopyURL, configuratorModifyURL, zipURL , configurationID , sapGUID;

    @AuraEnabled
    public Boolean isKIMProduct;


    @AuraEnabled
    public List<x7S_CartItemComponentModel> components;

}