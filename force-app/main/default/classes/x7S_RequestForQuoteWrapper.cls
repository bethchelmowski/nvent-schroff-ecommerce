/**
 * Created By ankitSoni on 06/30/2020
 */
public class x7S_RequestForQuoteWrapper {
    
	public String projectListId;
	public Long requestDate;
	public String customerNumber;
	public String firstName;
	public String lastName;
	public String jobTitle;
	public String companyName;
	public String street;
	public String city;
	public String zip;
	public String state;
	public String country;
	public String phone;
	public Object mobile;
	public String email;
	public String additionalComments;
	public String distributor;
	public String language;
	public String currencyCode;
	public List<Positions> positions;

	public class Positions {
		public Integer pos;
		public String itemNumber;
		public String description;
		public Double quantity;
		public Double listPrice;
		public Double netPrice;
		public String unitOfMeasure;
		public Double posTotal;
		public Double discount;
		public List<Components> components;
	}
    
    public class Components {
		public String itemNumber;
		public String description;
		public Double quantity;
		public String unitOfMeasure;
	}

	public static x7S_RequestForQuoteWrapper parse(String json) {
		return (x7S_RequestForQuoteWrapper) System.JSON.deserialize(json, x7S_RequestForQuoteWrapper.class);
	}
}
