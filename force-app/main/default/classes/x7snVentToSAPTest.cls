@isTest
public class x7snVentToSAPTest{
    
    @isTest
    static void testPricing(){
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/nVent';  //Request URL
        req.httpMethod = 'GET';//HTTP Request Type
        req.addParameter('customerNumber', '7000021793');
        req.addParameter('countryOfUser', 'DE');
        req.addParameter('itemNumber', '11527789');
        req.addParameter('quantity', '1.000');
        RestContext.request = req;
        RestContext.response= res;
        x7snVentToSAP.getPricing();
        Test.stopTest();
    }
}