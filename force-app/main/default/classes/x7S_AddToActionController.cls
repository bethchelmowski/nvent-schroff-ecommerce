/**
 * Created By ankitSoni on 08/18/2020
 */
public class x7S_AddToActionController {
    
    /**
     * Description : onClick for 'Add To Wishlist'button from Acquia
     * @param : requestBodyJSON consisting (customerNumber , quantity , item sku)
     * @return : add to cart message
     */
    public static String addToWishlist(String requestBodyString) {
        String addToWishlistMessage = '' , addToWishlistJSON;
        Integer addToWishlistItemCount;
        String currentWishlistId;

        Boolean addToWishlistSuccess;
        Boolean majorInsertSuccess = false , minorInsertSuccess = true;
        
        List<ccrz__E_CartItem__c> insertWishlistItem = new List<ccrz__E_CartItem__c>();
        List<ccrz__E_CartItem__c> insertComponentList = new List<ccrz__E_CartItem__c>();
        
        List<x7S_AddToWishlistWrapper.Components> componentList = new List<x7S_AddToWishlistWrapper.Components>();

        Map<String,Id> productSKUMap = new Map<String,Id>(); //get CC Products

        x7S_AddToWishlistWrapper inputWrapper = new x7S_AddToWishlistWrapper();  
        inputWrapper = x7S_AddToWishlistWrapper.parse(requestBodyString);
        System.debug('addToWishlist inputWrapper-'+inputWrapper);
        x7S_AddToWishlistWrapper.Items inputWishlistItem = new x7S_AddToWishlistWrapper.Items();


        List<ccrz__E_Cart__c> currentWishlist = [SELECT Id, ccrz__ActiveCart__c, ccrz__CartType__c,ccrz__User__c
                                        FROM ccrz__E_Cart__c
                                        WHERE ccrz__User__c=: inputWrapper.userId
                                        AND ccrz__ActiveCart__c=true
                                        AND ccrz__CartType__c='WishList'
                                        LIMIT 1];
        for(ccrz__E_Cart__c cart: currentWishlist){
            currentWishlistId = cart.Id;
        }

        for(x7S_AddToWishlistWrapper.Items inputItem : inputWrapper.items){
            inputWishlistItem = new x7S_AddToWishlistWrapper.Items();
            //Data to Add a product
            x7S_AddProduct.AddProductWrapper addProductWrap;

            for(x7S_AddToWishlistWrapper.Items inputWrapperItem : inputWrapper.items){
                inputWishlistItem = inputWrapperItem;

                addProductWrap = new x7S_AddProduct.AddProductWrapper();

                addProductWrap.itemNumber = inputWrapperItem.itemNumber;
                addProductWrap.itemDescription = inputWrapperItem.itemDescription;
                addProductWrap.itemImage = inputWrapperItem.itemImage;
            }
            /** Add a wishlist Item */
            ccrz__E_CartItem__c wishlistItem = new ccrz__E_CartItem__c();
            wishlistItem.ccrz__Cart__c = currentWishlistId;
            wishlistItem.ccrz__cartItemType__c = 'Major';
            wishlistItem.ccrz__Product__c =  x7S_AddProduct.addProductSKU(addProductWrap , '0.000', '', ''); //productSKUMap.get(inputItem.itemNumber);
            wishlistItem.ccrz__Quantity__c = 1.000;
            wishlistItem.nVent_Quantity__c = 1.000;
            wishlistItem.ccrz__Price__c = 0.000;
            wishlistItem.List_Price__c = 0.000;
            wishlistItem.nVent_SKU_Url__c = inputWishlistItem.skuUrl;
            /**
             * KIM Product item fields
             */
            wishlistItem.KIM_Configurator_Copy_URL__c = inputWishlistItem.configuratorCopyURL;
            wishlistItem.KIM_Configurator_Modify_URL__c = inputWishlistItem.configuratorModifyURL;
            wishlistItem.KIM_PDF_URL__c = inputWishlistItem.zipURL;
            wishlistItem.KIM_Configurator_Id__c = inputWishlistItem.configurationID;
            wishlistItem.KIM_Tracking_Number__c = inputWishlistItem.sapGUID;
            wishlistItem.KIM_Product__c	= (inputWishlistItem.configuratorCopyURL != null) ? true : false ;
                
            insertWishlistItem.add(wishlistItem);

            componentList = inputItem.components;
        }

        if (!insertWishlistItem.isEmpty()) {
            try {
                insert insertWishlistItem;
                majorInsertSuccess = true;
            } catch (DmlException e) {
                majorInsertSuccess = false;
                System.debug('DML Exception -' + e.getMessage());
            }
        }

        //Insert Minor Wishlist Items if any
        for(x7S_AddToWishlistWrapper.Items inputItem : inputWrapper.items){
            if (inputItem.components != null) {
                for(x7S_AddToWishlistWrapper.Components componentItem : inputItem.components){
                
                    //Data to Add a product
                    x7S_AddProduct.AddProductWrapper addProductWrap;

                    for(x7S_AddToWishlistWrapper.Components inputWrapperComponent : inputWrapper.items[0].components){
                        addProductWrap = new x7S_AddProduct.AddProductWrapper();

                        addProductWrap.itemNumber = inputWrapperComponent.itemNumber;
                        addProductWrap.itemDescription = inputWrapperComponent.itemDescription;
                        addProductWrap.itemImage = inputWrapperComponent.itemImage;
                    }

                    // Add a component (minor) item for the above major wishlist item
                    ccrz__E_CartItem__c minorWishlistItem = new ccrz__E_CartItem__c();
                    minorWishlistItem.ccrz__Cart__c = currentWishlistId;
                    minorWishlistItem.ccrz__cartItemType__c = 'Minor';
                    minorWishlistItem.ccrz__Product__c = x7S_AddProduct.addProductSKU(addProductWrap , '0.000', '', ''); //productSKUMap.get(componentItem.itemNumber);
                    minorWishlistItem.ccrz__ParentCartItem__c = insertWishlistItem[0].Id; //Id major item inserted above
                    minorWishlistItem.ccrz__Quantity__c = 1.000;
                    minorWishlistItem.nVent_Quantity__c = 1.000;
                    minorWishlistItem.ccrz__Price__c = 0.000;
                    minorWishlistItem.List_Price__c = 0.000;
                        
                    insertComponentList.add(minorWishlistItem);
                }
            }
            
        }

        if (!insertComponentList.isEmpty()) {
            try {
                insert insertComponentList;
                minorInsertSuccess = true;
            } catch (DmlException e) {
                minorInsertSuccess = false;
                System.debug('DML Exception -' + e.getMessage());
            }

        }
            
        
        addToWishlistSuccess = majorInsertSuccess && minorInsertSuccess;
        
        List<AggregateResult> aggregateResultList = [SELECT count(Id) itemCount FROM ccrz__E_CartItem__c WHERE ccrz__Cart__c =:currentWishlistId AND ccrz__cartItemType__c = 'Major'];
        if(aggregateResultList != null && aggregateResultList.size() > 0){
            for(AggregateResult aggr : aggregateResultList){
                addToWishlistItemCount = (Integer) aggr.get('itemCount');
            }
    
        }
        if(addToWishlistSuccess){
            addToWishlistMessage = System.Label.nVent_Response_addToWishlist_Success;

            AddToWishlistResponse succesResponse = new AddToWishlistResponse();
            succesResponse.success = addToWishlistSuccess;
            succesResponse.message = addToWishlistMessage;
            succesResponse.wishListId = currentWishlistId;
            succesResponse.wishListItemCount = addToWishlistItemCount;
        
            addToWishlistJSON = JSON.serializePretty(succesResponse);
        }
        else{
            addToWishlistMessage = System.Label.nVent_Response_addToWishlist_Failure;

            addToWishlistJSON = '{"success":'+false+',""message:"'+addToWishlistMessage+'"}';
        }
        
        return addToWishlistJSON;
    }

    /**
     * Description : onClick for 'Add To Cart'button from Acquia
     * @param : requestBodyJSON consisting (customerNumber , countryOfUser , quantity , sku (List))
     * @return : add to cart message
     */
    public static String addSkuToCart(String requestBodyString) {
        String addToCartMessage = '' , addToCartId = '';
        Integer addToCartItemCount;
        Boolean addToCartSuccess = false , addToCartComponentSuccess = false;
        String addToCartResponseJSON;

        String currencyIsoCode = '';

        String inputJSON = requestBodyString;

        x7S_PricingResponse prResponse = new x7S_PricingResponse();

        List < ccrz__E_Cart__c > newCartList = new List < ccrz__E_Cart__c > ();
        List < ccrz__E_CartItem__c > exisitingCartItems = new List < ccrz__E_CartItem__c > ();
        List < ccrz__E_CartItem__c > cartItemList = new List < ccrz__E_CartItem__c > ();
        List < ccrz__E_CartItem__c > minorCartItemList = new List < ccrz__E_CartItem__c > ();
        List < Id > exisitingCartIdList = new List < Id > ();
    
        Map < String, Object > isoMap = new Map < String, Object > (); // to get currencyIsoCode

        // Deserialize addToCart request body into x7S_AddToCartWrapper
        x7S_AddToCartWrapper inputAddToCartWrapper = x7S_AddToCartWrapper.parse(inputJSON);
        System.debug('inputAddToCartWrapper-'+inputAddToCartWrapper);

        x7S_AddToCartWrapper.Items inputItem = new x7S_AddToCartWrapper.Items();

       //get pricing info 
        String pricingResponseString = x7S_PricingRequestCallout.getPricingInfo(inputJSON);

        if (String.isNotBlank(pricingResponseString) && pricingResponseString != 'Callout Error') {

            prResponse = x7S_PricingResponse.parse(pricingResponseString);

            isoMap = (Map < String, Object > ) JSON.deserializeUntyped(pricingResponseString);
            for (String str: isoMap.keySet()) {
                if (str == 'items') {
                    List < Object > tmpList = (List < Object > ) isoMap.get('items');
                    Map < String, Object > temp = (Map < String, Object > ) tmpList[0];
                    if (temp.containsKey('listPrice')) {
                        Map < String, Object > temp1 = (Map < String, Object > ) temp.get('listPrice');
                        System.debug('Currency ISO Code - ' + temp1.get('currency'));
                        if (temp1.get('currency') != null) {
                            currencyIsoCode = String.valueOf(temp1.get('currency')); // Get CurrencyISOCode
                        }
                    }
                }
            }
        } else {
            System.debug('Pricing Response is Blank');
        }

        if (String.isNotBlank(pricingResponseString)) {

            System.debug('Current user - ' + UserInfo.getUserId());
            List < ccrz__E_Cart__c > activeCartList = [SELECT Id FROM ccrz__E_Cart__c WHERE ccrz__User__c =: inputAddToCartWrapper.userId AND ccrz__CartType__c='Cart' AND ccrz__CartStatus__c = 'Open' AND ccrz__ActiveCart__c = true];
            for (ccrz__E_Cart__c cart: activeCartList) {
                exisitingCartIdList.add(cart.Id);
            }
            System.debug('exisitingCartIdList' + exisitingCartIdList);

            if (!exisitingCartIdList.isEmpty()) {
                Set < Id > cartItems = new Set < Id > ();
                exisitingCartItems = [SELECT Id, ccrz__Cart__c FROM ccrz__E_CartItem__c WHERE ccrz__Cart__c =: exisitingCartIdList];

            } else {
                // Add a new Cart
                ccrz__E_Cart__c newCart = new ccrz__E_Cart__c();
                newCart.ccrz__CartType__c = 'Cart';
                newCart.ccrz__CartStatus__c = 'Open';
                newCart.ccrz__CurrencyISOCode__c = currencyIsoCode;
                newCart.ccrz__User__c = inputAddToCartWrapper.userId;
                newCartList.add(newCart);
                if (!newCartList.isEmpty()) {
                    try {
                        insert newCartList;
                    } catch (DmlException e) {
                        System.debug('Insert new cart DML Exception -' + e.getMessage());
                    }
                }
            }

            for (x7S_PricingResponse.Items items: prResponse.items) {
                System.debug('addSkuToCart item - ' + items);
                inputItem = new x7S_AddToCartWrapper.Items();
                //Data to Add a product
                x7S_AddProduct.AddProductWrapper addProductWrap;

                for(x7S_AddToCartWrapper.Items inputWrapperItem : inputAddToCartWrapper.items){
                    inputItem = inputWrapperItem;

                    addProductWrap = new x7S_AddProduct.AddProductWrapper();

                    addProductWrap.itemNumber = inputWrapperItem.itemNumber;
                    addProductWrap.itemDescription = inputWrapperItem.itemDescription;
                    addProductWrap.itemImage = inputWrapperItem.itemImage;
                }
                
                // Add a Cart Item
                ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c();
                //cartItem.ccrz__Product__c = productSKUMap.get(items.itemNumber);
                cartItem.ccrz__Product__c = x7S_AddProduct.addProductSKU(addProductWrap , items.leadTime , items.unitOfMeasure , currencyIsoCode);
                cartItem.nVent_SKU_Url__c = inputItem.skuUrl; //inputAddToCartWrapper.findSkuUrl(items.itemNumber);
                cartItem.ccrz__Cart__c = (exisitingCartIdList.size() > 0) ? exisitingCartIdList[0] : newCartList[0].Id;
                cartItem.nVent_Quantity__c = Double.valueOf(items.quantity);
                cartItem.ccrz__UnitOfMeasure__c = items.unitOfMeasure;
                cartItem.List_Price__c = Decimal.valueOf(items.listPrice.value);
                cartItem.ccrz__Price__c = Decimal.valueOf(items.netPrice.value);
                cartItem.CurrencyIsoCode = currencyIsoCode;
                cartItem.ccrz__AbsoluteDiscount__c = Decimal.valueOf(items.discount);
                cartItem.nVent_MOQ__c = ( Decimal.valueOf(items.MOQ) == null || Decimal.valueOf(items.MOQ) == 0.000 )? Decimal.valueOf(items.MOQ) : 1.000;
                cartItem.nVent_DLS__c = ( Decimal.valueOf(items.DLS) == null || Decimal.valueOf(items.DLS) == 0.000 )? Decimal.valueOf(items.DLS) : 1.000;
                cartItem.ccrz__SubAmount__c = Decimal.valueOf(items.netPrice.value) * Double.valueOf(items.quantity);
                /**
                 * KIM Items Field
                 */
                cartItem.KIM_Configurator_Copy_URL__c = inputItem.configuratorCopyURL;
                cartItem.KIM_Configurator_Modify_URL__c = inputItem.configuratorModifyURL;
                cartItem.KIM_PDF_URL__c = inputItem.zipURL;
                cartItem.KIM_Configurator_Id__c = inputItem.configurationID;
                cartItem.KIM_Tracking_Number__c = inputItem.sapGUID;
                cartItem.KIM_Product__c	= (inputItem.configuratorCopyURL != null) ? true : false ;
                
                if (items.isAvailableFlag == 'false') {
                    cartItem.ccrz__ItemStatus__c = 'Invalid';
                } else if (items.isAvailableFlag == 'true') {
                    cartItem.ccrz__ItemStatus__c = 'Available';
                } else {
                    cartItem.ccrz__ItemStatus__c = '';
                }

                cartItemList.add(cartItem);
            }

            if (!cartItemList.isEmpty()) {
                try {
                    insert cartItemList;
                    addToCartSuccess = true;

                } catch (DmlException e) {
                    System.debug('DML Exception -' + e.getMessage());
                }
            }


            for (x7S_PricingResponse.Items items: prResponse.items) {
                if (items.components != null) {
                    for (x7S_PricingResponse.Components comp: items.components) {
                        System.debug('addSkuToCart Component - ' + comp);

                        //Data to Add a product
                        x7S_AddProduct.AddProductWrapper addProductWrap = new x7S_AddProduct.AddProductWrapper();

                        for(x7S_AddToCartWrapper.Components inputWrapperComponent : inputAddToCartWrapper.items[0].components){
                            
                            if(inputWrapperComponent.itemNumber == comp.itemNumber){
                                addProductWrap.itemNumber = inputWrapperComponent.itemNumber;
                                addProductWrap.itemDescription = inputWrapperComponent.itemDescription;
                                addProductWrap.itemImage = inputWrapperComponent.itemImage;
                            }
                            
                        }
                        
                        // Add a component (minor) item for the above major Cart item
                        ccrz__E_CartItem__c minorCartItem = new ccrz__E_CartItem__c();
                        minorCartItem.ccrz__Cart__c = (exisitingCartIdList.size() > 0) ? exisitingCartIdList[0] : newCartList[0].Id;
                        //minorCartItem.ccrz__Product__c = productSKUMap.get(comp.itemNumber);
                        minorCartItem.ccrz__Product__c = x7S_AddProduct.addProductSKU(addProductWrap, '0.000', comp.unitOfMeasure, currencyIsoCode);
                        minorCartItem.ccrz__cartItemType__c = 'Minor';
                        minorCartItem.ccrz__ParentCartItem__c = cartItemList[0].Id;
                        minorCartItem.nVent_Quantity__c = Double.valueOf(comp.quantity);
                        minorCartItem.ccrz__UnitOfMeasure__c = comp.unitOfMeasure;
                        minorCartItem.List_Price__c = Decimal.valueOf(comp.listPrice.value);
                        minorCartItem.ccrz__Price__c = Decimal.valueOf(comp.netPrice.value);
                        minorCartItem.CurrencyIsoCode = currencyIsoCode;
                        minorCartItem.ccrz__AbsoluteDiscount__c = Decimal.valueOf(comp.discount);
                        if (items.isAvailableFlag == 'false') {
                            minorCartItem.ccrz__ItemStatus__c = 'Invalid';
                        } else if (items.isAvailableFlag == 'true') {
                            minorCartItem.ccrz__ItemStatus__c = 'Available';
                        } else {
                            minorCartItem.ccrz__ItemStatus__c = '';
                        }
                        minorCartItemList.add(minorCartItem);
                    }
                }
            }
            if (!minorCartItemList.isEmpty()) {
                try {
                    insert minorCartItemList;
                    addToCartComponentSuccess = true;
                    System.debug('The Component item(s) was successfully added to the cart.');
                } catch (DmlException e) {
                    System.debug('The Component item(s) was not successfully added to the cart.');
                    System.debug('DML Exception -' + e.getMessage());
                }

            }

        }

        addToCartId = (exisitingCartIdList.size() > 0) ? exisitingCartIdList[0] : newCartList[0].Id; //cart id

        List<AggregateResult> aggregateResultList = [SELECT count(Id) itemCount FROM ccrz__E_CartItem__c WHERE ccrz__Cart__c =:addToCartId AND ccrz__cartItemType__c = 'Major'];
        if(aggregateResultList != null && aggregateResultList.size() > 0){
            for(AggregateResult aggr : aggregateResultList){
                addToCartItemCount = (Integer) aggr.get('itemCount');
            }
    
        }
        
        if( addToCartSuccess || (addToCartSuccess && addToCartComponentSuccess) ){
            addToCartMessage = System.Label.nVent_Response_addToCart_Success;

            AddToCartResponse succesResponse = new AddToCartResponse();
            succesResponse.success = addToCartSuccess;
            succesResponse.message = addToCartMessage;
            succesResponse.cartId = addToCartId;
            succesResponse.cartItemCount = addToCartItemCount;
        
            addToCartResponseJSON = JSON.serializePretty(succesResponse);
        }
        else{
            addToCartMessage = System.Label.nVent_Response_addToCart_Failure;

            addToCartResponseJSON = '{"success":'+false+',""message:"'+addToCartMessage+'"}';
        }

        return addToCartResponseJSON;
    }

     /** Dummy method call to  addToWishlist() */
     public static String addToWishlistCall() {

        /** call addToWishlist() */
        String inputJSON ='{"customerNumber": "7000021793","items": [{"itemNumber": "60110456"}]}';

        String addToWishlistMessage = addToWishlist(inputJSON);

        return addToWishlistMessage;
    }

    /** Temporary method call to  addSkuToCart() until implementation of 'Add to Cart' button */
    public static String addSkuToCartCall() {

        /** trigger addSkuToCart() */
        String inputJSON ='{"customerNumber": "7000021793","countryOfUser": "","items": [{"itemNumber": "60110456","quantity": "1.000","unitOfMeasure": ""}]}';

        String addToCartMessage = addSkuToCart(inputJSON);

        return addToCartMessage;
    }

    public class AddToCartResponse{
        public Boolean success;
        public String message , cartId;
        public Decimal cartItemCount;
    }


    public class AddToWishlistResponse{
        public Boolean success;
        public String message , wishlistId;
        public Decimal wishListItemCount;
    }

}
