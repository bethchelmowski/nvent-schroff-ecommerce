/**
 * Created By ankitSoni on 08/18/2020
 */
@RestResource(urlMapping='/nVent/wishlist/*')
global with sharing class x7snVentWishlist {

    @httpPost
    global static void addToWishlist() {
        //get the wishlist items payload from the request body
        RestRequest req = RestContext.request;
    	Blob body = req.requestBody;
    	String requestString = body.toString();
        System.debug('Wishlist REQUEST >>> ' + requestString);
        
        String badRequestMessage = System.Label.nVent_Request_addToWishlist_Failure;
        String addToWishlistMessage;
        if(!Test.isRunningTest()){
            //call addToWishlist method in x7S_AddToActionController method that adds the item to wishlist

            //validate the request
            if( x7S_RequestValidator.isValidRequest(requestString , 'wishlist') ){
                addToWishlistMessage  = x7S_AddToActionController.addToWishlist(requestString);
            }
            else{
                addToWishlistMessage = '{"success":'+false+',"message":"'+badRequestMessage+'"}';
            }

        } else{
            Test.setMock(HttpCalloutMock.class, new x7S_PricingRequestCalloutMock());
            addToWishlistMessage = x7S_AddToActionController.addToWishlistCall();
        }

        System.debug('addToWishlistMessage'+addToWishlistMessage);
        //put JSON in response body in order to prevent string encoding and escaping quotations in response
        RestContext.response.addHeader('Content-Type', 'application/json'); 
        RestContext.response.responseBody = addToWishlistMessage!=null && addToWishlistMessage!=''? Blob.valueOf(addToWishlistMessage):null;


    }
}