/**
 * Created By ankitSoni on 06/30/2020
 */
public class x7S_RequestForQuoteCallout {
    
    private static String clientId = System.Label.nVent_Pricing_Client_Id; //f1a3a33eb61645b48e5b68ce278c0cc7
    private static String clientSecret = System.Label.nVent_Pricing_Client_Secret; //c3Da7106403447779946C3319C10925E
    private static String endpointURL = System.Label.nVent_Request_For_Quote_URL; //https://api-np.nvent.com/test/order2cash/v1/rfq
    
    /**
     * Description : POST method API Callout to Mulesoft API
     * @param : cart ID
     * @return : request for quote message
     */
    public static RFQResponseWrapper requestForQuote(String cartId){
        String responseMessage = '';
        Boolean wasSuccessful = false , wasCalloutSuccessful;

        String inputBodyString = prepareInputBody(cartId);
        System.debug('rfq inputBodyString-'+inputBodyString);

        // HTTP Callout to Mulesoft 'POST' method /rfq
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpointURL);
        req.setMethod('POST');
        req.setHeader('client_id', clientId);
        req.setHeader('client_secret', clientSecret);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(inputBodyString);
        req.setTimeout(30000);

        HttpResponse response = http.send(req);
        System.debug('RFQ HTTP Response Code ->'+ response.getStatusCode()); // API response
        System.debug('RFQ HTTP Response Body ->'+ response.getBody()); // API response

        RFQResponseWrapper responseWrapper = new RFQResponseWrapper();
        
        //Success Response
        if (response.getStatusCode() == 200) {
            wasCalloutSuccessful = true;
            responseWrapper = (RFQResponseWrapper) JSON.deserialize(response.getBody(), RFQResponseWrapper.class);    
            responseMessage = responseWrapper.message;

            System.debug('RFQ status Code: '+responseWrapper.statusCode);
            System.debug('RFQ message: '+responseWrapper.message);
            System.debug('RFQ salesDocumentNumber : '+responseWrapper.salesDocumentNumber);

            if(responseWrapper.statusCode == '201'){
                wasSuccessful = true;
            }

        }
        // Error Responses from API Callout
        else {
            responseMessage = 'HTTP Status : ' + response.getStatus() +' HTTP Status code : ' + response.getStatusCode();
        }

        System.debug('rfq responseMessage-'+responseMessage);

        return responseWrapper;
    }

    /**
     * Description : Prepare input body string for POST method
     * @param : cart ID
     * @return : input body string
     */
    public static String prepareInputBody(String cartId){
        String inputJSONString = '';
        List<ccrz__E_Cart__c> cartDetails = [SELECT Id,Name ,ccrz__CurrencyISOCode__c,ccrz__Contact__c,ccrz__Account__c,ccrz__Account__r.Name, ccrz__Contact__r.FirstName ,ccrz__Contact__r.LastName , ccrz__Contact__r.Enclosures_SAP__c , ccrz__Contact__r.MailingStreet , ccrz__Contact__r.MailingCity , ccrz__Contact__r.MailingState, ccrz__Contact__r.MailingStateCode, ccrz__Contact__r.MailingPostalCode ,ccrz__Contact__r.MailingCountry, ccrz__Contact__r.Language__c,ccrz__Contact__r.Phone , ccrz__Contact__r.MobilePhone ,ccrz__Contact__r.Email
                                            FROM ccrz__E_Cart__c
                                            WHERE Id =:cartId];
                                            
        List<ccrz__E_CartItem__c> majorCartItemDetails = [SELECT Id,Name,CurrencyIsoCode, ccrz__cartItemType__c	,nVent_Quantity__c , List_Price__c , ccrz__Price__c , ccrz__UnitOfMeasure__c, ccrz__AbsoluteDiscount__c,ccrz__SubAmount__c, ccrz__Product__c , ccrz__Product__r.ccrz__SKU__c , ccrz__Product__r.ccrz__ShortDesc__c
                                                    FROM ccrz__E_CartItem__c
                                                    WHERE ccrz__Cart__c =:cartId AND ccrz__cartItemType__c = 'Major'];
        
        List<ccrz__E_CartItem__c> minorCartItemDetails = [SELECT Id,Name,CurrencyIsoCode, ccrz__cartItemType__c	,ccrz__ParentCartItem__c,nVent_Quantity__c , List_Price__c , ccrz__Price__c , ccrz__UnitOfMeasure__c, ccrz__AbsoluteDiscount__c,ccrz__SubAmount__c, ccrz__Product__c , ccrz__Product__r.ccrz__SKU__c , ccrz__Product__r.ccrz__ShortDesc__c , ccrz__Product__r.Name
                                                    FROM ccrz__E_CartItem__c
                                                    WHERE ccrz__Cart__c =:cartId AND ccrz__cartItemType__c = 'Minor'];

        System.debug('cartDetails-'+cartDetails);
        System.debug(majorCartItemDetails.size()+' majorCartItemDetails-'+majorCartItemDetails);
                
        x7S_RequestForQuoteWrapper rfq = new x7S_RequestForQuoteWrapper();
        
        if(!cartDetails.isEmpty()){
            Integer posCount = 1;

            for(ccrz__E_Cart__c cart : cartDetails){
                rfq.projectListId   = cart.Name;
                rfq.companyName     = cart.ccrz__Account__r.Name;
                rfq.currencyCode    = cart.ccrz__CurrencyISOCode__c;
                rfq.customerNumber  = cart.ccrz__Contact__r.Enclosures_SAP__c;
                rfq.firstName       = cart.ccrz__Contact__r.FirstName;
                rfq.lastName        = cart.ccrz__Contact__r.LastName;
                rfq.language        = cart.ccrz__Contact__r.Language__c;
                rfq.phone           = cart.ccrz__Contact__r.Phone;
                rfq.mobile          = cart.ccrz__Contact__r.MobilePhone;
                rfq.email           = cart.ccrz__Contact__r.Email;
                rfq.street          = cart.ccrz__Contact__r.MailingStreet;
                rfq.city            = cart.ccrz__Contact__r.MailingCity;
                rfq.state           = cart.ccrz__Contact__r.MailingState;
                rfq.zip             = cart.ccrz__Contact__r.MailingPostalCode;
                rfq.country         = cart.ccrz__Contact__r.MailingCountry;
                rfq.positions       = new List<x7S_RequestForQuoteWrapper.Positions>();

                if(!majorCartItemDetails.isEmpty()){

                    for(ccrz__E_CartItem__c cartItem : majorCartItemDetails){
                        x7S_RequestForQuoteWrapper.Positions position = new x7S_RequestForQuoteWrapper.Positions();
                            
                        position.pos           = posCount;
                        position.itemNumber    = cartItem.ccrz__Product__r.ccrz__SKU__c;
                        position.description   = cartItem.ccrz__Product__r.ccrz__ShortDesc__c;
                        position.discount      = cartItem.ccrz__AbsoluteDiscount__c;
                        position.unitOfMeasure = cartItem.ccrz__UnitOfMeasure__c;
                        position.listPrice     = cartItem.List_Price__c;
                        position.netPrice      = cartItem.ccrz__Price__c;
                        position.quantity      = cartItem.nVent_Quantity__c;
                        position.posTotal      = cartItem.ccrz__SubAmount__c;
                        position.components = new List<x7S_RequestForQuoteWrapper.Components>();

                        rfq.positions.add(position);
                        posCount++;

                        if(!minorCartItemDetails.isEmpty()){

                            for(ccrz__E_CartItem__c minorCartItem : minorCartItemDetails){
                                //Check if Major Cart item has any minor cart item
                                if(minorCartItem.ccrz__ParentCartItem__c == cartItem.Id){
                                    x7S_RequestForQuoteWrapper.Components component = new x7S_RequestForQuoteWrapper.Components();
                                    component.itemNumber    = minorCartItem.ccrz__Product__r.ccrz__SKU__c;
                                    component.description   = minorCartItem.ccrz__Product__r.Name;
                                    component.quantity      = minorCartItem.nVent_Quantity__c;
                                    component.unitOfMeasure = minorCartItem.ccrz__UnitOfMeasure__c;

                                    position.components.add(component);
                                }
                            }
                        }   
                    }    
                }               
            }    
        }

        inputJSONString = JSON.serialize(rfq);
        
        return inputJSONString;
    }

    public class RFQResponseWrapper {
        public String statusCode;
        public String message;
        public String salesDocumentNumber;
    }
}
