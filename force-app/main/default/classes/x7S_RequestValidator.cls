/**
 * Created By ankitSoni on 08/17/2020
 */
public class x7S_RequestValidator {
    
    /**
     * Description : Validate /nVent/addToCart API Callout 
     * @param : inputJSONBody
     * @return : True if valid
     */
    public static Boolean isValidRequest(String inputJSONBody , String cartType){
        Boolean isItemValid = false;
        Boolean isComponentValid = true;
        Boolean isValid;

        switch on cartType {
            when 'cart' {
                x7S_AddToCartWrapper inputWrapper = x7S_AddToCartWrapper.parse(inputJSONBody);
                System.debug('addToCartWrapper-'+inputWrapper);

                if(inputWrapper.customerNumber != null || inputWrapper.customerNumber != ''){

                    for(x7S_AddToCartWrapper.Items item : inputWrapper.items){

                        //Check if item has itemNumber and quantity
                        if( String.isNotBlank(item.itemNumber)  && String.isNotBlank(item.quantity) ){

                            isItemValid = true;

                            //Check if component is present , if present check itemNumber and quantity is not null
                            if(item.components != null){
                                for(x7S_AddToCartWrapper.Components comp : item.components){
                                    
                                    if( String.isNotBlank(comp.itemNumber) && String.isNotBlank(comp.quantity)  ){
                                        isComponentValid = true;
                                    }
                                    else {
                                        isComponentValid = false;
                                    }
                                }
                            }
                        }
                    }        
                }
        
            }
            when 'wishlist' {
                x7S_AddToWishlistWrapper inputWrapper = x7S_AddToWishlistWrapper.parse(inputJSONBody);
                System.debug('addToWishlistWrapper-'+inputWrapper);
        
                if(inputWrapper.customerNumber != null || inputWrapper.customerNumber != ''){
        
                    for(x7S_AddToWishlistWrapper.Items item : inputWrapper.items){
        
                        //Check if item has itemNumber and quantity
                        if( String.isNotBlank(item.itemNumber)){
        
                            isItemValid = true;
        
                            //Check if component is present , if present check itemNumber and quantity is not null
                            if(item.components != null){
                                for(x7S_AddToWishlistWrapper.Components comp : item.components){
                                    
                                    if( String.isNotBlank(comp.itemNumber)){
                                        isComponentValid = true;
                                    }
                                    else {
                                        isComponentValid = false;
                                    }
                                }
                            }
                        }
                    }        
                }                     
            }
        }
        
        isValid = isItemValid && isComponentValid;

        System.debug('isValidAddToCartRequest-'+isValid);
        return isValid;
    }

    /**
     * Description : Validate /nVent/ API Callout 
     * @param : inputJSONBody
     * @return : True if valid
     */
    public static Boolean isValidPricingRequest(String inputJSONBody){
        Boolean isItemValid = false;
        Boolean isComponentValid = true;
        Boolean isValid;


        x7S_PricingRequest inputWrapper = x7S_PricingRequest.parse(inputJSONBody);
        System.debug('PricingRequestWrapper-'+inputWrapper);

        if(inputWrapper.customerNumber != null || inputWrapper.customerNumber != ''){

            for(x7S_PricingRequest.Items item : inputWrapper.items){

                //Check if item has itemNumber and quantity
                if( String.isNotBlank(item.itemNumber)  && String.isNotBlank(item.quantity) ){

                    isItemValid = true;

                    //Check if component is present , if present check itemNumber and quantity is not null
                    if(item.components != null){
                        for(x7S_PricingRequest.Components comp : item.components){
                            
                            if( String.isNotBlank(comp.itemNumber) && String.isNotBlank(comp.quantity)  ){
                                isComponentValid = true;
                            }
                            else {
                                isComponentValid = false;
                            }
                        }
                    }
                }
            }        
        }

        
        isValid = isItemValid && isComponentValid;

        System.debug('isValidPricingRequest-'+isValid);
        return isValid;
    }


}
