/**
 * Created By ankitSoni on 05/20/2020
 */
public class x7S_AddProduct {
    
    /**
     * Description : Add CC Product Record in salesforce
     * @param : Wrapper(itemNumber , itemImage , item Description) , leadTime , unitOfMeasure , currencyIsoCode
     * @return : Product Id
     */
    public static String addProductSKU(AddProductWrapper inputWrapper , String leadTime , String unitOfMeasure , String currencyIsoCode){
        System.debug('addProductSKU '+inputWrapper);
        Boolean isExisiting;
        String addProductId;
       
        List<ccrz__E_Product__c> addProductList = new List<ccrz__E_Product__c>();
        List<ccrz__E_Product__c> existingProductList =  [SELECT Id FROM ccrz__E_Product__c WHERE ccrz__SKU__c =:inputWrapper.itemNumber];

        isExisiting = (existingProductList.size() > 0) ? true : false;
        
        if(isExisiting){
            System.debug('Product present in CC Product');
            for(ccrz__E_Product__c existingProduct : existingProductList){
                addProductId = existingProduct.Id;
            }

            //Check if it has a product media record
            List<ccrz__E_ProductMedia__c> prodMediaList = [SELECT Id , ccrz__Product__c , ccrz__MediaType__c 
                                                    FROM ccrz__E_ProductMedia__c 
                                                    WHERE ccrz__Product__c =:addProductId AND ccrz__MediaType__c = 'Product Search Image'];
            if(prodMediaList.isEmpty()){
                createProductMedia(addProductId, inputWrapper.itemImage , currencyIsoCode);
            }

        }
        else{

            System.debug('Product not present in CC Product');

            //Add CC Product Record
            ccrz__E_Product__c addProduct = new ccrz__E_Product__c();
            addProduct.ccrz__SKU__c = inputWrapper.itemNumber;
            addProduct.Name = inputWrapper.itemDescription;
            addProduct.ccrz__UnitOfMeasure__c = unitOfMeasure;
            addProduct.ccrz__Storefront__c = 'DefaultStore';
            addProduct.ccrz__ProductType__c = 'Product';
            addProduct.ccrz__ProductStatus__c = 'Released';
            addProduct.ccrz__ProductIndexStatus__c = 'Not Current';
            addProduct.ccrz__LeadTime__c = Double.valueOf(leadTime);
            addProduct.CurrencyIsoCode = currencyIsoCode;
            addProductList.add(addProduct);

            if(!addProductList.isEmpty()){
                try{
                    insert addProductList;
                    addProductId = addProductList[0].Id;
                    System.debug('Product Inserted Successfully');
                }
                catch(DmlException e){
                    System.debug('Product insert DML Exception - '+e.getMessage());
                }

                //Create CC Product Media record
                createProductMedia(addProductId, inputWrapper.itemImage , currencyIsoCode);
            }

        }

        return addProductId;
    }

    /**
     * Description : Create Image of type 'Product Search Image' in CC Product Media
     * @param : productId , imageURI , currencyIsoCode
     * @return : void
     */
    public static void createProductMedia(String productId , String imageURI , String currencyIsoCode){
        //Add new Product Media
        ccrz__E_ProductMedia__c addProductMedia = new ccrz__E_ProductMedia__c();
        addProductMedia.ccrz__Product__c = productId;
        addProductMedia.ccrz__MediaType__c = 'Product Search Image';
        addProductMedia.ccrz__Locale__c = 'en_US';
        addProductMedia.ccrz__ProductMediaSource__c = 'URI';
        addProductMedia.ccrz__URI__c = imageURI;
        addProductMedia.ccrz__Enabled__c = true;
        addProductMedia.CurrencyIsoCode = currencyIsoCode;
        addProductMedia.ccrz__StartDate__c = Date.today();
        addProductMedia.ccrz__EndDate__c = Date.today().addYears( 2099 - Date.today().year() );

        try{
            insert addProductMedia;
        }
        catch(DmlException dex){
            System.debug('Insert Product Media Exception-'+dex.getMessage());
        }

    }


    public class AddProductWrapper { 
        public String itemNumber;
        public String itemImage;
        public String itemDescription;
    }
    
}
