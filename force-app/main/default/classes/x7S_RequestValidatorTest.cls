@isTest
public class x7S_RequestValidatorTest {
    
    @IsTest
    static void isValidRequestTest(){
        Boolean isValidCartRequest;
        Boolean isValidWishlistRequest;

        User testUser = x7S_TestUtility.createCommunityUser();
        String cartInputJSONBody = '{"userId":"+' +testUser.Id+'","customerNumber": "7000021793","countryOfUser": "","items": [{"itemNumber": "60110456","quantity": "1.000","unitOfMeasure": ""}]}';
        String cartInputJSONBodyWithComponent = '{"userId":"+' +testUser.Id+'","customerNumber":"7000021793","countryOfUser":"","items":[{"itemNumber":"N200331A044","quantity":"2.000","unitOfMeasure":"","components":[{"itemNumber":"25230081","quantity":"1.000","unitOfMeasure":""}]}]}';
        String wrongCartInputJSONBody = '{"userId":"+' +testUser.Id+'","customerNumber":"7000021793","countryOfUser":"","items":[{"itemNumber":"","quantity":"2.000","unitOfMeasure":"","components":[{"itemNumber":"","quantity":"1.000","unitOfMeasure":""}]}]}';

        String wishlistInputJSONBody = '{"userId":"+' +testUser.Id+'","customerNumber": "7000021793","items": [{"itemNumber": "60110456"}]}';
        String wishlistInputJSONBodyWithComponent = '{"userId":"+' +testUser.Id+'","customerNumber":"7000021793","items":[{"itemNumber":"N200331A044","components":[{"itemNumber":"25230081"}]}]}';
        
        
        Test.startTest();
        System.runAs(testUser){
            //Cart requests
            isValidCartRequest = x7S_RequestValidator.isValidRequest(cartInputJSONBody, 'cart');
            System.assertEquals(true, isValidCartRequest);

            //Cart request With Component
            isValidCartRequest = x7S_RequestValidator.isValidRequest(cartInputJSONBodyWithComponent, 'cart');
            System.assertEquals(true, isValidCartRequest);

            //Wrong input request body item number missing
            isValidCartRequest = x7S_RequestValidator.isValidRequest(wrongCartInputJSONBody, 'cart');
            System.assertEquals(false, isValidCartRequest);

            //Cart requests
            isValidWishlistRequest = x7S_RequestValidator.isValidRequest(wishlistInputJSONBody, 'wishlist');
            System.assertEquals(true, isValidWishlistRequest);

            //Cart request With Component
            isValidWishlistRequest = x7S_RequestValidator.isValidRequest(wishlistInputJSONBodyWithComponent, 'wishlist');
            System.assertEquals(true, isValidWishlistRequest);

        }
        Test.stopTest();
        
    }
}
