global class x7s_ConfigSelfRegistrationHandler implements Auth.ConfigurableSelfRegHandler {

	private final Long CURRENT_TIME = Datetime.now().getTime();
	private final String[] UPPERCASE_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
	private final String[] LOWERCASE_CHARS = 'abcdefghijklmnopqrstuvwxyz'.split('');
	private final String[] NUMBER_CHARS = '1234567890'.split('');
	private final String[] SPECIAL_CHARS = '!#$%-_=+<>'.split('');

	// This method is called once after verification (if any was configured)
	// This method should create a user and insert it
	// Password can be null
	// Return null or throw an exception to fail creation
	global Id createUser(Id accountId, Id profileId, Map<SObjectField, String> registrationAttributes, String password) {
		User u = new User();
		u.ProfileId = profileId;
		for (SObjectField field : registrationAttributes.keySet()) {
			String value = registrationAttributes.get(field);
			u.put(field, value);
		}

		u = handleUnsetRequiredFields(u);
		generateContact(u, accountId);
		if (String.isBlank(password)) {
			password = generateRandomPassword();
		}
		Site.validatePassword(u, password, password);
		if (u.contactId == null) {
			return Site.createExternalUser(u, accountId, password);
		}
		u.languagelocalekey = UserInfo.getLocale();
		u.localesidkey = UserInfo.getLocale();
		u.emailEncodingKey = 'UTF-8';
		u.timeZoneSidKey = UserInfo.getTimezone().getID();
		insert u;
		System.setPassword(u.Id, password);
		return u.id;
	}

	// Method to autogenerate a password if one was not passed in
	// By setting a password for a user, we won't send a welcome email to set the password
	private String generateRandomPassword() {
		String[] characters = new List<String>(UPPERCASE_CHARS);
		characters.addAll(LOWERCASE_CHARS);
		characters.addAll(NUMBER_CHARS);
		characters.addAll(SPECIAL_CHARS);
		String newPassword = '';
		Boolean needsUpper = true, needsLower = true, needsNumber = true, needsSpecial = true;
		while (newPassword.length() < 50) {
			Integer randomInt = generateRandomInt(characters.size());
			String c = characters[randomInt];
			if (needsUpper && c.isAllUpperCase()) {
				needsUpper = false;
			} else if (needsLower && c.isAllLowerCase()) {
				needsLower = false;
			} else if (needsNumber && c.isNumeric()) {
				needsNumber = false;
			} else if (needsSpecial && !c.isAlphanumeric()) {
				needsSpecial = false;
			}
			newPassword += c; 
		}
		newPassword = addMissingPasswordRequirements(newPassword, needsLower, needsUpper, needsNumber, needsSpecial);
		return newPassword;
	}

	private String addMissingPasswordRequirements(String password, Boolean addLowerCase, Boolean addUpperCase, Boolean addNumber, Boolean addSpecial) {
		if (addLowerCase) {
			password += LOWERCASE_CHARS[generateRandomInt(LOWERCASE_CHARS.size())];
		}
		if (addUpperCase) {
			password += UPPERCASE_CHARS[generateRandomInt(UPPERCASE_CHARS.size())];
		}
		if (addNumber) {
			password += NUMBER_CHARS[generateRandomInt(NUMBER_CHARS.size())];
		}
		if (addSpecial) {
			password += SPECIAL_CHARS[generateRandomInt(SPECIAL_CHARS.size())];
		}
		return password;
	}

	// Generates a random number from 0 up to, but not including, max.
	private Integer generateRandomInt(Integer max) {
		return Math.mod(Math.abs(Crypto.getRandomInteger()), max);
	}

	// Loops over required fields that were not passed in to set to some default value
	private User handleUnsetRequiredFields(User u) {
		if (String.isBlank(u.LastName)){
			u.LastName = generateLastName();
		}
		if (String.isBlank(u.Username)) {
			//u.Username = generateUsername();
			u.Username = u.Email;
		}
		if (String.isBlank(u.Email)) {
			u.Email = generateEmail();
		}
		if (String.isBlank(u.Alias)) {
			u.Alias = generateAlias();
		}
		if (String.isBlank(u.CommunityNickname)) {
			u.CommunityNickname = generateCommunityNickname();
		}
		return u;
	}

	// Method to construct a contact for a user
	private void generateContact(User u, Id accountId) {
		// Add logic here if you want to build your own contact for the user

		Boolean attachAccountToContact = false;
		//Check if contact exists with same Email
		List<Contact> contactWithEmail = [SELECT Id , Email FROM Contact WHERE Email =:u.Email LIMIT 1];
		
		if(contactWithEmail.isEmpty()){

			Contact newContact = new Contact();
			newContact.FirstName = u.FirstName;
			newContact.LastName = u.LastName;
			newContact.Email = u.Email;
			newContact.MobilePhone = u.MobilePhone;
			newContact.Phone = u.Phone;
			newContact.Extension__c	= u.Extension;
			newContact.Fax = u.Fax;
			newContact.Title = u.Title;
			newContact.AccountId = u.AccountId;
			newContact.Department = u.Department;
			newContact.Description = u.AboutMe;
			newContact.Preferred_Distribution_Partner__c = u.Preferred_Distribution_Partner__c;
			//newContact.Name = u.CommunityNickname;
			newContact.Schroff_Newsletter_Opt_In__c = u.Schroff_Newsletter_Opt_In__c;
			newContact.MailingStreet = u.Street;
			newContact.MailingCity = u.City;
			newContact.MailingState = u.State;
			newContact.MailingStateCode = u.StateCode;
			newContact.MailingCountry = u.Country;
			newContact.MailingCountryCode = u.CountryCode;
			newContact.MailingPostalCode = u.PostalCode;
			newContact.MailingLatitude = u.Latitude;
			newContact.MailingLongitude = u.Longitude;
			newContact.MailingGeocodeAccuracy = u.GeocodeAccuracy;

			//find the account to be associated
			String accountIdToAssociate = findAccount(u.Email);

			newContact.AccountId = accountIdToAssociate;
			
			try{
				insert newContact;
				//Associate this newly created contact to the user
				u.ContactId = newContact.Id;

			}
			catch(DmlException dex){
				System.debug('Insert Contact Exception - '+dex.getMessage());
			}
		}
		else{

			for(Contact con : contactWithEmail){
				u.ContactId = con.Id;
			}
		}

	}

	//Logic to associate an Account to the new contact
	private String findAccount(String userEmail /* ,String userZip*/){
		String emailToMatch = userEmail;
		String domainToMatch = '';
		String accountId;
		List<Account> accountWithPostalCode = new List<Account>();
		List<Account> defaultAccountList = new List<Account>();

 		domainToMatch = emailToMatch.split('@').get(1);

		List<Account> accountWithDomain = [
			SELECT Id, Name, Website, Domain__c 
			FROM Account 
			WHERE Domain__c like :domainToMatch];
	
		//contact domain = account domain 	
		if(!accountWithDomain.isEmpty()){

			//If contact domain = account domain AND it is a unique match then associate the contact to the account
			if(accountWithDomain.size() == 1){
				accountId = accountWithDomain[0].Id;
			}
			else if(accountWithDomain.size() > 1){
				//If contact domain = account domain AND there are multiple matches to account domain,
				//assign to default account for company (sold to number = ship to number)
				for(Account acc : [
					SELECT Id 
					FROM Account 
					WHERE Id IN :accountWithDomain 
					AND ENC_SAP_EUR_SoldTo__c != null 
					AND ENC_SAP_EUR_ShipTo__c != null]){

					if(acc.ENC_SAP_EUR_SoldTo__c == acc.ENC_SAP_EUR_ShipTo__c){
						defaultAccountList.add(acc);
					}
				}

				if(!defaultAccountList.isEmpty()){
					accountId = defaultAccountList[0].Id;
				}

			}

		}
		else{
			//contact domain != account domain 
			//Assign dummy account "nVent Schroff Web Users"
			accountId = [SELECT Id FROM Account WHERE Name ='nVent Schroff Web Users' LIMIT 1].Id;
		}

		return accountId;
	}

	// Default implementation to try to provide uniqueness
	private String generateAlias() {
		String timeString = String.valueOf(CURRENT_TIME);
		return timeString.substring(timeString.length() - 8);
	}

	// Default implementation to try to provide uniqueness
	private String generateLastName() {
		return 'ExternalUser' + CURRENT_TIME;
	}

	// Default implementation to try to provide uniqueness
	private String generateUsername() {
		return 'externaluser' + CURRENT_TIME + '@company.com';
	}

	// Default implementation to try to provide uniqueness
	private String generateEmail() {
		return 'externaluser' + CURRENT_TIME + '@company.com';
	}

	// Default implementation to try to provide uniqueness
	private String generateCommunityNickname() {
		return 'ExternalUser' + CURRENT_TIME;
	}
}