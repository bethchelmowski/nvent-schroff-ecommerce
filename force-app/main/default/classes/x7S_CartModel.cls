/**
* Created By ankitSoni on 05/27/2020
*/
public with sharing class x7S_CartModel {
    
    @AuraEnabled
    public String cartId,cartEncId,currencyISOCode,customerSAPNumber;
    
	@AuraEnabled
	public Decimal totalQuantity, totalPrice, itemCount;

	@AuraEnabled
	public List<x7S_CartItemModel> items;

    public x7S_CartModel() {
        cartId = '';
		cartEncId = '';
		customerSAPNumber = '';
		items  = new List<x7S_CartItemModel>();
    }

    public x7S_CartItemModel findParentCartItem(String parentCartItemId){
		for (x7S_CartItemModel item : items){
			if (item.id == parentCartItemId){
				return item;
			}
		}
		
		return null;
	}
}