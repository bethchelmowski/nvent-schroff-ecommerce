/**
 * Created By ankitSoni on 07/29/2020
 */
@IsTest
public class x7S_UserInfoControllerTest {
    
    @TestSetup
    static void makeData(){
        User schroffRepUser = x7S_TestUtility.createUser(true, 'SchroffRepTest');
    }


    @IsTest
    static void getCustomerCareRepTest(){
        User schroffCustomerCare = x7S_TestUtility.createUser(true,'CustomerCareTest');
        User schroffRep = x7S_TestUtility.createUser(true,'SalesRepTest');    
    
        User testUser = x7S_TestUtility.createCommunityUser();
        Account testAccount = [SELECT Id,Name FROM Account WHERE Name ='testAccount1' LIMIT 1];

        AccountTeamMember member1 = x7S_TestUtility.createAccountTeamMember(true, testaccount.Id, schroffCustomerCare.Id, 'Schroff Customer Care');
        AccountTeamMember member2 = x7S_TestUtility.createAccountTeamMember(true, testaccount.Id, schroffRep.Id, 'Schroff Rep');
        
        Test.startTest();
        System.runAs(testUser){
            List<x7S_UserInfoController.TeamMemberWrapper> result = x7S_UserInfoController.getRepInformation();
            System.assert(result.size() > 0);
            System.assertEquals(schroffCustomerCare.FirstName+' '+schroffCustomerCare.LastName, result[0].name);
            System.assertNotEquals('', result[0].name);

            System.assertEquals(schroffRep.FirstName+' '+schroffRep.LastName, result[1].name);
            System.assertNotEquals('', result[1].name);

        }
        Test.stopTest();      
    }
}
