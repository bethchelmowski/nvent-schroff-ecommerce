/**
* Created By ankitSoni on 09/23/2020
*/
public class x7S_BOMServiceCallout {
    
    private static String clientId = System.Label.nVent_Pricing_Client_Id; //f1a3a33eb61645b48e5b68ce278c0cc7
    private static String clientSecret = System.Label.nVent_Pricing_Client_Secret; //c3Da7106403447779946C3319C10925E
    private static String endpointURL = System.Label.nVent_BOM_Service_URL; //https://api-np.nvent.com/test/order2cash/v1/config-bom

    /**
     * Description : POST method BOM Service Callout method to Mulesoft API endpoint
     * @param : cart ID
     * @return : BOMResponseWrapper
     */
    @future(callout = true)
    public static void bomRequest(String cartId) {
        Integer calloutCount =0;
        BOMResponseWrapper responseWrapper = new BOMResponseWrapper();

        /*
         Map Details :
            Major Item Id => JSON Body for this configured major item 
         */
        Map<Id , String> requestBodyMap = new Map<Id , String>();
        requestBodyMap = prepareInputBody(cartId);

        if(!requestBodyMap.isEmpty()){

            for(Id itemId : requestBodyMap.keySet()){
                calloutCount++;

                HttpRequest req = new HttpRequest();
                req.setEndpoint(endpointURL);
                req.setMethod('POST');
                req.setHeader('client_id', clientId);
                req.setHeader('client_secret', clientSecret);
                req.setHeader('Content-Type', 'application/json');
                req.setBody(requestBodyMap.get(itemId));

                HttpResponse res;
                
                res = new Http().send(req);
                
                if(res.getStatusCode() == 200) {
                    // parse response
                    System.debug('BOM Response: '+res.getBody());
                    responseWrapper = (BOMResponseWrapper) JSON.deserialize(res.getBody(), BOMResponseWrapper.class);  
                    System.debug('BOM Response Wrapper : '+responseWrapper);

                } else {
                    // throw error
                    System.debug('HTTP Status : ' + res.getStatus() +' HTTP Status code : ' + res.getStatusCode());
 
                } 
            }
        }

        System.debug('BOM Callout count :'+calloutCount);

    }



    public static Map<Id , String> prepareInputBody(String cartId) {
        String xmlBOMBody , jsonBOMCalloutBody;

        Map<Id , List<ccrz__E_CartItem__c>> mapOfMajorToMinorItems = new Map<Id , List<ccrz__E_CartItem__c>>();
        Map<Id , ccrz__E_CartItem__c> mapOfIdToMajorItem = new Map<Id , ccrz__E_CartItem__c>();

        Map<Id , String> mapOfMajorToBOMXMLString = new Map<Id , String>();
        Map<Id , String> mapOfMajorToBOMJSONString = new Map<Id , String>();

        List<ccrz__E_CartItem__c> majorCartItemDetails = [
            SELECT Id,Name,CurrencyIsoCode, ccrz__cartItemType__c	,nVent_Quantity__c , List_Price__c , ccrz__Price__c , ccrz__Product__c , ccrz__Product__r.ccrz__SKU__c ,
            KIM_Configurator_Copy_URL__c ,KIM_PDF_URL__c ,KIM_Configurator_Id__c,KIM_Tracking_Number__c
            FROM ccrz__E_CartItem__c
            WHERE ccrz__Cart__c =:cartId 
            AND ccrz__cartItemType__c = 'Major'];
        
        List<ccrz__E_CartItem__c> minorCartItemDetails = [
            SELECT Id,Name,CurrencyIsoCode, ccrz__cartItemType__c	,ccrz__ParentCartItem__c,nVent_Quantity__c , List_Price__c , ccrz__Price__c , ccrz__Product__c , ccrz__Product__r.ccrz__SKU__c ,
            KIM_Configurator_Copy_URL__c ,KIM_PDF_URL__c ,KIM_Configurator_Id__c, KIM_Tracking_Number__c ,ccrz__Product__r.Name
            FROM ccrz__E_CartItem__c
            WHERE ccrz__Cart__c =:cartId 
            AND ccrz__cartItemType__c = 'Minor'];

        for(ccrz__E_CartItem__c majorItem : majorCartItemDetails){
            mapOfIdToMajorItem.put(majorItem.Id, majorItem);
        }
        
        
        //Check if we have any components in our cart
        if(minorCartItemDetails.size() > 0){

            for(ccrz__E_CartItem__c item : minorCartItemDetails){
                if(mapOfMajorToMinorItems.containsKey(item.ccrz__ParentCartItem__c)){

                    mapOfMajorToMinorItems.get(item.ccrz__ParentCartItem__c).add(item);
                }
                else{
                    mapOfMajorToMinorItems.put(item.ccrz__ParentCartItem__c, new List<ccrz__E_CartItem__c>{item} );
                }
            }

            System.debug('mapOfMajorToMinorItems-'+mapOfMajorToMinorItems);

            //Create XML body data for every configured product
            for(ccrz__E_CartItem__c item : majorCartItemDetails){
                
                if(mapOfMajorToMinorItems.containsKey(item.Id)){
                    xmlBOMBody = x7S_PrepareBOMCalloutData.createXMLData(mapOfMajorToMinorItems.get(item.Id));
                    mapOfMajorToBOMXMLString.put(item.Id , xmlBOMBody);
                }
            }

            System.debug('mapOfMajorToBOMXMLString-'+mapOfMajorToBOMXMLString);

            //Create Request Body JSONs for every configured product
            mapOfMajorToBOMJSONString = x7S_PrepareBOMCalloutData.createJSONBodyData(mapOfIdToMajorItem , mapOfMajorToBOMXMLString);

            System.debug('mapOfMajorToBOMJSONString-'+mapOfMajorToBOMJSONString);

        }  
        
        return mapOfMajorToBOMJSONString;
    }

    public class BOMResponseWrapper {
        public String statusCode;
        public String message;
    }
}