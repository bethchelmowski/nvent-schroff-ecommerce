/**
 * Created By ankitSoni on 05/01/2020
 * Description : Test Class for x7S_CartStatusController
 */
@isTest
public class x7S_CartStatusControllerTest {
    
    @isTest
    static void fetchCartStatusTest(){
        
        User testUser = x7S_TestUtility.createCommunityUser();
         
        System.runAs(testUser) {
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            insert cart;

            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, true);
            
        	List<ccrz__E_Cart__c> cartList =  [SELECT Id , Name , ccrz__ActiveCart__c , ccrz__User__c , ccrz__CartStatus__c , OwnerId FROM ccrz__E_Cart__c];
        	System.debug('Cart query+'+cartList);
            String cartStatus = x7S_CartStatusController.fetchCartStatus();  
                
            System.assertEquals('Open', cartStatus);
       	}
    }
}