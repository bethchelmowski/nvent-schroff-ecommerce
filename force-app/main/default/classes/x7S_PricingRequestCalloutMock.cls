@isTest
global class x7S_PricingRequestCalloutMock implements HttpCalloutMock{

    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
     
        // Create a fake response
        String fakeBody = '{"errorFlag":"false","errorMessage":null,"items":[{"itemNumber":"11527789","quantity":"1.000","unitOfMeasure":null,"listPrice":{"value":"19.71","currency":"EUR"},"netPrice":{"value":"19.71","currency":"EUR"},"isAvailableFlag":"false","leadTime":"0","discount":"0.000","MOQ":"0.000","DLS":"0.000","components":[{"itemNumber":"64560001","quantity":"2.000","unitOfMeasure":"EA","listPrice":{"value":"0.48","currency":"EUR"},"netPrice":{"value":"0.48","currency":"EUR"},"discount":"0.000"},{"itemNumber":"34561752","quantity":"1.000","unitOfMeasure":"EA","listPrice":{"value":"18.75","currency":"EUR"},"netPrice":{"value":"18.75","currency":"EUR"},"discount":"0.000"}]}]}';
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(fakeBody);
        res.setStatusCode(200);
        return res;
    }
}