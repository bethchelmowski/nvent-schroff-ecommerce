/**
 * Created By ankitSoni on 05/06/2020
 * Description : Wrapper Class for x7S_PricingRequestCallout
 */
public class x7S_PricingRequest {
    public String userId;
    public String customerNumber;
    public String countryOfUser;
    public List<Items> items;
    
    public class Items {
        public String itemNumber;
        public String quantity;
        public String unitOfMeasure;
        public List<Components> components;
    }

    public class Components {
        public String itemNumber;
        public String quantity;
        public String unitOfMeasure;
        public List<Properties> properties;
    }

    public class Properties {
        public String code;
        public String value;
    }

    
    public static x7S_PricingRequest parse(String json) {
        return (x7S_PricingRequest) System.JSON.deserialize(json, x7S_PricingRequest.class);
    }
}