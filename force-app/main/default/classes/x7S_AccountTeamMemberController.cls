/**
 * Created By ankitSoni on 09/28/2020
 * Description : Class is without sharing since AccountTeamMember object is not accessible to the portal user.
 */
public without sharing class x7S_AccountTeamMemberController {
    
    /**
     * Description : Get Account Team Member List for the portal user because AccountTeamMember is not accessible by portal user.
     * @param : AccountId
     * @return : List<AccountTeamMember>
     */
    public static List<AccountTeamMember> getMemberList(String accountId) {
        List<AccountTeamMember> teamMemberList = [
					SELECT Id , AccountId , TeamMemberRole , UserId , AccountAccessLevel ,CaseAccessLevel , ContactAccessLevel ,OpportunityAccessLevel
					FROM AccountTeamMember 
                    WHERE AccountId =:accountId];
        
        System.debug('teamMemberList'+teamMemberList);               
        
        return teamMemberList;               
    }
}
