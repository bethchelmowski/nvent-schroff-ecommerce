/**
 * Created By ankitSoni on 09/16/2020
 */
public class x7S_ContactTriggerHandler {

    public static void onAfterUpdate(List<Contact> newContactList , Map<Id,Contact> oldContactMap ){
        System.debug('x7S_ContactTriggerHandler onAfterUpdate');
        System.debug('newContactList'+newContactList);
        
        createChatterPostForUpdates(newContactList,oldContactMap);
    }

    /**
     * Description : Create a chatter post to Schroff Customer Care Account Team Member when Contact Record is updated.
     */
    public static void createChatterPostForUpdates(List<Contact> newContactList , Map<Id,Contact> oldContactMap) {

        Set<String> fieldsToBeTracked = new Set<String>{
            'Salutation','FirstName','LastName','Nickname','MobilePhone','Phone',
            'Extension__c','Fax','Title','Department','Description','Preferred_Distribution_Partner__c',
            'MailingStreet','MailingCity','MailingState','MailingStateCode','MailingCountry','MailingCountryCode','MailingPostalCode',
            'MailingLatitude','MailingLongitude','MailingGeocodeAccuracy'
        };

        Set<Id> accountIdSet = new Set<Id>();
        String customerCareUserId;
        List<FeedItem> feedItemList = new List<FeedItem>();

        Contact conObject = new Contact(); // This takes all available fields from the required object. 
		Schema.SObjectType objType = conObject.getSObjectType();         
		Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.Contact.fields.getMap(); 
        

        for(Contact con : newContactList){
            if(con.AccountId != null){
                accountIdSet.add(con.AccountId);
            }
        }

        List<AccountTeamMember> teamMemberList =  [
            SELECT Id,AccountId,UserId,TeamMemberRole 
            FROM AccountTeamMember
            WHERE AccountId IN :accountIdSet];
            
        if(!teamMemberList.isEmpty()){
            for(AccountTeamMember act : teamMemberList){
                if(act.TeamMemberRole == 'Schroff Customer Care'){
                    customerCareUserId = act.UserId; 
                }
            }
        }    
         
        System.debug('Customer Care User Id: '+customerCareUserId);

        for(Contact con : newContactList){
            
            Contact oldcon = oldContactMap.get(con.Id);

            for (Schema.SObjectField objField : mapFields.values()) {
                
                String fieldName  = objField.getDescribe().getName();
                String fieldLabel = objField.getDescribe().getLabel();
                
                if( fieldsToBeTracked.contains(fieldName) ){

                    if ( con.get(fieldName) != oldcon.get(fieldName)){
                        String oldValue = String.valueOf(oldcon.get(fieldName));
                        String newValue = String.valueOf(con.get(fieldName));
            
                        System.Debug('Contact Field changed: fieldName: +' +fieldName+'. The value has changed from: ' + oldValue + ' to: ' + newValue);
    
                        FeedItem post = new FeedItem();
                        post.ParentId = customerCareUserId; // RecordId
                        post.Body = String.format(
                            '{0} has updated their field:{1}\nNew field: {2}\nPrevious field: {3}\nTo revert the field to the previous value, please make the change on the Contact: {4}',
                            new List<String> {
                                con.FirstName+' '+con.LastName,
                                fieldLabel,
                                newValue,
                                oldValue,
                                URL.getSalesforceBaseUrl().toExternalForm() +'/' + con.Id
                            }
                        ); 
                        feedItemList.add(post);
                    }
                }
                
            }

        }

        if(!feedItemList.isEmpty()){
            insert feedItemList;
            System.debug('feedItemList'+feedItemList);
        }
    }
}
