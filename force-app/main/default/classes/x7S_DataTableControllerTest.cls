/**
* Created By ankitSoni on 06/01/2020
*/
@isTest
public class x7S_DataTableControllerTest {
    
   @isTest
    static void getWishlistItems(){
        Test.startTest();
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            ccrz__E_ProductMedia__c productMedia = x7S_TestUtility.createProductMedia(true, product1.Id);
            
            ccrz__E_Cart__c wishlist = x7S_TestUtility.createWishlist(false);
            wishlist.ccrz__Storefront__c = 'DefaultStore';
            wishlist.ccrz__ActiveCart__c = true;
            wishlist.ccrz__CurrencyISOCode__c = 'EUR';
            insert wishlist;
            
            List<ccrz__E_CartItem__c> wishlistItems = x7S_TestUtility.createCartItem(1, wishlist.Id, false);
            wishlistItems[0].ccrz__cartItemType__c = 'Major';
            wishlistItems[0].ccrz__Product__c = product1.Id;
            wishlistItems[0].ccrz__Price__c = 0.00;
            wishlistItems[0].ccrz__Quantity__c = 1.00;
            insert wishlistItems;

            List<ccrz__E_CartItem__c> minorWishlistItems = x7S_TestUtility.createCartItem(1, wishlist.Id, false);
            minorWishlistItems[0].ccrz__cartItemType__c = 'Minor';
            minorWishlistItems[0].ccrz__Product__c = product1.Id;
            minorWishlistItems[0].ccrz__Price__c = 0.00;
            minorWishlistItems[0].ccrz__Quantity__c = 1.00;
            minorWishlistItems[0].ccrz__ParentCartItem__c = wishlistItems[0].Id;
            insert minorWishlistItems;
            
            x7S_WishListModel returnValue = x7S_DataTableController.getWishlistItems();
            System.assertEquals(wishlist.Id, returnValue.cartId);
            
            Test.stopTest();
        }
    }

   @IsTest
    static void addWishlistItemToCartTest(){
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            
            ccrz__E_Product__c product2 = x7S_TestUtility.createProduct(false, 'Test Product', 'SKU', 'SKU');
            product2.ccrz__ProductType__c = 'Product';
            insert product2;

            ccrz__E_ProductMedia__c productMedia2 = x7S_TestUtility.createProductMedia(true, product2.Id);
            
            ccrz__E_Cart__c wishlist = x7S_TestUtility.createWishlist(false);
            wishlist.ccrz__Storefront__c = 'DefaultStore';
            wishlist.ccrz__ActiveCart__c = true;
            wishlist.ccrz__CurrencyISOCode__c = 'EUR';
            insert wishlist;
            
            List<ccrz__E_CartItem__c> wishlistItems = x7S_TestUtility.createCartItem(1, wishlist.Id, false);
            wishlistItems[0].ccrz__Product__c = product2.Id;
            wishlistItems[0].ccrz__Price__c = 10.00;
            wishlistItems[0].ccrz__Quantity__c = 1;
            insert wishlistItems;
            
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            cart.ccrz__CurrencyISOCode__c = 'EUR';
            insert cart;

            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product2.Id;
            insert cartItems;

            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new x7S_PricingRequestCalloutMock());
            Boolean result = x7S_DataTableController.addWishlistItemToCart(wishlist.Id, wishlistItems[0].Id,false);
            System.assertEquals(true, result);
            Test.stopTest();

        }
    }
    
    @IsTest
    static void addCartItemToWishlistTest(){
        Test.startTest();
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            ccrz__E_ProductMedia__c productMedia = x7S_TestUtility.createProductMedia(true, product1.Id);
            
            ccrz__E_Product__c product2 = x7S_TestUtility.createProduct(true, 'Test Product-2', 'SKU-2', 'SKU-2');
            ccrz__E_Product__c product3 = x7S_TestUtility.createProduct(true, 'Test Minor Product-3', 'SKU-3', 'SKU-3');
            
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            insert cart;
            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            insert cartItems;
            
            List<ccrz__E_CartItem__c> majorCartItems = x7S_TestUtility.createZeroPricedCartItem(1, cart.Id, false);    
            majorCartItems[0].ccrz__Product__c = product2.Id;
            majorCartItems[0].ccrz__cartItemType__c = 'Major';
            insert majorCartItems;

            List<ccrz__E_CartItem__c> minorCartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            minorCartItems[0].ccrz__Product__c = product3.Id;
            minorCartItems[0].ccrz__cartItemType__c = 'Minor';
            minorCartItems[0].ccrz__ParentCartItem__c = cartItems[0].Id;
            insert minorCartItems;

            ccrz__E_Cart__c wishlist = x7S_TestUtility.createWishlist(false);
            wishlist.ccrz__Storefront__c = 'DefaultStore';
            wishlist.ccrz__ActiveCart__c = true;
            insert wishlist;
            
            Boolean result = x7S_DataTableController.addCartItemToWishlist(cart.Id, cartItems[0].Id);
            System.assertEquals(true, result);

        }
        Test.stopTest();
    }

    @IsTest
    static void updateCartItemQuantityTest(){

        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            ccrz__E_ProductMedia__c productMedia = x7S_TestUtility.createProductMedia(true, product1.Id);
            
            ccrz__E_Product__c product2 = x7S_TestUtility.createProduct(true, 'Test Product-2', 'SKU-2', 'SKU-2');
            ccrz__E_Product__c product3 = x7S_TestUtility.createProduct(true, 'Test Minor Product-3', 'SKU-3', 'SKU-3');
           
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            insert cart;

            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            insert cartItems;
            
            List<ccrz__E_CartItem__c> majorCartItems = x7S_TestUtility.createZeroPricedCartItem(1, cart.Id, false);    
            majorCartItems[0].ccrz__Product__c = product2.Id;
            majorCartItems[0].ccrz__cartItemType__c = 'Major';
            insert majorCartItems;

            List<ccrz__E_CartItem__c> minorCartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            minorCartItems[0].ccrz__Product__c = product3.Id;
            minorCartItems[0].ccrz__cartItemType__c = 'Minor';
            minorCartItems[0].ccrz__ParentCartItem__c = cartItems[0].Id;
            insert minorCartItems;

            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new x7S_PricingRequestCalloutMock());
            x7S_DataTableController.PricingWrapper pWrapResult = x7S_DataTableController.updateCartItemQuantity(cartItems[0].Id, '10.0000');
            System.assert(pWrapResult.listPrice != null);
            Test.stopTest();
        }
    }

    @IsTest
    static void getOrdersTest(){

        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            ccrz__E_ProductMedia__c productMedia = x7S_TestUtility.createProductMedia(true, product1.Id);
            
            String userId = UserInfo.getUserId();
		    List<User> currentUserList =  [SELECT Id,ContactId FROM User WHERE Id=:userId LIMIT 1];
		    String contactId = currentUserList[0].ContactId;

            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            cart.ccrz__Contact__c = contactId;
            insert cart;

            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            insert cartItems;
            
            ccrz__E_ContactAddr__c shipToContact = x7S_TestUtility.createContactAddress(true);

            ccrz__E_Order__c testOrder = x7S_TestUtility.createOrder(false, contactId, testUser.Id);
            testOrder.ccrz__OriginatedCart__c = cart.Id;
            testOrder.ccrz__OrderDate__c = Date.today();
            testOrder.ccrz__OrderStatus__c = 'Shipped';
            testOrder.ccrz__ShipTo__c = shipToContact.Id;
            insert testOrder;

            Test.startTest();
            List<x7S_OrderModel> orders = x7S_DataTableController.getOrders();
            System.assert(orders.size() > 0);
            Test.stopTest();
        }
    }
    
    @IsTest
    static void getOrderItemsTest(){
        User testUser = x7S_TestUtility.createCommunityUser();

        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            ccrz__E_ProductMedia__c productMedia = x7S_TestUtility.createProductMedia(true, product1.Id);
            
            String userId = UserInfo.getUserId();
		    List<User> currentUserList =  [SELECT Id,ContactId FROM User WHERE Id=:userId LIMIT 1];
		    String contactId = currentUserList[0].ContactId;

            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            cart.ccrz__Contact__c = contactId;
            insert cart;

            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            insert cartItems;
            
            ccrz__E_ContactAddr__c shipToContact = x7S_TestUtility.createContactAddress(true);

            ccrz__E_Order__c testOrder = x7S_TestUtility.createOrder(false, contactId, testUser.Id);
            testOrder.ccrz__OriginatedCart__c = cart.Id;
            testOrder.ccrz__OrderDate__c = Date.today();
            testOrder.ccrz__OrderStatus__c = 'Shipped';
            testOrder.ccrz__ShipTo__c = shipToContact.Id;
            insert testOrder;

            ccrz__E_OrderItem__c orderItem = x7S_TestUtility.createOrderItem(false);
            orderItem.ccrz__Order__c = testOrder.Id;
            
            insert orderItem;

            Test.startTest();
            List<x7S_OrderItemModel> orderItems = x7S_DataTableController.getOrderItems();
            System.assert(orderItems.size() > 0);
            Test.stopTest();
            
        }
        
    }
}
