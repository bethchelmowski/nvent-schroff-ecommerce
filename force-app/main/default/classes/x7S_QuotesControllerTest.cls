/**
 * Created By ankitSoni on 07/16/2020
 */
@IsTest
public class x7S_QuotesControllerTest {
    
    @IsTest
    static void getQuotesTest(){
        Test.startTest();
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            
            String userId = UserInfo.getUserId();
		    List<User> currentUserList =  [SELECT Id,ContactId FROM User WHERE Id=:userId LIMIT 1];
		    String contactId = currentUserList[0].ContactId;
		
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            cart.ccrz__CartStatus__c = 'Submitted';
            cart.ccrz__Contact__c = contactId;
            cart.ERP_Number__c = '123123';
            insert cart;
            
            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            insert cartItems;

            
            RFQ__c testRFQ = x7S_TestUtility.createRFQ(false, cart.Id, cart.ccrz__Contact__c, cart.ccrz__Account__c);
            testRFQ.ERP_Number__c = '123123';
            testRFQ.SAP_Created_By__c = 'Mulelink';
            testRFQ.Owner_Doc__c = 'S7999';
            insert testRFQ;
        
            List<x7S_QuoteWrapper> quotes = x7S_QuotesController.getQuotes();
            System.assert(quotes.size() > 0);
            System.assertEquals(quotes[0].relatedCartId , cart.Id);            
            System.assertNotEquals(null, quotes[0].relatedCartId);
        }
        Test.stopTest();
    }

}
