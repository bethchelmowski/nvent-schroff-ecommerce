/*
 * Created by 7Summits, Inc.
 * Last modified 09.11.2020
*/
@RestResource(urlMapping='/nVent/*')
global with sharing class x7snVentToSAP {

    @httpPost
    global static void getPricing(){
        //get the JSON payload from the request body
        RestRequest req = RestContext.request;
        Blob body = req.requestBody;
    	String requestString = body.toString();
        
        String pricingResponseBodyJSON;
        String badRequestMessage = System.label.nVent_Request_getPricing_Failure;

        if(!Test.isRunningTest()){
            //call x7S_PricingRequestCallout class method that makes callout to mulesoft

            if(x7S_RequestValidator.isValidPricingRequest(requestString)){
                pricingResponseBodyJSON = x7S_PricingRequestCallout.getPricingInfo(requestString);
            }
            else {
                RestContext.response.statuscode = 400;
                pricingResponseBodyJSON = '{"success":'+false+',"message":"'+badRequestMessage+'"}';
            }

        } else{
            Test.setMock(HttpCalloutMock.class, new x7S_PricingRequestCalloutMock());
            pricingResponseBodyJSON = x7S_PricingRequestCallout.getPricingInfo(requestString);
        }

        system.debug('PriceResponseJSON >>> ' + pricingResponseBodyJSON);

        //put JSON in response body in order to prevent string encoding and escaping quotations in response
        RestContext.response.addHeader('Content-Type', 'application/json'); 
		RestContext.response.responseBody = Blob.valueOf(pricingResponseBodyJSON);
    }
    
}