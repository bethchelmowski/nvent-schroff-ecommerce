/**
 * Created By ankitSoni on 07/29/2020
 */
public without sharing class x7S_UserInfoController {
    
    /**
     * Description : Get All Rep information of current logged in User
     * @return : TeamMemberWrapper
     */
    @AuraEnabled(cacheable=true)
    public static List<TeamMemberWrapper> getRepInformation(){
        
        List<TeamMemberWrapper> repList = new List<TeamMemberWrapper>();
        Set<String> repUserIDs = new Set<String>();
        String teamMemberRole = '';
        Map<String,String> idToRoleMap = new Map<String,String>();

        //get current user-account
        String currentUserId = UserInfo.getUserId();
        List<User> currentUser = [SELECT Id,AccountId FROM User WHERE Id =:currentUserId LIMIT 1];
        String currentUserAccountID = currentUser[0].AccountId;
        
        List<AccountTeamMember> accountTeamMembersList = [SELECT Id , AccountId , TeamMemberRole , UserId FROM AccountTeamMember WHERE AccountId =:currentUserAccountID];

        if(accountTeamMembersList.size() > 0){
            for(AccountTeamMember atm : accountTeamMembersList){
                idToRoleMap.put(atm.UserId,atm.TeamMemberRole);
            }

            List<User> repUserQuery = [SELECT Id,Name,FirstName,LastName,Phone,MobilePhone,Email,SmallPhotoUrl,MediumPhotoUrl,Customer_Care_Email_address__c FROM User WHERE Id IN :idToRoleMap.keySet()];

            for(User user : repUserQuery){ 
                TeamMemberWrapper rep = new TeamMemberWrapper();
                rep.role = idToRoleMap.get(user.Id);
                rep.name = user.FirstName + ' ' + user.LastName;
                
                if(idToRoleMap.get(user.Id) == 'Schroff Customer Care'){
                    rep.email = user.Customer_Care_Email_address__c;
                    rep.phone = user.Phone;
                }
                else if(idToRoleMap.get(user.Id) == 'Schroff Rep'){
                    rep.email = user.Email;
                    rep.phone = user.MobilePhone;
                }
                rep.smallPhotoUrl = user.SmallPhotoUrl;
                rep.mediumPhotoUrl = user.MediumPhotoUrl;
                repList.add(rep);
            }
            
        }
        
        System.debug('Rep List of '+currentUserId+' -'+repList);
        return repList;
    }

    public class TeamMemberWrapper{
        @AuraEnabled public String name;
        @AuraEnabled public String phone;
        @AuraEnabled public String email;
        @AuraEnabled public String role;
        @AuraEnabled public String smallPhotoUrl;
        @AuraEnabled public String mediumPhotoUrl;        
    }
}
