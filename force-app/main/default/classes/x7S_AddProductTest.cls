/**
* Created By ankitSoni on 05/22/2020
*/
@isTest
public class x7S_AddProductTest {
    
    @isTest
    static void testAddProductSKU(){
        User testUser = x7S_TestUtility.createCommunityUser();
        x7S_AddProduct.AddProductWrapper addWrap = new x7S_AddProduct.AddProductWrapper();
        addWrap.itemNumber = '11527789';
        addWrap.itemImage = '';
        addWrap.itemDescription = 'Test Product Name';
        System.runAs(testUser) {
           
            Test.startTest();
            x7S_AddProduct.addProductSKU(addWrap, '0.000', '', 'USD');
            Test.stopTest();
        }
    }

    @isTest
    static void testAddExisitingProductSKU(){

        ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product Name', '11527789', '11527789');
            
            
        User testUser = x7S_TestUtility.createCommunityUser();
        x7S_AddProduct.AddProductWrapper addWrap = new x7S_AddProduct.AddProductWrapper();
        
        addWrap.itemNumber = '11527789';
        addWrap.itemImage = '';
        addWrap.itemDescription = 'Test Product Name';
        System.runAs(testUser) {
           
            Test.startTest();
            x7S_AddProduct.addProductSKU(addWrap, '0.000', '', 'USD');
            Test.stopTest();
        }
    }
}