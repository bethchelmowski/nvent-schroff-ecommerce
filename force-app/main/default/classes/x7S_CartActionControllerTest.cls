/**
* Created By ankitSoni on 05/27/2020
*/
@IsTest
public class x7S_CartActionControllerTest {

    @IsTest
    static void getCurrentCartTest(){
        Test.startTest();
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            ccrz__E_ProductMedia__c productMedia = x7S_TestUtility.createProductMedia(true, product1.Id);

            ccrz__E_Product__c product2 = x7S_TestUtility.createProduct(true, 'Test Product-2', 'SKU-2', 'SKU-2');
            ccrz__E_ProductMedia__c productMedia2 = x7S_TestUtility.createProductMedia(true, product2.Id);

            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            insert cart;
            
            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            cartItems[0].ccrz__cartItemType__c = 'Major';
            insert cartItems;

            List<ccrz__E_CartItem__c> minorCartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            minorCartItems[0].ccrz__Product__c = product2.Id;
            minorCartItems[0].ccrz__cartItemType__c = 'Minor';
            minorCartItems[0].ccrz__ParentCartItem__c = [SELECT Id FROM ccrz__E_CartItem__c WHERE ccrz__Product__c =: product1.Id LIMIT 1].Id;
            insert minorCartItems;

            x7S_CartModel model = x7S_CartActionController.getCurrentCart();
            System.assertEquals(cart.Id, model.cartId);
            System.assertNotEquals(null, model.cartId);
        }
        Test.stopTest();
    }

    @IsTest
    static void getCartByIdTest(){
        Test.startTest();
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            ccrz__E_ProductMedia__c productMedia = x7S_TestUtility.createProductMedia(true, product1.Id);

            ccrz__E_Product__c product2 = x7S_TestUtility.createProduct(true, 'Test Product-2', 'SKU-2', 'SKU-2');
            ccrz__E_ProductMedia__c productMedia2 = x7S_TestUtility.createProductMedia(true, product2.Id);

            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            insert cart;
            
            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            cartItems[0].ccrz__cartItemType__c = 'Major';
            insert cartItems;

            List<ccrz__E_CartItem__c> minorCartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            minorCartItems[0].ccrz__Product__c = product2.Id;
            minorCartItems[0].ccrz__cartItemType__c = 'Minor';
            minorCartItems[0].ccrz__ParentCartItem__c = [SELECT Id FROM ccrz__E_CartItem__c WHERE ccrz__Product__c =: product1.Id LIMIT 1].Id;
            insert minorCartItems;

            x7S_CartModel model = x7S_CartActionController.getCartById(cart.Id ,'Shared Cart');
            System.assertEquals(cart.Id, model.cartId);
            System.assertNotEquals(null, model.cartId);
        }
        Test.stopTest();
    }

    @IsTest
    static void shareCartTest(){
        Test.startTest();
        String shareEmailAddress = 'testEmail@gmail.com';
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            cart.ccrz__CartStatus__c = 'Submitted';
            insert cart;
            
            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            insert cartItems;

            Boolean result = x7S_CartActionController.shareCart(shareEmailAddress, cart.Id);
            System.assertEquals(true, result);
        }
        Test.stopTest();

    }

    @IsTest
    static void getSubmittedCartsTest(){
        Test.startTest();
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            
            String userId = UserInfo.getUserId();
		    List<User> currentUserList =  [SELECT Id,ContactId FROM User WHERE Id=:userId LIMIT 1];
		    String contactId = currentUserList[0].ContactId;
		
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            cart.ccrz__CartStatus__c = 'Submitted';
            cart.ccrz__Contact__c = contactId;
            insert cart;
            
            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            insert cartItems;

            List<x7S_SubmittedCartModel> modelList = x7S_CartActionController.getSubmittedCarts();
            System.assertEquals(cart.Id, modelList[0].cartId);
            System.assertNotEquals(null, modelList[0].cartId);
        }
        Test.stopTest();

    }

    @IsTest
    static void requestQuoteTest(){
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            insert cart;
            
            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            insert cartItems;

            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new x7S_RequestForQuoteMock());
            Boolean requestQuote = x7S_CartActionController.requestQuote();
            System.assertEquals(true,requestQuote);
            Test.stopTest();

        } 

    }

    @IsTest
    static void requestQuoteTestZeroPriced(){

        User testUser = x7S_TestUtility.createCommunityUser();

        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test KIM Product-1', 'N200924A008', 'N200924A008');
            ccrz__E_Product__c product2 = x7S_TestUtility.createProduct(true, 'Test KIM Minor Product-1', 'SKU-123', 'SKU-123');
            
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            insert cart;
            
            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createZeroPricedCartItem(1, cart.Id, false);
            
            cartItems[0].ccrz__Product__c = product1.Id;
            cartItems[0].ccrz__cartItemType__c = 'Major';

            insert cartItems;
    
            List<ccrz__E_CartItem__c> minorCartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            minorCartItems[0].ccrz__Product__c = product2.Id;
            minorCartItems[0].ccrz__cartItemType__c = 'Minor';
            minorCartItems[0].ccrz__ParentCartItem__c = cartItems[0].Id;
            insert minorCartItems;

            Test.startTest();
            
            Boolean requestQuote = x7S_CartActionController.requestQuote();
            System.assertEquals(true,requestQuote);
            
            Test.stopTest();
                
        }

        
        
    }
    @IsTest
    static void updateCartItemsTest(){
        Test.startTest();
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            insert cart;
            
            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            cartItems[0].ccrz__Quantity__c = 2;
            insert cartItems;

            List<String> itemIdList = new List<String>();
            itemIdList.add(cartItems[0].Id);

            // Test Add/Update action
            x7S_CartModel model = x7S_CartActionController.updateCartItems('update', itemIdList, '5', '2');
            System.assertEquals(cart.Id,model.cartId);
            System.assertNotEquals(null, model.cartId);
        } 
        Test.stopTest();

    }

    @IsTest
    static void updateCartItemsTest_delete(){
        Test.startTest();
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            
            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            insert cart;
            
            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            cartItems[0].ccrz__Quantity__c = 2;
            insert cartItems;

            List<String> itemIdList = new List<String>();
            itemIdList.add(cartItems[0].Id);

            // Test Delete action
            x7S_CartModel deleteModel = x7S_CartActionController.updateCartItems('delete', itemIdList, '2', '2');
            System.assertEquals(cart.Id,deleteModel.cartId);
            System.assertNotEquals(null, deleteModel.cartId);
        } 
        Test.stopTest();
    }
    
    @IsTest
    static void cloneSubmittedCartTest(){
        Test.startTest();
        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            
            //Current Cart
            ccrz__E_Cart__c currentCart = x7S_TestUtility.createCart(false);
            currentCart.ccrz__Storefront__c = 'DefaultStore';
            currentCart.ccrz__CartStatus__c = 'Submitted';
            insert currentCart;

            //Submitted Cart
            ccrz__E_Cart__c submittedCart = x7S_TestUtility.createCart(false);
            submittedCart.ccrz__Storefront__c = 'DefaultStore';
            submittedCart.ccrz__CartStatus__c = 'Submitted';
            insert submittedCart;
            
            List<ccrz__E_CartItem__c> submittedCartItems = x7S_TestUtility.createCartItem(1, submittedCart.Id, false);
            submittedCartItems[0].ccrz__Product__c = product1.Id;
            insert submittedCartItems;

            Boolean cloneResult = x7S_CartActionController.cloneSubmittedCart(submittedCart.Id);
            System.assertEquals(true, cloneResult);
            
        }
        Test.stopTest();

    }
}