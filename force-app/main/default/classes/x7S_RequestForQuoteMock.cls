@isTest
global class x7S_RequestForQuoteMock implements HttpCalloutMock{

    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
     
        // Create a fake response
        String fakeBody = '{"statusCode":"201","message":"Quotation created successfully","salesDocumentNumber":"0020247991"}';
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(fakeBody);
        res.setStatusCode(200);
        return res;
    }
}