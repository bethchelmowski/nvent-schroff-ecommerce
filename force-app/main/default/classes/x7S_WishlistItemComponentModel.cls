/**
 * Created By ankitSoni on 08/10/2020
 */
public with sharing class x7S_WishlistItemComponentModel {
    
    @AuraEnabled
	public String id, parentCartItemId , productId, productName, productSkuCode, productDescription;
    
	@AuraEnabled
    public Decimal quantity; 
}
