
@IsTest
public class x7S_BOMServiceCalloutTest {
    
    @TestSetup
    static void makeData(){
        ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test KIM Product-1', 'N200924A008', 'N200924A008');
        ccrz__E_Product__c product2 = x7S_TestUtility.createProduct(true, 'Test KIM Minor Product-1', 'SKU-123', 'SKU-123');
        
        ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
        cart.ccrz__Storefront__c = 'DefaultStore';
        insert cart;
        
        List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createZeroPricedCartItem(1, cart.Id, false);
        
        cartItems[0].ccrz__Product__c = product1.Id;
        cartItems[0].ccrz__cartItemType__c = 'Major';
        cartItems[0].nVent_SKU_Url__c = 'https://schroff.issue-1223.nvent.kube.fayze2.com/en-us/p/ENC_N200924A008';
        cartItems[0].KIM_Product__c = true;
        cartItems[0].KIM_Configurator_Modify_URL__c ='https://schroff.issue-1223.nvent.kube.fayze2.com/en-us/configurators/3d/novastar/update?sapNumber=N200924A008&configId=aeb200c15f2ec767983246c156bb9886';
        cartItems[0].KIM_Configurator_Copy_URL__c = 'https://schroff.issue-1223.nvent.kube.fayze2.com/en-us/configurators/3d/novastar/new?configId=aeb200c15f2ec767983246c156bb9886';
        cartItems[0].KIM_PDF_URL__c = 'https://schroff-configurator-stage.nventco.com/Novastar/ProjectFiles/DATA/aeb200c15f2ec767983246c156bb9886.zip';
        cartItems[0].KIM_Configurator_Id__c = 'N200924A008';
        cartItems[0].KIM_Tracking_Number__c = 'aeb200c15f2ec767983246c156bb9886';
    
        insert cartItems;

        List<ccrz__E_CartItem__c> minorCartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
        minorCartItems[0].ccrz__Product__c = product2.Id;
        minorCartItems[0].ccrz__cartItemType__c = 'Minor';
        minorCartItems[0].ccrz__ParentCartItem__c = cartItems[0].Id;
        insert minorCartItems;


    }

    @IsTest
    static void bomRequestTest(){
        ccrz__E_Cart__c cart = [SELECT Id  FROM ccrz__E_Cart__c LIMIT 1];
            
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new x7S_BOMServiceCalloutMock());
        x7S_BOMServiceCallout.bomRequest(cart.Id);
        Test.stopTest();
            
    }
}
