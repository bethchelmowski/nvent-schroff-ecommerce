public class x7S_QuoteWrapper {
    
    @AuraEnabled
    public String quoteId , quoteName ,quoteStatus , quoteNumber , relatedCartId;

    @AuraEnabled
    public String cartNumber , cartCurrencyISOCode;
    
    @AuraEnabled
    public Decimal totalAmount,distinctSKUs;

    @AuraEnabled
    public Date lastModifiedDate;
    
}
