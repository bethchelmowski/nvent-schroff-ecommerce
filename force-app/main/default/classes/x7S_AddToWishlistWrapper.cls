/**
 * Created By ankitSoni on 08/18/2020
 * Description : Wrapper Class for /nVent/addToWishlist API Request
 */
public class x7S_AddToWishlistWrapper {
    public String customerNumber;
    public String userId;
    public List<Items> items;
    
    public class Items {
        public String itemNumber;
        public String itemImage;
        public String itemDescription;
        public String skuUrl;
        public String configuratorCopyURL;
        public String configuratorModifyURL;
        public String zipURL;
        public String sapGUID;
        public String configurationID;
        public List<Components> components;
    }

    public class Components {
        public String itemNumber;
        public String itemImage;
        public String itemDescription;
        public String skuUrl;
    }
    
    public String findSkuUrl(String itemNumber){
        for(Items item: items){
            if(item.itemNumber == itemNumber){
                return item.skuUrl;
            }
        }

        return null;
    }
    
    public static x7S_AddToWishlistWrapper parse(String json) {
        return (x7S_AddToWishlistWrapper) System.JSON.deserialize(json, x7S_AddToWishlistWrapper.class);
    }
}