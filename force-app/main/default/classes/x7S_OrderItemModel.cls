public class x7S_OrderItemModel {
    
    @AuraEnabled
    public String skuNumber , productName , shippingCarrier;
    
    @AuraEnabled
    public Decimal quantityOrder , quantityCancelled , quantityShipped;

    @AuraEnabled
    public String promisedDate , shipDate;

    @AuraEnabled
    public String status , trackingNumber;
    
}

