/**
* Created By ankitSoni on 09/23/2020
*/
public class x7S_PrepareBOMCalloutData {
    

    public static String createXMLData(List<ccrz__E_CartItem__c> minorItemList) {
        
        String xmlString;
        ccrz__E_CartItem__c parentItem = new ccrz__E_CartItem__c();
        String parentItemId;

        for(ccrz__E_CartItem__c item : minorItemList){

            if(item.ccrz__cartItemType__c == 'Minor'){
                parentItemId = item.ccrz__ParentCartItem__c;
            }

        }

        ccrz__E_CartItem__c parentCartItem = [
            SELECT Id , KIM_Configurator_Copy_URL__c ,KIM_PDF_URL__c , KIM_Configurator_Id__c,KIM_Tracking_Number__c ,ccrz__Product__r.ccrz__SKU__c
            FROM ccrz__E_CartItem__c 
            WHERE Id =:parentItemId
            LIMIT 1];

        String zipUrl = (parentCartItem.KIM_PDF_URL__c == null) ? 'testzipurl' : parentCartItem.KIM_PDF_URL__c;
        String configurationID = (parentCartItem.KIM_Configurator_Id__c == null ? 'testconfigId': parentCartItem.KIM_Configurator_Id__c );
        String sapGUID = (parentCartItem.ccrz__Product__r.ccrz__SKU__c == null ? 'testsapGUID ': parentCartItem.ccrz__Product__r.ccrz__SKU__c );
            
        DOM.Document doc = new DOM.Document();
        
        //Create Root XMLNode <Bom>
        dom.XmlNode bomRootNode = doc.createRootElement('Bom', null, null);
        bomRootNode.setAttribute('zipUrl', zipUrl );
        bomRootNode.setAttribute('configurationID', configurationID);
        bomRootNode.setAttribute('sapGUID', sapGUID);

        //Create child elements <item>
        for(ccrz__E_CartItem__c component : minorItemList){

            dom.XmlNode itemNode = bomRootNode.addChildElement('item', null, null);

            itemNode.addChildElement('partNumber', null , null).addTextNode(component.ccrz__Product__r.ccrz__SKU__c);
            itemNode.addChildElement('partDescription', null , null).addTextNode(component.ccrz__Product__r.Name);
            itemNode.addChildElement('quantity', null , null).addTextNode(String.valueOf(component.nVent_Quantity__c));
            itemNode.addChildElement('location', null , null).addTextNode('');
        }

        
        xmlString = doc.toXmlString();

        return xmlString;
    }

    public static Map<Id,String> createJSONBodyData(Map<Id,ccrz__E_CartItem__c> majorItemMap , Map<Id , String> xmlStringMap) {
        Map<Id , String> jsonMap = new Map<Id , String >();

        String jsonString; 
        RequestBodyWrapper jsonBodyWrapper = new RequestBodyWrapper();

        for(ccrz__E_CartItem__c majorItem : majorItemMap.values()){

            if(xmlStringMap.containsKey(majorItem.Id)){
                jsonBodyWrapper.trackerNo = majorItem.KIM_Tracking_Number__c;
                jsonBodyWrapper.bomXml = xmlStringMap.get(majorItem.Id);
                jsonBodyWrapper.timestamp = '';         //TBD
                
                jsonString = JSON.serializePretty(jsonBodyWrapper);
                jsonMap.put(majorItem.Id , jsonString);
            }

        }

        return jsonMap;
    }

    public class RequestBodyWrapper {
        public string trackerNo;
        public string bomXml;
        public string timestamp;

    }
}