@isTest
global class x7S_BOMServiceCalloutMock implements HttpCalloutMock{

    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
     
        // Create a fake response
        String fakeBody = '{"statusCode": "201","message": "Data saved in staging table ZTRACEPARTS_BOM"}';
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(fakeBody);
        res.setStatusCode(200);
        return res;
    }
}