@isTest
public class x7snVentCartTest{
    
    @TestSetup
    static void makeData(){
        
        
    }

    @isTest
    static void getCartTest(){
        User testUser = x7S_TestUtility.createCommunityUser();
        
        ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');

        ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
        cart.ccrz__Storefront__c = 'DefaultStore';
        cart.ccrz__User__c = testUser.Id;
        cart.ccrz__ActiveCart__c = true;
        insert cart;
        
        List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
        cartItems[0].ccrz__Product__c = product1.Id;
        cartItems[0].ccrz__Quantity__c = 2;
        insert cartItems;

        System.runAs(testUser){
            Test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/nVent/cart';  //Request URL
            req.httpMethod = 'GET';//HTTP Request Type
            req.addParameter('customerNumber', '7000021793');
            req.addParameter('cartType', 'Cart');
            req.addParameter('userId', String.valueOf(testUser.Id));
            RestContext.request = req;
            RestContext.response= res;
            x7snVentCart.getCart();
            Test.stopTest();
        }    
    }
    
    @isTest
    static void testAddToCart(){
        User testUser = x7S_TestUtility.createCommunityUser();

        System.runAs(testUser){
            Test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/nVent/cart';  //Request URL
            req.httpMethod = 'POST';//HTTP Request Type
            String input = '{"userId":"+' +testUser.Id+'","customerNumber": "7000021793","countryOfUser": "","items": [{"itemNumber": "60110456","quantity": "1.000","unitOfMeasure": ""}]}';
            //req.requestBody = Blob.valueOf(JSON.serialize(input));
            RestContext.request = req;
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(input));
            RestContext.response= res;
            x7snVentCart.addToCart();
            Test.stopTest();
        }
    
    }
}