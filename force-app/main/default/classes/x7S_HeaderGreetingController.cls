/**
 * Created By ankitSoni on 09/02/2020
 */
public class x7S_HeaderGreetingController {
    
    /**
     * Description : Get number of wishlist items of current logged in user
     * @return : wishlistItemCount
     */
    @AuraEnabled
    public static Integer getWishlistItemCount(){
        Integer wishlistItemCount;
        String currentWishlistId;

        String userId = UserInfo.getUserId();

        //get current wishlist of the user
        List<ccrz__E_Cart__c> wishlist = [SELECT Id, ccrz__ActiveCart__c, ccrz__CartType__c,ccrz__User__c
                                        FROM ccrz__E_Cart__c
                                        WHERE ccrz__User__c=: userId
                                        AND ccrz__ActiveCart__c=true
                                        AND ccrz__CartType__c='WishList'
                                        LIMIT 1];
        for(ccrz__E_Cart__c cart: wishlist){
            currentWishlistId = cart.Id;
        }

        List<AggregateResult> aggregateResultList = [SELECT count(Id) itemCount FROM ccrz__E_CartItem__c WHERE ccrz__Cart__c =:currentWishlistId AND ccrz__cartItemType__c = 'Major'];
        if(aggregateResultList != null && aggregateResultList.size() > 0){
            for(AggregateResult aggr : aggregateResultList){
                wishlistItemCount = (Integer) aggr.get('itemCount');
            }
    
        }

        return wishlistItemCount;
    }
    
    /**
     * Description : Get number of orders of current logged in user
     * @return : ordersCount
     */
    @AuraEnabled(cacheable=true)
    public static Integer getOrderCount(){
        Integer ordersCount;

        String userId = UserInfo.getUserId();
		List<User> currentUserList =  [SELECT Id,ContactId FROM User WHERE Id=:userId LIMIT 1];
		String contactId = currentUserList[0].ContactId;
		

        List<AggregateResult> aggregateResultList = [SELECT count(Id) orderCount FROM ccrz__E_Order__c WHERE ccrz__User__c =:userId AND ccrz__Contact__c =:contactId];
        if(aggregateResultList != null && aggregateResultList.size() > 0){
            for(AggregateResult aggr : aggregateResultList){
                ordersCount = (Integer) aggr.get('orderCount');
            }
    
        }
        
        return ordersCount;
    }


}
