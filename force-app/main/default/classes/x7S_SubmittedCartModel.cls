/**
 * Created By ankitSoni on 06/07/2020
 */

public with sharing class x7S_SubmittedCartModel {
    
    @AuraEnabled
    public String cartId , cartNumber , currencyISOCode;
    
    @AuraEnabled
    public Decimal totalAmount,distinctSKUs;

    @AuraEnabled
    public Date lastModifiedDate;

    public x7S_SubmittedCartModel() {

    }
}
