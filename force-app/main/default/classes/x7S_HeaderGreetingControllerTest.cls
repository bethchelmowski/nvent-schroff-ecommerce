/**
* Created By ankitSoni on 09/03/2020
*/
@isTest
public class x7S_HeaderGreetingControllerTest {
    @IsTest
    static void getWishlistItemCountTest(){

        User testUser = x7S_TestUtility.createCommunityUser();

        Test.startTest();
        System.runAs(testUser){
            String userId = UserInfo.getUserId();
		    List<User> currentUserList =  [SELECT Id,ContactId FROM User WHERE Id=:userId LIMIT 1];
		    String contactId = currentUserList[0].ContactId;

            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            ccrz__E_ProductMedia__c productMedia = x7S_TestUtility.createProductMedia(true, product1.Id);
            
            ccrz__E_Cart__c wishlist = x7S_TestUtility.createWishlist(false);
            wishlist.ccrz__Storefront__c = 'DefaultStore';
            wishlist.ccrz__ActiveCart__c = true;
            wishlist.ccrz__CurrencyISOCode__c = 'USD';
            wishlist.ccrz__User__c = testUser.Id;
            wishlist.ccrz__Contact__c = contactId;
            
            insert wishlist;
            
            List<ccrz__E_CartItem__c> wishlistItems = x7S_TestUtility.createCartItem(1, wishlist.Id, false);
            wishlistItems[0].ccrz__cartItemType__c = 'Major';
            wishlistItems[0].ccrz__Product__c = product1.Id;
            wishlistItems[0].ccrz__Price__c = 0.00;
            wishlistItems[0].ccrz__Quantity__c = 1.00;
            insert wishlistItems;

            Integer wishlistItemCount = x7S_HeaderGreetingController.getWishlistItemCount();
            System.assertEquals(1, wishlistItemCount);
        }
        Test.stopTest();      
    }

    @IsTest
    static void getOrderCountTest(){

        User testUser = x7S_TestUtility.createCommunityUser();
        
        System.runAs(testUser){
            ccrz__E_Product__c product1 = x7S_TestUtility.createProduct(true, 'Test Product-1', 'SKU-1', 'SKU-1');
            ccrz__E_ProductMedia__c productMedia = x7S_TestUtility.createProductMedia(true, product1.Id);
            
            String userId = UserInfo.getUserId();
		    List<User> currentUserList =  [SELECT Id,ContactId FROM User WHERE Id=:userId LIMIT 1];
		    String contactId = currentUserList[0].ContactId;

            ccrz__E_Cart__c cart = x7S_TestUtility.createCart(false);
            cart.ccrz__Storefront__c = 'DefaultStore';
            cart.ccrz__Contact__c = contactId;
            insert cart;

            List<ccrz__E_CartItem__c> cartItems = x7S_TestUtility.createCartItem(1, cart.Id, false);
            cartItems[0].ccrz__Product__c = product1.Id;
            insert cartItems;
            
            ccrz__E_ContactAddr__c shipToContact = x7S_TestUtility.createContactAddress(true);

            ccrz__E_Order__c testOrder = x7S_TestUtility.createOrder(false, contactId, testUser.Id);
            testOrder.ccrz__OriginatedCart__c = cart.Id;
            testOrder.ccrz__OrderDate__c = Date.today();
            testOrder.ccrz__OrderStatus__c = 'Shipped';
            testOrder.ccrz__ShipTo__c = shipToContact.Id;
            insert testOrder;

            Test.startTest();
            Integer ordersCount = x7S_HeaderGreetingController.getOrderCount();
            System.assertEquals(1, ordersCount);
            Test.stopTest();
        }
    }
}
