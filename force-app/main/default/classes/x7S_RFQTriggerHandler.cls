/**
 * Created By ankitSoni on 07/22/2020
 */

public class x7S_RFQTriggerHandler {
    
    public static void onBeforeInsert(List<RFQ__c> newListRFQ){
        System.debug('onBeforeInsert');
        createCartRelationship(newListRFQ);
    }

    public static void createCartRelationship(List<RFQ__c> newListRFQ){
        System.debug('createCartRelationship()');
        String quoteNumber = '';                                              
        Map<String,Id> quoteNumberToCartIdMap = new Map<String,Id>();
        Map<Id,Id> cartIdToContactMap = new Map<Id,Id>();
        Map<Id,Id> cartIdToAccountMap = new Map<Id,Id>();
        
        for(RFQ__c rfq : newListRFQ){
            if(rfq.SAP_Created_By__c == 'Mulelink' && rfq.Owner_Doc__c == 'S7999'){
                quoteNumber = rfq.ERP_Number__c;
            }
        }

        List<ccrz__E_Cart__c> submittedCarts = [SELECT Id,ERP_Number__c,ccrz__Contact__c,ccrz__Account__c 
                                                FROM ccrz__E_Cart__c 
                                                WHERE ERP_Number__c =:quoteNumber AND ccrz__CartStatus__c='Submitted' AND ccrz__ActiveCart__c=false ];
        

        if(submittedCarts.size() > 0){
            for(ccrz__E_Cart__c cart : submittedCarts){
                quoteNumberToCartIdMap.put(cart.ERP_Number__c, cart.Id);
                cartIdToContactMap.put(cart.Id,cart.ccrz__Contact__c);
                cartIdToAccountMap.put(cart.Id,cart.ccrz__Account__c);  
            }
    
            System.debug('QuoteCartIdMap'+quoteNumberToCartIdMap);
            System.debug('CartIdContactMap'+cartIdToContactMap);
            System.debug('cartIdToAccountMap'+cartIdToAccountMap);
                                                                                    
            for(RFQ__c rfq : newListRFQ){
    
                if(rfq.SAP_Created_By__c == 'Mulelink' && rfq.Owner_Doc__c == 'S7999'){
                    
                    if(quoteNumberToCartIdMap.containsKey(rfq.ERP_Number__c)){
                        rfq.Related_Cart__c = quoteNumberToCartIdMap.get(rfq.ERP_Number__c);
                        rfq.Contact__c = cartIdToContactMap.get(quoteNumberToCartIdMap.get(rfq.ERP_Number__c));
                        rfq.Account__c = cartIdToAccountMap.get(quoteNumberToCartIdMap.get(rfq.ERP_Number__c));
                    }
                }
            }

        }                                        
    }
}
