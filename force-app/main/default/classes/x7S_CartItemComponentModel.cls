/**
 * Created By ankitSoni on 06/07/2020
 */
public with sharing class x7S_CartItemComponentModel {
    
    @AuraEnabled
	public String id, parentCartItemId , productId, productName, productSkuCode, productDescription;
    
	@AuraEnabled
    public Decimal listPrice, netPrice, quantity, subTotal;

}
