@isTest
public class x7snVentWishlistTest{
        
    @isTest
    static void testAddToWishlist(){
        User testUser = x7S_TestUtility.createCommunityUser();

        System.runAs(testUser){
            Test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/nVent/wishlist';  //Request URL
            req.httpMethod = 'POST';//HTTP Request Type
            String input = '{"userId":"+' +testUser.Id+'","customerNumber": "7000021793","items": [{"itemNumber": "60110456"}]}';
            //req.requestBody = Blob.valueOf(JSON.serialize(input));
            RestContext.request = req;
            RestContext.request.requestBody = Blob.valueOf(JSON.serialize(input));
            RestContext.response= res;
            x7snVentWishlist.addToWishlist();
            Test.stopTest();
        }
    
    }
}