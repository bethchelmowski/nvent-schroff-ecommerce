/**
 * Created By ankitSoni on 08/17/2020
 * Description : Wrapper Class for /nVent/addToCart API Request
 */
public class x7S_AddToCartWrapper {
    public String userId;
    public String customerNumber;
    public List<Items> items;
    
    public class Items {
        public String itemNumber;
        public String itemImage;
        public String itemDescription;
        public String skuUrl;
        public String quantity;
        public String unitOfMeasure;
        public String configuratorCopyURL;
        public String configuratorModifyURL;
        public String zipURL;
        public String sapGUID;
        public String configurationID;
        public List<Components> components;
    }

    public class Components {
        public String itemNumber;
        public String itemImage;
        public String itemDescription;
        public String skuUrl;
        public String quantity;
        public String unitOfMeasure;
    }
    
    public String findSkuUrl(String itemNumber){
        for(Items item: items){
            if(item.itemNumber == itemNumber){
                return item.skuUrl;
            }
        }

        return null;
    }

    public static x7S_AddToCartWrapper parse(String json) {
        return (x7S_AddToCartWrapper) System.JSON.deserialize(json, x7S_AddToCartWrapper.class);
    }
}