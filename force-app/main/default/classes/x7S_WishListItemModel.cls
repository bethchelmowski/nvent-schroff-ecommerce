/**
 * Created By ankitSoni on 05/28/2020
 */
public with sharing class x7S_WishListItemModel {
    @AuraEnabled
	public String id, productId, productName, productSkuCode , productDescription;
    
    @AuraEnabled
	public String imageUrl , itemURL;

	@AuraEnabled
    public Decimal quantity;

    @AuraEnabled
    public Date wishlistItemAddedDate;

    //KIM Products item fields
    @AuraEnabled
    public String configuratorCopyURL, configuratorModifyURL, zipURL , configurationID , sapGUID;

    @AuraEnabled
    public Boolean isKIMProduct;
    
    @AuraEnabled
    public List<x7S_WishlistItemComponentModel> components;

    
}
