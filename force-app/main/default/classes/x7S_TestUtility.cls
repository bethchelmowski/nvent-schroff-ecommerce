public class x7S_TestUtility {
    
    /** 
    * Method Name: createCommunityUser
    * Description: Creating Community user record 
    * @return: User
    */
    public static User createCommunityUser(){
        Profile communityUserProfile = [SELECT Id FROM Profile WHERE Name='nVent Customer Community Plus Login User Profile' LIMIT 1];
        Map<String,Object> retData =
            ccrz.ccApiTestData.setupData(new Map<String,Map<String,Object>>{
                ccrz.ccApiTestData.ACCOUNT_DATA => new Map<String,Object>{
                    ccrz.ccApiTestData.ACCOUNT_LIST => new List<Map<String,Object>>{
                        new Map<String,Object>{
                            'name' => 'testAccount1',
                                'ccrz__dataId__c' => 'testAccount1'
                                }
                    }
                },
                    ccrz.ccApiTestData.CONTACT_DATA => new Map<String,Object>{
                        ccrz.ccApiTestData.CONTACT_LIST => new List<Map<String,Object>>{
                            new Map<String,Object>{
                                'ccrz__dataId__c' => 'testContact1',
                                    'account' => new Account(ccrz__dataId__c = 'testAccount1'),
                                    'email' => 'testcontact1.ccrz@cloudcraze.com',
                                    'lastName' => 'Contact1',
                                    'firstName' => 'test'
                                    }
                        }
                    },
                        ccrz.ccApiTestData.USER_DATA => new Map<String,Object>{
                            ccrz.ccApiTestData.USER_LIST => new List<Map<String,Object>>{
                                new Map<String,Object>{
                                    'ccrz__dataId__c' => 'testUser1',
                                        'alias' => 'defusr1',
                                        'email' => 'test.ccrz1@cloudcraze.com',
                                        'lastName' => 'User1',
                                        'firstName' => 'Test1',
                                        'languageLocaleKey' => 'fr',
                                        'localeSIDKey' => 'fr_FR',
                                        'emailEncodingKey' => 'UTF-8',
                                        'profileId' => communityUserProfile.Id,
                                        'username' => System.currentTimeMillis() + 'test1@cloudcraze.com',
                                        'ccrz__CC_CurrencyCode__c' => 'EUR',
                                        'contact' => new Contact(ccrz__dataId__c = 'testContact1'),
                                        'timezoneSIDKey' => 'GMT'
                                        }
                            }
                        }
            });
        Map<String,Object> theData = (Map<String,Object>)retData.get(ccrz.ccApiTestData.USER_DATA);
        List<sObject> theList = (List<sObject>)theData.get(ccrz.ccApiTestData.USER_LIST); 
        User theUser = (User)theList.get(0);
        
        return theUser;
    }

    /**
    * Method Name: createUser
    * Description: Creating User record.
    * @param: Boolean isInsert , FirstName
    * @return: User 
    */    
    public static User createUser(Boolean isInsert , String fname){
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User user = new User(LastName = 'userTest',
                            FirstName=fname,
                            Alias = 'tttt',
                            Email = fname+'.testieUser@asdf.com',
                            Username = fname+'.testieUser@asdf.com.test',
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US');
        
        if(isInsert){
            insert user;
        }
        return user;    
    }
    
    /**
    * Method Name: createAccountTeamMember
    * Description: Creating AccountTeamMember record.
    * @param: Boolean isInsert , AccountId , UserId , TeamMemberRole
    * @return: User 
    */    
    public static AccountTeamMember createAccountTeamMember(Boolean isInsert , String accountId , String userId , String teamMemberRole){
        AccountTeamMember atm = new AccountTeamMember();
        atm.UserId = userId;
        atm.AccountId = accountId;
        atm.TeamMemberRole = teamMemberRole;
        
        if(isInsert){
            insert atm;
        }
        
        return atm;

    }

    /**
    * Method Name: createCart
    * Description: Creating cc cart record.
    * @param: Boolean isInsert 
    * @return: ccrz__E_Cart__c 
    */    
    public static ccrz__E_Cart__c createCart(Boolean isInsert ){
        ccrz__E_Cart__c cart = new ccrz__E_Cart__c();
        cart.ccrz__CartStatus__c = 'Open';
        cart.ccrz__CartType__c = 'Cart';
        cart.ccrz__User__c = UserInfo.getUserId();
        
        if(isInsert){
            insert cart;
        }
        return cart;
    }

    public static List<ccrz__E_CartItem__c> createCartItem(Integer noOfRecords , Id cartId, Boolean isInsert ){
        List<ccrz__E_CartItem__c> cartItemList = new List<ccrz__E_CartItem__c>();
        
        for(Integer i = 1; i <= noOfRecords; i++){
            ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c();
            cartItem.ccrz__Cart__c = cartId;
            cartItem.ccrz__Quantity__c = 1.000;
            cartItem.ccrz__Price__c = 100.000;
            cartItem.List_Price__c = 100.000;
            cartItem.ccrz__ItemStatus__c = 'Available';
            cartItemList.add(cartItem);    
        }
        if(isInsert){
            insert cartItemList;
        }
        return cartItemList;
    } 

    public static List<ccrz__E_CartItem__c> createZeroPricedCartItem(Integer noOfRecords , Id cartId, Boolean isInsert ){
        List<ccrz__E_CartItem__c> cartItemList = new List<ccrz__E_CartItem__c>();
        
        for(Integer i = 1; i <= noOfRecords; i++){
            ccrz__E_CartItem__c cartItem = new ccrz__E_CartItem__c();
            cartItem.ccrz__Cart__c = cartId;
            cartItem.ccrz__Quantity__c = 1.000;
            cartItem.ccrz__Price__c = 0.000;
            cartItem.List_Price__c = 0.000;
            cartItem.ccrz__SubAmount__c = 0.000;
            cartItem.ccrz__ItemStatus__c = 'Available';
            cartItemList.add(cartItem);    
        }
        if(isInsert){
            insert cartItemList;
        }
        return cartItemList;
    } 
    
    /**
    * Method Name: createCart
    * Description: Creating cc cart record.
    * @param: Boolean isInsert 
    * @return: ccrz__E_Cart__c 
    */    
    public static ccrz__E_Cart__c createWishlist(Boolean isInsert ){
        ccrz__E_Cart__c wishlist = new ccrz__E_Cart__c();
        wishlist.ccrz__CartStatus__c = 'Open';
        wishlist.ccrz__CartType__c = 'WishList';
        wishlist.ccrz__User__c = UserInfo.getUserId();
        
        if(isInsert){
            insert wishlist;
        }
        return wishlist;
    }

    /*
    * Method Name: createProduct
    * Description: Creating cc product record.
    * @param: Boolean isInsert,String prodName,String prodId, String prodSku 
    * @return: ccrz__E_Product__c
    */
    public static ccrz__E_Product__c createProduct(Boolean isInsert,String prodName,String prodId, String prodSku){
        ccrz__E_Product__c prod = new ccrz__E_Product__c();
        prod.Name=prodName;
        prod.ccrz__ProductId__c =prodId;
        prod.ccrz__SKU__c=prodSku;
        prod.ccrz__Storefront__c='DefaultStore';
        prod.ccrz__ProductStatus__c ='Released';
        prod.ccrz__LongDescRT__c='dummy product with dummy description';
        prod.ccrz__ShortDescRT__c='dummy product with dummy description';
        
        if(isInsert){
            insert prod;
        }
        return prod;
    }

    /*
    * Method Name: createProductMedia
    * Description: Creating cc productmedia record.
    * @param: Boolean isInsert ,Id productId 
    * @return: ccrz__E_ProductMedia__c
    */
    public static ccrz__E_ProductMedia__c createProductMedia(Boolean isInsert , Id prodId){
        ccrz__E_ProductMedia__c prodMedia=new ccrz__E_ProductMedia__c();
        prodMedia.ccrz__FilePath__c='BC-COFMAC.jpeg';
        prodMedia.ccrz__StaticResourceName__c='CC_Capricorn_Assets_2';
        prodMedia.ccrz__MediaType__c = 'Product Search Image';
        prodMedia.ccrz__ProductMediaSource__c = 'Static Resource';
        prodMedia.ccrz__Enabled__c = true;
        prodMedia.ccrz__Product__c=prodId;
        if(isInsert){
            insert prodMedia;
        }
        return prodMedia;
    }

    /*
    * Method Name: createRFQ
    * Description: Creating RFQ__c record.
    * @param: Boolean isInsert , Id relatedCartId
    * @return: RFQ__c
    */
    public static RFQ__c createRFQ(Boolean isInsert , Id relatedCartId , Id contactId , Id accountId){
        
        Id rfqvRecordTypeId = Schema.SObjectType.RFQ__c.getRecordTypeInfosByName().get('RFQ').getRecordTypeId();
        
        RFQ__c rfq = new RFQ__c();
        rfq.RecordTypeId = rfqvRecordTypeId;
        rfq.Related_Cart__c = relatedCartId;
        rfq.Contact__c = contactId;
        rfq.Account__c = accountId;

        if(isInsert){
            insert rfq;
        }
        
        return rfq;
    }

    /*
    * Method Name: createOrder
    * Description: Creating cc order record.
    * @param: Boolean isInsert, Id contactId, Id userId
    * @return: ccrz__E_Order__c
    */
    public static ccrz__E_Order__c createOrder(Boolean isInsert, Id contactId, Id userId){
        ccrz__E_Order__c order = new ccrz__E_Order__c();
        order.ccrz__User__c = userId;
        order.ccrz__Contact__c = contactId;
        order.ccrz__Storefront__c = 'DefaultStore';
        
        if(isInsert)
            insert order;
        return order;
    }

    /*
    * Method Name: createOrderItem
    * Description: Creating cc order item record.
    * @param: Boolean isInsert, Id contactId, Id userId
    * @return: ccrz__E_Order__c
    */
    public static ccrz__E_OrderItem__c createOrderItem(Boolean isInsert){
        ccrz__E_OrderItem__c orderItem = new ccrz__E_OrderItem__c();
        orderItem.SAP_Material_Number__c = 111333;
        orderItem.SAP_Short_Description__c = 'Test Short Desc';
        orderItem.Quantity_Cancelled__c = 0.000 ;
        orderItem.Quantity_Ordered__c = 100.000;
        orderItem.Quantity_Shipped__c = 100.000;
        orderItem.Status_Code__c = 'Shipped';
        orderItem.Parcel_Tracking_Number__c = '00340360880000384050';
        orderItem.Confirmed_Dispatch__c = 'test1';
        orderItem.Requested_Dispatch__c = 'test1';
        orderItem.Carrier_Name__c = 'DHL' ;
        orderItem.ccrz__Quantity__c =100.000 ;
        orderItem.ccrz__Price__c = 100.000; 
        orderItem.ccrz__SubAmount__c = 10000.000;
        


        if(isInsert)
            insert orderItem;
        return orderItem;
    }

    /*
    * Method Name: createContactAddress
    * Description: Creating contact address record.
    * @param: Boolean isInsert
    * @return: ccrz__E_ContactAddr__c
    */
    public static ccrz__E_ContactAddr__c createContactAddress(Boolean isInsert){
        ccrz__E_ContactAddr__c contactAddress=new ccrz__E_ContactAddr__c();
        contactAddress.ccrz__AddressFirstline__c = '425 Grove Street';
        contactAddress.ccrz__AddressSecondline__c = 'Apt';
        contactAddress.ccrz__AddressThirdline__c = '290';
        contactAddress.ccrz__City__c = 'New York';
        contactAddress.ccrz__StateISOCode__c = 'NY';
        contactAddress.ccrz__PostalCode__c = '10001';
        contactAddress.ccrz__Country__c = 'United States';
        contactAddress.ccrz__FirstName__c='Test';
        contactAddress.ccrz__LastName__c='Test';
        
        if(isInsert)
            insert contactAddress;
        return contactAddress;
    }
}