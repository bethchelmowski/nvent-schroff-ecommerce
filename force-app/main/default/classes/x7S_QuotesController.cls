/**
 * Created By ankitSoni on 07/16/2020
 */
public class x7S_QuotesController {
    
    @AuraEnabled
    public static List<RFQ__c> getQuoteData(String contactId){ 
        List<RFQ__c> rfqQuery = [SELECT Id,Name,Status__c,ERP_Number__c,Proposal_Number__c,Related_Cart__c,External__c FROM RFQ__c WHERE Contact__c=:contactId];
        System.debug('getQuoteData()-'+rfqQuery);

        return rfqQuery;
    }

    @AuraEnabled
    public static List<x7S_QuoteWrapper> getQuotes(){

        //Get current logged in Contact-User
        String currentUserId = UserInfo.getUserId();
        List<User> currentUserList =  [SELECT Id,ContactId FROM User WHERE Id=:currentUserId LIMIT 1];
       
        List<x7S_SubmittedCartModel> submittedCarts = x7S_CartActionController.getSubmittedCarts();
        List<x7S_QuoteWrapper> quoteWrapperList = new List<x7S_QuoteWrapper>();


        if(!currentUserList.isEmpty()){
            List<RFQ__c> allRFQs = getQuoteData(currentUserList[0].ContactId);

            for(RFQ__c rfq : allRFQs){
                x7S_QuoteWrapper quote = new x7S_QuoteWrapper();

                quote.quoteId = rfq.Id;
                quote.quoteName = rfq.Name;
                if(rfq.ERP_Number__c != null){
                    quote.quoteNumber = rfq.ERP_Number__c;
                }
                else {
                    quote.quoteNumber = rfq.Proposal_Number__c;
                }

                quote.quoteStatus = rfq.External__c;
                quote.relatedCartId = rfq.Related_Cart__c;
                quoteWrapperList.add(quote);
            }

            for(x7S_QuoteWrapper quote : quoteWrapperList){
                for(x7S_SubmittedCartModel cart : submittedCarts){

                    if(quote.relatedCartId == cart.cartId){
                        quote.cartNumber = cart.cartNumber;
                        quote.cartCurrencyISOCode = cart.currencyISOCode;
                        quote.distinctSKUs = cart.distinctSKUs;
                        quote.totalAmount = cart.totalAmount;
                        quote.lastModifiedDate = cart.lastModifiedDate;
                    }
                }
            }

        }

        System.debug('getQuotesList()-'+quoteWrapperList);
        return quoteWrapperList;
    }
}
