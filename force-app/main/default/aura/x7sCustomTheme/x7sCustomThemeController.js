/**
 * Created by brandon.franklin on 5/8/2020.
 */

({
  doInit: function(component, event, helper) {
    helper.getBodyClass(component);
  },

  gotoUrl: function(component) {
    component.find("navigationService").navigate({
      type: "standard__webPage",
      attributes: {
        url: "https://schroff.nvent.com/"
      }
    });
  },

  navigateToCart: function() {
    const urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({ url: "/cart" });
    urlEvent.fire();
  }
});
