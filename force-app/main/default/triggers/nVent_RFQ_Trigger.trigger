/* ********************************************
Developer - Apurva Prasade
Trigger - nVent_RFQ_Trigger / RFQ
Description - RFQ combined trigger

Developer   Date        Version
Apurva P    5-11-2019  V1.0

********************************************** */


trigger nVent_RFQ_Trigger on RFQ__c (before delete, before insert, before update, 
                                    after delete, after insert, after update) {
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
            nVent_RFQ_TriggerHandler.afterinsertUtil(Trigger.new);
        if(Trigger.isUpdate)
            nVent_RFQ_TriggerHandler.afterUpdateUtil(Trigger.new,Trigger.Oldmap);
    }
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
            nVent_RFQ_TriggerHandler.beforeinsertUtil(Trigger.new);
        if(Trigger.isUpdate)
            nVent_RFQ_TriggerHandler.beforeUpdateUtil(Trigger.new,Trigger.Oldmap);
    }
}