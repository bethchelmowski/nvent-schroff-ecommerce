/**
 * Created By ankitSoni on 07/22/2020
 */
trigger x7S_RFQTrigger on RFQ__c (before insert, after insert) {

    if(trigger.isAfter){
        if(trigger.isInsert){

        }
    }
    if(trigger.isBefore){
        if(trigger.isInsert){
            x7S_RFQTriggerHandler.onBeforeInsert(trigger.new);
        }
    }
    
}