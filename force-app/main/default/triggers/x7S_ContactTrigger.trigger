/**
 * Created By ankitSoni on 09/16/2020
 */
trigger x7S_ContactTrigger on Contact (after update) {

    if(trigger.isBefore){

    }
    if(trigger.isAfter){
      if(trigger.isUpdate){
          x7S_ContactTriggerHandler.onAfterUpdate(trigger.new, trigger.oldMap);
      }  
    }
}